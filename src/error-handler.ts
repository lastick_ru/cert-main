import {Optional, Injectable, ErrorHandler, Inject} from '@angular/core';
import { of, throwError } from 'rxjs';

@Injectable()
export class CustomErrorHandlerService extends ErrorHandler {

  constructor( @Optional() @Inject('ERROR_WRAPPER') private errorWrapper: any ) {
    super();
  }

  handleError(error: Error) {
    if (this.errorWrapper) {
      this.errorWrapper.error = error;
    }
    return throwError(error);
  }
}
