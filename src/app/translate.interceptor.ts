import { REQUEST } from '@nguniversal/express-engine/tokens';
import * as express from 'express';
import {HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {map} from 'rxjs/operators';

@Injectable()
export class TranslateInterceptor implements HttpInterceptor {
  private readonly DEFAULT_PORT = 4000;
  private readonly PORT = process.env.PORT || this.DEFAULT_PORT;

  constructor(@Inject(REQUEST) private request: express.Request) {}

  getBaseUrl(req: express.Request) {
    const { protocol, hostname } = req;
    return this.PORT ?
      `${protocol}://${hostname}:${this.PORT}` :
      `${protocol}://${hostname}`;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    let req: HttpRequest<any> = request;
    if (request.url.match('i18n')) {
      const baseUrl = this.getBaseUrl(this.request);
      // console.log('TRANSLATE', `${baseUrl}${request.url}`);
      req = request.clone({
        url: `${baseUrl}${request.url}`
      });
    }
    return next.handle(req);
      // .pipe(
      //   map(
      //     (event) => {
      //       if (event instanceof HttpResponse && event.url.match('i18n')) {
      //         console.log('---I18N---', event);
      //       }
      //       return event;
      //     }
      //   )
      // );
  }
}
