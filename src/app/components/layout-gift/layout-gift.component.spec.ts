import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutGiftComponent } from './layout-gift.component';

describe('LayoutGiftComponent', () => {
  let component: LayoutGiftComponent;
  let fixture: ComponentFixture<LayoutGiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LayoutGiftComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutGiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
