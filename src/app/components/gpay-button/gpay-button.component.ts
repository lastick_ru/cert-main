import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer, Renderer2, ViewChild } from '@angular/core';
import { ICertificateOrder, IPaymentMethod } from '../../types/Entities';
import { IContacts, PaymentService } from '../../services/payment.service';
import { PlatformService } from '../../services/platform.service';

declare const google: any;

@Component({
  selector: 'app-gpay-button',
  templateUrl: './gpay-button.component.html',
  styleUrls: ['./gpay-button.component.styl']
})
export class GpayButtonComponent implements OnInit, AfterViewInit {
  @Input('data') data: IPaymentMethod;
  @Input() total_cost: number;
  @Input() contacts: IContacts;
  @Input() order: ICertificateOrder;
  @Input() disabled = false;
  @ViewChild('container', { static: false }) container: ElementRef;
  public supported = false;
  public acsUrl: string;
  public acsMethod: string;
  private session: PaymentRequest;
  private paymentMethodData;
  private paymentDetails;
  private paymentOptions;

  constructor(
    public payment: PaymentService,
    private renderer: Renderer2,
    private platform: PlatformService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    if (this.data && this.platform.isBrowser) {
      try {
        console.log(this.data)
     
        let paymentOptions = this.payment.getOptions(this.data, this.total_cost || 0);
        
        
        this.paymentMethodData = paymentOptions.paymentMethodData;
        this.paymentDetails = paymentOptions.paymentDetails;
        this.paymentOptions = paymentOptions.paymentOptions;
        console.log(paymentOptions)
        const pr = new PaymentRequest([paymentOptions.paymentMethodData], paymentOptions.paymentDetails, paymentOptions.paymentOptions);
        console.log(pr)
        pr.canMakePayment().then((s) => {
          if (s === true && google) {
            this.supported = true;

            const paymentsClient = new google.payments.api.PaymentsClient({
              environment: paymentOptions.paymentMethodData.data.environment
            });
            const button = paymentsClient.createButton({
              onClick: () => this.buttonClick(),
              buttonType: 'short',
              buttonColor: 'white'
            });
            this.renderer.appendChild(this.container.nativeElement, button);
          }
        });
      } catch (err) {
        console.error(err);
      }
    }
  }

  public buttonClick() {
    this.payment.checkout(this.data, this.total_cost, this.order, this.contacts);
  }
}
