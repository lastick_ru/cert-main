import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ICertificateBody, ICertificateBodyAddressee, ICertificateBodyClient, ICertificateBodyItem } from '../../../types/req';
import { ControlContainer, NgForm, NgModel } from '@angular/forms';
import { IAddresseeDataItem } from '../gift.component';
import { FormHelperComponent } from '../../form-helper/form-helper.component';

@Component({
  selector: 'app-gift-form',
  templateUrl: './gift-form.component.html',
  styleUrls: ['./gift-form.component.styl'],
  viewProviders: [{provide: ControlContainer, useExisting: NgForm}]
})
export class GiftFormComponent extends FormHelperComponent implements OnInit, OnChanges {
  @Input() md: ICertificateBody;
  @Input() type = 'gift';
  @Input() index = 0;
  @Input() phoneInput = true;
  @Input() addresseeData: IAddresseeDataItem[];
  @Input() mdItem: ICertificateBodyItem;
  @Input() formSubmitted: boolean;
  @Input() form: NgForm;

  @ViewChild('addresseeNameInp', {static: false}) _addresseeNameInp: NgModel;
  @ViewChild('addresseeEmailInp', {static: false}) _addresseeEmailInp: NgModel;

  public mdAddressee: ICertificateBodyAddressee;
  public mdClient: ICertificateBodyClient;
  public _addresseeData: IAddresseeDataItem[];
  public selectedItem: IAddresseeDataItem;
  public showAddresseeData = false;
  public debounceTimer;

  constructor() { super(); }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.md && changes.md.currentValue) {
      if (this.md.items) {
        const item =  this.md.items[this.index];
        this.mdAddressee = (item || {}).addressee;
        // this.mdClient = (item || {}).;
      }
    }
    if (changes.addresseeData && changes.addresseeData.currentValue) {
      this._addresseeData = this.addresseeData;
    }
  }

  searchAddresseeDataFn(term: string, item: IAddresseeDataItem) {
    const normalizeString = (str) => {
      return (str || '').replace(/\s/, '').toLowerCase();
    };
    return normalizeString(item.name).indexOf(normalizeString(term)) > -1;
  }

  onAddresseeNameChange(term: string) {
    this.selectedItem = null;
    clearTimeout(this.debounceTimer);
    this.debounceTimer = setTimeout(() => {
      this.checkAndFilterData(term);
    }, 100);
  }

  checkAndFilterData(term: string) {
    if (!this._addresseeData) return;
    this._addresseeData = this.addresseeData.filter(item => {
      return this.searchAddresseeDataFn(term, item);
    });
    if (!term) {
      this._addresseeData = this.addresseeData;
    }
    this.showAddresseeData = !!(this._addresseeData || []).length;
  }

  onAddresseeDataItemClick(e: Event, item: IAddresseeDataItem) {
    e.stopPropagation();
    this.mdAddressee.name = item.name;
    this.mdAddressee.email = item.email;
    this.selectedItem = item;
    this.showAddresseeData = false;
    // this.mdAddressee.name = item.name;
  }

  onAddresseeNameFocus(term: string, e: Event) {
    if (this.selectedItem) {
      this.checkAndFilterData('');
    } else {
      this.checkAndFilterData(term);
    }
    this.fhOnInputFocus(e);
  }

  onAddresseeNameBlur(e: Event) {
    setTimeout(() => {
      this.showAddresseeData = false;
      if (e.target === this.fhFocusedElement) {
        this.fhOnInputBlur(e);
      }
    }, 200);
  }
}
