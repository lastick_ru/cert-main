import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GiftComponent } from './gift.component';
import { SharedModule } from '../../modules/shared/shared.module';
import { TabsModule } from '../../modules/tabs/tabs.module';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FormsModule } from '@angular/forms';
import { GiftFormComponent } from './gift-form/gift-form.component';
import { ValidatorsModule } from '../../validators/validators.module';
import { CardButtonsModule } from '../../modules/card-buttons/card-buttons.module';
import { GiftsRelatedModule } from '../../modules/gifts-related/gifts-related.module';


@NgModule({
  declarations: [
    GiftComponent,
    GiftFormComponent,
  ],
  exports: [
    GiftComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TabsModule,
    TranslateModule,
    AngularSvgIconModule,
    ValidatorsModule,
    CardButtonsModule,
    GiftsRelatedModule,
  ],
})
export class GiftModule { }
