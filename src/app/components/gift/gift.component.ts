import {
  Component,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransportService } from '../../services/transport.service';
import {
  ICertificate,
  ICertificateOrder,
  ICertificateSkinSettings,
  IMediaInfo,
  IPriceModifier, ISliderSettings,
} from '../../types/Entities';
import { ICertificateBody, ICertificateBodyItem } from '../../types/req';
import { StorageService } from '../../services/storage.service';
import { clone, isEmpty } from 'ramda';
import { IPlusMinusData } from '../plus-minus/plus-minus.component';
import { NgForm } from '@angular/forms';
import { FormService } from '../../services/form.service';
import { MetricService } from '../../services/metric.service';
import { FsService } from '../../services/fs.service';
import { DocService } from '../../services/doc.service';
import {LoggerService} from '../../services/logger.service';
import {Meta} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {path} from 'ramda';
import { ISliderLogo } from '../gifts-related/gifts-related.component';

interface ITabs {
  gift?: ICertificateBody;
  corporate?: ICertificateBody;
}

export interface IAddresseeDataItem {
  name?: string;
  url?: string;
  email?: string;
  person_name?: string;
  image_url?: string;
}

@Component({
  selector: 'app-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.styl'],
  encapsulation: ViewEncapsulation.None,
})
export class GiftComponent implements OnInit, OnChanges {
  @Input() gift: ICertificate;
  @Input() skin: ICertificateSkinSettings;
  @Input() widgetUrl: string;
  @Input() showGiftCheckout = true;
  @Input() showActivate = false;
  @Input() showHeading: boolean;
  @Input() headingText: string;
  @Input() showGiftTabs = true;
  @Input() showCover = true;
  @Input() showName = true;
  @Input() showBFee = true;
  @Input() showSubscribeForm = false;
  @Input() showSubscribePhone = true;
  @Input() showSubscribeMessage = true;
  @Input() showSubscribeCompany = true;
  @Input() showHeaderCompanyLogo = true;
  @Input() setMeta = true;
  @Input() subscribeFormTitle: string;
  @Input() giftTabTitle: string;
  @Input() corpTabTitle: string;
  @Input() related: ICertificate[];
  @Input() relatedTitle: string;
  @Input() first_logos: ISliderLogo[];
  @Input() first_logos_title: string;
  @Input() second_logos: ISliderLogo[];
  @Input() second_logos_title: string;
  @Input() activate_text: string;
  @Input() first_logos_slider_settings: ISliderSettings;
  @Input() second_logos_slider_settings: ISliderSettings;
  @Input() head_hash_tag: string;
  @Input() corpPhoneInput = true;
  @Input() giftPhoneInput = true;
  @Input() addresseeData: IAddresseeDataItem[];
  @Input() showMoreBtn = true;
  @Input() headerLogo: string;
  @Input() headerLogoSz: string;

  @ViewChild('form', { static: false }) _form: NgForm;

  public partners: any[];
  public desktopWidth = 992;
  public isDesktop = false;
  public initialTab: ICertificateBody = {
    client: {},
    // addressee: {},
    // is_gift: true,
    items: [],
    payment_method: undefined,
  };
  public initialItem: ICertificateBodyItem;
  public tab: keyof ITabs = 'gift';
  public tabs: ITabs = {
    gift: {},
    corporate: {},
  };
  public prices_range: number[];
  public order: ICertificateOrder;
  public total_cost = 0;
  public fee: IPriceModifier;
  public showCustomValue = false;
  public logo: File;
  public _logo: IMediaInfo;
  public logoSize: { w: number; h: number };
  public formSubmitted = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: TransportService,
    private storage: StorageService,
    private formService: FormService,
    private metric: MetricService,
    private fsService: FsService,
    public doc: DocService,
    private logger: LoggerService,
    private meta: Meta,
    private translate: TranslateService
  ) {}

  @HostListener('window:resize')
  onResize() {
    this.isDesktop = window.innerWidth >= this.desktopWidth;
  }

  get isInvalid() {
    return this._form.invalid || !this.total_cost;
  }

  ngOnInit() {
    this.isDesktop = window.innerWidth >= this.desktopWidth;
  }

  log(data){
    console.log(data)
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.gift && changes.gift.currentValue) {
      this.skin = this.skin || (this.gift || { skin_settings: { skin: 'default' } }).skin_settings;
      const time_mark = this.storage.getItem('time_mark');
      if (!time_mark) {
        this.storage.setItem('time_mark', Date.now());
      }
      const order = time_mark && Date.now() - parseInt(time_mark, 10) >= 1000 * 60 * 60 ?
        null : this.storage.getItem('gift_order');
      const lang = this.translate.currentLang;
      this.order = order
        ? JSON.parse(order)
        : { type: 'gift', data: clone(this.initialTab), agreement: false };
      this.tabs[this.order.type] = clone(this.order.data);
      this.fee = isEmpty(this.gift.price_modifier) ? null : this.gift.price_modifier;
      this.prices_range = this.gift.prices_range;
      this.initialItem = {
        count: 0,
        price: 0,
        cert_config: this.gift._uuid,
        cert_view: this.gift.Views[0]._uuid,
        addressee: {},
        is_corporate: this.order.type === 'corporate',
        is_gift: this.order.type === 'gift',
      };
      this.initialTab = {
        ...this.initialTab,
        items: [clone(this.initialItem)],
      };
      this.initTabsData();
      this.initQueryPrice();
      this.calculate();

      if (this.setMeta && this.gift.seo && this.gift.seo.meta) {
        const { meta } = this.gift.seo;
        const description = path([lang], meta.description) || path([this.translate.defaultLang], meta.description);
        const title = path([lang], meta.title) || path([this.translate.defaultLang], meta.title);
        const keywords = path([lang], meta.keywords) || path([this.translate.defaultLang], meta.keywords);
        if (description) {
          this.meta.updateTag({
            property: 'og:description', content: description
          });
        }
        if (title) {
          this.meta.updateTag({
            property: 'og:title', content: title
          });
        }
        if (keywords) {
          this.meta.updateTag({
            name: 'keywords', content: keywords
          });
        }
      }

      this.logger.l(['gift', this.gift]);
      this.logger.l(['tabs', this.tabs]);

      setTimeout(() => {
        const hash = location.hash || '';
        if (hash.length) {
          const $el: HTMLElement = document.querySelector(`${hash}`);
          if ($el) {
            window.scrollTo(0, $el.offsetTop);
          }
        }
      }, 400);
    }
  }

  initTabsData() {
    Object.keys(this.tabs).map(type => {
      if (!this.tabs[type]) {
        this.tabs[type] = clone(this.initialTab);
      }
      const initItem = clone(this.initialItem);
      const _items = this.tabs[type].items;
      this.tabs[type].client = clone(this.order.data.client);
      this.tabs[type].items = (_items && _items.length ? _items : [{}]).map(item => {
        return {
          ...initItem,
          ...clone(item),
          cert_view: initItem.cert_view,
          cert_config: initItem.cert_config,
        };
      });
    });
  }

  initQueryPrice() {
    this.route.queryParams.subscribe(query => {
      if (query && query.price) {
        this.calcPrice(this.tabs[this.tab], parseInt(query.price, 10), 0);
      }
      this.calculate();
    });
  }

  calcPrice(md: ICertificateBody, price: number, index: number) {
    const item = md.items[index];
    if (price) {
      item.price = price;
      item.count = item.count || 1;
    } else {
      item.price = 0;
      item.count = 0;
    }
  }

  calculate(nullify = true) {
    const order: ICertificateOrder = {
      ...this.order,
      type: this.tab,
      gift: this.gift,
      data: { ...clone(this.initialTab), items: [] },
    };
    let total_cost = 0;
    const data = this.tabs[this.tab];
    const _items = data.items || [];
    _items.map(item => {
      if (item.count > 0) {
        const _data = item;
        const price = _data.price * _data.count;
        // if (this.prices_range && (this.prices_range[0] || this.prices_range[1])) {
        //   const min = this.prices_range[0];
        //   const max = this.prices_range[1];
        //   if ((max && _data.price <= max) || (min && _data.price >= min)) {
        //     total_cost += price;
        //   } else if (_data.price && nullify) {
        //     _data.price = 0;
        //     _data.count = 0;
        //   }
        // } else {
        //   total_cost += price;
        // }
        total_cost += price;
        order.data.items.push(_data);
        order.data.client = clone(data.client);
        // order.data.addressee = clone(data.addressee);
      }
    });
    this.order = order;
    // this.order.data.is_gift = order.type === 'gift';
    this.order.total_cost = total_cost;
    this.total_cost = total_cost;
  }

  onPriceClick(md: ICertificateBody, price: number, index: number) {
    this.logger.l(['_form', this._form]);
    // if (!price) return;
    this.calcPrice(md, price, index);
    this.calculate(false);
    this.metric.trackFb('AddToCart', { value: price });
  }

  onCountChange(data: IPlusMinusData, md: ICertificateBody, index: number) {
    md.items[index].count = data.value;
    this.calculate(false);
  }

  onTabsShow(data: { name: keyof ITabs }) {
    this.tab = data.name;
    this.tabs[this.tab].items.map(item => {
      item.is_gift = this.tab === 'gift';
      item.is_corporate = this.tab === 'corporate';
    });
    this.calculate();
  }

  onCardRemoveClick(index: number) {
    this.tabs[this.tab].items.splice(index, 1);
    this.calculate();
  }

  addMore() {
    const _tab: ICertificateBody = this.tabs[this.tab];
    _tab.items.push(clone({ ...this.initialItem } as ICertificateBodyItem));
  }

  submit() {
    this.formService.markFormGroupTouched(this._form);
    this.formSubmitted = true;
    this.logger.l(['_form', this._form]);
    if (this.isInvalid) {
      return;
    }
    this.calculate();
    const navigate = () => {
      this.saveToStorage();
      this.router.navigate([`checkout`]);
      this.metric.trackFb('InitiateCheckout', { value: this.total_cost });
      this.metric.trackFb('InitiateCheckout', { value: this.total_cost });
      this.logger.l(['logo', this._logo]);
      this.logger.l(['order', this.order]);
      this.logger.l(['form', this._form]);
    };
    if (this.logo) {
      const path = `cert/${this.fsService.makeFilePathForUpload(this.logo)}`;
      this.api.uploadFile({ filePath: path, file: this.logo }).subscribe(data => {
        // this._logo = {path};
        this.order.data.items[0].logo = path;
        navigate();
      });
    } else {
      this.order.data.items.map(item => {
        delete item.logo;
      });
      navigate();
    }
  }

  saveToStorage() {
    if (this.order) {
      this.storage.setItem('gift_order', JSON.stringify(this.order));
      this.storage.setItem('time_mark', Date.now());
    }
  }
}
