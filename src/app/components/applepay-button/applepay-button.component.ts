import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { ICertificateOrder, IPaymentMethod } from '../../types/Entities';
import { PaymentService } from '../../services/payment.service';
import { ICertificateBody } from '../../types/req';
import { LoggerService } from '../../services/logger.service';
import { isNil, isEmpty, path } from 'ramda';
import { TransportService } from '../../services/transport.service';
import { PlatformService } from '../../services/platform.service';

declare const ApplePaySession: any;

@Component({
  selector: 'app-applepay-button',
  templateUrl: './applepay-button.component.html',
  styleUrls: ['./applepay-button.component.styl'],
})
export class ApplepayButtonComponent implements OnInit, AfterViewInit {
  @Input() pm: IPaymentMethod;
  @Input() order: ICertificateOrder;
  @Input() md: ICertificateBody;
  @Input() total_cost: number;
  @Input() disabled = false;
  public fetching;

  private cart_data: ICertificateBody;

  constructor(
    private payment: PaymentService,
    private logger: LoggerService,
    private api: TransportService,
    private platform: PlatformService,
  ) {
  }

  ngOnInit() {
    const self = this;
    (window as any).startApplePaySession = function() {
      self.fetching = true;
      const cart_data: ICertificateBody = self.order.data;
      cart_data.payment_method = self.pm._uuid;
      cart_data.client = { ...self.md.client };
      self.cart_data = { ...cart_data };
      self.logger.l(['cart_data onApplePayClick', self.cart_data], false);
      const paymentOptions = {
        requestPayerEmail: isNil(self.md.client.email) || isEmpty(self.md.client.email),
        requestPayerPhone: isNil(self.md.client.phone) || isEmpty(self.md.client.phone),
      };

      const requiredShippingContactFields = [];
      if (paymentOptions.requestPayerEmail) {
        requiredShippingContactFields.push('email');
      }
      if (paymentOptions.requestPayerPhone) {
        requiredShippingContactFields.push('phone');
      }

      const { paymentMethodData, paymentDetails } = self.payment.getOptions(self.pm, self.total_cost);

      const version = 3;

      // ([1, 2, 3, 4, 5, 6]).map(v => {
      //   if (ApplePaySession.supportsVersion(v)) {
      //     version = v;
      //   }
      //   this.logger.l([`supportsVersion ${v}`, ApplePaySession.supportsVersion(v)], false);
      // });

      const request = {
        countryCode: 'RU',
        currencyCode: 'RUB',
        merchantCapabilities: paymentMethodData.data.merchantCapabilities,
        supportedNetworks: paymentMethodData.data.supportedNetworks,
        total: {
          label: path(['total', 'label'], paymentMethodData.data),
          type: 'final',
          amount: path(['total', 'amount', 'value'], paymentMethodData.data),
        },
        requiredShippingContactFields
      };

      self.logger.l(['session request', request], false);

      const session = new ApplePaySession(version, request);

      session.oncancel = function(event) {
        self.logger.l(['oncancel', event ], false);
        self.fetching = false;
      };
      // session.onpaymentmethodselected = function(event) {
      //   self.logger.l(['onpaymentmethodselected', event ], false);
      // };
      // session.onshippingmethodselected = function(event) {
      //   self.logger.l(['onshippingmethodselected', event ], false);
      // };
      session.onvalidatemerchant = function(event) {
        const validationURL = event.validationURL;
        self.logger.l([`[onmerchantvalidation] ${validationURL}`], false);

        self.api
          .validateMerchant(self.pm._uuid, { validationURL })
          .toPromise()
          .then(response => {
            self.logger.l(['validateMerchant complete', { response }], false);
            session.completeMerchantValidation(response);
          })
          .catch(e => {
            self.logger.l(['validateMerchant error', { error: e }], false);
            session.abort();
            self.fetching = false;
          });
      };
      // session.onshippingcontactselected = function(event) {
      //   self.logger.l(['onshippingcontactselected', { event }], false);
      // };
      session.onpaymentauthorized = function(response) {
        self.logger.l(['session.onpaymentauthorized', response], false);
        self.logger.l(['session.onpaymentauthorized session', session], false);
        const client = self.cart_data.client;
        client.phone = path(['payment', 'shippingContact', 'phoneNumber'], response) || client.phone;
        client.email = path(['payment', 'shippingContact', 'emailAddress'], response) || client.email;

        self.cart_data['payment_token'] = path(['payment', 'token', 'paymentData'], response);
        self.logger.l(['session cart_data', self.cart_data], false);

        self.api.newOrder(JSON.stringify(self.cart_data)).subscribe(
          success => {
            session.completePayment(ApplePaySession.STATUS_SUCCESS);
            setTimeout(() => {
              self.logger.l(['success payment', success.payment], false);
              window.location.href = success.payment['payment_url'];
            }, 250);
          },
          error => {
            self.fetching = false;
            session.completePayment(ApplePaySession.STATUS_FAILURE);
          },
        );
      };
      session.begin();
      self.logger.l(['try session show'], false);
    };
  }

  ngAfterViewInit(): void {

  }

  onClick() {
    // console.log('ApplePaySession');
    // const session = new ApplePaySession(3, {
    //   "countryCode": "RU",
    //   "currencyCode": "RUB",
    //   "merchantCapabilities": [
    //     "supports3DS"
    //   ],
    //   "supportedNetworks": [
    //     "visa",
    //     "masterCard",
    //     "amex",
    //     "discover"
    //   ],
    //   "total": {
    //     "label": "Demo (Card is not charged)",
    //     "type": "final",
    //     "amount": "1.99"
    //   }
    // });
    // session.oncancel = function(event) {
    //   console.log('oncancel', event);
    // };
    // session.onvalidatemerchant = function(event) {
    //   console.log('onvalidatemerchant', event);
    // };
    // session.begin();
    // const self = this;
    //
    // if (!this.pm || !this.total_cost) return;
    //
    // const cart_data: ICertificateBody = this.order.data;
    // cart_data.payment_method = this.pm._uuid;
    // cart_data.client = { ...this.md.client };
    // this.cart_data = { ...cart_data };
    // this.logger.l(['cart_data onApplePayClick', this.cart_data], false);
    // const paymentOptions = {
    //   requestPayerEmail: isNil(this.md.client.email) || isEmpty(this.md.client.email),
    //   requestPayerPhone: isNil(this.md.client.phone) || isEmpty(this.md.client.phone),
    // };
    //
    // const requiredBillingContactFields = [];
    // if (paymentOptions.requestPayerEmail) {
    //   requiredBillingContactFields.push('email');
    // }
    // if (paymentOptions.requestPayerPhone) {
    //   requiredBillingContactFields.push('phone');
    // }
    //
    // const { paymentMethodData, paymentDetails } = this.payment.getOptions(this.pm, this.total_cost);
    //
    // const version = 3;
    //
    // // ([1, 2, 3, 4, 5, 6]).map(v => {
    // //   if (ApplePaySession.supportsVersion(v)) {
    // //     version = v;
    // //   }
    // //   this.logger.l([`supportsVersion ${v}`, ApplePaySession.supportsVersion(v)], false);
    // // });
    //
    // const request = {
    //   countryCode: 'RU',
    //   currencyCode: 'RUB',
    //   merchantCapabilities: paymentMethodData.data.merchantCapabilities,
    //   supportedNetworks: paymentMethodData.data.supportedNetworks,
    //   total: {
    //     label: path(['total', 'label'], paymentMethodData.data),
    //     type: 'final',
    //     amount: path(['total', 'amount', 'value'], paymentMethodData.data),
    //   },
    //   // requiredBillingContactFields
    // };
    //
    // this.logger.l(['session request', request], false);
    //
    // const session = new ApplePaySession(version, request);
    //
    // session.oncancel = function(event) {
    //   self.logger.l(['oncancel', event ], false);
    // };
    // session.onpaymentmethodselected = function(event) {
    //   self.logger.l(['onpaymentmethodselected', event ], false);
    // };
    // session.onshippingmethodselected = function(event) {
    //   self.logger.l(['onshippingmethodselected', event ], false);
    // };
    // session.onvalidatemerchant = function(event) {
    //   const validationURL = event.validationURL;
    //   self.logger.l(['onmerchantvalidation', { event }], false);
    //   self.logger.l([`[onmerchantvalidation] ${validationURL}`], false);
    //
    //   self.api
    //     .validateMerchant(this.pm._uuid, { validationURL })
    //     .toPromise()
    //     .then(response => {
    //       self.logger.l(['validateMerchant complete', { response }], false);
    //       session.completeMerchantValidation(response);
    //     })
    //     .catch(e => {
    //       self.logger.l(['validateMerchant error', { error: e }], false);
    //       session.abort();
    //     });
    // };
    // session.onshippingcontactselected = function(event) {
    //   self.logger.l(['onshippingcontactselected', { event }], false);
    // };
    // session.onpaymentauthorized = function(response) {
    //   const state = { status: ApplePaySession.STATUS_SUCCESS };
    //   self.logger.l(['session.onpaymentauthorized', response], false);
    //   self.logger.l(['session.onpaymentauthorized cart_data', self.cart_data], false);
    //   self.logger.l(['session.onpaymentauthorized session', session], false);
    //
    //   if (!isNil(response.payerPhone) && !isEmpty(response.payerPhone)) {
    //     self.cart_data.client.phone = response.payerPhone;
    //     self.logger.l(['payerPhone', response.payerPhone], false);
    //   }
    //   if (!isNil(response.payerEmail) && !isEmpty(response.payerEmail)) {
    //     self.cart_data.client.email = response.payerEmail;
    //     self.logger.l(['payerEmail', response.payerEmail], false);
    //   }
    //   self.cart_data['payment_token'] = path(['payment', 'token', 'paymentData'], response);
    //
    //   self.logger.l(['session cart_data', self.cart_data], false);
    //
    //   self.api.newOrder(JSON.stringify(self.cart_data)).subscribe(
    //     success => {
    //       session.completePayment(ApplePaySession.STATUS_SUCCESS);
    //       window.location.href = success.payment['payment_url'];
    //     },
    //     error => {
    //       session.completePayment(ApplePaySession.STATUS_FAILURE);
    //     },
    //   );
    // };
    // session.begin();
    // this.logger.l(['try session show'], false);
  }
}
