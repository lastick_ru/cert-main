import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ICertificate, ICertificateSkinSettings, IPriceModifier } from '../../types/Entities';

@Component({
  selector: 'app-card-buttons',
  templateUrl: './card-buttons.component.html',
  styleUrls: ['./card-buttons.component.styl'],
})
export class CardButtonsComponent implements OnInit {
  @Input() prices: number[];
  @Input() gift: ICertificate;
  @Input() sz = '1';
  @Input() theme = '1';
  @Input() skin: ICertificateSkinSettings;
  @Input() custom = false;
  @Input() customValue: number;
  @Input() route: { link: string; text: string } = null;
  @Input() price;
  @Input() prices_range: number[];
  @Input() fee: IPriceModifier;

  @Output() priceClick: EventEmitter<number> = new EventEmitter<number>();
  @Output() customValueShowHide: EventEmitter<boolean> = new EventEmitter<boolean>();

  public prevPrice: number;
  public currPrice: number;
  public currCustomValue: number;
  public prevCustomValue: number;
  public showCustomInput = false;
  public maxValuePattern = /^[1-9]+\d*$/;
  public submitted = false;

  constructor() {}

  ngOnInit() {
    this.currPrice = this.price;
    this.prevPrice = this.price;
    this.currCustomValue = this.customValue || 0;
    if (this.price && this.prices && !this.prices.some(p => p === this.price)) {
      // if (this.isValidValue(this.price)) {
      this.currCustomValue = this.price / 100;
      this.onCustomValueSubmit();
      // }
    }
  }

  onPriceClick(price) {
    if (this.showCustomInput) return;

    this.currPrice = this.currPrice !== price ? price : undefined;
    this.showCustomInput = false;
    this.currCustomValue = undefined;
    this.priceClick.emit(this.currPrice);
  }

  closeCustomValue() {
    this.showCustomInput = false;
    this.customValueShowHide.emit(false);
  }

  openCustomValue() {
    this.showCustomInput = true;
    this.customValueShowHide.emit(true);
  }

  isValidValue(value: number): boolean {
    if (!value) return !!value;

    const min = this.prices_range[0];
    const max = this.prices_range[1];

    if (this.prices_range && (min || max)) {
      return (max && value <= max) || (min && value >= min);
    } else {
      return true;
    }
  }

  onPopoverOutsideClick() {
    this.closeCustomValue();
    setTimeout(() => {
      this.currCustomValue = this.prevCustomValue;
      if (!this.isValidValue(this.currCustomValue)) {
        this.currPrice = this.prevPrice;
        this.prevPrice = this.currPrice;
      } else {
        this.currPrice = undefined;
        this.prevPrice = this.currPrice;
      }
    }, 1);
  }

  customValueToggle() {
    if (this.showCustomInput) {
      this.currCustomValue = this.prevCustomValue;
      if (!this.currCustomValue) {
        this.currPrice = this.prevPrice;
        this.prevPrice = this.currPrice;
      }
      this.closeCustomValue();
    } else {
      this.prevCustomValue = this.currCustomValue;
      this.prevPrice = this.currPrice;
      this.currPrice = undefined;
      this.openCustomValue();
    }
    // this.prevCustomValue = this.currCustomValue;
  }

  onCustomValueClick() {
    this.customValueToggle();
  }

  onCustomValueSubmit() {
    this.submitted = true;
    const value = (this.currCustomValue || 0) * 100;
    if (!this.isValidValue(value)) {
      return;
    }
    this.closeCustomValue();
    if (!value) {
      this.currPrice = this.prevPrice;
      this.prevPrice = this.currPrice;
    } else {
      this.currPrice = value;
      this.priceClick.emit(this.currPrice);
    }
    // if (this.currCustomValue) {
    // }
    // else {
    //   this.currPrice = this.prevPrice;
    //   this.prevPrice = this.currPrice;
    // }
    this.prevCustomValue = value;
  }
}
