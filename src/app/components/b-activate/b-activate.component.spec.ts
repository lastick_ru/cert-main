import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BActivateComponent } from './b-activate.component';

describe('BActivateComponent', () => {
  let component: BActivateComponent;
  let fixture: ComponentFixture<BActivateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BActivateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BActivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
