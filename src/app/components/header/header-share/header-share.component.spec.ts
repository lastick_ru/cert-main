import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderShareComponent } from './header-share.component';

describe('HeaderShareComponent', () => {
  let component: HeaderShareComponent;
  let fixture: ComponentFixture<HeaderShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
