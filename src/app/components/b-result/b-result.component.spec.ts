import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BResultComponent } from './b-result.component';

describe('BResultComponent', () => {
  let component: BResultComponent;
  let fixture: ComponentFixture<BResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
