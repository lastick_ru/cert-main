import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { TransportService } from '../../services/transport.service';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICertificate } from '../../types/Entities';
import { ITag } from '../../types/ITag';
import { get as getCookie, set as setCookie } from 'js-cookie';
import { addMinutes } from 'date-fns';
import { StorageService } from '../../services/storage.service';
import { LoggerService } from '../../services/logger.service';
// import Fuse from 'fuse.js';

export interface IGiftsPage {
  data?: ICertificate[];
}

export interface IGiftsState {
  name?: 'all' | 'search' | 'tag';
  page?: number;
  scroll?: number;
  tag?: ITag;
  search?: string;
}

@Component({
  selector: 'app-gifts',
  templateUrl: './gifts.component.html',
  styleUrls: ['./gifts.component.styl'],
})
export class GiftsComponent implements OnInit {
  @Output() giftsFetched: EventEmitter<ICertificate[]> = new EventEmitter<ICertificate[]>();

  public gifts: ICertificate[];
  public _gifts: ICertificate[];
  public tags: ITag[];
  private storageKey = 'gifts';
  private scrollTimeout;
  public showSearch = false;
  public pageLength = 20;
  public pages: IGiftsPage[] = [];
  public prevState: IGiftsState = {};
  public initialState: IGiftsState = {
    page: 0,
    scroll: 0,
    tag: null,
    search: '',
    name: 'all',
  };
  public currState: IGiftsState = {
    page: 0,
    scroll: 0,
    tag: null,
    search: '',
    name: 'all',
  };
  public stateHistory: IGiftsState[] = [];
  public infiniteScrollDisabled = true;
  public tagTimeout;
  public searchDebounce = 600;
  public searchTimeout;
  public hide_gifts: string[] = ['spasibodoc'];
  cardIsOpen = false;
  constructor(
    private api: TransportService,
    private storage: StorageService,
    private logger: LoggerService
  ) { }

  @HostListener('window:scroll', ['$event'])
  onScroll(e) {
    clearTimeout(this.scrollTimeout);
    this.scrollTimeout = setTimeout(() => {
      this.currState.scroll = pageYOffset;
      this.saveToStorage();
    }, 400);
  }

  ngOnInit() {
    const storageData: string = this.storage.getCookie(this.storageKey);
    const storage: IGiftsState = storageData ? JSON.parse(storageData) : null;
    this.currState = storage || this.currState;
    this.showSearch = this.currState.name === 'search';
    combineLatest([
      this.api.getGifts().pipe(
        map(gifts => {
          // const x = new Array(40).fill(gifts[3]);
          // this.gifts = gifts.concat(x);

          this.gifts = gifts.filter(gift => !this.hide_gifts.find(url => gift.user_friendly_url === url));
          this.initPages();
          this.giftsFetched.emit(gifts);
          return gifts;
        })
      ),
    ]).subscribe(([gifts]) => {

      this.api.getTags().subscribe(data => {
        this.tags = data;
      })


      if (gifts && gifts.length) {

        // this.tags = gifts.reduce((acc, gift) => {
        //   if (gift.Tags && gift.Tags.length) {
        //     acc = acc.concat(
        //       gift.Tags.filter(tag => {
        //         return !acc.some(t => t._uuid === tag._uuid);
        //       })
        //     );
        //   }
        //   return acc;
        // }, [] as ITag[]);

        this.setState({ ...this.currState });
        setTimeout(() => {
          if (storage && storage.scroll) {
            window.scrollTo(0, storage.scroll);
          }
          setTimeout(() => {
            this.infiniteScrollDisabled = false;
          }, 1);
        }, 200);
      }
    });
  }

  private get storageExpires(): Date {
    return addMinutes(new Date(), 30);
  }



  getUrl(gift) {
    const type = gift.meta.restaurant_gift_type;
    if (type === 'one') {
      return 'certificate';
    } else if (type === 'few' || type === 'universal') {
      return 'certificates';
    }
  }


  setState(newState: IGiftsState, history = true, back = false) {
    if (history) {
      this.prevState = { ...this.currState };
      this.stateHistory.push({ ...this.currState, ...newState });
    }
    this.currState = { ...this.currState, ...newState };
    this.logger.l(['state history', this.stateHistory]);
  }

  getStateFromHistory(state: IGiftsState, reverse = false): IGiftsState {
    const filtered = this.stateHistory.filter(item => {
      return reverse ? item.name !== state.name : item.name === state.name;
    });
    return filtered[filtered.length - 1];
  }

  initPages() {
    let currPage = 0;
    this.gifts.map((item, i) => {
      const page: IGiftsPage = this.pages[currPage] || { data: [] };
      page.data.push(item);
      this.pages[currPage] = page;
      if ((i + 1) % this.pageLength === 0) {
        currPage++;
      }
    });
    this._gifts = this.getPageData(0, this.currState.page);
    this.logger.l(['pages', this.pages]);
  }

  filterByTag(data: ICertificate[], tag: ITag): ICertificate[] {
    return data.filter(gift => {

      if (gift.Partners && gift.Partners.length > 0) {
        const flag = gift.Partners.find(el => (el.tags || []).some(_uuid => _uuid === tag._uuid))
        if (flag) {
          return gift;
        }
      }

      return (gift.tags || []).some(_uuid => _uuid === tag._uuid);
    });
  }

  search(data: ICertificate[]): ICertificate[] {
    if (!this.currState.search) {
      return data;
    } else {
      const normalizeStr = (str: string): string => {
        return str.replace(/\s/g, '').toLowerCase();
      };
      const { search } = this.currState;
      // const fuse = new Fuse(data, {
      //   // includeScore: true,
      //   minMatchCharLength: 4,
      //   threshold: 0.5,
      //   keys: ['name.ru'],
      // });
      // const result = fuse.search(this.currState.search).map(fItem => fItem.item) as ICertificate[];
      return data.filter(item => {
        return normalizeStr((item.name_variant || { ru: '' }).ru).match(normalizeStr(search)) ||
          normalizeStr((item.name || { ru: '' }).ru).match(normalizeStr(search));
      });
    }
  }

  getPageData(page: number, to?: number): ICertificate[] {
    const { tag } = this.currState;
    let _page = this.currState.page;
    const data = to >= 0 ? this.getGiftsFromToPage(to) : (this.pages[page] || {}).data;
    const isSearch = this.currState.name === 'search';

    if (!data) return;

    const allFiltered = tag ? this.filterByTag(this.gifts, tag) : this.gifts;
    let gifts: ICertificate[];
    if (isSearch) {
      gifts = this.search(this.pages[page].data);
    } else if (tag) {
      gifts = this.filterByTag(data, tag);
    } else {
      gifts = data;
    }
    if (gifts.length < this.pageLength && this.pages[page + 1]) {
      while (
        gifts.length < this.pageLength &&
        gifts.length < allFiltered.length &&
        this.pages[_page]
      ) {
        const _data = this.pages[page + 1].data;
        gifts = gifts.concat(
          tag && !isSearch ? this.filterByTag(_data, tag) : isSearch ? this.search(_data) : []
        );
        _page++;
        this.setState({ page: _page }, false);
      }
    }
    return gifts;
  }

  getGiftsFromToPage(to: number, from = 0): ICertificate[] {
    let gifts = [];
    for (let page = from; page <= to && this.pages[page]; page++) {
      gifts = gifts.concat(this.pages[page].data);
    }
    return gifts;
  }

  onTagClick(tag: ITag) {
    clearTimeout(this.tagTimeout);
    this.tagTimeout = setTimeout(() => {
      this.setState({ tag, page: 0, name: 'tag' });
      this._gifts = this.getPageData(0);
      this.saveToStorage();
    }, 200);
  }

  onAllClick() {
    clearTimeout(this.tagTimeout);
    this.tagTimeout = setTimeout(() => {
      this.setState({
        ...this.getStateFromHistory({ name: 'all' }),
        tag: null,
      });
      this._gifts = this.getGiftsFromToPage(this.currState.page);
    }, 200);
    this.saveToStorage();
  }

  onSearchChange() {
    clearTimeout(this.searchTimeout);
    this.searchTimeout = setTimeout(() => {
      this.setState({ page: 0 });
      this._gifts = this.getPageData(this.currState.page);
      this.saveToStorage();
    }, this.searchDebounce);
  }


  toggleCard(){
    this.cardIsOpen = !this.cardIsOpen;
  }


  onSearchClick() {
    this.showSearch = true;
    this.cardIsOpen = false;
    this.setState({ name: 'search', page: 0 });
    if (this.currState.search) {
      this.setState({ tag: null });
      this._gifts = this.getPageData(this.currState.page);
    }
    // if (!(this.prevState.search === this.currState.search))
  }

  onCloseSearchClick() {
    this.showSearch = false;
    this.setState({
      ...this.initialState,
      ...this.getStateFromHistory({ name: 'search' }, true),
      page: 0,
    });
    this._gifts = this.getPageData(this.currState.page);
  }

  onInfiniteScroll() {
    const { page, tag } = this.currState;
    const pageData = this.getPageData(page + 1);
    if (pageData && pageData.length) {
      this.setState({ page: page + 1 });
      this._gifts = this._gifts.concat(pageData);
    }
  }

  saveToStorage() {
    this.storage.setCookie(this.storageKey, JSON.stringify(this.currState), { expires: this.storageExpires });
  }
}
