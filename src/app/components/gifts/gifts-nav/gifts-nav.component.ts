import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { IItem } from '../../scroll/scroll.component';
import { ITag } from '../../../types/ITag';

@Component({
  selector: 'app-gifts-nav',
  templateUrl: './gifts-nav.component.html',
  styleUrls: ['./gifts-nav.component.styl'],
})
export class GiftsNavComponent implements OnInit, OnChanges {
  @Input() tags: any[];
  @Input() activeTag: ITag = null;
  @Input() disabled: boolean = false;
  @Output() tagClick: EventEmitter<ITag> = new EventEmitter<any>();
  @Output() allClick: EventEmitter<any> = new EventEmitter<any>();
  public itemClick$ = new Subject<IItem>();
  public all = true;

  constructor() { }

  ngOnInit() {

    this.all = !this.activeTag;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.all = !this.activeTag;
    console.log(this.disabled)
    if (this.disabled) {
      this.all = true;
      this.activeTag = null;
      this.allClick.emit();
    }
  }

  onClick(e: Event, tag: any | 'all') {
    if (this.disabled) {
      return;
    }
    if (tag === 'all') {
      this.all = true;
      this.activeTag = null;
      this.allClick.emit();
      return;
    } else {
      this.all = false;
      this.activeTag = tag;
      this.tagClick.emit(tag);
    }
    this.itemClick$.next({ $: e.currentTarget as HTMLElement });
  }
}
