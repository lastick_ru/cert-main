import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsNavComponent } from './gifts-nav.component';

describe('GiftsNavComponent', () => {
  let component: GiftsNavComponent;
  let fixture: ComponentFixture<GiftsNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GiftsNavComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
