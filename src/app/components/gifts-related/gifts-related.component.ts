import { AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { ICertificate, ISliderSettings } from '../../types/Entities';
import { SwiperDirective } from 'ngx-swiper-wrapper';
import {PlatformService} from '../../services/platform.service';
import {mergeDeepRight, concat} from 'ramda';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper/dist/lib/swiper.interfaces';

export interface ISliderLogo {
  url?: string;
  img: string;
  classes?: string;
}

@Component({
  selector: 'app-gifts-related',
  templateUrl: './gifts-related.component.html',
  styleUrls: ['./gifts-related.component.styl']
})
export class GiftsRelatedComponent implements OnInit, AfterViewInit {
  @Input() gifts: ICertificate[];
  @Input() logos: ISliderLogo[];
  @Input() title: string;
  @Input() slider_settings: ISliderSettings;

  @ViewChild(SwiperDirective, { static: false }) swiper: SwiperDirective;

  public defaultSliderSettings;
  public sliderSettings: SwiperConfigInterface;
  public desktopWidth = 992;
  public isDesktop = false;
  // public itemWidth = 400;
  public itemWidth = 150;
  public slideGap = 32;
  public isFirstSlide = false;
  public isLastSlide = false;
  public showControls = true;
  private timeOut;

  constructor(private elRef: ElementRef, private platform: PlatformService) { }

  @HostListener('window:resize')
  onResize() {
    this.checkAndSetSlides();
  }

  ngOnInit() {
    this.setSettings();
    // const s = this.slider_settings || {};
    // this.sliderSettings = {
    //   ...this.defaultSliderSettings, ...(this.sliderSettings || {}), ...s
    // };
    // this.defaultSliderSettings.slidesPerView = s.slidesPerView ||;
  }

  ngAfterViewInit(): void {
    if (this.platform.isBrowser) {
      this.checkAndSetSlides();
    }
  }

  setSettings() {
    const s = this.slider_settings || {};
    this.defaultSliderSettings = {
      followFinger: true,
      centeredSlides: false,
      slidesPerView: 6,
      slidesPerGroup: 6,
      spaceBetween: this.slideGap,
    };
    this.defaultSliderSettings.slidesPerView = s.slidesPerView || this.defaultSliderSettings.slidesPerView;
    this.defaultSliderSettings.slidesPerGroup = s.slidesPerGroup || this.defaultSliderSettings.slidesPerGroup;
    this.sliderSettings = {...this.defaultSliderSettings, ...(this.sliderSettings || {}), ...s};
  }

  checkAndSetSlides() {
    clearTimeout(this.timeOut);
    this.timeOut = setTimeout(() =>  {
      this.setSize();
      this.checkSlideIndex();
    }, 200);
  }

  setSize() {
    const length = Math.round(this.elRef.nativeElement.getBoundingClientRect().width / this.itemWidth);
    const l = length > this.defaultSliderSettings.slidesPerView ? this.defaultSliderSettings.slidesPerView : length;
    const gr = this.slider_settings.slidesPerGroup;
    this.isDesktop = window.innerWidth >= this.desktopWidth;
    this.sliderSettings.slidesPerView = l;
    this.sliderSettings.slidesPerGroup = this.slider_settings && gr && gr < l ? gr : l;
    // this.setSettings();
    this.slideGap = this.sliderSettings.slidesPerGroup > 1 ? 32 : 10;
  }

  checkSlideIndex() {
    if (this.swiper && !this.sliderSettings.loop) {
      this.isFirstSlide = this.swiper.getIndex() === 0;
      this.isLastSlide = this.swiper.getIndex() === (this.gifts || this.logos).length - this.sliderSettings.slidesPerGroup;
      this.showControls = !(this.isFirstSlide && this.isLastSlide);
    }
    this.sliderSettings.allowTouchMove = this.showControls;
  }

  onIndexChange(index) {
    this.checkSlideIndex();
  }
}
