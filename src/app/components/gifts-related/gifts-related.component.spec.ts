import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsRelatedComponent } from './gifts-related.component';

describe('GiftsRelatedComponent', () => {
  let component: GiftsRelatedComponent;
  let fixture: ComponentFixture<GiftsRelatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsRelatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsRelatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
