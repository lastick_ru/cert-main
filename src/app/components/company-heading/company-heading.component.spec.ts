import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyHeadingComponent } from './company-heading.component';

describe('CompanyHeadingComponent', () => {
  let component: CompanyHeadingComponent;
  let fixture: ComponentFixture<CompanyHeadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyHeadingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyHeadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
