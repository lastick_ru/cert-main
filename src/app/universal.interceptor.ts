import { Injectable, Inject, Optional } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

@Injectable()
export class UniversalInterceptor implements HttpInterceptor {

  constructor(@Optional() @Inject('serverUrl') protected serverUrl: string) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // console.log('UniversalInterceptor serverUrl', this.serverUrl);
    // console.log('UniversalInterceptor reqUrl', req.url);

    const serverReq = !this.serverUrl || req.url.match(/\/assets\//) ? req : req.clone({
      url: `${this.serverUrl}${req.url}`
    });
    console.log('UniversalInterceptor serverReq', serverReq.url);
    return next.handle(serverReq);
  }
}
