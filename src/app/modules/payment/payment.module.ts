import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentMethodFilterPipe } from '../../pipes/payment_method_filter.pipe';
import { PaymentService } from '../../services/payment.service';
import {GpayButtonComponent} from '../../components/gpay-button/gpay-button.component';
import { ApplepayButtonComponent } from '../../components/applepay-button/applepay-button.component';


@NgModule({
  declarations: [
    GpayButtonComponent,
    PaymentMethodFilterPipe,
    ApplepayButtonComponent
  ],
  exports: [
    PaymentMethodFilterPipe,
    GpayButtonComponent,
    ApplepayButtonComponent
  ],
  imports: [
    CommonModule,
  ],
})
export class PaymentModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PaymentModule,
      providers: [
        PaymentService
      ]
    };
  }
}
