import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidatorsModule } from '../../validators/validators.module';
import { FormsModule } from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import {CardButtonsComponent} from '../../components/card-buttons/card-buttons.component';



@NgModule({
  declarations: [CardButtonsComponent],
  exports: [CardButtonsComponent],
  imports: [
    CommonModule,
    SharedModule,
    TranslateModule,
    RouterModule,
    ValidatorsModule,
    FormsModule,
    AngularSvgIconModule,
  ]
})
export class CardButtonsModule { }
