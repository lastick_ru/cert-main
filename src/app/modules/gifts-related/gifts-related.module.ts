import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { SwiperModule } from 'ngx-swiper-wrapper';
import {GiftsRelatedComponent} from '../../components/gifts-related/gifts-related.component';



@NgModule({
  declarations: [GiftsRelatedComponent],
  exports: [GiftsRelatedComponent],
  imports: [
    CommonModule,
    SharedModule,
    SwiperModule
  ]
})
export class GiftsRelatedModule { }
