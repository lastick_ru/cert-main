import { NgModule } from '@angular/core';
import {ServerModule, ServerTransferStateModule} from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import {TransferState} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {AngularSvgIconModule, SvgLoader} from 'angular-svg-icon';
import {SvgServerLoader} from './svg-server-loader';
import {TranslateInterceptor} from './translate.interceptor';
import { UniversalInterceptor } from './universal.interceptor';

export function svgLoaderFactory(http: HttpClient, transferState: TransferState) {
  return new SvgServerLoader('', transferState);
}

@NgModule({
  imports: [
    AngularSvgIconModule.forRoot({
      loader: {
        provide: SvgLoader,
        useFactory: svgLoaderFactory,
        deps: [ HttpClient, TransferState ],
      }
    }),
    AppModule,
    ServerModule,
    ServerTransferStateModule,
    ModuleMapLoaderModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TranslateInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UniversalInterceptor,
      /* Multi is important or you will delete all the other interceptors */
      multi: true
    }
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {}
