import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ICertificate, ICertPageSliderSettings } from '../../types/Entities';
import { ActivatedRoute } from '@angular/router';
import { IAddresseeDataItem } from '../../components/gift/gift.component';
import { path } from 'ramda';
import { TransportService } from '../../services/transport.service';
import { GlobalService } from '../../services/global.service';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.styl'],
  encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit {
  public gifts: ICertificate[];
  private activatePath = '/activate';
  public activeTab = 'gifts';
  public activateMd = '';
  public isDesktop = true;
  public state: 'gift' | 'index';
  public gift: ICertificate;
  public certSliderSettings: ICertPageSliderSettings;
  public bannerCert: ICertificate = null;

  constructor(
    private route: ActivatedRoute,
    private api: TransportService,
    public global: GlobalService,
  ) {
  }

  public routeSettings = {
    user_friendly_url: 'spasibodoc',
    widget_url: 'http://placekitten.com',
    giftTabTitle: 'Подарить врачу',
    corpTabTitle: 'Подарить больнице',
    first_logos_title: 'Где можно использовать сертификат',
    second_logos_title: 'Партнёры акции',
    action: true,
    actionText: `
      <h1>
      <strong>Доктор, теперь вы к нам.</strong>
    </h1>
    <p>
      Жертв коронавируса могло быть больше, если бы не ваша работа - работа врачей, медсестёр, санитарок, нянечек. <strong>Спасибо</strong> вам за то, что вы нас спасали!
    </p>
    <p>
      <strong>Музеи, театры, галереи и другие культурные площадки</strong> Екатеринбурга хотят добавить в вашу жизнь немного радости и помочь вернуться к «мирной жизни».
    </p>
    <p>
      В ваших руках <strong>подарочный сертификат</strong>, выпущенный специально для вас, он дает вам возможность <strong>бесплатно</strong> сходить в театр, музей, на концерт или выставку. Будем рады видеть вас без белых халатов.
    </p>
    `,
    second_logos: [
      // '/assets/img/logo/disney.svg',
      // '/assets/img/logo/smf.svg',
      { img: '/assets/img/logo/e1.svg', classes: 'sz-2' },
      { img: '/assets/img/logo/znak.svg', classes: '' },
      { img: '/assets/img/logo/tass.svg', classes: '' },
      { img: '/assets/img/logo/echom.svg', classes: 'sz-2' },
    ],
    first_logos: [
      { img: '/assets/img/support_logo/1.svg' },
      { img: '/assets/img/support_logo/2.svg' },
      { img: '/assets/img/support_logo/3.svg' },
      { img: '/assets/img/support_logo/4.svg' },
      { img: '/assets/img/support_logo/5.svg' },
      { img: '/assets/img/support_logo/6.svg' },
      { img: '/assets/img/support_logo/7.svg' },
      { img: '/assets/img/support_logo/8.svg' },
    ],
    addresseeData: [
      {
        name: 'Больнице РАН в Троицке',
        url: 'https://hospital-ran-troitsk.ru/',
        email: 'admin@hospital.troitsk.ru',
        person_name: 'Корицкий Андрей Владимирович',
      },
      {
        name: 'Городской клинической больнице №15 им. О.М.Филатова',
        url: 'http://gkb15.moscow/',
        email: 'gkb15@zdrav.mos.ru',
        person_name: 'Владимир Николаевич Мудрак',
      },
      {
        name: 'Городской клиничеcкой больнице №67 им. Ворохобова',
        email: 'gkb67@zdrav.mos.ru',
      },
      {
        name: 'Государственному научному центру колопроктологии им. Рыжих',
        email: 'info@gnck.ru',
      },
      {
        name: 'Детской клинической больнице им. З.А. Башляевой',
        email: 'dgkb-bashlyaevoy@zdrav.mos.ru',
      },
      {
        name: 'Инфекционной клинической больнице №1',
        email: 'ikb1@zdrav.mos.ru',
      },
      {
        name: 'Инфекционной клинической больнице №2',
        email: 'ikb2@zdrav.mos.ru',
      },
      {
        name: 'Клинической больнице им. Семашко РЖД',
        email: 'ckb2semashko@mail.ru',
      },
      {
        name: 'Клинической больнице №85 ФМБА',
        email: 'info@kb85.ru',
      },
      {
        name: 'Лечебно-реабилитационному центру Минздрава',
        email: 'info@med-rf.ru',
      },
      {
        name: 'Медицинскому центру в Коммунарке',
        email: 'gkb40@zdrav.mos.ru',
      },
      {
        name: 'Научному центру здоровья детей',
        email: 'info@nczd.ru',
      },
      {
        name: 'Национальному медико-хирургическому центру им. Пирогова',
        email: 'info@pirogov-center.ru',
      },
      {
        name: 'НИИ медицины труда им. Измерова',
        email: 'info@irioh.ru',
      },
      {
        name: 'НИИ скорой помощи им. Склифосовского',
        email: 'sklif@zdrav.mos.ru',
      },
      {
        name: 'НМИЦ акушерства, гинекологии и перинатологии им. Кулакова',
        email: 'secretariat@oparina4.ru',
      },
      {
        name: 'НМИЦ кардиологии Минздрава',
        email: 'info@cardioweb.ru',
      },
      {
        name: 'Российскому геронтологическому научно-клиническому центру ',
        email: 'anticorruption@rgnkc.ru',
      },
      {
        name: 'УКБ №1 Сеченовского Университета',
        email: 'rektorat@sechenov.ru',
      },
      {
        name: 'Федеральному бюро медико-социальной экспертизы Минтруда',
        email: 'fbmse@fbmse.ru',
      },
      {
        name: 'Федеральному исследовательскому центру питания биотехнологии и безопасности пищи',
        email: 'mailbox@ion.ru',
      },
      {
        name: 'Федеральному медицинскому биофизическому центру им. Бурназяна',
        email: 'fmbc-fmba@bk.ru',
      },
      {
        name: 'Федеральному научно-клиническому центру ФМБА России',
        email: 'info@fnkc-fmba.ru',
      },
      {
        name: 'Центральной клинической больнице РАН',
        email: 'ckb@ckbran.ru',
      },
      {
        name: 'Центральной клинической больнице РЖД-Медицина',
        email: 'ckb2semashko@mail.ru',
      },
      {
        name: 'Центру высоких медицинских технологий ФМБА',
        email: 'do@kb119.ru',
      },
      {
        name: 'Челюстно-лицевому госпиталю для ветеранов войн',
        email: 'chlg@zdrav.mos.ru',
      },
    ],
    subscribeFormTitle: `Если вы музей, театр, <br>объект культуры<br> <strong>Присоединяйтесь к проекту</strong>`
  };

  ngOnInit() {
  
    if (this.global.settings) {
      this.init();
    } else {
      this.global.settings$.subscribe(s => {
        this.init();
      });
    }
  }

  init() {

    this.certSliderSettings = this.global.settings.widget_settings.cert_page_sliders;
    const setting = path(['routeSettings', 'index'], this.global.settings.widget_settings);
    if (setting) {
      this.routeSettings = { ...this.routeSettings, ...setting };

      this.api.getGift(this.routeSettings.user_friendly_url).subscribe(gift => this.gift = gift);
      this.state = 'gift';
    } else {
      this.state = 'index';
    }
    if (location.pathname === this.activatePath) {
      this.activeTab = 'gifts_activate';
      this.route.queryParams.subscribe(q => {
        if (q.number) {
          this.activateMd = q.number;
        }
      });
    }
  }

  onGiftsFetched(gifts: ICertificate[]) {
    this.gifts = gifts;
    this.bannerCert = this.gifts.find(el => el.meta.restaurant_gift_type === 'universal')

  }

  ngAfterViewInit(){
    this.isDesktop = window.innerWidth > 991;
  }
}
