import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from './index.component';
import {SharedModule} from '../../modules/shared/shared.module';
import {TabsModule} from '../../modules/tabs/tabs.module';
import {TranslateModule} from '@ngx-translate/core';
import {BActivateComponent} from '../../components/b-activate/b-activate.component';
import {FormsModule} from '@angular/forms';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {SwiperModule} from 'ngx-swiper-wrapper';
import {GiftsComponent} from '../../components/gifts/gifts.component';
import {GiftsNavComponent} from '../../components/gifts/gifts-nav/gifts-nav.component';
import {ScrollModule} from '../../modules/scroll/scroll.module';
import {ValidatorsModule} from '../../validators/validators.module';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { GiftComponent } from '../../components/gift/gift.component';
import { GiftModule } from '../../components/gift/gift.module';



export const ROUTES: Routes = [{ path: '', component: IndexComponent }];

const declarations = [
  IndexComponent,
  GiftsComponent,
  GiftsNavComponent,
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    SharedModule,
    FormsModule,
    TabsModule,
    TranslateModule,
    AngularSvgIconModule,
    ValidatorsModule,
    ScrollModule,
    SwiperModule,
    InfiniteScrollModule,
    GiftModule
  ]
})
export class IndexModule { }
