import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../modules/shared/shared.module';
import { ContactsComponent } from './contacts.component';


export const ROUTES: Routes = [{ path: '', component: ContactsComponent }];

@NgModule({
  declarations: [
    ContactsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
})
export class ContactsModule { }
