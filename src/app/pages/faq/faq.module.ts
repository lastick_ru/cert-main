import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../modules/shared/shared.module';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { FaqComponent } from './faq.component';


export const ROUTES: Routes = [{ path: '', component: FaqComponent }];

const declarations = [
  FaqComponent,
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    SwiperModule
  ],
})
export class FaqModule {
}
