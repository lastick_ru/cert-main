import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ICertificateBody } from '../../types/req';
import { StorageService } from '../../services/storage.service';
import { ICertificateOrder, IPaymentMethod } from '../../types/Entities';
import { FormService } from '../../services/form.service';
import { NgForm, NgModel } from '@angular/forms';
import { TransportService } from '../../services/transport.service';
import { IContacts, PaymentService } from '../../services/payment.service';
import { isNil, isEmpty, path } from 'ramda';
import { DocService } from '../../services/doc.service';
import { Subscription } from 'rxjs';
import { FormHelperComponent } from '../../components/form-helper/form-helper.component';
import { LoggerService } from '../../services/logger.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.styl'],
  encapsulation: ViewEncapsulation.None,
})
export class CheckoutComponent extends FormHelperComponent implements OnInit, OnDestroy {
  public initialMd = { client: {} };
  public md: ICertificateBody;
  public order: ICertificateOrder;
  public methods: IPaymentMethod[];
  public contacts: IContacts;
  public total_cost = 0;
  public fetching = false;
  private formName = 'checkoutForm';
  public formSubmitted = false;
  private subscriptions: Subscription[] = [];
  private applePaySession;
  public focusedElement: EventTarget;
  public cart_data: ICertificateBody;

  @ViewChild('form', { static: false }) _form: NgForm;

  constructor(
    private storage: StorageService,
    private formService: FormService,
    private api: TransportService,
    private payment: PaymentService,
    public doc: DocService,
    private logger: LoggerService
  ) {
    super();
  }

  ngOnInit() {
    //const savedData: ICertificateBody = this.formService.getFormValues(this.formName);
    this.api.getGiftPaymentMethod().subscribe((methods) => {
      this.methods = methods;
      console.log(this.methods)
    });
    const order = this.storage.getItem('cert_order');
    
    this.order = order ? JSON.parse(order) : { data: { } };
    this.initialMd.client = {
      email: '',
      phone: ''
    }
    this.md = { ...this.initialMd };
    console.log(this.md)
    this.total_cost = this.order.total_cost;
    this.logger.l(['cart_data', this.md]);
    this.logger.l(['order', this.order]);
  }

  ngOnDestroy(): void {
    this.subscriptions.map(s => {
      s.unsubscribe();
    });
  }

  onSubmit() {
  }

  onBtnClick(pm: IPaymentMethod) {

    this.formService.markFormGroupTouched(this._form);
    this.formSubmitted = true;
    if (this._form.invalid) {
      return;
    }
    this.fetching = true;
    this.formService.saveFormValues(this.formName, this.md);
    this.payment.checkout(pm, this.total_cost, this.order, {
      email: this.md.client.email,
      phone: this.md.client.phone
    }).finally(
      () => {
        setTimeout(() => {
          this.fetching = false;
        }, 1000);
      }
    );
  }

  // onApplePayClick(pm: IPaymentMethod) {
  //   try {
  //     const cart_data: ICertificateBody = this.order.data;
  //     cart_data.payment_method = pm._uuid;
  //     cart_data.client = {...this.md.client};
  //     this.cart_data = {...cart_data};
  //     this.logger.l(['cart_data onApplePayClick', this.cart_data]);
  //     const paymentOptions = {
  //       requestPayerEmail: isNil(this.md.client.email) || isEmpty(this.md.client.email),
  //       requestPayerPhone: isNil(this.md.client.phone) || isEmpty(this.md.client.phone),
  //     };
  //     const {paymentMethodData, paymentDetails} = this.payment.getOptions(pm, this.total_cost);
  //     this.logger.l(['try payment session', {
  //       paymentMethodData: [paymentMethodData],
  //       paymentDetails,
  //       paymentOptions,
  //     }]);
  //
  //     this.applePaySession = new PaymentRequest(
  //       [paymentMethodData],
  //       paymentDetails,
  //       paymentOptions
  //     );
  //     this.applePaySession['onmerchantvalidation'] = event => {
  //       const validationURL = event.validationURL;
  //       this.logger.l(['onmerchantvalidation', { event }]);
  //       this.logger.l([`[onmerchantvalidation] ${validationURL}`]);
  //
  //       this.api
  //         .validateMerchant(pm._uuid, { validationURL })
  //         .toPromise()
  //         .then(response => {
  //           this.logger.l(['validateMerchant complete', { response }]);
  //           event.complete(response);
  //         })
  //         .catch(e => {
  //           this.logger.l(['validateMerchant error', { error: e }]);
  //           this.applePaySession.abort();
  //         });
  //     };
  //     this.logger.l(['try session show']);
  //     this.applePaySession
  //       .show()
  //       .then(response => {
  //         this.logger.l(['session.show', { response }]);
  //         this.logger.l(['session show cart_data', this.cart_data]);
  //
  //         if (!isNil(response.payerPhone) && !isEmpty(response.payerPhone)) {
  //           this.cart_data.client.phone = response.payerPhone;
  //           this.logger.l(['payerPhone', response.payerPhone]);
  //         }
  //         if (!isNil(response.payerEmail) && !isEmpty(response.payerEmail)) {
  //           this.cart_data.client.email = response.payerEmail;
  //           this.logger.l(['payerEmail', response.payerEmail]);
  //         }
  //         this.cart_data['payment_token'] = path(['details', 'token', 'paymentData'], response);
  //
  //         this.logger.l(['session cart_data', this.cart_data]);
  //
  //         this.api.newOrder(JSON.stringify(this.cart_data)).subscribe(
  //           success => {
  //             response.complete('success');
  //             window.location.href = success.payment['payment_url'];
  //           },
  //           error => {
  //             response.complete('fail');
  //           }
  //         );
  //       })
  //       .catch(e => this.logger.e(['[session.show] REJECTED', e]));
  //   } catch (err) {
  //     this.logger.e(['catch payment session', { error: err }]);
  //     try {
  //       if (this.applePaySession) {
  //         this.applePaySession.abort();
  //       }
  //     } catch (e) {
  //       this.logger.e([e]);
  //     }
  //   }
  // }
}
