import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../modules/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FormsModule } from '@angular/forms';
import { CheckoutComponent } from './checkout.component';
import { ValidatorsModule } from '../../validators/validators.module';
import { PaymentModule } from '../../modules/payment/payment.module';


export const ROUTES: Routes = [{ path: '', component: CheckoutComponent }];

@NgModule({
  declarations: [
    CheckoutComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    TranslateModule,
    AngularSvgIconModule,
    ValidatorsModule,
    PaymentModule
  ],
})
export class CheckoutModule { }
