import { Component, OnInit, ViewChild, HostListener, ElementRef } from '@angular/core';
import * as $ from 'jquery';
import { SocialService } from '../../../services/social.service';
@Component({
  selector: 'new-app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  public showNav = false;
  showMenu = false;
  @ViewChild('menu', { static: false }) menu: ElementRef;


  public menuState = false;

  // @HostListener('document:click', ['$event'])
  // onClick(event: Event) {
  //   if (this.menu && !this.menu.nativeElement.parentNode.contains(event.target)) {
  //     $('.main-header--mobile .header__menu-container .header-menu').removeClass('active')
  //   }
  // }


  constructor(
    public social: SocialService,
  ) { }

  ngOnInit() {
  }

  show(){}
}
