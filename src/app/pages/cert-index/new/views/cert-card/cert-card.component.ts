import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICertificate, ICertificateSkinSettings } from 'src/app/types/Entities';

@Component({
  selector: 'app-cert-card',
  templateUrl: './cert-card.component.html',
  styleUrls: ['./cert-card.component.styl'],
  host: {
    'class': 'app-cert-card-wrapper',
  }
})
export class CertCardComponent implements OnInit {
  @Input() data:any = null;
  @Input() skin?: ICertificateSkinSettings;
  public rating: number = null;
  path:string=null;

  constructor(
    private router:Router
  ) { }

  ngOnInit() {
  
    this.initRating();

    this.skin = this.skin || (this.data || {}).skin_settings;
    this.path = this.data.image?this.data.image.path:null
  }

  routeLink(){
    const link = this.data._uuid;
    if(link){
      this.router.navigate(['/'+link])
    }
  }
  initRating() {

    this.rating = this.data.data.restaurant_rating;
    
  }
}
