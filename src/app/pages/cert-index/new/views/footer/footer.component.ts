import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'new-app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {

  @Input() phone: string = "+7 (495) 191 18 88";
  constructor() { }

  ngOnInit() {
  }

}
