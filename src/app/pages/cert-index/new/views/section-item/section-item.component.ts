import { Component, Input, OnInit } from '@angular/core';
import { NgbSlideEvent } from '@ng-bootstrap/ng-bootstrap';
import { SlickCarouselComponent } from 'ngx-slick-carousel';

@Component({
  selector: 'app-section-item',
  templateUrl: './section-item.component.html',
  styleUrls: ['./section-item.component.less']
})
export class SectionItemComponent implements OnInit {
  @Input() tabs: any = [];
  @Input() slider: any = [];
  @Input() contactData: any = null;
  @Input() tabsWithSlider: number[] = null;
  isShown: boolean = false;
  slideConfig = { "slidesToShow": 1, "slidesToScroll": 1, };
  slides = [
    {
      img: "/assets/new/img/slider/slider1/1.png"
    },
    {
      img: "/assets/new/img/slider/slider1/1.png"
    },
    {
      img: "/assets/new/img/slider/slider1/1.png"
    }
  ];
  constructor() { }

  ngOnInit() {

  }

  toggle() {
    this.isShown = !this.isShown;
  }


  getLink(value): string {
    return value.replace(/[^\d]/g, '');
  }

  withSlider(index: number): boolean {
    if (!this.tabsWithSlider || this.tabsWithSlider.length === 0) {
      return false;
    }

    return this.tabsWithSlider.findIndex(el => index === el) > -1;
  }

}
