import { Component, Input, OnInit,OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorService } from 'ngx-device-detector';
import { QueryBuilderService } from 'src/app/services/query-builder.service';
import { TransportService } from 'src/app/services/transport.service';
import { ICertificate } from 'src/app/types/Entities';
import { ITag } from 'src/app/types/ITag';
import { RestarauntListModalComponent } from '../modals/restaraunt-list-modal/restaraunt-list-modal.component';

@Component({
  selector: 'cert-list',
  templateUrl: './cert-list.component.html',
  styleUrls: ['./cert-list.component.less']
})
export class CertListComponent implements OnInit {

  @Input() partners: ICertificate = null;
  tags:ITag[]=null;
  subscription:any;
  constructor(
    private api: TransportService,
    private modalService: NgbModal,
    public deviceService: DeviceDetectorService,
    public queryService: QueryBuilderService
  ) {
   }

  ngOnInit() {
    console.log(this.partners)
    this.subscription = this.api.getTags().subscribe(data => {
      this.tags = data;
     
    })
  }

  showAll() {
    const modalRef = this.modalService.open(RestarauntListModalComponent, { centered: true, windowClass: 'restaraunt-list-modal', backdropClass: 'restaraunt-list-backdrop' });
    modalRef.componentInstance.partners = this.partners;
    modalRef.componentInstance.tags = this.tags;
    modalRef.result.then((result) => {

    }, (reason) => {

    });
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
