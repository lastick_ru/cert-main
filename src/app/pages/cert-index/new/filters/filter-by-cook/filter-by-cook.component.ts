import { Component, Input, OnInit } from '@angular/core';
import { CertificateService } from 'src/app/services/certificate.service';
import { QueryBuilderService } from 'src/app/services/query-builder.service';
import { ICertificate } from 'src/app/types/Entities';
import { ITag } from 'src/app/types/ITag';

@Component({
  selector: 'filter-by-cook',
  templateUrl: './filter-by-cook.component.html',
  styleUrls: ['./filter-by-cook.component.less']
})
export class FilterByCookComponent implements OnInit {
  @Input() tags:ITag[] = null;
  getAll: boolean = true;
  constructor(
    public service: CertificateService,
    public queryService: QueryBuilderService
  ) { }

  ngOnInit() {
    console.log(this.queryService.tags)
  }


  filterByTag(data: ICertificate[], tag: ITag): ICertificate[] {
    return data.filter(gift => {

      if (gift.Partners && gift.Partners.length > 0) {
        const flag = gift.Partners.find(el => (el.tags || []).some(_uuid => _uuid === tag._uuid))
        if (flag) {
          return gift;
        }
      }

      return (gift.tags || []).some(_uuid => _uuid === tag._uuid);
    });
  }

  toggleAll() {
    this.getAll = !this.getAll;
    if (this.getAll) {
      this.queryService.tags = [];
      this.queryService.fireChangeTagsQuery();
    }
  }
  toggle(tag: ITag) {
    const pos = this.queryService.tags.findIndex(el => tag._uuid === el._uuid);
    if (pos > -1) {
      this.queryService.tags.splice(pos, 1);
    } else {
      this.queryService.tags.push(tag);
    }

    if (this.queryService.tags.length < 1) {
      this.getAll = true;
    } else {
      this.getAll = false;
    }

    this.queryService.fireChangeTagsQuery();
  }

  isActive(uid: string): boolean {
    return this.queryService.tags.findIndex(el => el._uuid === uid) > -1;
  }
}
