import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CertificateService } from 'src/app/services/certificate.service';
import { ITag } from 'src/app/types/ITag';
import { clone, isEmpty } from 'ramda';
import { QueryBuilderService } from 'src/app/services/query-builder.service';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-restaraunt-list-modal',
  templateUrl: './restaraunt-list-modal.component.html',
  styleUrls: ['./restaraunt-list-modal.component.less']
})
export class RestarauntListModalComponent implements OnInit {
  @Input() partners: any = null;
  @Input() tags: ITag[] = null;
  restNameQuery:string = null;
  data: any = null
  isCard = false;
  inputIsOpen = false;
  subscription: Subscription;
  restNameUpdate = new Subject<string>();
  constructor(
    public activeModal: NgbActiveModal,
    public service: CertificateService,
    public queryService: QueryBuilderService
  ) {

    this.subscription = this.queryService.changeTagsQueryEvent.subscribe(() => {
      this.filterByTag();
    });

    this.restNameUpdate.pipe(
      debounceTime(200),
      distinctUntilChanged())
      .subscribe(value => {
        this.filterByTag()
        this.data = this.data.filter(el => el.name.ru.toLowerCase().indexOf(value.toLowerCase()) !== -1);
      });
  }


  filterByTag(){
    if (this.queryService.tags.length === 0) {
      this.data = clone(this.partners)
    } else {
      this.data = clone(this.partners).filter(el => {
        for (let i = 0; i < this.queryService.tags.length; i++) {
          if (el.Tags.some(item => item._uuid === this.queryService.tags[i]._uuid)) {
            return true;
          }
        }
        return false;
      });
    }
  }

  ngOnInit() {
    this.data = clone(this.partners);
  }

  close() {
    this.activeModal.close();
  }


  onCloseInput(){
    this.inputIsOpen=false;
    this.restNameQuery = '';
    this.filterByTag();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
