

import { Component, OnInit, ViewEncapsulation, ViewChild, Renderer2, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbSlideEvent } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SlickCarouselComponent } from 'ngx-slick-carousel';
import { ICertificate, ICertificateOrder } from 'src/app/types/Entities';
import { TransportService } from '../../../../services/transport.service';
import { clone, isEmpty } from 'ramda';
import { addDays, addHours, addMonths, addYears, parse } from 'date-fns';
import { LoggerService } from 'src/app/services/logger.service';
import { MetricService } from 'src/app/services/metric.service';
import { CertificateService } from '../../services/certificate.service';
import { StorageService } from 'src/app/services/storage.service';
import { DocService } from 'src/app/services/doc.service';
import { InformationModalComponent } from '../../new/modals/restaraunt-list-modal/information-modal/information-modal.component';
import { Meta, Title } from '@angular/platform-browser';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { QueryBuilderService } from '../../../../services/query-builder.service';
import { ITag } from '../../../../types/ITag';

const parseInterval = require('postgres-interval');

@Component({
  selector: 'app-universal-cert',
  templateUrl: './universal-cert.component.html',
  styleUrls: ['./universal-cert.component.less']
})
export class UniversalCertComponent implements OnInit {
  @ViewChild('slickModal', { static: false }) slickModal: SlickCarouselComponent;
  @Input() gift: ICertificate = null;
  model = {
    name: '',
    message: '',
    email: '',
    phone: '',
    price: null,
    certCount: 1,
    person: '',
    total: 0,
    is_gift: true,
    is_corporate: false
  };

  contactForm = {
    name: '',
    message: '',
    email: '',
    phone: '',
  };

  enablePolicy: boolean = false;
  minPrice: number = null;
  skin = null;
  skin_bg = null;
  certStrings = ['сертификат', 'сертификата', 'сертификатов'];
  bg_theme = 1;
  bg = null;
  customPriceValue: number = null;
  customPriceValid: boolean = true;
  theme = 2;
  partners:ICertificate[] = null
  tabs: any[] = [];
  slider: any[] = [];
  tags: any[] = [];
  city = null;

  giftTab = Tab.gift;
  corpTab = Tab.corporate;
  customPrice: boolean = false;
  public order: ICertificateOrder;
  isValid: boolean = true;
  lastDate: Date = null;
  slides = [
    { name: 'Светлана Алексеева', text: `Отличное место с реально вкусными морепродуктами! Устрицы великолепные, отменный гребешок, вкус пагра запомнился. Правильное место для желающих окунуться в тематическую гастрономию.` },
    { name: 'Кирилл Иванов', text: `Отличное место для любителей морепродуктов! Максимальный выбор и всегда достойное качество блюд! С официантами приятно поболтать на темы блюд.` },
    { name: 'Артем Круглов', text: `Уютно, весело, культурно. Яркие находки в баре, ребята стараются. Отличный выбор морегадов, к ним достойное вино. Рекомендую для ознакомления с морской кухней.` },
    { name: 'Светлана Алексеева', text: `Отличное место с реально вкусными морепродуктами! Устрицы великолепные, отменный гребешок, вкус пагра запомнился. Правильное место для желающих окунуться в тематическую гастрономию.` },
    { name: 'Кирилл Иванов', text: `Отличное место для любителей морепродуктов! Максимальный выбор и всегда достойное качество блюд! С официантами приятно поболтать на темы блюд.` },
    { name: 'Артем Круглов', text: `Уютно, весело, культурно. Яркие находки в баре, ребята стараются. Отличный выбор морегадов, к ним достойное вино. Рекомендую для ознакомления с морской кухней.` },
  ];
  slideConfig1 = { "slidesToShow": 3, "slidesToScroll": 1 };

  question1 = 'На какую сумму можно подарить сертификат?';
  description1 = 'Сертификаты доступны на любую сумму от 5 000 рублей. Стандартно мы предлагаем номиналы 50 000, 30 000, 15 000 и 5 000 рублей. Также можно задать свою сумму.';

  question2 = 'Как выглядит сертификат?';
  description2 = 'Вот в таком виде сертификат приходит получателю: Сертификат.pdf';

  question3 = 'Как воспользоваться сертификатом?';
  description3 = 'Сертификат действует во всех ресторанах, подключенных к программе. При оплате счета нужно просто назвать номер сертификата или показать официанту QR-код — распечатывать необязательно.';

  question4 = 'Как проверить баланс?';
  description4 = 'Свой баланс можно проверить здесь. В первый раз потребуется ввести номер сертификата или email. Также мы отправляем уведомления на email после каждого списания средств: там тоже будет указан баланс.';

  question5 = 'Как долго действует сертификат?';
  description5 = 'Подарочный сертификат действует 6 месяцев со дня покупки. Срок действия всегда можно проверить на самом сертификате или на сайте.';

  question6 = 'Сколько раз можно использовать сертификат?';
  description6 = 'Сертификат действует, пока с него не будут списаны все средства или пока не закончится срок действия.';

  question7 = 'Что делать, если стоимость счета в ресторане окажется больше номинала?';
  description7 = 'Все просто: остаток можно доплатить любым удобным способом в ресторане.';

  question8 = 'Что делать, если стоимость счета в ресторане окажется меньше номинала?';
  description8 = 'Остаток не сгорает — он сохранится для повторного использования. Можно продолжать посещать рестораны, пока на счету есть средства.';

  restNameQuery = null;
  restNameUpdate = new Subject<string>();
  subscription: Subscription;
  tagSubscription: Subscription;
  inputIsOpen = false;
  data: any = null;

  allFilters = true;
  active = 1;
  activeFaq = 0;


  slider1 = [
    {
      img: "/assets/new/img/slider1.png"
    }
  ];




  constructor(
    private deviceService: DeviceDetectorService,
    private renderer: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
    private api: TransportService,
    private logger: LoggerService,
    private metric: MetricService,
    private service: CertificateService,
    private storage: StorageService,
    public doc: DocService,
    private modalService: NgbModal,
    private title: Title,
    private meta: Meta,
    public queryService: QueryBuilderService
  ) {
    this.renderer.addClass(document.body, 'zapomny-page-container-single')
    if (this.deviceService.isMobile()) {
      this.slideConfig1.slidesToShow = 1;
    }

    this.tagSubscription = this.api.getTags().subscribe(data => {
      this.tags = data;

    });

    this.subscription = this.queryService.changeTagsQueryEvent.subscribe(() => {
      this.filterByTag();
    });

    this.restNameUpdate.pipe(
      debounceTime(200),
      distinctUntilChanged())
      .subscribe(value => {
        console.log('SSS', value);
        this.filterByTag();
        if (value.trim().length > 0) {
          this.data = this.data.filter(el => el.name.ru.toLowerCase().indexOf(value.toLowerCase()) !== -1);
        } else {
          this.data = clone(this.partners);
        }
      });

  }

  filterByTag() {
    if (this.queryService.tags.length === 0) {
      this.data = clone(this.partners);
    } else {
      this.data = clone(this.partners).filter(el => {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.queryService.tags.length; i++) {
          if (el.Tags.some(item => item._uuid === this.queryService.tags[i]._uuid)) {
            return true;
          }
        }
        return false;
      });
    }
  }
  onCloseInput() {
    this.inputIsOpen = false;
    this.restNameQuery = '';
    this.filterByTag();
  }

  calcTotal() {
    this.model.total = this.model.price * this.model.certCount;
  }

  addCert() {
    this.model.certCount += 1;
    this.calcTotal();
  }

  delCert() {
    if (this.model.certCount > 0) {
      this.model.certCount -= 1;
      this.calcTotal();
    }
  }

  initTabs() {
    this.tabs = clone(this.gift.tabs);
  }

  initSlider() {
    this.slider = clone(this.gift.entity_icons)
  }

  ngOnInit() {

    console.log('ON', this.data);

    console.log(this.gift)

    this.minPrice = this.gift.prices_range[0];
    this.gift.prices.sort((a, b) => b - a);
    this.setSeo();
    this.initLastDate();
    this.initQueryPrice();
    this.initTabs();
    this.initSlider();
    if (!this.gift.skin_settings) {
      this.gift.skin_settings = {
        "skin": "planetarium",
        "vars": "--skin: #311004;--skin-text: #fff;--skin-card-text:#ffffff;--skin-gradient-angle-1: rgba(231, 217, 185, 0.0);--skin-gradient-angle-2: transparent;--skin-gradient-bottom-1: transparent;--skin-gradient-bottom-2: #202020;--skin-page-bg: #202020;--skin-page-gradient-bottom-1: rgba(255, 255, 255, 0.29);--skin-page-gradient-bottom-2: transparent;--skin-page-gradient-bottom-3: #ae9865;--skin-btn-bg: rgba(255,255,255,0.1);--skin-btn-text: #ffffff;--skin-btn-bg-hover: rgba(255,255,255,0.2);--skin-btn-text-hover: #ffffff;--skin-btn-bg-active: #ffffff;--skin-btn-text-active: #ae9865;"
      };
    }

    this.skin = this.skin || (this.gift || {}).skin_settings;
    this.skin_bg = this.skin_bg || (this.gift || { skin_settings: { skin: 'default' } }).skin_settings;

    this.skin_bg = this.gift.image && this.gift.image.path ? this.gift.image.path : '';





    this.bg = this.gift && this.gift.background_image ? this.gift.background_image.path : '';

    this.service.getPartners().then(data => {
      this.partners = data;
      this.data = clone(this.partners);
    });
  }

  setSeo() {
    this.title.setTitle(this.gift.seo.meta.title.ru);
    this.meta.updateTag({ name: 'keywords', content: this.gift.seo.meta.keywords.ru });
    this.meta.updateTag({ property: 'og:keywords', content: this.gift.seo.meta.keywords.ru });
    this.meta.updateTag({ property: 'og:title', content: this.gift.seo.meta.title.ru });
    this.meta.updateTag({ name: 'description', content: this.gift.seo.meta.description.ru });
    this.meta.updateTag({ property: 'og:description', content: this.gift.seo.meta.description.ru });
  }

  onSlide(slideEvent: NgbSlideEvent) { }

  validateEmail(): boolean {
    const mailPattern = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    const isValid = mailPattern.test(this.model.email);

    return isValid;
  }


  validate(): boolean {

    if (!this.model.name) {
      return false;
    }

    if (!this.model.person) {
      return false;
    }

    if (!this.model.certCount || this.model.certCount === 0) {
      return false;
    }

    if (!this.validateEmail()) {
      return false;
    }

    if (!this.model.phone || !this.validatePhone()) {
      return false;
    }

    if (!this.model.message) {
      return false;
    }

    if (!this.enablePolicy) {
      return false;
    }
    return true;
  }

  validatePhone():boolean{
    let regex = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
    return regex.test(this.model.phone)
  }

  genOrder() {
    const items = [];
    items.push({
      count: this.model.certCount,
      price: this.model.price,
      sender_name: this.model.name,
      is_gift: this.model.is_gift,
      is_corporate: this.model.is_corporate,
      cert_view: this.gift.views[0],
      cert_config: this.gift._uuid,
      addressee: {
        name: this.model.name,
        email: this.model.email,
        phone: this.model.phone,
        message: this.model.message
      }
    });
    this.order = {
      agreement: true,
      gift: clone(this.gift),
      total_cost: this.model.total,
      type: 'gift',
      data: {
        client: clone(this.model),
        items: items
      }
    };

    // const order: ICertificateOrder = {
    //   ...this.order,
    //   type: this.tab,
    //   gift: this.gift,
    //   data: { ...clone(this.initialTab), items: [] },
    // };
    // let total_cost = 0;
    // const data = this.tabs[this.tab];
    // const _items = data.items || [];
    // _items.map(item => {
    //   if (item.count > 0) {
    //     const _data = item;
    //     const price = _data.price * _data.count;
    //     total_cost += price;
    //     order.data.items.push(_data);
    //     order.data.client = clone(data.client);
    //   }
    // });
    // this.order = order;
    // this.order.total_cost = total_cost;
  }


  declOfNum(n: number, text_forms: string[]): string {
    n = Math.abs(n) % 100;
    let n1 = n % 10;
    if (n > 10 && n < 20) { return text_forms[2]; }
    if (n1 > 1 && n1 < 5) { return text_forms[1]; }
    if (n1 === 1) { return text_forms[0]; }
    return text_forms[2];
  }


  pay(e) {
    e.preventDefault();
    this.isValid = this.validate();

    if (this.isValid) {
      this.genOrder();
      this.logger.l(['_form', this.model]);
      const navigate = () => {
        this.saveToStorage();
        this.router.navigate([`checkout`]);
        console.log(this.order)
        this.metric.trackFb('InitiateCheckout', { value: this.model.total });
        this.metric.trackFb('InitiateCheckout', { value: this.model.total });
        this.logger.l(['form', this.model]);
      };

      navigate();
    }

  }

  feedback() {

  }
  initQueryPrice() {
    if (this.gift.prices && this.gift.prices.length > 0) {
      this.model.price = this.gift.prices[0];
    }

    this.route.queryParams.subscribe(query => {
      if (query && query.price) {
        this.model.price = +query.price;
        console.log(query.price)
        //this.calcPrice(this.tabs[this.tab], parseInt(query.price, 10), 0);
      }


      //this.calculate();
    });
  }

  initLastDate() {
    let d: Date = new Date();
    if (this.gift.activation_last_date) {
      d = new Date(
        this.gift.activation_last_date.replace(/\s/, 'T').replace(/\+.+/, '')
      );
    } else if (this.gift.activation_ttl) {

      const ttl = parseInterval(this.gift.activation_ttl);

      Object.keys(ttl).map(k => {
        switch (k) {
          case 'years':
            d = addYears(d, ttl.years);
            break;
          case 'months':
            d = addMonths(d, ttl.months);
            break;
          case 'days':
            d = addDays(d, ttl.days);
            break;
          case 'hours':
            d = addHours(d, ttl.hours);
            break;
        }
      });
    }

    if (d.getTime() - Date.now() >= 1000 * 60 * 60 * 24 * 30 * 12 * 2) {
      this.lastDate = null;
    } else {
      this.lastDate = d;

    }
  }


  changePrice(value: number) {
    this.customPrice = false;
    this.customPriceValue = null;
    if (this.model.price !== value) {
      this.model.price = value;
      if (this.model.certCount == 0) {
        this.model.certCount = 1;
      }
    } else {
      this.model.price = null;
    }
    this.calcTotal();


  }

  saveToStorage() {
    if (this.order) {
      this.storage.setItem('cert_order', JSON.stringify(this.order));
      this.storage.setItem('time_mark1', Date.now());
    }
  }

  openModal() {
    this.modalService.open(InformationModalComponent, { centered: true, windowClass: "cert-example-modal" }).result.then((result) => {

    }, (reason) => {

    });
  }

  onCustomValueSubmit() {
    this.customPriceValid = true;

    if (this.customPriceValue < this.minPrice / 100) {
      this.customPriceValid = false;

      return;
    }
    this.customPrice = false;
    const value = (this.customPriceValue || 0) * 100;
    this.model.price = value;

  }


  openDoc() {
    this.doc.openPopup('gift_offer');
    return false;
  }

  tab(type: Tab) {
    if (type === this.giftTab) {
      this.model.is_gift = true;
      this.model.is_corporate = false;
    } else {
      this.model.is_gift = false;
      this.model.is_corporate = true;
    }

  }
  changeCustomPrice() {
    this.model.price = null;
    this.customPrice = !this.customPrice;
    this.customPriceValid = true;
    if (this.model.certCount == 0) {
      this.model.certCount = 1;
    }
    if (this.customPriceValue && this.customPriceValue < this.minPrice / 100) {
      this.customPriceValid = false;
      this.customPriceValue = null;
      this.customPrice = false;
      this.model.price = this.gift.prices[0];
      return;
    }
  }

  prevent(e) {
    e.stopPropagation();
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'zapomny-page-container-single')
  }

  tagSelected(uuid: string): boolean {
    return this.queryService.tags.findIndex(el => el._uuid === uuid) > -1;
  }
  toggle(tag: ITag) {
    const pos = this.queryService.tags.findIndex(el => tag._uuid === el._uuid);
    if (pos > -1) {
      this.queryService.tags.splice(pos, 1);
    } else {
      this.queryService.tags.push(tag);
    }

    if (this.queryService.tags.length < 1) {
      this.allFilters = true;
    } else {
      this.allFilters = false;
    }


    this.queryService.fireChangeTagsQuery();
  }
  toggleAll() {
    this.allFilters = !this.allFilters;
    if (this.allFilters) {
      this.queryService.tags = [];
      this.queryService.fireChangeTagsQuery();
    }
  }

  getBackgroundImage(item: ICertificate): string {
    const path = item.image ? item.image.path : null;
    const urlPart = path ? `, url("${path}")` : '';
    const gradientPart = 'linear-gradient(180deg, rgba(23, 34, 73, 0) 50%, #172249 82.03%)';

    return `${gradientPart}${urlPart}`;
  }

  closeSearch() {
    this.inputIsOpen = false;
    this.restNameQuery = '';
    this.restNameUpdate.next('');
  }

  contact($event: MouseEvent) {

  }
}


export enum Tab {
  gift = 1,
  corporate = 2
}
