
import { Component, OnInit, ViewEncapsulation, ViewChild, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbSlideEvent } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SlickCarouselComponent } from 'ngx-slick-carousel';
import { ICertificate, ICertificateOrder } from 'src/app/types/Entities';
import { TransportService } from '../../services/transport.service';
import { clone, isEmpty } from 'ramda';
import { addDays, addHours, addMonths, addYears, parse } from 'date-fns';
import { LoggerService } from 'src/app/services/logger.service';
import { MetricService } from 'src/app/services/metric.service';
import { CertificateService } from './services/certificate.service';
import { StorageService } from 'src/app/services/storage.service';
import { DocService } from 'src/app/services/doc.service';
import { InformationModalComponent } from './new/modals/restaraunt-list-modal/information-modal/information-modal.component';
import { Meta, Title } from '@angular/platform-browser';

const parseInterval = require('postgres-interval');

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.less'],

})
export class IndexCertComponent implements OnInit {

  public gift: ICertificate = null;
  type:any = null;
  constructor(

    private renderer: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
    private api: TransportService,

  ) {
    this.renderer.addClass(document.body, 'zapomny-page-container')

  }




  ngOnInit() {

    const uuid = '619abdc0-e87e-11ea-906a-2b559637a08e';

    this.api.getGift(uuid).subscribe(data => {

      console.log('DATA', data);

      this.gift = data;
      this.type = this.gift.meta.restaurant_gift_type;

    }, () => {
      this.router.navigate(['']);
    });

    // this.route.params.subscribe(params => {
    //   console.log(params.uuid)
    //
    //
    //
    // });
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'zapomny-page-container')
  }

}

