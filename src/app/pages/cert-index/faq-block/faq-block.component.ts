import { Component, Input, OnInit } from '@angular/core';
import { ICertificate } from '../../../types/Entities';

@Component({
  selector: 'app-faq-block',
  templateUrl: './faq-block.component.html',
  styleUrls: ['./faq-block.component.less']
})
export class FaqBlockComponent implements OnInit {

  @Input() question = '';
  @Input() description = '';

  constructor() { }

  ngOnInit() {
  }

}
