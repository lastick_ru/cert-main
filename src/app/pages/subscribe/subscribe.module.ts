import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../modules/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { SubscribeComponent } from './subscribe.component';


export const ROUTES: Routes = [{ path: '', component: SubscribeComponent }];

@NgModule({
  declarations: [
    SubscribeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
})
export class SubscribeModule { }
