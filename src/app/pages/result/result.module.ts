import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../modules/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FormsModule } from '@angular/forms';
import { ResultComponent } from './result.component';


export const ROUTES: Routes = [{ path: '', component: ResultComponent }];

@NgModule({
  declarations: [
    ResultComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    TranslateModule,
    AngularSvgIconModule
  ],
})
export class ResultModule { }
