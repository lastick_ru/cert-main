import { Component, OnInit } from '@angular/core';
import { ICertificate } from '../../types/Entities';
import { TransportService } from '../../services/transport.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.styl']
})
export class ParkingComponent implements OnInit {
  public gift: ICertificate;
  public related: ICertificate[];
  private user_friendly_url = 'parking';
  public logos = [
    // '/assets/img/logo/disney.svg',
    // '/assets/img/logo/smf.svg',
    '/assets/img/logo/cons.png',
    '/assets/img/logo/planetarium.svg',
    '/assets/img/logo/zapomni.svg',
    '/assets/img/logo/park.png',
    '/assets/img/logo/zal.png',
    '/assets/img/logo/cdk.png',
    '/assets/img/logo/moskino.svg',
    '/assets/img/logo/teatrarmii_logo.png',
    '/assets/img/logo/shkola.png',
  ];
  constructor(
    private api: TransportService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.api.getGifts().subscribe(gifts => {
      this.gift = gifts.find(gift => gift.user_friendly_url === this.user_friendly_url);
      // this.related = gifts.filter(gift => gift._uuid !== this.gift._uuid);
    });
  }

}
