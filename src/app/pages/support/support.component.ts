import { Component, OnInit } from '@angular/core';
import { ICertificate } from '../../types/Entities';
import { TransportService } from '../../services/transport.service';
import { ActivatedRoute } from '@angular/router';
import { IAddresseeDataItem } from '../../components/gift/gift.component';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.styl']
})
export class SupportComponent implements OnInit {
  public gift: ICertificate;
  public related: ICertificate[];
  private user_friendly_url = 'spasibodoc';
  public logos = [
    // '/assets/img/logo/disney.svg',
    // '/assets/img/logo/smf.svg',
    {img: '/assets/img/logo/cons.png'},
    {img: '/assets/img/logo/planetarium.svg'},
    {img: '/assets/img/logo/zapomni.svg'},
    {img: '/assets/img/logo/park.png'},
    {img: '/assets/img/logo/zal.png'},
    {img: '/assets/img/logo/cdk.png'},
    {img: '/assets/img/logo/moskino.svg'},
    {img: '/assets/img/logo/teatrarmii_logo.png'},
    {img: '/assets/img/logo/shkola.png'},
  ];
  public first_logos = [
    {img: '/assets/img/support_logo/1.svg'},
    {img: '/assets/img/support_logo/2.svg'},
    {img: '/assets/img/support_logo/3.svg'},
    {img: '/assets/img/support_logo/4.svg'},
    {img: '/assets/img/support_logo/5.svg'},
    {img: '/assets/img/support_logo/6.svg'},
    {img: '/assets/img/support_logo/7.svg'},
    {img: '/assets/img/support_logo/8.svg'},
  ];
  public addresseeData: IAddresseeDataItem[] = [
    {
      name: 'Больнице РАН в Троицке',
      url: 'https://hospital-ran-troitsk.ru/',
      email: 'admin@hospital.troitsk.ru',
      person_name: 'Корицкий Андрей Владимирович',
    },
    {
      name: 'Городской клинической больнице №15 им. О.М.Филатова',
      url: 'http://gkb15.moscow/',
      email: 'gkb15@zdrav.mos.ru',
      person_name: 'Владимир Николаевич Мудрак'
    },
    {
      name: 'Городской клиничеcкой больнице №67 им. Ворохобова',
      email: 'gkb67@zdrav.mos.ru',
    },
    {
      name: 'Государственному научному центру колопроктологии им. Рыжих',
      email: 'info@gnck.ru',
    },
    {
      name: 'Детской клинической больнице им. З.А. Башляевой',
      email: 'dgkb-bashlyaevoy@zdrav.mos.ru',
    },
    {
      name: 'Инфекционной клинической больнице №1',
      email: 'ikb1@zdrav.mos.ru',
    },
    {
      name: 'Инфекционной клинической больнице №2',
      email: 'ikb2@zdrav.mos.ru',
    },
    {
      name: 'Клинической больнице им. Семашко РЖД',
      email: 'ckb2semashko@mail.ru',
    },
    {
      name: 'Клинической больнице №85 ФМБА',
      email: 'info@kb85.ru',
    },
    {
      name: 'Лечебно-реабилитационному центру Минздрава',
      email: 'info@med-rf.ru',
    },
    {
      name: 'Медицинскому центру в Коммунарке',
      email: 'gkb40@zdrav.mos.ru',
    },
    {
      name: 'Научному центру здоровья детей',
      email: 'info@nczd.ru',
    },
    {
      name: 'Национальному медико-хирургическому центру им. Пирогова',
      email: 'info@pirogov-center.ru',
    },
    {
      name: 'НИИ медицины труда им. Измерова',
      email: 'info@irioh.ru',
    },
    {
      name: 'НИИ скорой помощи им. Склифосовского',
      email: 'sklif@zdrav.mos.ru',
    },
    {
      name: 'НМИЦ акушерства, гинекологии и перинатологии им. Кулакова',
      email: 'secretariat@oparina4.ru',
    },
    {
      name: 'НМИЦ кардиологии Минздрава',
      email: 'info@cardioweb.ru',
    },
    {
      name: 'Российскому геронтологическому научно-клиническому центру ',
      email: 'anticorruption@rgnkc.ru',
    },
    {
      name: 'УКБ №1 Сеченовского Университета',
      email: 'rektorat@sechenov.ru',
    },
    {
      name: 'Федеральному бюро медико-социальной экспертизы Минтруда',
      email: 'fbmse@fbmse.ru',
    },
    {
      name: 'Федеральному исследовательскому центру питания биотехнологии и безопасности пищи',
      email: 'mailbox@ion.ru',
    },
    {
      name: 'Федеральному медицинскому биофизическому центру им. Бурназяна',
      email: 'fmbc-fmba@bk.ru',
    },
    {
      name: 'Федеральному научно-клиническому центру ФМБА России',
      email: 'info@fnkc-fmba.ru',
    },
    {
      name: 'Центральной клинической больнице РАН',
      email: 'ckb@ckbran.ru',
    },
    {
      name: 'Центральной клинической больнице РЖД-Медицина',
      email: 'ckb2semashko@mail.ru',
    },
    {
      name: 'Центру высоких медицинских технологий ФМБА',
      email: 'do@kb119.ru',
    },
    {
      name: 'Челюстно-лицевому госпиталю для ветеранов войн',
      email: 'chlg@zdrav.mos.ru',
    },
  ];

  constructor(
    private api: TransportService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.api.getGift(this.user_friendly_url).subscribe(gift => {
      this.gift = gift;
    });
    // this.api.getGifts().subscribe(gifts => {
    //   this.gift = gifts.find(gift => gift.user_friendly_url === this.user_friendly_url);
    // });
  }
}
