import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../modules/shared/shared.module';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { HowComponent } from './how.component';


export const ROUTES: Routes = [{ path: '', component: HowComponent }];

const declarations = [
  HowComponent,
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    SwiperModule
  ],
})
export class HowModule {
}
