import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'wrench-page',
  templateUrl: './wrench-page.component.html',
  styleUrls: ['./wrench-page.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class WrenchPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
