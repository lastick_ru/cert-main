import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';

import {SharedModule} from '../../modules/shared/shared.module';
import {TabsModule} from '../../modules/tabs/tabs.module';
import {TranslateModule} from '@ngx-translate/core';
import {BActivateComponent} from '../../components/b-activate/b-activate.component';
import {FormsModule} from '@angular/forms';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {SwiperModule} from 'ngx-swiper-wrapper';
import {ScrollModule} from '../../modules/scroll/scroll.module';
import {ValidatorsModule} from '../../validators/validators.module';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { GiftModule } from '../../components/gift/gift.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { FooterComponent } from './new/views/footer/footer.component';
import { HeaderComponent } from './new/views/header/header.component';
import { CertificateService } from './services/certificate.service';
import { SocialService } from './services/social.service';
import { SectionItemComponent } from './new/views/section-item/section-item.component';
import { InformationModalComponent } from './new/modals/restaraunt-list-modal/information-modal/information-modal.component';
import { SingleCertComponent } from './cert-single-page.component';
import { CertListComponent } from './new/cert-list/cert-list.component';
import { SectionItemSingleCertComponent } from './new/views/section-item-single-cert/section-item-single-cert.component';


export const ROUTES: Routes = [{ path: '', component: SingleCertComponent }];

const declarations = [
  SingleCertComponent,
  FooterComponent,
  HeaderComponent,
  SectionItemComponent,
  InformationModalComponent,
  CertListComponent,
  SectionItemSingleCertComponent
];

@NgModule({
  declarations,
  imports: [
    NgbModule,
    SlickCarouselModule,
    CommonModule,
    RouterModule.forChild(ROUTES),
    SharedModule,
    FormsModule,
    TabsModule,
    TranslateModule,
    AngularSvgIconModule,
    ValidatorsModule,
    ScrollModule,
    SwiperModule,
    InfiniteScrollModule,
    GiftModule,
  ],
  providers:[
    CertificateService,
    SocialService
  ],
  entryComponents:[
    InformationModalComponent
  ]
})
export class SingleCertPageModule { }
