import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'new-app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {

  @Input() phone: string = "8 495 120 42 64";
  constructor() { }

  ngOnInit() {
  }

}
