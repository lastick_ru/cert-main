import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../modules/shared/shared.module';
import { GiftModule } from '../../components/gift/gift.module';
import { SpecialComponent } from './special.component';


export const ROUTES: Routes = [{ path: '', component: SpecialComponent }];

@NgModule({
  declarations: [
    SpecialComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    GiftModule
  ],
})
export class SpecialModule { }
