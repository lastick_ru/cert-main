import { Component, OnInit } from '@angular/core';
import { ICertificate } from '../../types/Entities';
import { TransportService } from '../../services/transport.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-special',
  templateUrl: './special.component.html',
  styleUrls: ['./special.component.styl']
})
export class SpecialComponent implements OnInit {
  public gift: ICertificate;
  public related: ICertificate[];
  private user_friendly_url = 'corona';
  public logos = [
    // '/assets/img/logo/disney.svg',
    // '/assets/img/logo/smf.svg',
    {img: '/assets/img/logo/cons.png'},
    {img: '/assets/img/logo/planetarium.svg'},
    {img: '/assets/img/logo/zapomni.svg'},
    {img: '/assets/img/logo/park.png'},
    {img: '/assets/img/logo/zal.png'},
    {img: '/assets/img/logo/cdk.png'},
    {img: '/assets/img/logo/moskino.svg'},
    {img: '/assets/img/logo/teatrarmii_logo.png'},
    {img: '/assets/img/logo/shkola.png'},
  ];

  constructor(
    private api: TransportService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.api.getGifts().subscribe(gifts => {
      this.gift = gifts.find(gift => gift.user_friendly_url === this.user_friendly_url);
      // this.related = gifts.filter(gift => gift._uuid !== this.gift._uuid);
    });
  }
}
