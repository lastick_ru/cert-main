import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../modules/shared/shared.module';
import { GiftPageComponent } from './gift-page.component';
import { GiftModule } from '../../components/gift/gift.module';


export const ROUTES: Routes = [{ path: '', component: GiftPageComponent }];

@NgModule({
  declarations: [
    GiftPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    GiftModule
  ],
})
export class GiftPageModule { }
