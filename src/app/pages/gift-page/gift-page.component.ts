import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransportService } from '../../services/transport.service';
import { ICertificate } from '../../types/Entities';

@Component({
  selector: 'app-gift-page',
  templateUrl: './gift-page.component.html',
  styleUrls: ['./gift-page.component.styl']
})
export class GiftPageComponent implements OnInit {
  public gift: ICertificate;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: TransportService,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      console.log(params.uuid)
      this.api.getGift(params.uuid).subscribe(data => {
        this.gift = data;
        console.log(data)

      }, () => {
        this.router.navigate(['']);
      });
    });
  }
}
