import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../modules/shared/shared.module';
import { AboutComponent } from './about.component';
import { SwiperModule } from 'ngx-swiper-wrapper';


export const ROUTES: Routes = [{ path: '', component: AboutComponent }];

const declarations = [
  AboutComponent,
];

@NgModule({
  declarations,
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    SwiperModule
  ],
})
export class AboutModule {
}
