import { Component, OnInit } from '@angular/core';
import { ICertificate } from '../../types/Entities';
import { TransportService } from '../../services/transport.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.styl']
})
export class AboutComponent implements OnInit {
  public gifts: ICertificate[];
  public isDesktop = window.innerWidth > 991;

  constructor(
    private api: TransportService
  ) { }

  ngOnInit() {
    this.api.getGifts().subscribe(gifts => this.gifts = gifts.slice(0, 30));
  }

}
