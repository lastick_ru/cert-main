import {Observable, of, pipe} from 'rxjs';
import {SvgLoader} from 'angular-svg-icon';
import {makeStateKey, StateKey, TransferState} from '@angular/platform-browser';
import {map} from 'rxjs/operators';

const fs = require('fs');
const join = require('path').join;
const parseUrl = require('url').parse;
const baseName = require('path').basename;

export class SvgServerLoader implements SvgLoader {

  constructor(private iconPath: string,
              private transferState: TransferState) {
  }

  getSvg(url: string): Observable<string> {
    const parsedUrl: URL = parseUrl(url);
    const fileNameWithHash = baseName(parsedUrl.pathname);
    // console.log('SVG PATH', parsedUrl);
    // Remove content hashing
    const fileName = fileNameWithHash.replace(/^(.*)(\.[0-9a-f]{16,})(\.svg)$/i, '$1$3');
    const filePath = join(this.iconPath, fileName);
    return of(filePath);
    //   .pipe(
    //   map((path) => {
    //     const svgData: string = fs.readFileSync(path, 'utf8');
    //
    //     // Here we save the translations in the transfer-state
    //     const key: StateKey<number> = makeStateKey<number>('transfer-svg:' + url);
    //     this.transferState.set(key, svgData);
    //     return svgData;
    //   })
    // );
  }
}
