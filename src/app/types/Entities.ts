
import { ITag } from './ITag';
import { ISchedule } from './ISchedule';
import { IEvent } from './IEvent';
import { ILanguages } from "./ILanguages";
import { ICertificateBody } from './req';
import { INav } from '../components/header/header-nav/header-nav.component';

export type TCurrency = 'rur' | 'rub' | 'usd' | 'eur' | 'amd';

export interface IMultiLangField {
  [lang_code: string]: any;
}

export interface IMediaInfo {
  name?: string;
  path?: string;
  size?: number;
  width?: number;
  height?: number;
  palette?: any;
  color?: any;
  type?: string;
}

export type TCertificateOrderType = 'gift' | 'corporate';

export interface ICertificateOrder {
  type: TCertificateOrderType;
  gift?: ICertificate;
  data?: ICertificateBody;
  total_cost?: number;
  agreement?: boolean;
}

export interface IProductTicket extends IProductItem {
  place_uuid: string;
  meta: any;
  is_for_sale: boolean;
  is_for_booking: boolean;
  is_in_cart?: boolean;
}

export interface IServerCartItem {
  cart_uuid: string;
  uuid: string;
  ProductItem: {
    uuid: string;
  };
  product_item?: any;
  error: any;
  meta: any;
  quantity: number;
  price: number;
  PriceValue: {
    uuid: string;
    is_base_price: boolean;
    amount: number;
  };
  AppliedPromos: [
    {
      uuid: string;
      name: IMultiLangField;
    }
  ];

  PriceModifiers: IPriceModifier[];
  total_cost: number;
  expired_at: string;
  is_expired: boolean;
  is_reserved: boolean;
  expiration_countdown?: number;
  currency?: TCurrency;
}

export interface ICartItem {
  applied_promocodes: string[];
  nominal_price?: number;
  uuid?: string;
  local_uuid: string;
  ProductItem?: any;
  PriceValue: IPriceValue;
  PriceModifiers: IPriceModifier[];
  meta?: any;
  total_cost: number;
  service_fee: number;
  quantity: number;
  schedule?: any;
  is_expired: boolean;
  is_reserved: boolean;
  max_count?: number;
  price?: number;
  currency?: TCurrency;
  Schedule?: ISchedule;
  Event?: IEvent;
}

export interface IPromo {
  uuid: string;
  created_at: string;
  updated_at: string;
  deleted_at: any;
  is_active: boolean;
  is_deleted: boolean;
  name: IMultiLangField;
  validity_range: string;
  rules: any;
  meta: any;
  promo_type: string;
  price_modifier_uuid: string;
  allow_overpayment: boolean;
}

export interface IPromocode {
  uuid: string;
  code: string;
  Promo: IPromo;
  promo_uuid: string;
}

export interface IProductItem {
  uuid: string;
  product_item_type: string;
  // quantity: number;
  quantity: any;
  price_values: string[];
  schedule?: any;
  Place?: any;
}

export interface ICertificateView {
  _uuid?: string;
  created_at?: string;
  description?: IMultiLangField;
  is_active?: boolean;
  media?: IMediaInfo;
  name?: IMultiLangField;
  summary?: IMultiLangField;
  translation?: IMultiLangField;
  updated_at?: string;
  uuid_o?: string;
}

export interface ICertificateTab {
  title: IMultiLangField;
  text: IMultiLangField;
}

export interface ICertificateSkinSettings {
  skin?: string;
  vars?: string;
}

export interface ICertificateSeoMeta {
  title?: IMultiLangField;
  description?: IMultiLangField;
  keywords?: IMultiLangField;
}

export interface ICertificateSeo {
  meta: ICertificateSeoMeta | any;
}


export interface ICertificate {
  Tags?: ITag[];
  Views?: ICertificateView[];
  _uuid?: string;
  activation_last_date?: string;
  activation_ttl?: string;
  description?: IMultiLangField;
  background_image?: IMediaInfo;
  image?: IMediaInfo;
  poster?: IMediaInfo;
  price_modifier?: IPriceModifier;
  prices?: number[];
  prices_range?: number[];
  restrictions?: any[];
  skin_settings?: ICertificateSkinSettings;
  is_active?: 1 | 0 | boolean;
  is_multiuse?: 1 | 0 | boolean;
  meta?: any;
  name?: IMultiLangField;
  name_variant?: IMultiLangField;
  summary?: IMultiLangField;
  tabs?: ICertificateTab[];
  tags?: string[];
  uuid_o?: any;
  views?: string[];
  user_friendly_url?: string;
  entity_icons?: IMediaInfo[];
  entity_label?: any;
  seo?: ICertificateSeo;
  Partners?:any[];
}

export interface IPriceModifier {
  created_at?: string;
  deleted_at?: string;
  formatted_value?: string;
  is_active?: boolean;
  is_deleted?: boolean;
  name?: string;
  operation?: string;
  updated_at?: string;
  uuid?: string;
  value?: number;
  value_type?: string;
}

export type TPaymentMethod  = 'common' | '_custom' | 'applepay' | 'gift' | 'gpay';

export interface IPaymentMethod {
  _uuid: string;
  uuid: string;
  type: TPaymentMethod;
  name: IMultiLangField;
  ext_provider_data?: any;
}

export interface IPriceValue {
  uuid: string;
  is_base_price: boolean;
  is_active?: boolean;
  amount: number;
  PriceCategory?: IPriceCategory;
  amount_currency?: TCurrency;
}

export interface IPriceCategory {
  uuid: string;
  name: IMultiLangField;
  description: IMultiLangField;
  is_base_category?: boolean;
}
export type TOrderSuccessStatus = 'done' | 'paid' | 'on_delivery';
export type TOrderErrorStatus = 'refund' | 'cancel' | 'outdated' | 'error';
export type TOrderStatus = 'done' | 'paid' | 'on_delivery' | 'refund' | 'cancel' | 'outdated' | 'error';

export interface IContacts {
  contacts: {
    email: string;
    phone: string;
  };
  payment_method?: string;
  payment_token?: string;
}

export interface IEventsCalendar {
  dates: Object;
  max: string;
  min: string;
}

// export interface IFilterTag {
//   color: string;
//   name: IMultiLangField;
//   type: string;
//   uuid: string;
// }

export interface IAuthMethod {
  app_id: string;
  type: 'facebook' | string;
}

export interface ISettingsRoutes {
  [key: string]: string;
}

export interface ISliderSettings {
  speed?: number;
  loop?: boolean;
  slidesPerView?: number;
  slidesPerGroup?: number;
  autoplay?: {
    delay?: number,
    disableOnInteraction?: boolean;
  };
}

export interface ICertPageSliderSettings {
  first?: ISliderSettings;
  second?: ISliderSettings;
}

export interface ISettingsContacts {
  email: string;
  phone: string;
  time?: string;
}

export interface IWidgetSettings {
  defaultLang?: string;
  additionalLanguages?: ILanguages[];
  buy_text?: IMultiLangField;
  buy_title?: IMultiLangField;
  calendar?: { expanded: boolean; length: number };
  limits?: { tickets_in_order: number };
  routes?: ISettingsRoutes;
  show_back_text?: boolean;
  show_close_frame?: boolean;
  show_rules?: boolean;
  schedules?: { prices: boolean };
  events_hide_time?: boolean;
  authentication?: 'mpl' | string;
  card_auth?: boolean;
  card_conditions?: { url: string; text: string };
  header_promo_logo?: boolean;
  header_logo?: {img: string, svg?: string, url?: string};
  logo_replacement?: boolean;
  event_promo_logo?: boolean;
  orderResult?: { backBtn: boolean };
  cart?: { name: boolean };
  hide_spots?: boolean;
  own_event?: boolean;
  enablePromocodes?: boolean;
  routeSettings?: {index?: {user_friendly_url: string}};
  nav?: INav[];
  hide_routes?: string[];
  cert_page_sliders?: ICertPageSliderSettings;
  noScrollRoutes?: string[];
  contacts?: ISettingsContacts;
  origin_url?: string;
}

export interface ISettings {
  FilterTags?: ITag[];
  allowed_payment_methods?: string[];
  auth_method?: IAuthMethod[];
  gift_offer?: IMultiLangField;
  name?: IMultiLangField;
  offer?: IMultiLangField;
  widget_settings?: IWidgetSettings;
}

export interface ICertificateMetaConfig {
  is_active?: boolean;
}

export interface ICertificateMetaView {
  is_active?: boolean;
}

export interface ICertificateMeta {
  config?: ICertificateMetaConfig;
  view?: ICertificateMetaView;
}
