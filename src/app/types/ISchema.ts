export interface IGeometryPlaceViewObj {
  name?: string;
  path?: string;
  size?: number;
  type?: string;
  color?: number[];
  width?: number;
  height?: number;
  palette?: Array<number[]>;
  uploaded_at?: number;
}

export interface IGeometryPlaceView {
  place_views: { [key: string]: IGeometryPlaceViewObj };
  place_map: { [key: string]: string[] };
}
