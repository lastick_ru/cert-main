import { IMediaInfo, IMultiLangField } from './Entities';

export interface ITag {
  uuid: string;
  _uuid: string;
  type:
    | 'movie_prop'
    | 'genre'
    | 'system'
    | 'advert'
    | 'person'
    | 'festival'
    | '_form'
    | '_custom'
    | '_api';
  name: IMultiLangField;
  short_name?: IMultiLangField;
  grade_title?: IMultiLangField;
  description?: IMultiLangField;
  address?: IMultiLangField;
  contacts?: IMultiLangField;
  poster?: IMediaInfo;
  media_info?: IMediaInfo[];
  is_active?: boolean;
  has_webpage?: boolean;
  website?: string;
  work_hours?: any;
  location?: any;
  tabs?: any;
  _api?: string;
  _index?: boolean;
}
