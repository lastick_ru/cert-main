import {
  ICartItem,
  ICertificate, ICertificateMeta, IMediaInfo, IMultiLangField,
  IPaymentMethod, IPriceModifier,
  IPriceValue,
  IProductTicket,
  IPromocode, TOrderStatus,
} from './Entities';

export interface ICartResponse {
  certificate: ICertificate;
  promocodes: IPromocode[];
  items: ICartItem[];
  total_cost: number;
  payment_method: IPaymentMethod;
}

export interface IAvailableTicketsResponse {
  tickets: IProductTicket[];
  prices: { [price_value_uuid: string]: IPriceValue };
}

export interface ICertificateBodyItem {
  cert_config?: string;
  cert_view?: string;
  count?: number;
  price?: number;
  addressee?: ICertificateBodyAddressee;
  is_gift?: boolean;
  is_corporate?: boolean;
  sender_name?: string;
  logo?: string;
}

export interface ICertificateBodyClient {
  email?: string;
  phone?: string;
  name?: string;
}

export interface ICertificateBodyAddressee {
  email?: string;
  phone?: string;
  message?: string;
  name?: string;
}

export interface ICertificateBody {
  items?: ICertificateBodyItem[];
  client?: ICertificateBodyClient;
  payment_method?: string;
  payment_token?: string;
  lang?: string;
}

export interface ICertificateRes {
  _uuid: string;
  payment: {
    payment_url: string;
    status: 'confirm' | 'success';
  };
  status: 'complete';
  total_amount: string | number;
  total_count: number;
}

export interface IShowcaseOrderItem {
  activation_range?: string;
  amount?: number;
  media?: IMediaInfo;
  name?: IMultiLangField;
  number?: string;
  pdf?: string;
}

export interface IShowcaseOrder {
  _uuid?: string;
  code?: string;
  created_at?: string;
  items?: IShowcaseOrderItem[];
  payment?: {status: string};
  status?: TOrderStatus;
  total_amount?: string;
  total_count?: number;
}

export interface IAppealData {
  type: 'partner' | 'client';
  name: string;
  phone: string;
  email: string;
  message: string;
}

export interface ICertificateActivated {
  PriceModifier?: IPriceModifier;
  allow_overpayment?: boolean;
  created_at?: string;
  deleted_at?: null;
  ext_id?: string;
  ext_number?: string;
  initial_price_modifier_uuid?: string;
  is_active?: boolean;
  is_deleted?: boolean;
  is_multi?: boolean;
  price_modifier_uuid?: string;
  promo_type?: string;
  rules?: any;
  updated_at?: string;
  uuid?: string;
  validity_range?: string;
  meta?: ICertificateMeta;
  name?: IMultiLangField;
}
