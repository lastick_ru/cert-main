import { IMediaInfo, IMultiLangField } from './Entities';

export interface ISpot {
  uuid: string;
  name: IMultiLangField;
  short_name: IMultiLangField;
  description: IMultiLangField;
  address: IMultiLangField;
  contacts: IMultiLangField;
  website: string;
  title: string;
  work_hours: any;
  location: any;
  media_info: [];
  poster: IMediaInfo;
  tabs: any;
}
