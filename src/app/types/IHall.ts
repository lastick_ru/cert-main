import { IDuration } from './IDuration';
import { IMediaInfo, IMultiLangField } from './Entities';

export interface IHall {
  uuid: string;
  name: IMultiLangField;
  Spot: any;
  LinkedSchedules: Array<any>;
  SeasonEvent: any;
  has_geometry: boolean;
  is_for_sale: boolean;
  season_event_uuid: string;
  is_season_head: boolean;
  is_season_part: boolean;
  season_schedules: any;
  begin: string;
  begin_time: string;
  end_time: string;
  duration: IDuration;
  PriceValues: any;
  MinPrice?: any;
  MaxPrice?: any;
  media_info: IMediaInfo[];
}
