import { IHall } from './IHall';
import { ISpot } from './ISpot';
import { IEvent } from './IEvent';
import { ITag } from './ITag';
import { IPriceValue } from './Entities';

export interface ISchedule {
  uuid: string;
  Hall: IHall;
  Spot: ISpot;
  Event?: IEvent;
  Tags?: ITag[];
  has_geometry: boolean;
  is_for_sale: boolean;
  season_event_uuid: string;
  is_season_head: boolean;
  is_season_part: boolean;
  season_schedules: any;
  begin: string;
  end: string;
  begin_time: string;
  end_time: string;
  duration: any;
  PriceValues: IPriceValue[];
  PriceCategories: any;
  SeasonEvent: any;
  LinkedSchedules: ISchedule[];
}
