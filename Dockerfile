# latest official node image
FROM nginx:stable-alpine

RUN ln -sf /dev/stdout /var/log/nginx/access.log \ 	&& ln -sf /dev/stderr /var/log/nginx/error.log

WORKDIR /usr/data

COPY ["./nginx-main.conf", "/etc/nginx/nginx.conf"]
CMD ["nginx-debug", "-g", "daemon off;"]

COPY ["./nginx.default.conf", "/etc/nginx/conf.d/default.conf"]
COPY ["./public", "./"]

