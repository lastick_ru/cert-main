(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-faq-faq-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/faq/faq.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/faq/faq.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-layout>\r\n  <div class=\"page\">\r\n    <div class=\"page__head page__head_mb-2\">\r\n      <div class=\"container container_gap-1\">\r\n        <h1 class=\"page__title\">\r\n          Частые вопросы<br>\r\n          и ответы\r\n        </h1>\r\n      </div>\r\n    </div>\r\n    <div class=\"page__body\">\r\n      <div class=\"how\">\r\n        <div class=\"container container_gap-1\">\r\n          <ol class=\"how__list\">\r\n            <li class=\"how__item\">\r\n              <div class=\"how__item-wrap\">\r\n                <div class=\"how__item-head\">\r\n                  <h4 class=\"how__item-title\">\r\n                    Что мне подарили?\r\n                  </h4>\r\n                  <p class=\"how__item-text\">\r\n                    Вам подарили подарочный сертификат, который вы можете обменять на билеты в лучшие учреждения культуры города Екатеринбурга.\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li class=\"how__item\">\r\n              <div class=\"how__item-wrap\">\r\n                <div class=\"how__item-head\">\r\n                  <h4 class=\"how__item-title\">\r\n                    Как этим пользоваться?\r\n                  </h4>\r\n                </div>\r\n                <div class=\"how__item-body\">\r\n                  <p class=\"how__item-text\">\r\n                    1. Активируйте сертификат на <a href=\"/\" routerLink=\"/\" fragment=\"activate\">странице</a> акции.<br>\r\n                    2. Введите номер сертификата для просмотра доступных мероприятий и участников акции.<br>\r\n                    3. Выберите события, даты и учреждения на которые хотите пойти. <strong>Но суммарно не более 6 билетов.</strong><br>\r\n                    4. Электронные билеты будут направлены на указанную вами почту.<br>\r\n                    5. Сертификат имеет ограниченный срок действия до 31.12.2020\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li class=\"how__item\">\r\n              <div class=\"how__item-wrap\">\r\n                <div class=\"how__item-head\">\r\n                  <h4 class=\"how__item-title\">\r\n                    Где можно воспользоваться сертификатом?\r\n                  </h4>\r\n                  <p class=\"how__item-text\">\r\n                    Сертификатом можно воспользоваться только на странице <a href=\"/\" routerLink=\"/\">акции</a> в указанных учреждениях культуры. Вход на мероприятия строго по электронным билетам.\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li class=\"how__item\">\r\n              <div class=\"how__item-wrap\">\r\n                <div class=\"how__item-head\">\r\n                  <h4 class=\"how__item-title\">\r\n                    Что делать, если я потерял сертификат или билет?\r\n                  </h4>\r\n                  <p class=\"how__item-text\">\r\n                    Обратитесь в нашу службу поддержки по номеру <a href=\"tel: +73433124343\">8 343 312 43 43</a> или на почту <a href=\"mailto: spasibodoc@ycenter.ru\">spasibodoc@ycenter.ru</a>\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li class=\"how__item\">\r\n              <div class=\"how__item-wrap\">\r\n                <div class=\"how__item-head\">\r\n                  <h4 class=\"how__item-title\">\r\n                    Что делать, если я не смогу пойти на мероприятие?\r\n                  </h4>\r\n                  <p class=\"how__item-text\">\r\n                    Билет возвращается только в случае отмены/переноса даты мероприятия.\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li class=\"how__item\">\r\n              <div class=\"how__item-wrap\">\r\n                <div class=\"how__item-head\">\r\n                  <h4 class=\"how__item-title\">\r\n                    Я хочу купить такой сертификат в подарок, как это сделать?\r\n                  </h4>\r\n                  <p class=\"how__item-text\">\r\n                    Вы можете зайти на сайт организатора акции <a href=\"https://zapomni.gift\" target=\"_blank\">zapomni.gift</a> и выбрать на нем подарочный сертификат в учреждение культуры.\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n            <li class=\"how__item\">\r\n              <div class=\"how__item-wrap\">\r\n                <div class=\"how__item-head\">\r\n                  <h4 class=\"how__item-title\">\r\n                    У меня несколько сертификатов. Могу ли я подарить их?\r\n                  </h4>\r\n                  <p class=\"how__item-text\">\r\n                    Конечно, можете!\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </li>\r\n          </ol>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</app-layout>\r\n");

/***/ }),

/***/ "./src/app/pages/faq/faq.component.styl":
/*!**********************************************!*\
  !*** ./src/app/pages/faq/faq.component.styl ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".page__head {\n  margin-bottom: 30px;\n}\n.page__head_mb-2 {\n  margin-bottom: 45px;\n}\n.page__title {\n  font-size: 40px;\n  font-weight: 600;\n}\n.page__related {\n  margin-bottom: 58px;\n}\n.page__related-slide {\n  min-width: 98px;\n}\n.page__footer {\n  margin-top: 50px;\n}\n@media all and (min-width: 992px) {\n  .page__head {\n    margin-bottom: 45px;\n  }\n  .page__head_mb-2 {\n    margin-bottom: 60px;\n  }\n  .page__title {\n    font-size: 90px;\n  }\n  .page__related-slide {\n    min-width: 221px;\n  }\n}\n.how {\n  font-size: 16px;\n  line-height: 1.3;\n}\n.how a {\n  color: #bda776;\n}\n.how strong {\n  font-weight: 500;\n  color: #bda776;\n}\n.how__list {\n  counter-reset: item;\n}\n.how__item {\n  position: relative;\n  display: flex;\n  margin-bottom: 58px;\n}\n.how__item:last-child {\n  margin-bottom: 0;\n}\n.how__item:before {\n  content: counter(item);\n  counter-increment: item;\n  flex: 0 0 30px;\n  font-weight: 500;\n}\n.how__item-head {\n  margin-bottom: 12px;\n}\n.how__item-title {\n  font-weight: 500;\n}\n.how__item-img {\n  display: block;\n  max-width: 100%;\n  height: auto;\n}\n@media all and (min-width: 992px) {\n  .how {\n    font-size: 24px;\n  }\n  .how__list__item:before {\n    flex: 0 0 70px;\n  }\n}\n/*# sourceMappingURL=src/app/pages/faq/faq.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZmFxL3NyYy9zdHlsZXMvYmxvY2tzL3BhZ2Uuc3R5bCIsInNyYy9hcHAvcGFnZXMvZmFxL2ZhcS5jb21wb25lbnQuc3R5bCIsInNyYy9hcHAvcGFnZXMvZmFxL3NyYy9zdHlsZXMvaGVscGVycy5zdHlsIiwic3JjL2FwcC9wYWdlcy9mYXEvc3JjL3N0eWxlcy9ibG9ja3MvaG93LnN0eWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxtQkFBZTtBQ0FuQjtBRENJO0VBQ0UsbUJBQWU7QUNDckI7QURBRTtFQUNFLGVBQVc7RUFDWCxnQkFBYTtBQ0VqQjtBREFFO0VBQ0UsbUJBQWU7QUNFbkI7QURBSTtFQUNFLGVBQVU7QUNFaEI7QURBRTtFQUNFLGdCQUFZO0FDRWhCO0FDZGlDO0VGZ0I3QjtJQUNFLG1CQUFlO0VDQ25CO0VEQUk7SUFDRSxtQkFBZTtFQ0VyQjtFRERFO0lBQ0UsZUFBVztFQ0dmO0VEQUk7SUFDQyxnQkFBVTtFQ0VmO0FBQ0Y7QUVoQ0E7RUFDRSxlQUFXO0VBQ1gsZ0JBQWE7QUZrQ2Y7QUVoQ0U7RUFDRSxjQUFPO0FGa0NYO0FFaENFO0VBQ0UsZ0JBQWE7RUFDYixjQUFPO0FGa0NYO0FFaENFO0VBQ0UsbUJBQWU7QUZrQ25CO0FFakNFO0VBQ0Usa0JBQVU7RUFDVixhQUFTO0VBQ1QsbUJBQWU7QUZtQ25CO0FFbENJO0VBQ0UsZ0JBQWU7QUZvQ3JCO0FFbkNJO0VBQ0Usc0JBQXFCO0VBQ3JCLHVCQUFtQjtFQUNuQixjQUFNO0VBQ04sZ0JBQWE7QUZxQ25CO0FFbkNJO0VBQ0UsbUJBQWU7QUZxQ3JCO0FFcENJO0VBQ0UsZ0JBQWE7QUZzQ25CO0FFckNJO0VBQ0UsY0FBUztFQUNULGVBQVU7RUFDVixZQUFRO0FGdUNkO0FDbkVpQztFQytCL0I7SUFDRSxlQUFXO0VGdUNiO0VFcENNO0lBQ0UsY0FBTTtFRnNDZDtBQUNGO0FBQ0EsOERBQThEIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZmFxL2ZhcS5jb21wb25lbnQuc3R5bCIsInNvdXJjZXNDb250ZW50IjpbIi5wYWdlXHJcbiAgJl9faGVhZFxyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweFxyXG4gICAgJl9tYi0yXHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDQ1cHhcclxuICAmX190aXRsZVxyXG4gICAgZm9udC1zaXplOiA0MHB4XHJcbiAgICBmb250LXdlaWdodDogNjAwXHJcblxyXG4gICZfX3JlbGF0ZWRcclxuICAgIG1hcmdpbi1ib3R0b206IDU4cHhcclxuXHJcbiAgICAmLXNsaWRlXHJcbiAgICAgIG1pbi13aWR0aCA5OHB4XHJcblxyXG4gICZfX2Zvb3RlclxyXG4gICAgbWFyZ2luLXRvcDogNTBweFxyXG5cclxuK2Rlc2t0b3AoKVxyXG4gIC5wYWdlXHJcbiAgICAmX19oZWFkXHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDQ1cHhcclxuICAgICAgJl9tYi0yXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNjBweFxyXG4gICAgJl9fdGl0bGVcclxuICAgICAgZm9udC1zaXplOiA5MHB4XHJcblxyXG4gICAgJl9fcmVsYXRlZFxyXG4gICAgICAmLXNsaWRlXHJcbiAgICAgICBtaW4td2lkdGggMjIxcHhcclxuIiwiLnBhZ2VfX2hlYWQge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuLnBhZ2VfX2hlYWRfbWItMiB7XG4gIG1hcmdpbi1ib3R0b206IDQ1cHg7XG59XG4ucGFnZV9fdGl0bGUge1xuICBmb250LXNpemU6IDQwcHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ucGFnZV9fcmVsYXRlZCB7XG4gIG1hcmdpbi1ib3R0b206IDU4cHg7XG59XG4ucGFnZV9fcmVsYXRlZC1zbGlkZSB7XG4gIG1pbi13aWR0aDogOThweDtcbn1cbi5wYWdlX19mb290ZXIge1xuICBtYXJnaW4tdG9wOiA1MHB4O1xufVxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLnBhZ2VfX2hlYWQge1xuICAgIG1hcmdpbi1ib3R0b206IDQ1cHg7XG4gIH1cbiAgLnBhZ2VfX2hlYWRfbWItMiB7XG4gICAgbWFyZ2luLWJvdHRvbTogNjBweDtcbiAgfVxuICAucGFnZV9fdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogOTBweDtcbiAgfVxuICAucGFnZV9fcmVsYXRlZC1zbGlkZSB7XG4gICAgbWluLXdpZHRoOiAyMjFweDtcbiAgfVxufVxuLmhvdyB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbn1cbi5ob3cgYSB7XG4gIGNvbG9yOiAjYmRhNzc2O1xufVxuLmhvdyBzdHJvbmcge1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogI2JkYTc3Njtcbn1cbi5ob3dfX2xpc3Qge1xuICBjb3VudGVyLXJlc2V0OiBpdGVtO1xufVxuLmhvd19faXRlbSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luLWJvdHRvbTogNThweDtcbn1cbi5ob3dfX2l0ZW06bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4uaG93X19pdGVtOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IGNvdW50ZXIoaXRlbSk7XG4gIGNvdW50ZXItaW5jcmVtZW50OiBpdGVtO1xuICBmbGV4OiAwIDAgMzBweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbi5ob3dfX2l0ZW0taGVhZCB7XG4gIG1hcmdpbi1ib3R0b206IDEycHg7XG59XG4uaG93X19pdGVtLXRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbi5ob3dfX2l0ZW0taW1nIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmhvdyB7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICB9XG4gIC5ob3dfX2xpc3RfX2l0ZW06YmVmb3JlIHtcbiAgICBmbGV4OiAwIDAgNzBweDtcbiAgfVxufVxuLyojIHNvdXJjZU1hcHBpbmdVUkw9c3JjL2FwcC9wYWdlcy9mYXEvZmFxLmNvbXBvbmVudC5jc3MubWFwICovIiwibW9iaWxlKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiAkd19tKVxyXG4gICAge2Jsb2NrfVxyXG5kZXNrdG9wKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19kKVxyXG4gICAge2Jsb2NrfVxyXG5taW5faW5pdF9kKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19pbml0X2QpXHJcbiAgICB7YmxvY2t9XHJcbnNtYWxsKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19zbWFsbClcclxuICAgIHtibG9ja31cclxuc21hbGxfMigpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfc21hbGxfMilcclxuICAgIHtibG9ja31cclxubWVkaXVtKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19tZWRpdW0pXHJcbiAgICB7YmxvY2t9XHJcbmxhcmdlKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19sYXJnZSlcclxuICAgIHtibG9ja31cclxuZXh0cmFfbGFyZ2UoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X2V4dHJhX2xhcmdlKVxyXG4gICAge2Jsb2NrfVxyXG5tYXhfdygpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfbWF4X3BhZ2Vfd2lkdGgpXHJcbiAgICB7YmxvY2t9XHJcblxyXG5oX21lZGl1bSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi1oZWlnaHQ6ICRoX21lZGl1bSlcclxuICAgIHtibG9ja31cclxuXHJcbnBsYWNlaG9sZGVyKClcclxuICAmOjpwbGFjZWhvbGRlclxyXG4gICAge2Jsb2NrfVxyXG5cclxuYXV0b2NvbXBsZXRlKClcclxuICAmOi13ZWJraXQtYXV0b2ZpbGwsXHJcbiAgJjotd2Via2l0LWF1dG9maWxsOmhvdmVyLFxyXG4gICY6LXdlYmtpdC1hdXRvZmlsbDpmb2N1cyxcclxuICAmOi13ZWJraXQtYXV0b2ZpbGw6YWN0aXZlLFxyXG4gICY6LWludGVybmFsLWF1dG9maWxsLXByZXZpZXdlZCxcclxuICAmOi1pbnRlcm5hbC1hdXRvZmlsbC1zZWxlY3RlZFxyXG4gICAge2Jsb2NrfVxyXG5cclxuX2NhbGMoJHBlciwgJHZhbClcclxuICByZXR1cm4gXCJjYWxjKCVzIC0gJXMpXCIgJSAoJHBlciAkdmFsKVxyXG4iLCIuaG93XHJcbiAgZm9udC1zaXplOiAxNnB4XHJcbiAgbGluZS1oZWlnaHQ6IDEuM1xyXG5cclxuICBhXHJcbiAgICBjb2xvcjogI0JEQTc3NjtcclxuXHJcbiAgc3Ryb25nXHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICNiZGE3NzY7XHJcblxyXG4gICZfX2xpc3RcclxuICAgIGNvdW50ZXItcmVzZXQ6IGl0ZW07XHJcbiAgJl9faXRlbVxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hcmdpbi1ib3R0b206IDU4cHhcclxuICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwXHJcbiAgICAmOmJlZm9yZVxyXG4gICAgICBjb250ZW50OiBjb3VudGVyKGl0ZW0pO1xyXG4gICAgICBjb3VudGVyLWluY3JlbWVudDogaXRlbTtcclxuICAgICAgZmxleDogMCAwIDMwcHhcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMFxyXG5cclxuICAgICYtaGVhZFxyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMnB4XHJcbiAgICAmLXRpdGxlXHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDBcclxuICAgICYtaW1nXHJcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICBtYXgtd2lkdGggMTAwJVxyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcblxyXG4rZGVza3RvcCgpXHJcbiAgLmhvd1xyXG4gICAgZm9udC1zaXplOiAyNHB4XHJcbiAgICAmX19saXN0XHJcbiAgICAgICZfX2l0ZW1cclxuICAgICAgICAmOmJlZm9yZVxyXG4gICAgICAgICAgZmxleDogMCAwIDcwcHhcclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/faq/faq.component.ts":
/*!********************************************!*\
  !*** ./src/app/pages/faq/faq.component.ts ***!
  \********************************************/
/*! exports provided: FaqComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqComponent", function() { return FaqComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FaqComponent = /** @class */ (function () {
    function FaqComponent() {
    }
    FaqComponent.prototype.ngOnInit = function () {
    };
    FaqComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-how',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./faq.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/faq/faq.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./faq.component.styl */ "./src/app/pages/faq/faq.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FaqComponent);
    return FaqComponent;
}());



/***/ }),

/***/ "./src/app/pages/faq/faq.module.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/faq/faq.module.ts ***!
  \*****************************************/
/*! exports provided: ROUTES, FaqModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqModule", function() { return FaqModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-swiper-wrapper */ "./node_modules/ngx-swiper-wrapper/dist/ngx-swiper-wrapper.es5.js");
/* harmony import */ var _faq_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./faq.component */ "./src/app/pages/faq/faq.component.ts");







var ROUTES = [{ path: '', component: _faq_component__WEBPACK_IMPORTED_MODULE_6__["FaqComponent"] }];
var declarations = [
    _faq_component__WEBPACK_IMPORTED_MODULE_6__["FaqComponent"],
];
var FaqModule = /** @class */ (function () {
    function FaqModule() {
    }
    FaqModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: declarations,
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES),
                ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_5__["SwiperModule"]
            ],
        })
    ], FaqModule);
    return FaqModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-faq-faq-module.js.map