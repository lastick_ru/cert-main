(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-result-result-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/result/result.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/result/result.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"order order_page\">\r\n  <section class=\"section section_theme-1 section_sz-2 section_bg-2\">\r\n    <app-b-result\r\n      [title]=\"('Success.Thx' | translate) + '<br/>' + ('Success.SendStatic' | translate)\"\r\n      [sub_title]=\"'Success.ThxText' | translate\"\r\n      [status]=\"status\"\r\n    >\r\n    </app-b-result>\r\n  </section>\r\n</div>\r\n");

/***/ }),

/***/ "./src/app/pages/result/result.component.styl":
/*!****************************************************!*\
  !*** ./src/app/pages/result/result.component.styl ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".summary__head {\n  margin-bottom: 20px;\n}\n.summary__head svg-icon > svg {\n  width: 120px;\n  height: auto;\n}\n.summary__body-section {\n  margin-bottom: 28px;\n}\n.summary__body-section:last-child {\n  margin-bottom: 0;\n}\n.summary__form-row {\n  margin-bottom: 16px;\n}\n.summary__form-row:last-child {\n  margin-bottom: 0;\n}\n.summary__form-footer {\n  margin-top: 20px;\n}\n.summary__form-footer-row {\n  margin-bottom: 15px;\n}\n.summary__form-footer-row:last-child {\n  margin-bottom: 0;\n}\n.summary__form-buttons {\n  margin-top: 23px;\n  padding-top: 23px;\n  border-top: 1px solid $c-hr-1;\n}\n.summary__form-buttons-row {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 -8px -8px;\n}\n.summary__form-buttons-item {\n  display: flex;\n  flex: 1;\n  margin: 0 8px 8px;\n}\n.summary__form-buttons-item * {\n  display: flex;\n  width: 100%;\n}\n.summary__result-head {\n  margin-bottom: 20px;\n}\n.summary__result-head-row {\n  margin-bottom: 14px;\n}\n.summary__result-head-row:last-child {\n  margin-bottom: 0;\n}\n.summary__result-footer {\n  margin-top: 18px;\n}\n.summary__title {\n  font-size: 24px;\n  font-weight: 500;\n  line-height: 1.3;\n}\n.order_page {\n  min-height: 100vh;\n  padding: 96px 0 55px;\n  background-color: $c-section-1-theme-1-bg;\n}\n/*# sourceMappingURL=src/app/pages/result/result.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVzdWx0L3NyYy9zdHlsZXMvYmxvY2tzL3N1bW1hcnkuc3R5bCIsInNyYy9hcHAvcGFnZXMvcmVzdWx0L3Jlc3VsdC5jb21wb25lbnQuc3R5bCIsInNyYy9hcHAvcGFnZXMvcmVzdWx0L3NyYy9zdHlsZXMvYmxvY2tzL29yZGVyLnN0eWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxtQkFBZTtBQ0FuQjtBRENJO0VBQ0UsWUFBTztFQUNQLFlBQVE7QUNDZDtBREVJO0VBQ0UsbUJBQWU7QUNBckI7QURDTTtFQUNFLGdCQUFlO0FDQ3ZCO0FEQ0k7RUFDRSxtQkFBZTtBQ0NyQjtBREFNO0VBQ0UsZ0JBQWU7QUNFdkI7QURESTtFQUNFLGdCQUFZO0FDR2xCO0FERk07RUFDRSxtQkFBZTtBQ0l2QjtBREhRO0VBQ0UsZ0JBQWU7QUNLekI7QURKSTtFQUNFLGdCQUFZO0VBQ1osaUJBQWE7RUFDYiw2QkFBWTtBQ01sQjtBRExNO0VBQ0UsYUFBUztFQUNULGVBQVU7RUFDVixtQkFBUTtBQ09oQjtBRE5NO0VBQ0UsYUFBUztFQUNULE9BQU07RUFDTixpQkFBUTtBQ1FoQjtBRFBRO0VBQ0UsYUFBUztFQUNULFdBQU87QUNTakI7QURQSTtFQUNFLG1CQUFlO0FDU3JCO0FEUk07RUFDRSxtQkFBZTtBQ1V2QjtBRFRRO0VBQ0UsZ0JBQWU7QUNXekI7QURWSTtFQUNFLGdCQUFZO0FDWWxCO0FEWEU7RUFDRSxlQUFXO0VBQ1gsZ0JBQWE7RUFDYixnQkFBYTtBQ2FqQjtBQzlERTtFQUNFLGlCQUFXO0VBQ1gsb0JBQVM7RUFDVCx5Q0FBa0I7QURnRXRCO0FBQ0Esb0VBQW9FIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcmVzdWx0L3Jlc3VsdC5jb21wb25lbnQuc3R5bCIsInNvdXJjZXNDb250ZW50IjpbIi5zdW1tYXJ5XHJcbiAgJl9faGVhZFxyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweFxyXG4gICAgc3ZnLWljb24gPiBzdmdcclxuICAgICAgd2lkdGg6IDEyMHB4XHJcbiAgICAgIGhlaWdodDogYXV0bztcclxuXHJcbiAgJl9fYm9keVxyXG4gICAgJi1zZWN0aW9uXHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDI4cHhcclxuICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICZfX2Zvcm1cclxuICAgICYtcm93XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDE2cHhcclxuICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICAgJi1mb290ZXJcclxuICAgICAgbWFyZ2luLXRvcDogMjBweFxyXG4gICAgICAmLXJvd1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHhcclxuICAgICAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuICAgICYtYnV0dG9uc1xyXG4gICAgICBtYXJnaW4tdG9wOiAyM3B4XHJcbiAgICAgIHBhZGRpbmctdG9wOiAyM3B4XHJcbiAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAkYy1oci0xO1xyXG4gICAgICAmLXJvd1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC13cmFwIHdyYXBcclxuICAgICAgICBtYXJnaW46IDAgLThweCAtOHB4XHJcbiAgICAgICYtaXRlbVxyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleDogMVxyXG4gICAgICAgIG1hcmdpbjogMCA4cHggOHB4XHJcbiAgICAgICAgJiAqXHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCVcclxuICAmX19yZXN1bHRcclxuICAgICYtaGVhZFxyXG4gICAgICBtYXJnaW4tYm90dG9tOiAyMHB4XHJcbiAgICAgICYtcm93XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTRweFxyXG4gICAgICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICAgJi1mb290ZXJcclxuICAgICAgbWFyZ2luLXRvcDogMThweFxyXG4gICZfX3RpdGxlXHJcbiAgICBmb250LXNpemU6IDI0cHhcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDBcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjNcclxuIiwiLnN1bW1hcnlfX2hlYWQge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuLnN1bW1hcnlfX2hlYWQgc3ZnLWljb24gPiBzdmcge1xuICB3aWR0aDogMTIwcHg7XG4gIGhlaWdodDogYXV0bztcbn1cbi5zdW1tYXJ5X19ib2R5LXNlY3Rpb24ge1xuICBtYXJnaW4tYm90dG9tOiAyOHB4O1xufVxuLnN1bW1hcnlfX2JvZHktc2VjdGlvbjpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5zdW1tYXJ5X19mb3JtLXJvdyB7XG4gIG1hcmdpbi1ib3R0b206IDE2cHg7XG59XG4uc3VtbWFyeV9fZm9ybS1yb3c6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4uc3VtbWFyeV9fZm9ybS1mb290ZXIge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLnN1bW1hcnlfX2Zvcm0tZm9vdGVyLXJvdyB7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG4uc3VtbWFyeV9fZm9ybS1mb290ZXItcm93Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLnN1bW1hcnlfX2Zvcm0tYnV0dG9ucyB7XG4gIG1hcmdpbi10b3A6IDIzcHg7XG4gIHBhZGRpbmctdG9wOiAyM3B4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgJGMtaHItMTtcbn1cbi5zdW1tYXJ5X19mb3JtLWJ1dHRvbnMtcm93IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBtYXJnaW46IDAgLThweCAtOHB4O1xufVxuLnN1bW1hcnlfX2Zvcm0tYnV0dG9ucy1pdGVtIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleDogMTtcbiAgbWFyZ2luOiAwIDhweCA4cHg7XG59XG4uc3VtbWFyeV9fZm9ybS1idXR0b25zLWl0ZW0gKiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnN1bW1hcnlfX3Jlc3VsdC1oZWFkIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5zdW1tYXJ5X19yZXN1bHQtaGVhZC1yb3cge1xuICBtYXJnaW4tYm90dG9tOiAxNHB4O1xufVxuLnN1bW1hcnlfX3Jlc3VsdC1oZWFkLXJvdzpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5zdW1tYXJ5X19yZXN1bHQtZm9vdGVyIHtcbiAgbWFyZ2luLXRvcDogMThweDtcbn1cbi5zdW1tYXJ5X190aXRsZSB7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbn1cbi5vcmRlcl9wYWdlIHtcbiAgbWluLWhlaWdodDogMTAwdmg7XG4gIHBhZGRpbmc6IDk2cHggMCA1NXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAkYy1zZWN0aW9uLTEtdGhlbWUtMS1iZztcbn1cbi8qIyBzb3VyY2VNYXBwaW5nVVJMPXNyYy9hcHAvcGFnZXMvcmVzdWx0L3Jlc3VsdC5jb21wb25lbnQuY3NzLm1hcCAqLyIsIi5vcmRlclxyXG4gICZfcGFnZVxyXG4gICAgbWluLWhlaWdodCAxMDB2aFxyXG4gICAgcGFkZGluZzogOTZweCAwIDU1cHhcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjLXNlY3Rpb24tMS10aGVtZS0xLWJnO1xyXG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/result/result.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/result/result.component.ts ***!
  \**************************************************/
/*! exports provided: ResultComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultComponent", function() { return ResultComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_global_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/global.service */ "./src/app/services/global.service.ts");
/* harmony import */ var _services_metric_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/metric.service */ "./src/app/services/metric.service.ts");
/* harmony import */ var _services_platform_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/platform.service */ "./src/app/services/platform.service.ts");







var ResultComponent = /** @class */ (function () {
    function ResultComponent(route, 
    // private router: Router,
    api, global, metric, platform) {
        this.route = route;
        this.api = api;
        this.global = global;
        this.metric = metric;
        this.platform = platform;
        this.timer = null;
        this.timeout = null;
        this.successStatuses = ['done', 'paid', 'on_delivery'];
        this.errorStatuses = ['refund', 'cancel', 'outdated', 'error'];
        this.finalStatuses = this.successStatuses.concat(this.errorStatuses);
    }
    ResultComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.platform.isBrowser) {
            this.route.params.subscribe(function (data) {
                if (data && data.order_uuid) {
                    _this.order_uuid = data.order_uuid;
                    _this.checkOrder();
                    _this.timer = setInterval(function () {
                        _this.checkOrder();
                    }, 2000);
                    _this.timeout = setTimeout(function () {
                        clearInterval(_this.timer);
                    }, 1000 * 60 * 20);
                }
            });
        }
    };
    ResultComponent.prototype.isValidStatus = function () {
        return this.finalStatuses.indexOf(this.status) >= 0;
    };
    ResultComponent.prototype.isSuccessStatus = function () {
        return this.successStatuses.indexOf(this.status) >= 0;
    };
    ResultComponent.prototype.isErrorStatus = function () {
        return this.errorStatuses.indexOf(this.status) >= 0;
    };
    ResultComponent.prototype.checkStatus = function () {
        var isValidStatus = this.isValidStatus();
        if (isValidStatus) {
            if (this.isSuccessStatus() && !this.items) {
                return false;
            }
            clearInterval(this.timer);
            clearTimeout(this.timeout);
        }
        return isValidStatus;
    };
    ResultComponent.prototype.checkOrder = function () {
        var _this = this;
        if (this.checkStatus())
            return;
        setTimeout(function () {
            _this.api
                .showcaseOrder(_this.order_uuid)
                .toPromise()
                .then(function (order) {
                _this.status = order.status;
                _this.data = order;
                _this.items = order.items;
                if (_this.checkStatus() && _this.isSuccessStatus() && order.items) {
                    var total_cost = order.items.reduce(function (acc, item) {
                        return acc += item.amount / 100;
                    }, 0);
                    _this.metric.ecommmerceGtagPurchase({
                        transaction_id: order.code,
                        value: total_cost
                    }, { showcase: order.items });
                    _this.metric.ecommmerceYandexPurchase({
                        transaction_id: order.code,
                        value: total_cost
                    }, { showcase: order.items });
                    _this.metric.trackFb('Purchase', { value: total_cost });
                }
            }, function (err) {
                _this.status = 'error';
                _this.checkStatus();
            });
        }, 1000);
    };
    ResultComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"] },
        { type: _services_global_service__WEBPACK_IMPORTED_MODULE_4__["GlobalService"] },
        { type: _services_metric_service__WEBPACK_IMPORTED_MODULE_5__["MetricService"] },
        { type: _services_platform_service__WEBPACK_IMPORTED_MODULE_6__["PlatformService"] }
    ]; };
    ResultComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-result',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./result.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/result/result.component.html")).default,
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./result.component.styl */ "./src/app/pages/result/result.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"],
            _services_global_service__WEBPACK_IMPORTED_MODULE_4__["GlobalService"],
            _services_metric_service__WEBPACK_IMPORTED_MODULE_5__["MetricService"],
            _services_platform_service__WEBPACK_IMPORTED_MODULE_6__["PlatformService"]])
    ], ResultComponent);
    return ResultComponent;
}());



/***/ }),

/***/ "./src/app/pages/result/result.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/result/result.module.ts ***!
  \***********************************************/
/*! exports provided: ROUTES, ResultModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultModule", function() { return ResultModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var angular_svg_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-svg-icon */ "./node_modules/angular-svg-icon/fesm5/angular-svg-icon.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _result_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./result.component */ "./src/app/pages/result/result.component.ts");









var ROUTES = [{ path: '', component: _result_component__WEBPACK_IMPORTED_MODULE_8__["ResultComponent"] }];
var ResultModule = /** @class */ (function () {
    function ResultModule() {
    }
    ResultModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _result_component__WEBPACK_IMPORTED_MODULE_8__["ResultComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                angular_svg_icon__WEBPACK_IMPORTED_MODULE_6__["AngularSvgIconModule"]
            ],
        })
    ], ResultModule);
    return ResultModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-result-result-module.js.map