(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-parking-parking-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parking/parking.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parking/parking.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-gift\r\n  [skin]=\"\"\r\n  [gift]=\"gift\"\r\n  [related]=\"related\"\r\n  [showCover]=\"false\"\r\n  [showName]=\"false\"\r\n  [showHeading]=\"true\"\r\n  [showBFee]=\"false\"\r\n  [showSubscribeForm]=\"true\"\r\n  [showSubscribeCompany]=\"false\"\r\n  [showSubscribePhone]=\"false\"\r\n  [showSubscribeMessage]=\"true\"\r\n  [subscribeFormTitle]=\"\r\n  'Возникли вопросы?<br>'+\r\n  'Пишите нам'\r\n\"\r\n  headerLogo=\"/assets/img/parking.png\"\r\n  headerLogoSz=\"2\"\r\n  [headingText]=\"\r\n    gift && gift.price_modifier && gift.price_modifier.formatted_value\r\n      ? '<h1>' +\r\n        '  Парковка <strong>в подарок</strong>' +\r\n        '</h1>' +\r\n        '<p>' +\r\n        '  Подарите сертификат <strong>Московского паркинга</strong> своим друзьям и близким. ' +\r\n        '</p>' +\r\n        '<p>' +\r\n        '  При оплате картой <img src=\\'/assets/img/mir.png\\'> вы получите <strong>' +\r\n        gift.price_modifier.formatted_value +\r\n        '</strong> к номиналу сертификата.' +\r\n        '</p>' +\r\n        '<p>' +\r\n        'Подарочный сертификат на парковку - это электронный сертификат с предоплаченным, но неограниченным номиналом, который вы можете подарить друг другу. Получатель данного сертификата имеет возможность оплаты парковки в любой парковочной зоне Москвы.' +\r\n        '</p>'\r\n      : null\r\n  \"\r\n>\r\n</app-gift>\r\n");

/***/ }),

/***/ "./src/app/pages/parking/parking.component.styl":
/*!******************************************************!*\
  !*** ./src/app/pages/parking/parking.component.styl ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/pages/parking/parking.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGFya2luZy9wYXJraW5nLmNvbXBvbmVudC5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHNFQUFzRSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BhcmtpbmcvcGFya2luZy5jb21wb25lbnQuc3R5bCJ9 */");

/***/ }),

/***/ "./src/app/pages/parking/parking.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/parking/parking.component.ts ***!
  \****************************************************/
/*! exports provided: ParkingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParkingComponent", function() { return ParkingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ParkingComponent = /** @class */ (function () {
    function ParkingComponent(api, route) {
        this.api = api;
        this.route = route;
        this.user_friendly_url = 'parking';
        this.logos = [
            // '/assets/img/logo/disney.svg',
            // '/assets/img/logo/smf.svg',
            '/assets/img/logo/cons.png',
            '/assets/img/logo/planetarium.svg',
            '/assets/img/logo/zapomni.svg',
            '/assets/img/logo/park.png',
            '/assets/img/logo/zal.png',
            '/assets/img/logo/cdk.png',
            '/assets/img/logo/moskino.svg',
            '/assets/img/logo/teatrarmii_logo.png',
            '/assets/img/logo/shkola.png',
        ];
    }
    ParkingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getGifts().subscribe(function (gifts) {
            _this.gift = gifts.find(function (gift) { return gift.user_friendly_url === _this.user_friendly_url; });
            // this.related = gifts.filter(gift => gift._uuid !== this.gift._uuid);
        });
    };
    ParkingComponent.ctorParameters = function () { return [
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    ParkingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-parking',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./parking.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/parking/parking.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./parking.component.styl */ "./src/app/pages/parking/parking.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ParkingComponent);
    return ParkingComponent;
}());



/***/ }),

/***/ "./src/app/pages/parking/parking.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/parking/parking.module.ts ***!
  \*************************************************/
/*! exports provided: ROUTES, ParkingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParkingModule", function() { return ParkingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/gift/gift.module */ "./src/app/components/gift/gift.module.ts");
/* harmony import */ var _parking_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./parking.component */ "./src/app/pages/parking/parking.component.ts");







var ROUTES = [{ path: '', component: _parking_component__WEBPACK_IMPORTED_MODULE_6__["ParkingComponent"] }];
var ParkingModule = /** @class */ (function () {
    function ParkingModule() {
    }
    ParkingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _parking_component__WEBPACK_IMPORTED_MODULE_6__["ParkingComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES),
                _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_5__["GiftModule"]
            ],
        })
    ], ParkingModule);
    return ParkingModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-parking-parking-module.js.map