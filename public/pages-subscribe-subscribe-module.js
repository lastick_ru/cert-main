(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-subscribe-subscribe-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/subscribe/subscribe.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/subscribe/subscribe.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-layout-gift [skin]=\"{\r\n  skin: 'special', vars: '--skin-text: #fff;'+\r\n'    --skin-gradient-angle-1: rgba(255,255,255,0.21);'+\r\n'    --skin-gradient-angle-2: transparent;'+\r\n'    --skin-gradient-bottom-1: rgba(141,44,5,0);'+\r\n'    --skin-gradient-bottom-2: #0f0431;'+\r\n'    --skin-page-bg: #060431;'+\r\n'    --skin-page-gradient-bottom-1: rgba(255, 255, 255, 0.29);'+\r\n'    --skin-page-gradient-bottom-2: transparent;'+\r\n'    --skin-page-gradient-bottom-3: #060431;'+\r\n'    --skin-btn-bg: rgba(255,255,255,0.1);'+\r\n'    --skin-btn-text: rgba(255,255,255,0.6);'+\r\n'    --skin-btn-bg-hover: rgba(255,255,255,0.2);'+\r\n'    --skin-btn-text-hover: rgba(255,255,255,0.6);'+\r\n'    --skin-btn-bg-active: #010fad;'+\r\n'    --skin-btn-text-active: #ffd040;'+\r\n'    --skin-tab-text: #afa8a0;'+\r\n'    --skin-tab-text-active: #000;'+\r\n'    --skin-tab-bd: #e74e27;'+\r\n'    --skin-fee-nominal: #e74e27;'+\r\n'    --skin-header-logo-w-1: #fff;'+\r\n'    --skin-header-logo-w-2: #e74e27;'+\r\n'    --skin-active-color: #e74e27;'\r\n}\" theme=\"2\">\r\n  <section class=\"section section_sz-4 section_bg-3\">\r\n    <app-subscribe-form theme=\"2\" [showMessage]=\"false\"></app-subscribe-form>\r\n  </section>\r\n</app-layout-gift>\r\n");

/***/ }),

/***/ "./src/app/pages/subscribe/subscribe.component.styl":
/*!**********************************************************!*\
  !*** ./src/app/pages/subscribe/subscribe.component.styl ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/pages/subscribe/subscribe.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc3Vic2NyaWJlL3N1YnNjcmliZS5jb21wb25lbnQuc3R5bCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSwwRUFBMEUiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9zdWJzY3JpYmUvc3Vic2NyaWJlLmNvbXBvbmVudC5zdHlsIn0= */");

/***/ }),

/***/ "./src/app/pages/subscribe/subscribe.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/subscribe/subscribe.component.ts ***!
  \********************************************************/
/*! exports provided: SubscribeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscribeComponent", function() { return SubscribeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SubscribeComponent = /** @class */ (function () {
    function SubscribeComponent() {
    }
    SubscribeComponent.prototype.ngOnInit = function () {
    };
    SubscribeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-subscribe',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./subscribe.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/subscribe/subscribe.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./subscribe.component.styl */ "./src/app/pages/subscribe/subscribe.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SubscribeComponent);
    return SubscribeComponent;
}());



/***/ }),

/***/ "./src/app/pages/subscribe/subscribe.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/subscribe/subscribe.module.ts ***!
  \*****************************************************/
/*! exports provided: ROUTES, SubscribeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscribeModule", function() { return SubscribeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _subscribe_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./subscribe.component */ "./src/app/pages/subscribe/subscribe.component.ts");






var ROUTES = [{ path: '', component: _subscribe_component__WEBPACK_IMPORTED_MODULE_5__["SubscribeComponent"] }];
var SubscribeModule = /** @class */ (function () {
    function SubscribeModule() {
    }
    SubscribeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _subscribe_component__WEBPACK_IMPORTED_MODULE_5__["SubscribeComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(ROUTES),
            ],
        })
    ], SubscribeModule);
    return SubscribeModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-subscribe-subscribe-module.js.map