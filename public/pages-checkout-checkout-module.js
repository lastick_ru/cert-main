(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-checkout-checkout-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/applepay-button/applepay-button.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/applepay-button/applepay-button.component.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button type=\"button\"\r\n        class=\"mxn-btn mxn-btn_sz-4 mxn-btn_theme-pay apple-pay-button apple-pay-button-white-with-line\"\r\n        [disabled]=\"disabled || fetching\"\r\n        onclick=\"javascript:startApplePaySession()\"\r\n        id=\"applepay-button\">\r\n</button>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gpay-button/gpay-button.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/gpay-button/gpay-button.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"btn-wrap btn-wrap_gpay\" #container>\r\n\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/checkout/checkout.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/checkout/checkout.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"order order_page\">\r\n  <section class=\"section section_theme-1 section_sz-2 section_bg-2\" *ngIf=\"order\">\r\n    <div class=\"summary\">\r\n      <div class=\"summary__head\">\r\n        <a href=\"/\" routerLink=\"/\">\r\n          <svg-icon src=\"/assets/svg/logo.svg\"></svg-icon>\r\n        </a>\r\n      </div>\r\n      <div class=\"summary__body\">\r\n        <div class=\"summary__body-section\">\r\n          <div class=\"c-cart\" *ngIf=\"order.gift\">\r\n            <div class=\"c-cart__card-row\">\r\n              <div class=\"c-cart__card-item c-cart__card-item_card\">\r\n                <app-gift-card\r\n                  sz=\"3\"\r\n                  theme=\"2\"\r\n                  [gift]=\"order.gift\"\r\n                  [price]=\"order.total_cost\"\r\n                  [showTable]=\"true\"\r\n                  [showRelated]=\"false\"\r\n                  [showRate]=\"false\"\r\n                ></app-gift-card>\r\n              </div>\r\n              <div class=\"c-cart__card-item c-cart__card-item_data\">\r\n                <div class=\"c-cart__head\">\r\n                  <h4 class=\"c-cart__title\">\r\n                    {{ order.gift.name | multilang }}\r\n                  </h4>\r\n                </div>\r\n                <div\r\n                  class=\"c-cart__body\"\r\n                  *ngIf=\"order.data && order.data.items && order.data.items.length\"\r\n                >\r\n                  <div class=\"c-cart__table\">\r\n                    <div class=\"c-cart__table-row\" *ngFor=\"let item of order.data.items\">\r\n                      <div class=\"c-cart__table-td c-cart__table-td_title\">\r\n                        {{ item.price | money }}\r\n                        <span\r\n                          *ngIf=\"\r\n                            order.gift.price_modifier && order.gift.price_modifier.formatted_value\r\n                          \"\r\n                        >\r\n                          ({{ order.gift.price_modifier.formatted_value }})\r\n                        </span>\r\n                        x {{ item.count }}\r\n                      </div>\r\n                      <div class=\"c-cart__table-td c-cart__table-td_value\">\r\n                        {{ item.price * item.count | money }}\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"c-cart__table-row\">\r\n                      <div class=\"c-cart__table-td c-cart__table-td_title\">\r\n                        {{ 'Order.Total' | translate }}\r\n                      </div>\r\n                      <div class=\"c-cart__table-td c-cart__table-td_value\">\r\n                        {{ total_cost | money }}\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"summary__body-section\">\r\n          <form class=\"summary__form\" #form=\"ngForm\" (ngSubmit)=\"onSubmit()\">\r\n            <div class=\"summary__form-body\">\r\n              <div class=\"summary__form-row\">\r\n                <label\r\n                  appInputError\r\n                  [inpRef]=\"emailInp\"\r\n                  class=\"mxn-i-txt-wrap-label mxn-i-txt-wrap-label_flex mxn-i-txt-wrap-label_flex_ali-c mxn-i-txt-wrap-label_sz-1 mxn-i-txt-wrap-label_skin-default mxn-i-txt-wrap-label_cur-t\"\r\n                >\r\n                  <span\r\n                    [ngClass]=\"{ 'f-active': md.client.email && md.client.email.length }\"\r\n                    class=\"mxn-i-txt-wrap-placeholder mxn-i-txt-wrap-placeholder_dots mxn-i-txt-wrap-placeholder_sz-1 mxn-i-txt-wrap-placeholder_skin-default\"\r\n                  >\r\n                    {{ 'Form.ClientEmail' | translate }}</span\r\n                  >\r\n                  <input\r\n                    type=\"email\"\r\n                    appEmailValidator\r\n                    appNativeElementControl\r\n                    required\r\n                    #emailInp=\"ngModel\"\r\n                    #emailInpRef\r\n                    [(ngModel)]=\"md.client.email\"\r\n                    (focus)=\"fhOnInputFocus($event)\"\r\n                    (blur)=\"fhOnInputBlur()\"\r\n                    name=\"email\"\r\n                    autocomplete=\"off\"\r\n                    class=\"mxn-i-txt-wrap-input mxn-i-txt mxn-i-txt_skin-default mxn-i-txt_theme-1 mxn-i-txt-wrap-input_skin-default\"\r\n                  />\r\n                </label>\r\n                <div class=\"mxn-form-input-error\" *ngIf=\"emailInp.invalid && emailInp.dirty && emailInpRef !== fhFocusedElement\">\r\n                  <app-status-message\r\n                    [message]=\"emailInp.value ? 'Проверьте формат email' : 'Введите email'\"\r\n                  ></app-status-message>\r\n                </div>\r\n              </div>\r\n              <div class=\"summary__form-row\">\r\n                <label\r\n                  appInputError\r\n                  [inpRef]=\"phoneInp\"\r\n                  class=\"mxn-i-txt-wrap-label mxn-i-txt-wrap-label_flex mxn-i-txt-wrap-label_flex_ali-c mxn-i-txt-wrap-label_sz-1 mxn-i-txt-wrap-label_skin-default mxn-i-txt-wrap-label_cur-t\"\r\n                >\r\n                  <span\r\n                    [ngClass]=\"{ 'f-active': md.client.phone && md.client.phone.length }\"\r\n                    class=\"mxn-i-txt-wrap-placeholder mxn-i-txt-wrap-placeholder_dots mxn-i-txt-wrap-placeholder_sz-1 mxn-i-txt-wrap-placeholder_skin-default\"\r\n                  >\r\n                    {{ 'Form.ClientPhone' | translate }}</span\r\n                  >\r\n                  <input\r\n                    type=\"tel\"\r\n                    #phoneInp=\"ngModel\"\r\n                    #phoneInpRef\r\n                    appPhoneValidator\r\n                    appPhoneInputValue\r\n                    appNativeElementControl\r\n                    appMaskInputValue=\"+7\"\r\n                    required\r\n                    [(ngModel)]=\"md.client.phone\"\r\n                    (focus)=\"fhOnInputFocus($event)\"\r\n                    (blur)=\"fhOnInputBlur()\"\r\n                    name=\"phone\"\r\n                    autocomplete=\"off\"\r\n                    class=\"mxn-i-txt-wrap-input mxn-i-txt mxn-i-txt_skin-default mxn-i-txt_theme-1 mxn-i-txt-wrap-input_skin-default\"\r\n                  />\r\n                </label>\r\n                <div class=\"mxn-form-input-error\" *ngIf=\"phoneInp.invalid && phoneInp.dirty && phoneInpRef !== fhFocusedElement\">\r\n                  <app-status-message\r\n\r\n                    [message]=\"phoneInp.value ? 'Проверьте номер телефона' : 'Введите телефон'\"\r\n                  ></app-status-message>\r\n                </div>\r\n              </div>\r\n              <div class=\"summary__form-row\">\r\n                <div class=\"mxn-i-txt-wrap-label\">\r\n                  <span\r\n                    [innerHTML]=\"'Common.Agreement1' | translate\"\r\n                    class=\"mxn-i-txt-wrap-label-text mxn-i-txt-wrap-label-text_skin-default mxn-i-txt-wrap-label-text_sz-1\"\r\n                  >\r\n                  </span\r\n                  >&nbsp;<span\r\n                    class=\"mxn-cur-p mxn-link mxn-link_theme-1\"\r\n                    (click)=\"doc.openPopup('gift_offer')\"\r\n                    >{{ 'Common.PersonalDataProcessRules' | translate }}</span\r\n                  >\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <ng-template [ngIf]=\"methods && methods.length\">\r\n              <div\r\n                class=\"summary__form-footer\"\r\n                *ngFor=\"let pm of methods | payment_method_filter: 'common'\"\r\n              >\r\n                <div class=\"summary__form-footer-row\">\r\n                  <button\r\n                    [type]=\"\r\n                      (methods | payment_method_filter: 'common').length > 1 ? 'button' : 'submit'\r\n                    \"\r\n                    [disabled]=\"fetching\"\r\n                    [ngClass]=\"{ 'f-disabled': form.invalid }\"\r\n                    (click)=\"onBtnClick(pm)\"\r\n                    class=\"mxn-btn mxn-btn_sz-2 mxn-btn_full-width mxn-btn_ta-c mxn-btn_theme-1 mxn-btn_icon mxn-btn_icon_sz-1 mxn-btn_icon_mr-1\"\r\n                  >\r\n                    <svg-icon src=\"/assets/svg/payment-lock.svg\"></svg-icon\r\n                    >{{ pm.name | multilang }}\r\n                  </button>\r\n                </div>\r\n              </div>\r\n\r\n              <div\r\n                class=\"summary__form-buttons\"\r\n                *ngIf=\"(methods | payment_method_filter: '_custom').length\"\r\n              >\r\n                <div class=\"summary__form-buttons-row\">\r\n                  <div\r\n                    class=\"summary__form-buttons-item\"\r\n                    *ngIf=\"(methods | payment_method_filter: 'gpay').length\"\r\n                  >\r\n                    <app-gpay-button\r\n                      [data]=\"(methods | payment_method_filter: 'gpay')[0]\"\r\n                      [order]=\"order\"\r\n                      [total_cost]=\"total_cost\"\r\n                    >\r\n                    </app-gpay-button>\r\n                    <!--                    <button-->\r\n                    <!--                      *ngFor=\"let pm of (methods | payment_method_filter: 'gpay')\"-->\r\n                    <!--                      type=\"button\"-->\r\n                    <!--                      [ngClass]=\"{ 'f-disabled': form.invalid }\"-->\r\n                    <!--                      [disabled]=\"fetching\"-->\r\n                    <!--                      (click)=\"onBtnClick(pm)\"-->\r\n                    <!--                      class=\"mxn-btn mxn-btn_sz-4 mxn-btn_icon mxn-btn_icon_sz-2 mxn-btn_theme-pay mxn-btn_full-width mxn-btn_ta-c mxn-btn_pay\"-->\r\n                    <!--                    >-->\r\n                    <!--                      <svg-icon src=\"/assets/svg/googlepay.svg\"></svg-icon>-->\r\n                    <!--                    </button>-->\r\n                  </div>\r\n                  <div\r\n                    class=\"summary__form-buttons-item\"\r\n                    *ngIf=\"(methods | payment_method_filter: 'applepay').length\"\r\n                  >\r\n                    <app-applepay-button\r\n                      *ngFor=\"let pm of methods | payment_method_filter: 'applepay'\"\r\n                      [pm]=\"pm\"\r\n                      [order]=\"order\"\r\n                      [md]=\"md\"\r\n                      [total_cost]=\"total_cost\"\r\n                      [disabled]=\"fetching\"\r\n                    ></app-applepay-button>\r\n<!--                    <button-->\r\n<!--                      *ngFor=\"let pm of methods | payment_method_filter: 'applepay'\"-->\r\n<!--                      type=\"button\"-->\r\n<!--                      [disabled]=\"fetching\"-->\r\n<!--                      (click)=\"onApplePayClick(pm)\"-->\r\n<!--                      class=\"mxn-btn mxn-btn_sz-4 mxn-btn_icon mxn-btn_icon_sz-2 mxn-btn_theme-pay mxn-btn_full-width mxn-btn_ta-c mxn-btn_pay\"-->\r\n<!--                    >-->\r\n<!--                      <svg-icon src=\"/assets/svg/applepay.svg\"></svg-icon>-->\r\n<!--                    </button>-->\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </ng-template>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal-compatibility/index.js":
/*!*****************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal-compatibility/index.js ***!
  \*****************************************************************/
/*! exports provided: config, InnerSubscriber, OuterSubscriber, Scheduler, AnonymousSubject, SubjectSubscription, Subscriber, fromPromise, fromIterable, ajax, webSocket, ajaxGet, ajaxPost, ajaxDelete, ajaxPut, ajaxPatch, ajaxGetJSON, AjaxObservable, AjaxSubscriber, AjaxResponse, AjaxError, AjaxTimeoutError, WebSocketSubject, CombineLatestOperator, dispatch, SubscribeOnObservable, Timestamp, TimeInterval, GroupedObservable, defaultThrottleConfig, rxSubscriber, iterator, observable, ArgumentOutOfRangeError, EmptyError, Immediate, ObjectUnsubscribedError, TimeoutError, UnsubscriptionError, applyMixins, errorObject, hostReportError, identity, isArray, isArrayLike, isDate, isFunction, isIterable, isNumeric, isObject, isObservable, isPromise, isScheduler, noop, not, pipe, root, subscribeTo, subscribeToArray, subscribeToIterable, subscribeToObservable, subscribeToPromise, subscribeToResult, toSubscriber, tryCatch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _internal_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../internal/config */ "./node_modules/rxjs/_esm5/internal/config.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "config", function() { return _internal_config__WEBPACK_IMPORTED_MODULE_0__["config"]; });

/* harmony import */ var _internal_InnerSubscriber__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../internal/InnerSubscriber */ "./node_modules/rxjs/_esm5/internal/InnerSubscriber.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "InnerSubscriber", function() { return _internal_InnerSubscriber__WEBPACK_IMPORTED_MODULE_1__["InnerSubscriber"]; });

/* harmony import */ var _internal_OuterSubscriber__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../internal/OuterSubscriber */ "./node_modules/rxjs/_esm5/internal/OuterSubscriber.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OuterSubscriber", function() { return _internal_OuterSubscriber__WEBPACK_IMPORTED_MODULE_2__["OuterSubscriber"]; });

/* harmony import */ var _internal_Scheduler__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../internal/Scheduler */ "./node_modules/rxjs/_esm5/internal/Scheduler.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Scheduler", function() { return _internal_Scheduler__WEBPACK_IMPORTED_MODULE_3__["Scheduler"]; });

/* harmony import */ var _internal_Subject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../internal/Subject */ "./node_modules/rxjs/_esm5/internal/Subject.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AnonymousSubject", function() { return _internal_Subject__WEBPACK_IMPORTED_MODULE_4__["AnonymousSubject"]; });

/* harmony import */ var _internal_SubjectSubscription__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../internal/SubjectSubscription */ "./node_modules/rxjs/_esm5/internal/SubjectSubscription.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SubjectSubscription", function() { return _internal_SubjectSubscription__WEBPACK_IMPORTED_MODULE_5__["SubjectSubscription"]; });

/* harmony import */ var _internal_Subscriber__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../internal/Subscriber */ "./node_modules/rxjs/_esm5/internal/Subscriber.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Subscriber", function() { return _internal_Subscriber__WEBPACK_IMPORTED_MODULE_6__["Subscriber"]; });

/* harmony import */ var _internal_observable_fromPromise__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../internal/observable/fromPromise */ "./node_modules/rxjs/_esm5/internal/observable/fromPromise.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fromPromise", function() { return _internal_observable_fromPromise__WEBPACK_IMPORTED_MODULE_7__["fromPromise"]; });

/* harmony import */ var _internal_observable_fromIterable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../internal/observable/fromIterable */ "./node_modules/rxjs/_esm5/internal/observable/fromIterable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fromIterable", function() { return _internal_observable_fromIterable__WEBPACK_IMPORTED_MODULE_8__["fromIterable"]; });

/* harmony import */ var _internal_observable_dom_ajax__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../internal/observable/dom/ajax */ "./node_modules/rxjs/_esm5/internal/observable/dom/ajax.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajax", function() { return _internal_observable_dom_ajax__WEBPACK_IMPORTED_MODULE_9__["ajax"]; });

/* harmony import */ var _internal_observable_dom_webSocket__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../internal/observable/dom/webSocket */ "./node_modules/rxjs/_esm5/internal/observable/dom/webSocket.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "webSocket", function() { return _internal_observable_dom_webSocket__WEBPACK_IMPORTED_MODULE_10__["webSocket"]; });

/* harmony import */ var _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../internal/observable/dom/AjaxObservable */ "./node_modules/rxjs/_esm5/internal/observable/dom/AjaxObservable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajaxGet", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["ajaxGet"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajaxPost", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["ajaxPost"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajaxDelete", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["ajaxDelete"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajaxPut", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["ajaxPut"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajaxPatch", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["ajaxPatch"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajaxGetJSON", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["ajaxGetJSON"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AjaxObservable", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["AjaxObservable"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AjaxSubscriber", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["AjaxSubscriber"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AjaxResponse", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["AjaxResponse"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AjaxError", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["AjaxError"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AjaxTimeoutError", function() { return _internal_observable_dom_AjaxObservable__WEBPACK_IMPORTED_MODULE_11__["AjaxTimeoutError"]; });

/* harmony import */ var _internal_observable_dom_WebSocketSubject__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../internal/observable/dom/WebSocketSubject */ "./node_modules/rxjs/_esm5/internal/observable/dom/WebSocketSubject.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WebSocketSubject", function() { return _internal_observable_dom_WebSocketSubject__WEBPACK_IMPORTED_MODULE_12__["WebSocketSubject"]; });

/* harmony import */ var _internal_observable_combineLatest__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../internal/observable/combineLatest */ "./node_modules/rxjs/_esm5/internal/observable/combineLatest.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CombineLatestOperator", function() { return _internal_observable_combineLatest__WEBPACK_IMPORTED_MODULE_13__["CombineLatestOperator"]; });

/* harmony import */ var _internal_observable_range__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../internal/observable/range */ "./node_modules/rxjs/_esm5/internal/observable/range.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "dispatch", function() { return _internal_observable_range__WEBPACK_IMPORTED_MODULE_14__["dispatch"]; });

/* harmony import */ var _internal_observable_SubscribeOnObservable__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../internal/observable/SubscribeOnObservable */ "./node_modules/rxjs/_esm5/internal/observable/SubscribeOnObservable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SubscribeOnObservable", function() { return _internal_observable_SubscribeOnObservable__WEBPACK_IMPORTED_MODULE_15__["SubscribeOnObservable"]; });

/* harmony import */ var _internal_operators_timestamp__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../internal/operators/timestamp */ "./node_modules/rxjs/_esm5/internal/operators/timestamp.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Timestamp", function() { return _internal_operators_timestamp__WEBPACK_IMPORTED_MODULE_16__["Timestamp"]; });

/* harmony import */ var _internal_operators_timeInterval__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../internal/operators/timeInterval */ "./node_modules/rxjs/_esm5/internal/operators/timeInterval.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TimeInterval", function() { return _internal_operators_timeInterval__WEBPACK_IMPORTED_MODULE_17__["TimeInterval"]; });

/* harmony import */ var _internal_operators_groupBy__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../internal/operators/groupBy */ "./node_modules/rxjs/_esm5/internal/operators/groupBy.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "GroupedObservable", function() { return _internal_operators_groupBy__WEBPACK_IMPORTED_MODULE_18__["GroupedObservable"]; });

/* harmony import */ var _internal_operators_throttle__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../internal/operators/throttle */ "./node_modules/rxjs/_esm5/internal/operators/throttle.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "defaultThrottleConfig", function() { return _internal_operators_throttle__WEBPACK_IMPORTED_MODULE_19__["defaultThrottleConfig"]; });

/* harmony import */ var _internal_symbol_rxSubscriber__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../internal/symbol/rxSubscriber */ "./node_modules/rxjs/_esm5/internal/symbol/rxSubscriber.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "rxSubscriber", function() { return _internal_symbol_rxSubscriber__WEBPACK_IMPORTED_MODULE_20__["rxSubscriber"]; });

/* harmony import */ var _internal_symbol_iterator__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../internal/symbol/iterator */ "./node_modules/rxjs/_esm5/internal/symbol/iterator.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "iterator", function() { return _internal_symbol_iterator__WEBPACK_IMPORTED_MODULE_21__["iterator"]; });

/* harmony import */ var _internal_symbol_observable__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../internal/symbol/observable */ "./node_modules/rxjs/_esm5/internal/symbol/observable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "observable", function() { return _internal_symbol_observable__WEBPACK_IMPORTED_MODULE_22__["observable"]; });

/* harmony import */ var _internal_util_ArgumentOutOfRangeError__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../internal/util/ArgumentOutOfRangeError */ "./node_modules/rxjs/_esm5/internal/util/ArgumentOutOfRangeError.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ArgumentOutOfRangeError", function() { return _internal_util_ArgumentOutOfRangeError__WEBPACK_IMPORTED_MODULE_23__["ArgumentOutOfRangeError"]; });

/* harmony import */ var _internal_util_EmptyError__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../internal/util/EmptyError */ "./node_modules/rxjs/_esm5/internal/util/EmptyError.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EmptyError", function() { return _internal_util_EmptyError__WEBPACK_IMPORTED_MODULE_24__["EmptyError"]; });

/* harmony import */ var _internal_util_Immediate__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../internal/util/Immediate */ "./node_modules/rxjs/_esm5/internal/util/Immediate.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Immediate", function() { return _internal_util_Immediate__WEBPACK_IMPORTED_MODULE_25__["Immediate"]; });

/* harmony import */ var _internal_util_ObjectUnsubscribedError__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../internal/util/ObjectUnsubscribedError */ "./node_modules/rxjs/_esm5/internal/util/ObjectUnsubscribedError.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ObjectUnsubscribedError", function() { return _internal_util_ObjectUnsubscribedError__WEBPACK_IMPORTED_MODULE_26__["ObjectUnsubscribedError"]; });

/* harmony import */ var _internal_util_TimeoutError__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ../internal/util/TimeoutError */ "./node_modules/rxjs/_esm5/internal/util/TimeoutError.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TimeoutError", function() { return _internal_util_TimeoutError__WEBPACK_IMPORTED_MODULE_27__["TimeoutError"]; });

/* harmony import */ var _internal_util_UnsubscriptionError__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ../internal/util/UnsubscriptionError */ "./node_modules/rxjs/_esm5/internal/util/UnsubscriptionError.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UnsubscriptionError", function() { return _internal_util_UnsubscriptionError__WEBPACK_IMPORTED_MODULE_28__["UnsubscriptionError"]; });

/* harmony import */ var _internal_util_applyMixins__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ../internal/util/applyMixins */ "./node_modules/rxjs/_esm5/internal/util/applyMixins.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "applyMixins", function() { return _internal_util_applyMixins__WEBPACK_IMPORTED_MODULE_29__["applyMixins"]; });

/* harmony import */ var _internal_util_errorObject__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ../internal/util/errorObject */ "./node_modules/rxjs/_esm5/internal/util/errorObject.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "errorObject", function() { return _internal_util_errorObject__WEBPACK_IMPORTED_MODULE_30__["errorObject"]; });

/* harmony import */ var _internal_util_hostReportError__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ../internal/util/hostReportError */ "./node_modules/rxjs/_esm5/internal/util/hostReportError.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hostReportError", function() { return _internal_util_hostReportError__WEBPACK_IMPORTED_MODULE_31__["hostReportError"]; });

/* harmony import */ var _internal_util_identity__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ../internal/util/identity */ "./node_modules/rxjs/_esm5/internal/util/identity.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "identity", function() { return _internal_util_identity__WEBPACK_IMPORTED_MODULE_32__["identity"]; });

/* harmony import */ var _internal_util_isArray__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ../internal/util/isArray */ "./node_modules/rxjs/_esm5/internal/util/isArray.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isArray", function() { return _internal_util_isArray__WEBPACK_IMPORTED_MODULE_33__["isArray"]; });

/* harmony import */ var _internal_util_isArrayLike__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ../internal/util/isArrayLike */ "./node_modules/rxjs/_esm5/internal/util/isArrayLike.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isArrayLike", function() { return _internal_util_isArrayLike__WEBPACK_IMPORTED_MODULE_34__["isArrayLike"]; });

/* harmony import */ var _internal_util_isDate__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ../internal/util/isDate */ "./node_modules/rxjs/_esm5/internal/util/isDate.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isDate", function() { return _internal_util_isDate__WEBPACK_IMPORTED_MODULE_35__["isDate"]; });

/* harmony import */ var _internal_util_isFunction__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ../internal/util/isFunction */ "./node_modules/rxjs/_esm5/internal/util/isFunction.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isFunction", function() { return _internal_util_isFunction__WEBPACK_IMPORTED_MODULE_36__["isFunction"]; });

/* harmony import */ var _internal_util_isIterable__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ../internal/util/isIterable */ "./node_modules/rxjs/_esm5/internal/util/isIterable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isIterable", function() { return _internal_util_isIterable__WEBPACK_IMPORTED_MODULE_37__["isIterable"]; });

/* harmony import */ var _internal_util_isNumeric__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ../internal/util/isNumeric */ "./node_modules/rxjs/_esm5/internal/util/isNumeric.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isNumeric", function() { return _internal_util_isNumeric__WEBPACK_IMPORTED_MODULE_38__["isNumeric"]; });

/* harmony import */ var _internal_util_isObject__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ../internal/util/isObject */ "./node_modules/rxjs/_esm5/internal/util/isObject.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isObject", function() { return _internal_util_isObject__WEBPACK_IMPORTED_MODULE_39__["isObject"]; });

/* harmony import */ var _internal_util_isInteropObservable__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ../internal/util/isInteropObservable */ "./node_modules/rxjs/_esm5/internal/util/isInteropObservable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isObservable", function() { return _internal_util_isInteropObservable__WEBPACK_IMPORTED_MODULE_40__["isInteropObservable"]; });

/* harmony import */ var _internal_util_isPromise__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ../internal/util/isPromise */ "./node_modules/rxjs/_esm5/internal/util/isPromise.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isPromise", function() { return _internal_util_isPromise__WEBPACK_IMPORTED_MODULE_41__["isPromise"]; });

/* harmony import */ var _internal_util_isScheduler__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ../internal/util/isScheduler */ "./node_modules/rxjs/_esm5/internal/util/isScheduler.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isScheduler", function() { return _internal_util_isScheduler__WEBPACK_IMPORTED_MODULE_42__["isScheduler"]; });

/* harmony import */ var _internal_util_noop__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ../internal/util/noop */ "./node_modules/rxjs/_esm5/internal/util/noop.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "noop", function() { return _internal_util_noop__WEBPACK_IMPORTED_MODULE_43__["noop"]; });

/* harmony import */ var _internal_util_not__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ../internal/util/not */ "./node_modules/rxjs/_esm5/internal/util/not.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "not", function() { return _internal_util_not__WEBPACK_IMPORTED_MODULE_44__["not"]; });

/* harmony import */ var _internal_util_pipe__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ../internal/util/pipe */ "./node_modules/rxjs/_esm5/internal/util/pipe.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "pipe", function() { return _internal_util_pipe__WEBPACK_IMPORTED_MODULE_45__["pipe"]; });

/* harmony import */ var _internal_util_root__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ../internal/util/root */ "./node_modules/rxjs/_esm5/internal/util/root.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "root", function() { return _internal_util_root__WEBPACK_IMPORTED_MODULE_46__["root"]; });

/* harmony import */ var _internal_util_subscribeTo__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ../internal/util/subscribeTo */ "./node_modules/rxjs/_esm5/internal/util/subscribeTo.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "subscribeTo", function() { return _internal_util_subscribeTo__WEBPACK_IMPORTED_MODULE_47__["subscribeTo"]; });

/* harmony import */ var _internal_util_subscribeToArray__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ../internal/util/subscribeToArray */ "./node_modules/rxjs/_esm5/internal/util/subscribeToArray.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "subscribeToArray", function() { return _internal_util_subscribeToArray__WEBPACK_IMPORTED_MODULE_48__["subscribeToArray"]; });

/* harmony import */ var _internal_util_subscribeToIterable__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ../internal/util/subscribeToIterable */ "./node_modules/rxjs/_esm5/internal/util/subscribeToIterable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "subscribeToIterable", function() { return _internal_util_subscribeToIterable__WEBPACK_IMPORTED_MODULE_49__["subscribeToIterable"]; });

/* harmony import */ var _internal_util_subscribeToObservable__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ../internal/util/subscribeToObservable */ "./node_modules/rxjs/_esm5/internal/util/subscribeToObservable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "subscribeToObservable", function() { return _internal_util_subscribeToObservable__WEBPACK_IMPORTED_MODULE_50__["subscribeToObservable"]; });

/* harmony import */ var _internal_util_subscribeToPromise__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ../internal/util/subscribeToPromise */ "./node_modules/rxjs/_esm5/internal/util/subscribeToPromise.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "subscribeToPromise", function() { return _internal_util_subscribeToPromise__WEBPACK_IMPORTED_MODULE_51__["subscribeToPromise"]; });

/* harmony import */ var _internal_util_subscribeToResult__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ../internal/util/subscribeToResult */ "./node_modules/rxjs/_esm5/internal/util/subscribeToResult.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "subscribeToResult", function() { return _internal_util_subscribeToResult__WEBPACK_IMPORTED_MODULE_52__["subscribeToResult"]; });

/* harmony import */ var _internal_util_toSubscriber__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ../internal/util/toSubscriber */ "./node_modules/rxjs/_esm5/internal/util/toSubscriber.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "toSubscriber", function() { return _internal_util_toSubscriber__WEBPACK_IMPORTED_MODULE_53__["toSubscriber"]; });

/* harmony import */ var _internal_util_tryCatch__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ../internal/util/tryCatch */ "./node_modules/rxjs/_esm5/internal/util/tryCatch.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "tryCatch", function() { return _internal_util_tryCatch__WEBPACK_IMPORTED_MODULE_54__["tryCatch"]; });

/** PURE_IMPORTS_START  PURE_IMPORTS_END */























































//# sourceMappingURL=index.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/observable/dom/AjaxObservable.js":
/*!***************************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/observable/dom/AjaxObservable.js ***!
  \***************************************************************************/
/*! exports provided: ajaxGet, ajaxPost, ajaxDelete, ajaxPut, ajaxPatch, ajaxGetJSON, AjaxObservable, AjaxSubscriber, AjaxResponse, AjaxError, AjaxTimeoutError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxGet", function() { return ajaxGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxPost", function() { return ajaxPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxDelete", function() { return ajaxDelete; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxPut", function() { return ajaxPut; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxPatch", function() { return ajaxPatch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajaxGetJSON", function() { return ajaxGetJSON; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxObservable", function() { return AjaxObservable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxSubscriber", function() { return AjaxSubscriber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxResponse", function() { return AjaxResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxError", function() { return AjaxError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AjaxTimeoutError", function() { return AjaxTimeoutError; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _util_root__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../util/root */ "./node_modules/rxjs/_esm5/internal/util/root.js");
/* harmony import */ var _Observable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Observable */ "./node_modules/rxjs/_esm5/internal/Observable.js");
/* harmony import */ var _Subscriber__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Subscriber */ "./node_modules/rxjs/_esm5/internal/Subscriber.js");
/* harmony import */ var _operators_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../operators/map */ "./node_modules/rxjs/_esm5/internal/operators/map.js");
/** PURE_IMPORTS_START tslib,_.._util_root,_.._Observable,_.._Subscriber,_.._operators_map PURE_IMPORTS_END */





function getCORSRequest() {
    if (_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XMLHttpRequest) {
        return new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XMLHttpRequest();
    }
    else if (!!_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XDomainRequest) {
        return new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XDomainRequest();
    }
    else {
        throw new Error('CORS is not supported by your browser');
    }
}
function getXMLHttpRequest() {
    if (_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XMLHttpRequest) {
        return new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XMLHttpRequest();
    }
    else {
        var progId = void 0;
        try {
            var progIds = ['Msxml2.XMLHTTP', 'Microsoft.XMLHTTP', 'Msxml2.XMLHTTP.4.0'];
            for (var i = 0; i < 3; i++) {
                try {
                    progId = progIds[i];
                    if (new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].ActiveXObject(progId)) {
                        break;
                    }
                }
                catch (e) {
                }
            }
            return new _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].ActiveXObject(progId);
        }
        catch (e) {
            throw new Error('XMLHttpRequest is not supported by your browser');
        }
    }
}
function ajaxGet(url, headers) {
    if (headers === void 0) {
        headers = null;
    }
    return new AjaxObservable({ method: 'GET', url: url, headers: headers });
}
function ajaxPost(url, body, headers) {
    return new AjaxObservable({ method: 'POST', url: url, body: body, headers: headers });
}
function ajaxDelete(url, headers) {
    return new AjaxObservable({ method: 'DELETE', url: url, headers: headers });
}
function ajaxPut(url, body, headers) {
    return new AjaxObservable({ method: 'PUT', url: url, body: body, headers: headers });
}
function ajaxPatch(url, body, headers) {
    return new AjaxObservable({ method: 'PATCH', url: url, body: body, headers: headers });
}
var mapResponse = /*@__PURE__*/ Object(_operators_map__WEBPACK_IMPORTED_MODULE_4__["map"])(function (x, index) { return x.response; });
function ajaxGetJSON(url, headers) {
    return mapResponse(new AjaxObservable({
        method: 'GET',
        url: url,
        responseType: 'json',
        headers: headers
    }));
}
var AjaxObservable = /*@__PURE__*/ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AjaxObservable, _super);
    function AjaxObservable(urlOrRequest) {
        var _this = _super.call(this) || this;
        var request = {
            async: true,
            createXHR: function () {
                return this.crossDomain ? getCORSRequest() : getXMLHttpRequest();
            },
            crossDomain: true,
            withCredentials: false,
            headers: {},
            method: 'GET',
            responseType: 'json',
            timeout: 0
        };
        if (typeof urlOrRequest === 'string') {
            request.url = urlOrRequest;
        }
        else {
            for (var prop in urlOrRequest) {
                if (urlOrRequest.hasOwnProperty(prop)) {
                    request[prop] = urlOrRequest[prop];
                }
            }
        }
        _this.request = request;
        return _this;
    }
    AjaxObservable.prototype._subscribe = function (subscriber) {
        return new AjaxSubscriber(subscriber, this.request);
    };
    AjaxObservable.create = (function () {
        var create = function (urlOrRequest) {
            return new AjaxObservable(urlOrRequest);
        };
        create.get = ajaxGet;
        create.post = ajaxPost;
        create.delete = ajaxDelete;
        create.put = ajaxPut;
        create.patch = ajaxPatch;
        create.getJSON = ajaxGetJSON;
        return create;
    })();
    return AjaxObservable;
}(_Observable__WEBPACK_IMPORTED_MODULE_2__["Observable"]));

var AjaxSubscriber = /*@__PURE__*/ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AjaxSubscriber, _super);
    function AjaxSubscriber(destination, request) {
        var _this = _super.call(this, destination) || this;
        _this.request = request;
        _this.done = false;
        var headers = request.headers = request.headers || {};
        if (!request.crossDomain && !_this.getHeader(headers, 'X-Requested-With')) {
            headers['X-Requested-With'] = 'XMLHttpRequest';
        }
        var contentTypeHeader = _this.getHeader(headers, 'Content-Type');
        if (!contentTypeHeader && !(_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].FormData && request.body instanceof _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].FormData) && typeof request.body !== 'undefined') {
            headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        }
        request.body = _this.serializeBody(request.body, _this.getHeader(request.headers, 'Content-Type'));
        _this.send();
        return _this;
    }
    AjaxSubscriber.prototype.next = function (e) {
        this.done = true;
        var _a = this, xhr = _a.xhr, request = _a.request, destination = _a.destination;
        var result;
        try {
            result = new AjaxResponse(e, xhr, request);
        }
        catch (err) {
            return destination.error(err);
        }
        destination.next(result);
    };
    AjaxSubscriber.prototype.send = function () {
        var _a = this, request = _a.request, _b = _a.request, user = _b.user, method = _b.method, url = _b.url, async = _b.async, password = _b.password, headers = _b.headers, body = _b.body;
        try {
            var xhr = this.xhr = request.createXHR();
            this.setupEvents(xhr, request);
            if (user) {
                xhr.open(method, url, async, user, password);
            }
            else {
                xhr.open(method, url, async);
            }
            if (async) {
                xhr.timeout = request.timeout;
                xhr.responseType = request.responseType;
            }
            if ('withCredentials' in xhr) {
                xhr.withCredentials = !!request.withCredentials;
            }
            this.setHeaders(xhr, headers);
            if (body) {
                xhr.send(body);
            }
            else {
                xhr.send();
            }
        }
        catch (err) {
            this.error(err);
        }
    };
    AjaxSubscriber.prototype.serializeBody = function (body, contentType) {
        if (!body || typeof body === 'string') {
            return body;
        }
        else if (_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].FormData && body instanceof _util_root__WEBPACK_IMPORTED_MODULE_1__["root"].FormData) {
            return body;
        }
        if (contentType) {
            var splitIndex = contentType.indexOf(';');
            if (splitIndex !== -1) {
                contentType = contentType.substring(0, splitIndex);
            }
        }
        switch (contentType) {
            case 'application/x-www-form-urlencoded':
                return Object.keys(body).map(function (key) { return encodeURIComponent(key) + "=" + encodeURIComponent(body[key]); }).join('&');
            case 'application/json':
                return JSON.stringify(body);
            default:
                return body;
        }
    };
    AjaxSubscriber.prototype.setHeaders = function (xhr, headers) {
        for (var key in headers) {
            if (headers.hasOwnProperty(key)) {
                xhr.setRequestHeader(key, headers[key]);
            }
        }
    };
    AjaxSubscriber.prototype.getHeader = function (headers, headerName) {
        for (var key in headers) {
            if (key.toLowerCase() === headerName.toLowerCase()) {
                return headers[key];
            }
        }
        return undefined;
    };
    AjaxSubscriber.prototype.setupEvents = function (xhr, request) {
        var progressSubscriber = request.progressSubscriber;
        function xhrTimeout(e) {
            var _a = xhrTimeout, subscriber = _a.subscriber, progressSubscriber = _a.progressSubscriber, request = _a.request;
            if (progressSubscriber) {
                progressSubscriber.error(e);
            }
            var error;
            try {
                error = new AjaxTimeoutError(this, request);
            }
            catch (err) {
                error = err;
            }
            subscriber.error(error);
        }
        xhr.ontimeout = xhrTimeout;
        xhrTimeout.request = request;
        xhrTimeout.subscriber = this;
        xhrTimeout.progressSubscriber = progressSubscriber;
        if (xhr.upload && 'withCredentials' in xhr) {
            if (progressSubscriber) {
                var xhrProgress_1;
                xhrProgress_1 = function (e) {
                    var progressSubscriber = xhrProgress_1.progressSubscriber;
                    progressSubscriber.next(e);
                };
                if (_util_root__WEBPACK_IMPORTED_MODULE_1__["root"].XDomainRequest) {
                    xhr.onprogress = xhrProgress_1;
                }
                else {
                    xhr.upload.onprogress = xhrProgress_1;
                }
                xhrProgress_1.progressSubscriber = progressSubscriber;
            }
            var xhrError_1;
            xhrError_1 = function (e) {
                var _a = xhrError_1, progressSubscriber = _a.progressSubscriber, subscriber = _a.subscriber, request = _a.request;
                if (progressSubscriber) {
                    progressSubscriber.error(e);
                }
                var error;
                try {
                    error = new AjaxError('ajax error', this, request);
                }
                catch (err) {
                    error = err;
                }
                subscriber.error(error);
            };
            xhr.onerror = xhrError_1;
            xhrError_1.request = request;
            xhrError_1.subscriber = this;
            xhrError_1.progressSubscriber = progressSubscriber;
        }
        function xhrReadyStateChange(e) {
            return;
        }
        xhr.onreadystatechange = xhrReadyStateChange;
        xhrReadyStateChange.subscriber = this;
        xhrReadyStateChange.progressSubscriber = progressSubscriber;
        xhrReadyStateChange.request = request;
        function xhrLoad(e) {
            var _a = xhrLoad, subscriber = _a.subscriber, progressSubscriber = _a.progressSubscriber, request = _a.request;
            if (this.readyState === 4) {
                var status_1 = this.status === 1223 ? 204 : this.status;
                var response = (this.responseType === 'text' ? (this.response || this.responseText) : this.response);
                if (status_1 === 0) {
                    status_1 = response ? 200 : 0;
                }
                if (status_1 < 400) {
                    if (progressSubscriber) {
                        progressSubscriber.complete();
                    }
                    subscriber.next(e);
                    subscriber.complete();
                }
                else {
                    if (progressSubscriber) {
                        progressSubscriber.error(e);
                    }
                    var error = void 0;
                    try {
                        error = new AjaxError('ajax error ' + status_1, this, request);
                    }
                    catch (err) {
                        error = err;
                    }
                    subscriber.error(error);
                }
            }
        }
        xhr.onload = xhrLoad;
        xhrLoad.subscriber = this;
        xhrLoad.progressSubscriber = progressSubscriber;
        xhrLoad.request = request;
    };
    AjaxSubscriber.prototype.unsubscribe = function () {
        var _a = this, done = _a.done, xhr = _a.xhr;
        if (!done && xhr && xhr.readyState !== 4 && typeof xhr.abort === 'function') {
            xhr.abort();
        }
        _super.prototype.unsubscribe.call(this);
    };
    return AjaxSubscriber;
}(_Subscriber__WEBPACK_IMPORTED_MODULE_3__["Subscriber"]));

var AjaxResponse = /*@__PURE__*/ (function () {
    function AjaxResponse(originalEvent, xhr, request) {
        this.originalEvent = originalEvent;
        this.xhr = xhr;
        this.request = request;
        this.status = xhr.status;
        this.responseType = xhr.responseType || request.responseType;
        this.response = parseXhrResponse(this.responseType, xhr);
    }
    return AjaxResponse;
}());

function AjaxErrorImpl(message, xhr, request) {
    Error.call(this);
    this.message = message;
    this.name = 'AjaxError';
    this.xhr = xhr;
    this.request = request;
    this.status = xhr.status;
    this.responseType = xhr.responseType || request.responseType;
    this.response = parseXhrResponse(this.responseType, xhr);
    return this;
}
AjaxErrorImpl.prototype = /*@__PURE__*/ Object.create(Error.prototype);
var AjaxError = AjaxErrorImpl;
function parseJson(xhr) {
    if ('response' in xhr) {
        return xhr.responseType ? xhr.response : JSON.parse(xhr.response || xhr.responseText || 'null');
    }
    else {
        return JSON.parse(xhr.responseText || 'null');
    }
}
function parseXhrResponse(responseType, xhr) {
    switch (responseType) {
        case 'json':
            return parseJson(xhr);
        case 'xml':
            return xhr.responseXML;
        case 'text':
        default:
            return ('response' in xhr) ? xhr.response : xhr.responseText;
    }
}
function AjaxTimeoutErrorImpl(xhr, request) {
    AjaxError.call(this, 'ajax timeout', xhr, request);
    this.name = 'AjaxTimeoutError';
    return this;
}
var AjaxTimeoutError = AjaxTimeoutErrorImpl;
//# sourceMappingURL=AjaxObservable.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/observable/dom/WebSocketSubject.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/observable/dom/WebSocketSubject.js ***!
  \*****************************************************************************/
/*! exports provided: WebSocketSubject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebSocketSubject", function() { return WebSocketSubject; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _Subject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Subject */ "./node_modules/rxjs/_esm5/internal/Subject.js");
/* harmony import */ var _Subscriber__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Subscriber */ "./node_modules/rxjs/_esm5/internal/Subscriber.js");
/* harmony import */ var _Observable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Observable */ "./node_modules/rxjs/_esm5/internal/Observable.js");
/* harmony import */ var _Subscription__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../Subscription */ "./node_modules/rxjs/_esm5/internal/Subscription.js");
/* harmony import */ var _ReplaySubject__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../ReplaySubject */ "./node_modules/rxjs/_esm5/internal/ReplaySubject.js");
/** PURE_IMPORTS_START tslib,_.._Subject,_.._Subscriber,_.._Observable,_.._Subscription,_.._ReplaySubject PURE_IMPORTS_END */






var DEFAULT_WEBSOCKET_CONFIG = {
    url: '',
    deserializer: function (e) { return JSON.parse(e.data); },
    serializer: function (value) { return JSON.stringify(value); },
};
var WEBSOCKETSUBJECT_INVALID_ERROR_OBJECT = 'WebSocketSubject.error must be called with an object with an error code, and an optional reason: { code: number, reason: string }';
var WebSocketSubject = /*@__PURE__*/ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](WebSocketSubject, _super);
    function WebSocketSubject(urlConfigOrSource, destination) {
        var _this = _super.call(this) || this;
        if (urlConfigOrSource instanceof _Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"]) {
            _this.destination = destination;
            _this.source = urlConfigOrSource;
        }
        else {
            var config = _this._config = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, DEFAULT_WEBSOCKET_CONFIG);
            _this._output = new _Subject__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
            if (typeof urlConfigOrSource === 'string') {
                config.url = urlConfigOrSource;
            }
            else {
                for (var key in urlConfigOrSource) {
                    if (urlConfigOrSource.hasOwnProperty(key)) {
                        config[key] = urlConfigOrSource[key];
                    }
                }
            }
            if (!config.WebSocketCtor && WebSocket) {
                config.WebSocketCtor = WebSocket;
            }
            else if (!config.WebSocketCtor) {
                throw new Error('no WebSocket constructor can be found');
            }
            _this.destination = new _ReplaySubject__WEBPACK_IMPORTED_MODULE_5__["ReplaySubject"]();
        }
        return _this;
    }
    WebSocketSubject.prototype.lift = function (operator) {
        var sock = new WebSocketSubject(this._config, this.destination);
        sock.operator = operator;
        sock.source = this;
        return sock;
    };
    WebSocketSubject.prototype._resetState = function () {
        this._socket = null;
        if (!this.source) {
            this.destination = new _ReplaySubject__WEBPACK_IMPORTED_MODULE_5__["ReplaySubject"]();
        }
        this._output = new _Subject__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    };
    WebSocketSubject.prototype.multiplex = function (subMsg, unsubMsg, messageFilter) {
        var self = this;
        return new _Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"](function (observer) {
            try {
                self.next(subMsg());
            }
            catch (err) {
                observer.error(err);
            }
            var subscription = self.subscribe(function (x) {
                try {
                    if (messageFilter(x)) {
                        observer.next(x);
                    }
                }
                catch (err) {
                    observer.error(err);
                }
            }, function (err) { return observer.error(err); }, function () { return observer.complete(); });
            return function () {
                try {
                    self.next(unsubMsg());
                }
                catch (err) {
                    observer.error(err);
                }
                subscription.unsubscribe();
            };
        });
    };
    WebSocketSubject.prototype._connectSocket = function () {
        var _this = this;
        var _a = this._config, WebSocketCtor = _a.WebSocketCtor, protocol = _a.protocol, url = _a.url, binaryType = _a.binaryType;
        var observer = this._output;
        var socket = null;
        try {
            socket = protocol ?
                new WebSocketCtor(url, protocol) :
                new WebSocketCtor(url);
            this._socket = socket;
            if (binaryType) {
                this._socket.binaryType = binaryType;
            }
        }
        catch (e) {
            observer.error(e);
            return;
        }
        var subscription = new _Subscription__WEBPACK_IMPORTED_MODULE_4__["Subscription"](function () {
            _this._socket = null;
            if (socket && socket.readyState === 1) {
                socket.close();
            }
        });
        socket.onopen = function (e) {
            var _socket = _this._socket;
            if (!_socket) {
                socket.close();
                _this._resetState();
                return;
            }
            var openObserver = _this._config.openObserver;
            if (openObserver) {
                openObserver.next(e);
            }
            var queue = _this.destination;
            _this.destination = _Subscriber__WEBPACK_IMPORTED_MODULE_2__["Subscriber"].create(function (x) {
                if (socket.readyState === 1) {
                    try {
                        var serializer = _this._config.serializer;
                        socket.send(serializer(x));
                    }
                    catch (e) {
                        _this.destination.error(e);
                    }
                }
            }, function (e) {
                var closingObserver = _this._config.closingObserver;
                if (closingObserver) {
                    closingObserver.next(undefined);
                }
                if (e && e.code) {
                    socket.close(e.code, e.reason);
                }
                else {
                    observer.error(new TypeError(WEBSOCKETSUBJECT_INVALID_ERROR_OBJECT));
                }
                _this._resetState();
            }, function () {
                var closingObserver = _this._config.closingObserver;
                if (closingObserver) {
                    closingObserver.next(undefined);
                }
                socket.close();
                _this._resetState();
            });
            if (queue && queue instanceof _ReplaySubject__WEBPACK_IMPORTED_MODULE_5__["ReplaySubject"]) {
                subscription.add(queue.subscribe(_this.destination));
            }
        };
        socket.onerror = function (e) {
            _this._resetState();
            observer.error(e);
        };
        socket.onclose = function (e) {
            _this._resetState();
            var closeObserver = _this._config.closeObserver;
            if (closeObserver) {
                closeObserver.next(e);
            }
            if (e.wasClean) {
                observer.complete();
            }
            else {
                observer.error(e);
            }
        };
        socket.onmessage = function (e) {
            try {
                var deserializer = _this._config.deserializer;
                observer.next(deserializer(e));
            }
            catch (err) {
                observer.error(err);
            }
        };
    };
    WebSocketSubject.prototype._subscribe = function (subscriber) {
        var _this = this;
        var source = this.source;
        if (source) {
            return source.subscribe(subscriber);
        }
        if (!this._socket) {
            this._connectSocket();
        }
        this._output.subscribe(subscriber);
        subscriber.add(function () {
            var _socket = _this._socket;
            if (_this._output.observers.length === 0) {
                if (_socket && _socket.readyState === 1) {
                    _socket.close();
                }
                _this._resetState();
            }
        });
        return subscriber;
    };
    WebSocketSubject.prototype.unsubscribe = function () {
        var _socket = this._socket;
        if (_socket && _socket.readyState === 1) {
            _socket.close();
        }
        this._resetState();
        _super.prototype.unsubscribe.call(this);
    };
    return WebSocketSubject;
}(_Subject__WEBPACK_IMPORTED_MODULE_1__["AnonymousSubject"]));

//# sourceMappingURL=WebSocketSubject.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/observable/dom/ajax.js":
/*!*****************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/observable/dom/ajax.js ***!
  \*****************************************************************/
/*! exports provided: ajax */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajax", function() { return ajax; });
/* harmony import */ var _AjaxObservable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AjaxObservable */ "./node_modules/rxjs/_esm5/internal/observable/dom/AjaxObservable.js");
/** PURE_IMPORTS_START _AjaxObservable PURE_IMPORTS_END */

var ajax = _AjaxObservable__WEBPACK_IMPORTED_MODULE_0__["AjaxObservable"].create;
//# sourceMappingURL=ajax.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/observable/dom/webSocket.js":
/*!**********************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/observable/dom/webSocket.js ***!
  \**********************************************************************/
/*! exports provided: webSocket */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "webSocket", function() { return webSocket; });
/* harmony import */ var _WebSocketSubject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./WebSocketSubject */ "./node_modules/rxjs/_esm5/internal/observable/dom/WebSocketSubject.js");
/** PURE_IMPORTS_START _WebSocketSubject PURE_IMPORTS_END */

function webSocket(urlConfigOrSource) {
    return new _WebSocketSubject__WEBPACK_IMPORTED_MODULE_0__["WebSocketSubject"](urlConfigOrSource);
}
//# sourceMappingURL=webSocket.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/util/applyMixins.js":
/*!**************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/util/applyMixins.js ***!
  \**************************************************************/
/*! exports provided: applyMixins */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "applyMixins", function() { return applyMixins; });
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
function applyMixins(derivedCtor, baseCtors) {
    for (var i = 0, len = baseCtors.length; i < len; i++) {
        var baseCtor = baseCtors[i];
        var propertyKeys = Object.getOwnPropertyNames(baseCtor.prototype);
        for (var j = 0, len2 = propertyKeys.length; j < len2; j++) {
            var name_1 = propertyKeys[j];
            derivedCtor.prototype[name_1] = baseCtor.prototype[name_1];
        }
    }
}
//# sourceMappingURL=applyMixins.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/util/errorObject.js":
/*!**************************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/util/errorObject.js ***!
  \**************************************************************/
/*! exports provided: errorObject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "errorObject", function() { return errorObject; });
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var errorObject = { e: {} };
//# sourceMappingURL=errorObject.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/util/root.js":
/*!*******************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/util/root.js ***!
  \*******************************************************/
/*! exports provided: root */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "root", function() { return _root; });
/** PURE_IMPORTS_START  PURE_IMPORTS_END */
var __window = typeof window !== 'undefined' && window;
var __self = typeof self !== 'undefined' && typeof WorkerGlobalScope !== 'undefined' &&
    self instanceof WorkerGlobalScope && self;
var __global = typeof global !== 'undefined' && global;
var _root = __window || __global || __self;
/*@__PURE__*/ (function () {
    if (!_root) {
        throw /*@__PURE__*/ new Error('RxJS could not find any global context (window, self, global)');
    }
})();

//# sourceMappingURL=root.js.map


/***/ }),

/***/ "./node_modules/rxjs/_esm5/internal/util/tryCatch.js":
/*!***********************************************************!*\
  !*** ./node_modules/rxjs/_esm5/internal/util/tryCatch.js ***!
  \***********************************************************/
/*! exports provided: tryCatch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tryCatch", function() { return tryCatch; });
/* harmony import */ var _errorObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./errorObject */ "./node_modules/rxjs/_esm5/internal/util/errorObject.js");
/** PURE_IMPORTS_START _errorObject PURE_IMPORTS_END */

var tryCatchTarget;
function tryCatcher() {
    _errorObject__WEBPACK_IMPORTED_MODULE_0__["errorObject"].e = undefined;
    try {
        return tryCatchTarget.apply(this, arguments);
    }
    catch (e) {
        _errorObject__WEBPACK_IMPORTED_MODULE_0__["errorObject"].e = e;
        return _errorObject__WEBPACK_IMPORTED_MODULE_0__["errorObject"];
    }
    finally {
        tryCatchTarget = undefined;
    }
}
function tryCatch(fn) {
    tryCatchTarget = fn;
    return tryCatcher;
}
//# sourceMappingURL=tryCatch.js.map


/***/ }),

/***/ "./src/app/components/applepay-button/applepay-button.component.styl":
/*!***************************************************************************!*\
  !*** ./src/app/components/applepay-button/applepay-button.component.styl ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/components/applepay-button/applepay-button.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hcHBsZXBheS1idXR0b24vYXBwbGVwYXktYnV0dG9uLmNvbXBvbmVudC5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDJGQUEyRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYXBwbGVwYXktYnV0dG9uL2FwcGxlcGF5LWJ1dHRvbi5jb21wb25lbnQuc3R5bCJ9 */");

/***/ }),

/***/ "./src/app/components/applepay-button/applepay-button.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/applepay-button/applepay-button.component.ts ***!
  \*************************************************************************/
/*! exports provided: ApplepayButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplepayButtonComponent", function() { return ApplepayButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_payment_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/payment.service */ "./src/app/services/payment.service.ts");
/* harmony import */ var _services_logger_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/logger.service */ "./src/app/services/logger.service.ts");
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ramda */ "./node_modules/ramda/es/index.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var _services_platform_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/platform.service */ "./src/app/services/platform.service.ts");







var ApplepayButtonComponent = /** @class */ (function () {
    function ApplepayButtonComponent(payment, logger, api, platform) {
        this.payment = payment;
        this.logger = logger;
        this.api = api;
        this.platform = platform;
        this.disabled = false;
    }
    ApplepayButtonComponent.prototype.ngOnInit = function () {
        var self = this;
        window.startApplePaySession = function () {
            self.fetching = true;
            var cart_data = self.order.data;
            cart_data.payment_method = self.pm._uuid;
            cart_data.client = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, self.md.client);
            self.cart_data = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, cart_data);
            self.logger.l(['cart_data onApplePayClick', self.cart_data], false);
            var paymentOptions = {
                requestPayerEmail: Object(ramda__WEBPACK_IMPORTED_MODULE_4__["isNil"])(self.md.client.email) || Object(ramda__WEBPACK_IMPORTED_MODULE_4__["isEmpty"])(self.md.client.email),
                requestPayerPhone: Object(ramda__WEBPACK_IMPORTED_MODULE_4__["isNil"])(self.md.client.phone) || Object(ramda__WEBPACK_IMPORTED_MODULE_4__["isEmpty"])(self.md.client.phone),
            };
            var requiredShippingContactFields = [];
            if (paymentOptions.requestPayerEmail) {
                requiredShippingContactFields.push('email');
            }
            if (paymentOptions.requestPayerPhone) {
                requiredShippingContactFields.push('phone');
            }
            var _a = self.payment.getOptions(self.pm, self.total_cost), paymentMethodData = _a.paymentMethodData, paymentDetails = _a.paymentDetails;
            var version = 3;
            // ([1, 2, 3, 4, 5, 6]).map(v => {
            //   if (ApplePaySession.supportsVersion(v)) {
            //     version = v;
            //   }
            //   this.logger.l([`supportsVersion ${v}`, ApplePaySession.supportsVersion(v)], false);
            // });
            var request = {
                countryCode: 'RU',
                currencyCode: 'RUB',
                merchantCapabilities: paymentMethodData.data.merchantCapabilities,
                supportedNetworks: paymentMethodData.data.supportedNetworks,
                total: {
                    label: Object(ramda__WEBPACK_IMPORTED_MODULE_4__["path"])(['total', 'label'], paymentMethodData.data),
                    type: 'final',
                    amount: Object(ramda__WEBPACK_IMPORTED_MODULE_4__["path"])(['total', 'amount', 'value'], paymentMethodData.data),
                },
                requiredShippingContactFields: requiredShippingContactFields
            };
            self.logger.l(['session request', request], false);
            var session = new ApplePaySession(version, request);
            session.oncancel = function (event) {
                self.logger.l(['oncancel', event], false);
                self.fetching = false;
            };
            // session.onpaymentmethodselected = function(event) {
            //   self.logger.l(['onpaymentmethodselected', event ], false);
            // };
            // session.onshippingmethodselected = function(event) {
            //   self.logger.l(['onshippingmethodselected', event ], false);
            // };
            session.onvalidatemerchant = function (event) {
                var validationURL = event.validationURL;
                self.logger.l(["[onmerchantvalidation] " + validationURL], false);
                self.api
                    .validateMerchant(self.pm._uuid, { validationURL: validationURL })
                    .toPromise()
                    .then(function (response) {
                    self.logger.l(['validateMerchant complete', { response: response }], false);
                    session.completeMerchantValidation(response);
                })
                    .catch(function (e) {
                    self.logger.l(['validateMerchant error', { error: e }], false);
                    session.abort();
                    self.fetching = false;
                });
            };
            // session.onshippingcontactselected = function(event) {
            //   self.logger.l(['onshippingcontactselected', { event }], false);
            // };
            session.onpaymentauthorized = function (response) {
                self.logger.l(['session.onpaymentauthorized', response], false);
                self.logger.l(['session.onpaymentauthorized session', session], false);
                var client = self.cart_data.client;
                client.phone = Object(ramda__WEBPACK_IMPORTED_MODULE_4__["path"])(['payment', 'shippingContact', 'phoneNumber'], response) || client.phone;
                client.email = Object(ramda__WEBPACK_IMPORTED_MODULE_4__["path"])(['payment', 'shippingContact', 'emailAddress'], response) || client.email;
                self.cart_data['payment_token'] = Object(ramda__WEBPACK_IMPORTED_MODULE_4__["path"])(['payment', 'token', 'paymentData'], response);
                self.logger.l(['session cart_data', self.cart_data], false);
                self.api.newOrder(JSON.stringify(self.cart_data)).subscribe(function (success) {
                    session.completePayment(ApplePaySession.STATUS_SUCCESS);
                    setTimeout(function () {
                        self.logger.l(['success payment', success.payment], false);
                        window.location.href = success.payment['payment_url'];
                    }, 250);
                }, function (error) {
                    self.fetching = false;
                    session.completePayment(ApplePaySession.STATUS_FAILURE);
                });
            };
            session.begin();
            self.logger.l(['try session show'], false);
        };
    };
    ApplepayButtonComponent.prototype.ngAfterViewInit = function () {
    };
    ApplepayButtonComponent.prototype.onClick = function () {
        // console.log('ApplePaySession');
        // const session = new ApplePaySession(3, {
        //   "countryCode": "RU",
        //   "currencyCode": "RUB",
        //   "merchantCapabilities": [
        //     "supports3DS"
        //   ],
        //   "supportedNetworks": [
        //     "visa",
        //     "masterCard",
        //     "amex",
        //     "discover"
        //   ],
        //   "total": {
        //     "label": "Demo (Card is not charged)",
        //     "type": "final",
        //     "amount": "1.99"
        //   }
        // });
        // session.oncancel = function(event) {
        //   console.log('oncancel', event);
        // };
        // session.onvalidatemerchant = function(event) {
        //   console.log('onvalidatemerchant', event);
        // };
        // session.begin();
        // const self = this;
        //
        // if (!this.pm || !this.total_cost) return;
        //
        // const cart_data: ICertificateBody = this.order.data;
        // cart_data.payment_method = this.pm._uuid;
        // cart_data.client = { ...this.md.client };
        // this.cart_data = { ...cart_data };
        // this.logger.l(['cart_data onApplePayClick', this.cart_data], false);
        // const paymentOptions = {
        //   requestPayerEmail: isNil(this.md.client.email) || isEmpty(this.md.client.email),
        //   requestPayerPhone: isNil(this.md.client.phone) || isEmpty(this.md.client.phone),
        // };
        //
        // const requiredBillingContactFields = [];
        // if (paymentOptions.requestPayerEmail) {
        //   requiredBillingContactFields.push('email');
        // }
        // if (paymentOptions.requestPayerPhone) {
        //   requiredBillingContactFields.push('phone');
        // }
        //
        // const { paymentMethodData, paymentDetails } = this.payment.getOptions(this.pm, this.total_cost);
        //
        // const version = 3;
        //
        // // ([1, 2, 3, 4, 5, 6]).map(v => {
        // //   if (ApplePaySession.supportsVersion(v)) {
        // //     version = v;
        // //   }
        // //   this.logger.l([`supportsVersion ${v}`, ApplePaySession.supportsVersion(v)], false);
        // // });
        //
        // const request = {
        //   countryCode: 'RU',
        //   currencyCode: 'RUB',
        //   merchantCapabilities: paymentMethodData.data.merchantCapabilities,
        //   supportedNetworks: paymentMethodData.data.supportedNetworks,
        //   total: {
        //     label: path(['total', 'label'], paymentMethodData.data),
        //     type: 'final',
        //     amount: path(['total', 'amount', 'value'], paymentMethodData.data),
        //   },
        //   // requiredBillingContactFields
        // };
        //
        // this.logger.l(['session request', request], false);
        //
        // const session = new ApplePaySession(version, request);
        //
        // session.oncancel = function(event) {
        //   self.logger.l(['oncancel', event ], false);
        // };
        // session.onpaymentmethodselected = function(event) {
        //   self.logger.l(['onpaymentmethodselected', event ], false);
        // };
        // session.onshippingmethodselected = function(event) {
        //   self.logger.l(['onshippingmethodselected', event ], false);
        // };
        // session.onvalidatemerchant = function(event) {
        //   const validationURL = event.validationURL;
        //   self.logger.l(['onmerchantvalidation', { event }], false);
        //   self.logger.l([`[onmerchantvalidation] ${validationURL}`], false);
        //
        //   self.api
        //     .validateMerchant(this.pm._uuid, { validationURL })
        //     .toPromise()
        //     .then(response => {
        //       self.logger.l(['validateMerchant complete', { response }], false);
        //       session.completeMerchantValidation(response);
        //     })
        //     .catch(e => {
        //       self.logger.l(['validateMerchant error', { error: e }], false);
        //       session.abort();
        //     });
        // };
        // session.onshippingcontactselected = function(event) {
        //   self.logger.l(['onshippingcontactselected', { event }], false);
        // };
        // session.onpaymentauthorized = function(response) {
        //   const state = { status: ApplePaySession.STATUS_SUCCESS };
        //   self.logger.l(['session.onpaymentauthorized', response], false);
        //   self.logger.l(['session.onpaymentauthorized cart_data', self.cart_data], false);
        //   self.logger.l(['session.onpaymentauthorized session', session], false);
        //
        //   if (!isNil(response.payerPhone) && !isEmpty(response.payerPhone)) {
        //     self.cart_data.client.phone = response.payerPhone;
        //     self.logger.l(['payerPhone', response.payerPhone], false);
        //   }
        //   if (!isNil(response.payerEmail) && !isEmpty(response.payerEmail)) {
        //     self.cart_data.client.email = response.payerEmail;
        //     self.logger.l(['payerEmail', response.payerEmail], false);
        //   }
        //   self.cart_data['payment_token'] = path(['payment', 'token', 'paymentData'], response);
        //
        //   self.logger.l(['session cart_data', self.cart_data], false);
        //
        //   self.api.newOrder(JSON.stringify(self.cart_data)).subscribe(
        //     success => {
        //       session.completePayment(ApplePaySession.STATUS_SUCCESS);
        //       window.location.href = success.payment['payment_url'];
        //     },
        //     error => {
        //       session.completePayment(ApplePaySession.STATUS_FAILURE);
        //     },
        //   );
        // };
        // session.begin();
        // this.logger.l(['try session show'], false);
    };
    ApplepayButtonComponent.ctorParameters = function () { return [
        { type: _services_payment_service__WEBPACK_IMPORTED_MODULE_2__["PaymentService"] },
        { type: _services_logger_service__WEBPACK_IMPORTED_MODULE_3__["LoggerService"] },
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_5__["TransportService"] },
        { type: _services_platform_service__WEBPACK_IMPORTED_MODULE_6__["PlatformService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ApplepayButtonComponent.prototype, "pm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ApplepayButtonComponent.prototype, "order", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ApplepayButtonComponent.prototype, "md", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], ApplepayButtonComponent.prototype, "total_cost", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ApplepayButtonComponent.prototype, "disabled", void 0);
    ApplepayButtonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-applepay-button',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./applepay-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/applepay-button/applepay-button.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./applepay-button.component.styl */ "./src/app/components/applepay-button/applepay-button.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_payment_service__WEBPACK_IMPORTED_MODULE_2__["PaymentService"],
            _services_logger_service__WEBPACK_IMPORTED_MODULE_3__["LoggerService"],
            _services_transport_service__WEBPACK_IMPORTED_MODULE_5__["TransportService"],
            _services_platform_service__WEBPACK_IMPORTED_MODULE_6__["PlatformService"]])
    ], ApplepayButtonComponent);
    return ApplepayButtonComponent;
}());



/***/ }),

/***/ "./src/app/components/gpay-button/gpay-button.component.styl":
/*!*******************************************************************!*\
  !*** ./src/app/components/gpay-button/gpay-button.component.styl ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/components/gpay-button/gpay-button.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ncGF5LWJ1dHRvbi9ncGF5LWJ1dHRvbi5jb21wb25lbnQuc3R5bCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxtRkFBbUYiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2dwYXktYnV0dG9uL2dwYXktYnV0dG9uLmNvbXBvbmVudC5zdHlsIn0= */");

/***/ }),

/***/ "./src/app/components/gpay-button/gpay-button.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/gpay-button/gpay-button.component.ts ***!
  \*****************************************************************/
/*! exports provided: GpayButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GpayButtonComponent", function() { return GpayButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_payment_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/payment.service */ "./src/app/services/payment.service.ts");
/* harmony import */ var _services_platform_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/platform.service */ "./src/app/services/platform.service.ts");




var GpayButtonComponent = /** @class */ (function () {
    function GpayButtonComponent(payment, renderer, platform) {
        this.payment = payment;
        this.renderer = renderer;
        this.platform = platform;
        this.disabled = false;
        this.supported = false;
    }
    GpayButtonComponent.prototype.ngOnInit = function () {
    };
    GpayButtonComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (this.data && this.platform.isBrowser) {
            try {
                console.log(this.data);
                var paymentOptions_1 = this.payment.getOptions(this.data, this.total_cost || 0);
                this.paymentMethodData = paymentOptions_1.paymentMethodData;
                this.paymentDetails = paymentOptions_1.paymentDetails;
                this.paymentOptions = paymentOptions_1.paymentOptions;
                console.log(paymentOptions_1);
                var pr = new PaymentRequest([paymentOptions_1.paymentMethodData], paymentOptions_1.paymentDetails, paymentOptions_1.paymentOptions);
                console.log(pr);
                pr.canMakePayment().then(function (s) {
                    if (s === true && google) {
                        _this.supported = true;
                        var paymentsClient = new google.payments.api.PaymentsClient({
                            environment: paymentOptions_1.paymentMethodData.data.environment
                        });
                        var button = paymentsClient.createButton({
                            onClick: function () { return _this.buttonClick(); },
                            buttonType: 'short',
                            buttonColor: 'white'
                        });
                        _this.renderer.appendChild(_this.container.nativeElement, button);
                    }
                });
            }
            catch (err) {
                console.error(err);
            }
        }
    };
    GpayButtonComponent.prototype.buttonClick = function () {
        this.payment.checkout(this.data, this.total_cost, this.order, this.contacts);
    };
    GpayButtonComponent.ctorParameters = function () { return [
        { type: _services_payment_service__WEBPACK_IMPORTED_MODULE_2__["PaymentService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: _services_platform_service__WEBPACK_IMPORTED_MODULE_3__["PlatformService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('data'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GpayButtonComponent.prototype, "data", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], GpayButtonComponent.prototype, "total_cost", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GpayButtonComponent.prototype, "contacts", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GpayButtonComponent.prototype, "order", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GpayButtonComponent.prototype, "disabled", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('container', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], GpayButtonComponent.prototype, "container", void 0);
    GpayButtonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gpay-button',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gpay-button.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gpay-button/gpay-button.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gpay-button.component.styl */ "./src/app/components/gpay-button/gpay-button.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_payment_service__WEBPACK_IMPORTED_MODULE_2__["PaymentService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _services_platform_service__WEBPACK_IMPORTED_MODULE_3__["PlatformService"]])
    ], GpayButtonComponent);
    return GpayButtonComponent;
}());



/***/ }),

/***/ "./src/app/modules/payment/payment.module.ts":
/*!***************************************************!*\
  !*** ./src/app/modules/payment/payment.module.ts ***!
  \***************************************************/
/*! exports provided: PaymentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentModule", function() { return PaymentModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _pipes_payment_method_filter_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../pipes/payment_method_filter.pipe */ "./src/app/pipes/payment_method_filter.pipe.ts");
/* harmony import */ var _services_payment_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/payment.service */ "./src/app/services/payment.service.ts");
/* harmony import */ var _components_gpay_button_gpay_button_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/gpay-button/gpay-button.component */ "./src/app/components/gpay-button/gpay-button.component.ts");
/* harmony import */ var _components_applepay_button_applepay_button_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/applepay-button/applepay-button.component */ "./src/app/components/applepay-button/applepay-button.component.ts");







var PaymentModule = /** @class */ (function () {
    function PaymentModule() {
    }
    PaymentModule_1 = PaymentModule;
    PaymentModule.forRoot = function () {
        return {
            ngModule: PaymentModule_1,
            providers: [
                _services_payment_service__WEBPACK_IMPORTED_MODULE_4__["PaymentService"]
            ]
        };
    };
    var PaymentModule_1;
    PaymentModule = PaymentModule_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _components_gpay_button_gpay_button_component__WEBPACK_IMPORTED_MODULE_5__["GpayButtonComponent"],
                _pipes_payment_method_filter_pipe__WEBPACK_IMPORTED_MODULE_3__["PaymentMethodFilterPipe"],
                _components_applepay_button_applepay_button_component__WEBPACK_IMPORTED_MODULE_6__["ApplepayButtonComponent"]
            ],
            exports: [
                _pipes_payment_method_filter_pipe__WEBPACK_IMPORTED_MODULE_3__["PaymentMethodFilterPipe"],
                _components_gpay_button_gpay_button_component__WEBPACK_IMPORTED_MODULE_5__["GpayButtonComponent"],
                _components_applepay_button_applepay_button_component__WEBPACK_IMPORTED_MODULE_6__["ApplepayButtonComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            ],
        })
    ], PaymentModule);
    return PaymentModule;
}());



/***/ }),

/***/ "./src/app/pages/checkout/checkout.component.styl":
/*!********************************************************!*\
  !*** ./src/app/pages/checkout/checkout.component.styl ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".order_page {\n  min-height: 100vh;\n  padding: 96px 0 55px;\n  background-color: #f5f2ee;\n}\n.summary__head {\n  margin-bottom: 20px;\n}\n.summary__head svg-icon > svg {\n  width: 120px;\n  height: auto;\n}\n.summary__body-section {\n  margin-bottom: 28px;\n}\n.summary__body-section:last-child {\n  margin-bottom: 0;\n}\n.summary__form-row {\n  margin-bottom: 16px;\n}\n.summary__form-row:last-child {\n  margin-bottom: 0;\n}\n.summary__form-footer {\n  margin-top: 20px;\n}\n.summary__form-footer-row {\n  margin-bottom: 15px;\n}\n.summary__form-footer-row:last-child {\n  margin-bottom: 0;\n}\n.summary__form-buttons {\n  margin-top: 23px;\n  padding-top: 23px;\n  border-top: 1px solid rgba(160,148,136,0.18);\n}\n.summary__form-buttons-row {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 -8px -8px;\n}\n.summary__form-buttons-item {\n  display: flex;\n  flex: 1;\n  margin: 0 8px 8px;\n}\n.summary__form-buttons-item * {\n  display: flex;\n  width: 100%;\n}\n.summary__result-head {\n  margin-bottom: 20px;\n}\n.summary__result-head-row {\n  margin-bottom: 14px;\n}\n.summary__result-head-row:last-child {\n  margin-bottom: 0;\n}\n.summary__result-footer {\n  margin-top: 18px;\n}\n.summary__title {\n  font-size: 24px;\n  font-weight: 500;\n  line-height: 1.3;\n}\n.c-cart {\n  padding: 16px 16px 28px;\n  border-radius: 8px;\n  background-color: rgba(175,168,160,0.2);\n}\n.c-cart__card-row {\n  margin-bottom: -20px;\n}\n.c-cart__card-item {\n  margin-bottom: 20px;\n}\n.c-cart__card-item_card {\n  max-width: 124px;\n}\n.c-cart__head {\n  margin-bottom: 22px;\n}\n.c-cart__title {\n  font-size: 16px;\n  font-weight: 600;\n  line-height: 1.3;\n}\n.c-cart__table {\n  font-size: 14px;\n  font-weight: 500;\n  line-height: 1;\n  white-space: nowrap;\n}\n.c-cart__table-row {\n  display: flex;\n  justify-content: space-between;\n  margin: 0 -8px 22px;\n}\n.c-cart__table-row:last-child {\n  margin-bottom: 0;\n}\n.c-cart__table-td {\n  padding: 0 8px;\n}\n.c-cart__table-td_value {\n  color: #ff3838;\n}\n@media all and (min-width: 480px) {\n  .c-cart__card-row {\n    display: flex;\n  }\n  .c-cart__card-item {\n    flex: 0 1 auto;\n  }\n  .c-cart__card-item_card {\n    flex: 1 0 124px;\n    margin: 0 20px 20px 0;\n  }\n}\n/*# sourceMappingURL=src/app/pages/checkout/checkout.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY2hlY2tvdXQvc3JjL3N0eWxlcy9ibG9ja3Mvb3JkZXIuc3R5bCIsInNyYy9hcHAvcGFnZXMvY2hlY2tvdXQvY2hlY2tvdXQuY29tcG9uZW50LnN0eWwiLCJzcmMvYXBwL3BhZ2VzL2NoZWNrb3V0L3NyYy9zdHlsZXMvYmxvY2tzL3N1bW1hcnkuc3R5bCIsInNyYy9hcHAvcGFnZXMvY2hlY2tvdXQvc3JjL3N0eWxlcy9ibG9ja3MvYy1jYXJ0LnN0eWwiLCJzcmMvYXBwL3BhZ2VzL2NoZWNrb3V0L3NyYy9zdHlsZXMvaGVscGVycy5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsaUJBQVc7RUFDWCxvQkFBUztFQUNULHlCQUFrQjtBQ0F0QjtBQ0hFO0VBQ0UsbUJBQWU7QURLbkI7QUNKSTtFQUNFLFlBQU87RUFDUCxZQUFRO0FETWQ7QUNISTtFQUNFLG1CQUFlO0FES3JCO0FDSk07RUFDRSxnQkFBZTtBRE12QjtBQ0pJO0VBQ0UsbUJBQWU7QURNckI7QUNMTTtFQUNFLGdCQUFlO0FET3ZCO0FDTkk7RUFDRSxnQkFBWTtBRFFsQjtBQ1BNO0VBQ0UsbUJBQWU7QURTdkI7QUNSUTtFQUNFLGdCQUFlO0FEVXpCO0FDVEk7RUFDRSxnQkFBWTtFQUNaLGlCQUFhO0VBQ2IsNENBQVk7QURXbEI7QUNWTTtFQUNFLGFBQVM7RUFDVCxlQUFVO0VBQ1YsbUJBQVE7QURZaEI7QUNYTTtFQUNFLGFBQVM7RUFDVCxPQUFNO0VBQ04saUJBQVE7QURhaEI7QUNaUTtFQUNFLGFBQVM7RUFDVCxXQUFPO0FEY2pCO0FDWkk7RUFDRSxtQkFBZTtBRGNyQjtBQ2JNO0VBQ0UsbUJBQWU7QURldkI7QUNkUTtFQUNFLGdCQUFlO0FEZ0J6QjtBQ2ZJO0VBQ0UsZ0JBQVk7QURpQmxCO0FDaEJFO0VBQ0UsZUFBVztFQUNYLGdCQUFhO0VBQ2IsZ0JBQWE7QURrQmpCO0FFcEVBO0VBQ0UsdUJBQVM7RUFDVCxrQkFBZTtFQUNmLHVDQUFrQjtBRnNFcEI7QUVwRUk7RUFDRSxvQkFBZTtBRnNFckI7QUVyRUk7RUFDRSxtQkFBZTtBRnVFckI7QUV0RU07RUFDRSxnQkFBVTtBRndFbEI7QUV2RUU7RUFDRSxtQkFBZTtBRnlFbkI7QUV2RUU7RUFDRSxlQUFXO0VBQ1gsZ0JBQWE7RUFDYixnQkFBYTtBRnlFakI7QUV4RUU7RUFDRSxlQUFXO0VBQ1gsZ0JBQWE7RUFDYixjQUFhO0VBQ2IsbUJBQVk7QUYwRWhCO0FFekVJO0VBQ0UsYUFBUztFQUNULDhCQUFpQjtFQUNqQixtQkFBUTtBRjJFZDtBRTFFTTtFQUNFLGdCQUFlO0FGNEV2QjtBRTNFSTtFQUNFLGNBQVM7QUY2RWY7QUU1RU07RUFDRSxjQUFPO0FGOEVmO0FHakd1QztFRHdCakM7SUFDRSxhQUFTO0VGNEVmO0VFM0VJO0lBQ0UsY0FBTTtFRjZFWjtFRTVFTTtJQUNFLGVBQU07SUFDTixxQkFBUTtFRjhFaEI7QUFDRjtBQUNBLHdFQUF3RSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NoZWNrb3V0L2NoZWNrb3V0LmNvbXBvbmVudC5zdHlsIiwic291cmNlc0NvbnRlbnQiOlsiLm9yZGVyXHJcbiAgJl9wYWdlXHJcbiAgICBtaW4taGVpZ2h0IDEwMHZoXHJcbiAgICBwYWRkaW5nOiA5NnB4IDAgNTVweFxyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGMtc2VjdGlvbi0xLXRoZW1lLTEtYmc7XHJcbiIsIi5vcmRlcl9wYWdlIHtcbiAgbWluLWhlaWdodDogMTAwdmg7XG4gIHBhZGRpbmc6IDk2cHggMCA1NXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmMmVlO1xufVxuLnN1bW1hcnlfX2hlYWQge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuLnN1bW1hcnlfX2hlYWQgc3ZnLWljb24gPiBzdmcge1xuICB3aWR0aDogMTIwcHg7XG4gIGhlaWdodDogYXV0bztcbn1cbi5zdW1tYXJ5X19ib2R5LXNlY3Rpb24ge1xuICBtYXJnaW4tYm90dG9tOiAyOHB4O1xufVxuLnN1bW1hcnlfX2JvZHktc2VjdGlvbjpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5zdW1tYXJ5X19mb3JtLXJvdyB7XG4gIG1hcmdpbi1ib3R0b206IDE2cHg7XG59XG4uc3VtbWFyeV9fZm9ybS1yb3c6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4uc3VtbWFyeV9fZm9ybS1mb290ZXIge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLnN1bW1hcnlfX2Zvcm0tZm9vdGVyLXJvdyB7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG4uc3VtbWFyeV9fZm9ybS1mb290ZXItcm93Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLnN1bW1hcnlfX2Zvcm0tYnV0dG9ucyB7XG4gIG1hcmdpbi10b3A6IDIzcHg7XG4gIHBhZGRpbmctdG9wOiAyM3B4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgxNjAsMTQ4LDEzNiwwLjE4KTtcbn1cbi5zdW1tYXJ5X19mb3JtLWJ1dHRvbnMtcm93IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBtYXJnaW46IDAgLThweCAtOHB4O1xufVxuLnN1bW1hcnlfX2Zvcm0tYnV0dG9ucy1pdGVtIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleDogMTtcbiAgbWFyZ2luOiAwIDhweCA4cHg7XG59XG4uc3VtbWFyeV9fZm9ybS1idXR0b25zLWl0ZW0gKiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xufVxuLnN1bW1hcnlfX3Jlc3VsdC1oZWFkIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5zdW1tYXJ5X19yZXN1bHQtaGVhZC1yb3cge1xuICBtYXJnaW4tYm90dG9tOiAxNHB4O1xufVxuLnN1bW1hcnlfX3Jlc3VsdC1oZWFkLXJvdzpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5zdW1tYXJ5X19yZXN1bHQtZm9vdGVyIHtcbiAgbWFyZ2luLXRvcDogMThweDtcbn1cbi5zdW1tYXJ5X190aXRsZSB7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbn1cbi5jLWNhcnQge1xuICBwYWRkaW5nOiAxNnB4IDE2cHggMjhweDtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDE3NSwxNjgsMTYwLDAuMik7XG59XG4uYy1jYXJ0X19jYXJkLXJvdyB7XG4gIG1hcmdpbi1ib3R0b206IC0yMHB4O1xufVxuLmMtY2FydF9fY2FyZC1pdGVtIHtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5jLWNhcnRfX2NhcmQtaXRlbV9jYXJkIHtcbiAgbWF4LXdpZHRoOiAxMjRweDtcbn1cbi5jLWNhcnRfX2hlYWQge1xuICBtYXJnaW4tYm90dG9tOiAyMnB4O1xufVxuLmMtY2FydF9fdGl0bGUge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjM7XG59XG4uYy1jYXJ0X190YWJsZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGluZS1oZWlnaHQ6IDE7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG4uYy1jYXJ0X190YWJsZS1yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIG1hcmdpbjogMCAtOHB4IDIycHg7XG59XG4uYy1jYXJ0X190YWJsZS1yb3c6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4uYy1jYXJ0X190YWJsZS10ZCB7XG4gIHBhZGRpbmc6IDAgOHB4O1xufVxuLmMtY2FydF9fdGFibGUtdGRfdmFsdWUge1xuICBjb2xvcjogI2ZmMzgzODtcbn1cbkBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6IDQ4MHB4KSB7XG4gIC5jLWNhcnRfX2NhcmQtcm93IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG4gIC5jLWNhcnRfX2NhcmQtaXRlbSB7XG4gICAgZmxleDogMCAxIGF1dG87XG4gIH1cbiAgLmMtY2FydF9fY2FyZC1pdGVtX2NhcmQge1xuICAgIGZsZXg6IDEgMCAxMjRweDtcbiAgICBtYXJnaW46IDAgMjBweCAyMHB4IDA7XG4gIH1cbn1cbi8qIyBzb3VyY2VNYXBwaW5nVVJMPXNyYy9hcHAvcGFnZXMvY2hlY2tvdXQvY2hlY2tvdXQuY29tcG9uZW50LmNzcy5tYXAgKi8iLCIuc3VtbWFyeVxyXG4gICZfX2hlYWRcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHhcclxuICAgIHN2Zy1pY29uID4gc3ZnXHJcbiAgICAgIHdpZHRoOiAxMjBweFxyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcblxyXG4gICZfX2JvZHlcclxuICAgICYtc2VjdGlvblxyXG4gICAgICBtYXJnaW4tYm90dG9tOiAyOHB4XHJcbiAgICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuICAmX19mb3JtXHJcbiAgICAmLXJvd1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxNnB4XHJcbiAgICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuICAgICYtZm9vdGVyXHJcbiAgICAgIG1hcmdpbi10b3A6IDIwcHhcclxuICAgICAgJi1yb3dcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxNXB4XHJcbiAgICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwXHJcbiAgICAmLWJ1dHRvbnNcclxuICAgICAgbWFyZ2luLXRvcDogMjNweFxyXG4gICAgICBwYWRkaW5nLXRvcDogMjNweFxyXG4gICAgICBib3JkZXItdG9wOiAxcHggc29saWQgJGMtaHItMTtcclxuICAgICAgJi1yb3dcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtd3JhcCB3cmFwXHJcbiAgICAgICAgbWFyZ2luOiAwIC04cHggLThweFxyXG4gICAgICAmLWl0ZW1cclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXg6IDFcclxuICAgICAgICBtYXJnaW46IDAgOHB4IDhweFxyXG4gICAgICAgICYgKlxyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlXHJcbiAgJl9fcmVzdWx0XHJcbiAgICAmLWhlYWRcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweFxyXG4gICAgICAmLXJvd1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE0cHhcclxuICAgICAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuICAgICYtZm9vdGVyXHJcbiAgICAgIG1hcmdpbi10b3A6IDE4cHhcclxuICAmX190aXRsZVxyXG4gICAgZm9udC1zaXplOiAyNHB4XHJcbiAgICBmb250LXdlaWdodDogNTAwXHJcbiAgICBsaW5lLWhlaWdodDogMS4zXHJcbiIsIi5jLWNhcnRcclxuICBwYWRkaW5nOiAxNnB4IDE2cHggMjhweFxyXG4gIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkYy1zZWN0aW9uLTItdGhlbWUtMS1iZztcclxuICAmX19jYXJkXHJcbiAgICAmLXJvd1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAtMjBweFxyXG4gICAgJi1pdGVtXHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgICZfY2FyZFxyXG4gICAgICAgIG1heC13aWR0aCAxMjRweFxyXG4gICZfX2hlYWRcclxuICAgIG1hcmdpbi1ib3R0b206IDIycHhcclxuXHJcbiAgJl9fdGl0bGVcclxuICAgIGZvbnQtc2l6ZTogMTZweFxyXG4gICAgZm9udC13ZWlnaHQ6IDYwMFxyXG4gICAgbGluZS1oZWlnaHQ6IDEuM1xyXG4gICZfX3RhYmxlXHJcbiAgICBmb250LXNpemU6IDE0cHhcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDBcclxuICAgIGxpbmUtaGVpZ2h0OiAxXHJcbiAgICB3aGl0ZS1zcGFjZSBub3dyYXBcclxuICAgICYtcm93XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgbWFyZ2luOiAwIC04cHggMjJweFxyXG4gICAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwXHJcbiAgICAmLXRkXHJcbiAgICAgIHBhZGRpbmc6IDAgOHB4XHJcbiAgICAgICZfdmFsdWVcclxuICAgICAgICBjb2xvcjogJGMtMztcclxuXHJcbitzbWFsbF8yKClcclxuICAuYy1jYXJ0XHJcbiAgICAmX19jYXJkXHJcbiAgICAgICYtcm93XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgJi1pdGVtXHJcbiAgICAgICAgZmxleDogMCAxIGF1dG9cclxuICAgICAgICAmX2NhcmRcclxuICAgICAgICAgIGZsZXg6IDEgMCAxMjRweFxyXG4gICAgICAgICAgbWFyZ2luOiAwIDIwcHggMjBweCAwXHJcbiIsIm1vYmlsZSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogJHdfbSlcclxuICAgIHtibG9ja31cclxuZGVza3RvcCgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfZClcclxuICAgIHtibG9ja31cclxubWluX2luaXRfZCgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfaW5pdF9kKVxyXG4gICAge2Jsb2NrfVxyXG5zbWFsbCgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfc21hbGwpXHJcbiAgICB7YmxvY2t9XHJcbnNtYWxsXzIoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X3NtYWxsXzIpXHJcbiAgICB7YmxvY2t9XHJcbm1lZGl1bSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfbWVkaXVtKVxyXG4gICAge2Jsb2NrfVxyXG5sYXJnZSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfbGFyZ2UpXHJcbiAgICB7YmxvY2t9XHJcbmV4dHJhX2xhcmdlKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19leHRyYV9sYXJnZSlcclxuICAgIHtibG9ja31cclxubWF4X3coKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X21heF9wYWdlX3dpZHRoKVxyXG4gICAge2Jsb2NrfVxyXG5cclxuaF9tZWRpdW0oKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4taGVpZ2h0OiAkaF9tZWRpdW0pXHJcbiAgICB7YmxvY2t9XHJcblxyXG5wbGFjZWhvbGRlcigpXHJcbiAgJjo6cGxhY2Vob2xkZXJcclxuICAgIHtibG9ja31cclxuXHJcbmF1dG9jb21wbGV0ZSgpXHJcbiAgJjotd2Via2l0LWF1dG9maWxsLFxyXG4gICY6LXdlYmtpdC1hdXRvZmlsbDpob3ZlcixcclxuICAmOi13ZWJraXQtYXV0b2ZpbGw6Zm9jdXMsXHJcbiAgJjotd2Via2l0LWF1dG9maWxsOmFjdGl2ZSxcclxuICAmOi1pbnRlcm5hbC1hdXRvZmlsbC1wcmV2aWV3ZWQsXHJcbiAgJjotaW50ZXJuYWwtYXV0b2ZpbGwtc2VsZWN0ZWRcclxuICAgIHtibG9ja31cclxuXHJcbl9jYWxjKCRwZXIsICR2YWwpXHJcbiAgcmV0dXJuIFwiY2FsYyglcyAtICVzKVwiICUgKCRwZXIgJHZhbClcclxuIl19 */");

/***/ }),

/***/ "./src/app/pages/checkout/checkout.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/checkout/checkout.component.ts ***!
  \******************************************************/
/*! exports provided: CheckoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutComponent", function() { return CheckoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/storage.service */ "./src/app/services/storage.service.ts");
/* harmony import */ var _services_form_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/form.service */ "./src/app/services/form.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var _services_payment_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/payment.service */ "./src/app/services/payment.service.ts");
/* harmony import */ var _services_doc_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/doc.service */ "./src/app/services/doc.service.ts");
/* harmony import */ var _components_form_helper_form_helper_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/form-helper/form-helper.component */ "./src/app/components/form-helper/form-helper.component.ts");
/* harmony import */ var _services_logger_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/logger.service */ "./src/app/services/logger.service.ts");










var CheckoutComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CheckoutComponent, _super);
    function CheckoutComponent(storage, formService, api, payment, doc, logger) {
        var _this = _super.call(this) || this;
        _this.storage = storage;
        _this.formService = formService;
        _this.api = api;
        _this.payment = payment;
        _this.doc = doc;
        _this.logger = logger;
        _this.initialMd = { client: {} };
        _this.total_cost = 0;
        _this.fetching = false;
        _this.formName = 'checkoutForm';
        _this.formSubmitted = false;
        _this.subscriptions = [];
        return _this;
    }
    CheckoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        //const savedData: ICertificateBody = this.formService.getFormValues(this.formName);
        this.api.getGiftPaymentMethod().subscribe(function (methods) {
            _this.methods = methods;
            console.log(_this.methods);
        });
        var order = this.storage.getItem('cert_order');
        this.order = order ? JSON.parse(order) : { data: {} };
        this.initialMd.client = {
            email: '',
            phone: ''
        };
        this.md = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.initialMd);
        console.log(this.md);
        this.total_cost = this.order.total_cost;
        this.logger.l(['cart_data', this.md]);
        this.logger.l(['order', this.order]);
    };
    CheckoutComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.map(function (s) {
            s.unsubscribe();
        });
    };
    CheckoutComponent.prototype.onSubmit = function () {
    };
    CheckoutComponent.prototype.onBtnClick = function (pm) {
        var _this = this;
        this.formService.markFormGroupTouched(this._form);
        this.formSubmitted = true;
        if (this._form.invalid) {
            return;
        }
        this.fetching = true;
        this.formService.saveFormValues(this.formName, this.md);
        this.payment.checkout(pm, this.total_cost, this.order, {
            email: this.md.client.email,
            phone: this.md.client.phone
        }).finally(function () {
            setTimeout(function () {
                _this.fetching = false;
            }, 1000);
        });
    };
    CheckoutComponent.ctorParameters = function () { return [
        { type: _services_storage_service__WEBPACK_IMPORTED_MODULE_2__["StorageService"] },
        { type: _services_form_service__WEBPACK_IMPORTED_MODULE_3__["FormService"] },
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_5__["TransportService"] },
        { type: _services_payment_service__WEBPACK_IMPORTED_MODULE_6__["PaymentService"] },
        { type: _services_doc_service__WEBPACK_IMPORTED_MODULE_7__["DocService"] },
        { type: _services_logger_service__WEBPACK_IMPORTED_MODULE_9__["LoggerService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('form', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"])
    ], CheckoutComponent.prototype, "_form", void 0);
    CheckoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-checkout',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./checkout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/checkout/checkout.component.html")).default,
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./checkout.component.styl */ "./src/app/pages/checkout/checkout.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_storage_service__WEBPACK_IMPORTED_MODULE_2__["StorageService"],
            _services_form_service__WEBPACK_IMPORTED_MODULE_3__["FormService"],
            _services_transport_service__WEBPACK_IMPORTED_MODULE_5__["TransportService"],
            _services_payment_service__WEBPACK_IMPORTED_MODULE_6__["PaymentService"],
            _services_doc_service__WEBPACK_IMPORTED_MODULE_7__["DocService"],
            _services_logger_service__WEBPACK_IMPORTED_MODULE_9__["LoggerService"]])
    ], CheckoutComponent);
    return CheckoutComponent;
}(_components_form_helper_form_helper_component__WEBPACK_IMPORTED_MODULE_8__["FormHelperComponent"]));



/***/ }),

/***/ "./src/app/pages/checkout/checkout.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/checkout/checkout.module.ts ***!
  \***************************************************/
/*! exports provided: ROUTES, CheckoutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutModule", function() { return CheckoutModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var angular_svg_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-svg-icon */ "./node_modules/angular-svg-icon/fesm5/angular-svg-icon.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _checkout_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./checkout.component */ "./src/app/pages/checkout/checkout.component.ts");
/* harmony import */ var _validators_validators_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../validators/validators.module */ "./src/app/validators/validators.module.ts");
/* harmony import */ var _modules_payment_payment_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../modules/payment/payment.module */ "./src/app/modules/payment/payment.module.ts");











var ROUTES = [{ path: '', component: _checkout_component__WEBPACK_IMPORTED_MODULE_8__["CheckoutComponent"] }];
var CheckoutModule = /** @class */ (function () {
    function CheckoutModule() {
    }
    CheckoutModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _checkout_component__WEBPACK_IMPORTED_MODULE_8__["CheckoutComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES),
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                angular_svg_icon__WEBPACK_IMPORTED_MODULE_6__["AngularSvgIconModule"],
                _validators_validators_module__WEBPACK_IMPORTED_MODULE_9__["ValidatorsModule"],
                _modules_payment_payment_module__WEBPACK_IMPORTED_MODULE_10__["PaymentModule"]
            ],
        })
    ], CheckoutModule);
    return CheckoutModule;
}());



/***/ }),

/***/ "./src/app/pipes/payment_method_filter.pipe.ts":
/*!*****************************************************!*\
  !*** ./src/app/pipes/payment_method_filter.pipe.ts ***!
  \*****************************************************/
/*! exports provided: PaymentMethodFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentMethodFilterPipe", function() { return PaymentMethodFilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ramda */ "./node_modules/ramda/es/index.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/fesm5/ngx-device-detector.js");




var PaymentMethodFilterPipe = /** @class */ (function () {
    function PaymentMethodFilterPipe(device) {
        this.device = device;
    }
    PaymentMethodFilterPipe.prototype.transform = function (payment_methods, type) {
        var _this = this;
        // console.log('PM PIPE', payment_methods, type);
        if (Object(ramda__WEBPACK_IMPORTED_MODULE_2__["isNil"])(payment_methods) || Object(ramda__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(payment_methods)) {
            return [];
        }
        var filtered = payment_methods.slice().filter(function (p) {
            if (p.type === 'applepay') {
                return _this.canMakePayment();
                // && !this.device.isDesktop()
            }
            if (p.type === 'gpay' && _this.device.browser.toLowerCase() === 'safari') {
                return null;
            }
            return p;
        });
        if (type === '_custom') {
            return filtered.filter(function (p) { return p.type !== 'common'; });
        }
        return filtered.filter(function (el) { return el.type === type; });
    };
    PaymentMethodFilterPipe.prototype.canMakePayment = function () {
        var canMakePayment = false;
        try {
            canMakePayment =
                window['PaymentRequest'] &&
                    window['ApplePaySession'] &&
                    window['ApplePaySession'].canMakePayments &&
                    window['ApplePaySession'].canMakePayments();
        }
        catch (err) {
            console.error(err);
            return false;
        }
        return canMakePayment;
    };
    PaymentMethodFilterPipe.ctorParameters = function () { return [
        { type: ngx_device_detector__WEBPACK_IMPORTED_MODULE_3__["DeviceDetectorService"] }
    ]; };
    PaymentMethodFilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'payment_method_filter',
            pure: false,
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_device_detector__WEBPACK_IMPORTED_MODULE_3__["DeviceDetectorService"]])
    ], PaymentMethodFilterPipe);
    return PaymentMethodFilterPipe;
}());



/***/ }),

/***/ "./src/app/services/payment.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/payment.service.ts ***!
  \*********************************************/
/*! exports provided: PaymentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentService", function() { return PaymentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ramda */ "./node_modules/ramda/es/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _transport_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_internal_compatibility__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/internal-compatibility */ "./node_modules/rxjs/_esm5/internal-compatibility/index.js");







var PaymentService = /** @class */ (function () {
    function PaymentService(api) {
        this.api = api;
        this.label = 'Zapomni.Gift';
        this.appleMethods = 'https://apple.com/apple-pay';
        this.googleMethods = 'https://google.com/pay';
        this.paymentDetails = {
            total: {
                label: this.label,
            },
        };
        this.googlePaymentsConfiguration = {
            environment: 'TEST',
            apiVersion: 2,
            apiVersionMinor: 0,
            merchantInfo: {
                // A merchant ID is available after approval by Google.
                merchantId: undefined,
                merchantName: undefined,
            },
            transactionInfo: {
                countryCode: 'RU',
                totalPriceStatus: 'FINAL',
                totalPrice: undefined,
                currencyCode: 'RU',
            },
        };
    }
    PaymentService.prototype.setOptions = function (method, total_cost, contacts) {
        if (contacts === void 0) { contacts = {}; }
        if (method) {
            var _a = this.getOptions(method, total_cost) || {}, paymentOptions = _a.paymentOptions, paymentDetails = _a.paymentDetails, paymentMethodData = _a.paymentMethodData, googlePaymentsConfiguration = _a.googlePaymentsConfiguration;
            if (!contacts.phone) {
                paymentOptions['requestPayerPhone'] = true;
            }
            if (!contacts.email) {
                paymentOptions['requestPayerEmail'] = true;
            }
            this.paymentDetails = paymentDetails;
            this.paymentMethodData = paymentMethodData;
            this.paymentOptions = paymentOptions;
            this.googlePaymentsConfiguration = googlePaymentsConfiguration;
        }
    };
    PaymentService.prototype.getOptions = function (method, total_cost) {
        console.log(method);
        if (method) {
            var paymentOptions = {};
            var currencyCode = Object(ramda__WEBPACK_IMPORTED_MODULE_2__["path"])(['currency', 'alias'], method.ext_provider_data) || 'RUR';
            var paymentDetails = {
                total: {
                    label: this.label,
                    amount: {
                        value: (total_cost * 0.01).toFixed(2),
                        currency: currencyCode,
                    },
                },
            };
            var settings = Object(ramda__WEBPACK_IMPORTED_MODULE_2__["prop"])('params', method.ext_provider_data);
            var paymentMethodData = {
                data: Object.assign({ currencyCode: currencyCode, total: paymentDetails.total }, Object(ramda__WEBPACK_IMPORTED_MODULE_2__["prop"])('params', method.ext_provider_data)),
                supportedMethods: this.appleMethods,
            };
            var googlePaymentsConfiguration = void 0;
            if (method.type === 'gpay') {
                googlePaymentsConfiguration = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.googlePaymentsConfiguration, settings.options, { merchantInfo: {
                        // A merchant ID is available after approval by Google.
                        merchantId: settings.merchant.id || undefined,
                        merchantName: settings.merchant.name,
                    } });
                googlePaymentsConfiguration.transactionInfo.totalPrice = (total_cost * 0.01).toFixed(2);
                googlePaymentsConfiguration.transactionInfo.currencyCode = currencyCode;
                paymentMethodData = {
                    supportedMethods: 'https://google.com/pay',
                    data: googlePaymentsConfiguration,
                };
            }
            return { paymentOptions: paymentOptions, paymentDetails: paymentDetails, paymentMethodData: paymentMethodData, googlePaymentsConfiguration: googlePaymentsConfiguration };
        }
        else {
            return null;
        }
    };
    PaymentService.prototype.pay = function (method, total_cost, contacts) {
        var _this = this;
        if (contacts === void 0) { contacts = {}; }
        if (method && method.type !== 'common') {
            this.setOptions(method, total_cost, contacts);
            try {
                this.session = new PaymentRequest([this.paymentMethodData], this.paymentDetails, this.paymentOptions);
                if (method.type === 'applepay') {
                    this.session['onmerchantvalidation'] = function (event) {
                        var validationURL = event.validationURL;
                        _this.api.validateMerchant(method._uuid, { validationURL: validationURL })
                            .toPromise()
                            .then(function (response) {
                            event.complete(response);
                        })
                            .catch(function (e) {
                            _this.session.abort();
                        });
                    };
                }
                return Object(rxjs_internal_compatibility__WEBPACK_IMPORTED_MODULE_6__["fromPromise"])(this.session.show()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (response) {
                    if (response.payerPhone) {
                        contacts.phone = response.payerPhone;
                    }
                    if (response.payerEmail) {
                        contacts.email = response.payerEmail;
                    }
                    var token = Object(ramda__WEBPACK_IMPORTED_MODULE_2__["path"])(['details', 'token', 'paymentData'], response) || JSON.parse(Object(ramda__WEBPACK_IMPORTED_MODULE_2__["path"])(['details', 'paymentMethodData', 'tokenizationData', 'token'], response) || null);
                    var _data = {
                        contacts: contacts,
                        payment_method: method._uuid,
                        payment_token: token,
                        response: response,
                    };
                    return _data;
                }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (e) {
                    console.error('REJECTED', e);
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(e);
                }));
            }
            catch (e) {
                try {
                    if (this.session) {
                        this.session.abort();
                    }
                }
                catch (e) {
                    console.error(e);
                }
            }
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(null);
    };
    PaymentService.prototype.checkout = function (pm, total_cost, order, contacts) {
        var _this = this;
        contacts = contacts || {};
        return new Promise(function (resolve, reject) {
            _this.pay(pm, total_cost, {
                phone: contacts.phone,
                email: contacts.email,
            }).subscribe(function (data) {
                var orderData = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, order.data, { client: { email: contacts.email, phone: contacts.phone } });
                var cart_data = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, orderData, { payment_method: pm._uuid });
                var customPay = data && data.response ? data.response : null;
                if (customPay) {
                    if (!Object(ramda__WEBPACK_IMPORTED_MODULE_2__["isNil"])(data.response.payerPhone) && !Object(ramda__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(data.response.payerPhone)) {
                        cart_data.client.phone = data.response.payerPhone;
                    }
                    if (!Object(ramda__WEBPACK_IMPORTED_MODULE_2__["isNil"])(data.response.payerEmail) && !Object(ramda__WEBPACK_IMPORTED_MODULE_2__["isEmpty"])(data.response.payerEmail)) {
                        cart_data.client.email = data.response.payerEmail;
                    }
                    cart_data.payment_token = data.payment_token;
                }
                _this.api.newOrder(JSON.stringify(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, cart_data))).subscribe(function (res) {
                    var payment = res.payment;
                    var redirectType = Object(ramda__WEBPACK_IMPORTED_MODULE_2__["path"])(['payment_redirect', 'type'], payment);
                    if (redirectType === 'acs') {
                        var acsUrl = Object(ramda__WEBPACK_IMPORTED_MODULE_2__["path"])(['payment_redirect', 'acsUrl'], payment);
                        var acsMethod = Object(ramda__WEBPACK_IMPORTED_MODULE_2__["path"])(['payment_redirect', 'acsMethod'], payment);
                        var acsData_1 = Object(ramda__WEBPACK_IMPORTED_MODULE_2__["path"])(['payment_redirect', 'acsParam'], payment);
                        var acsFields = Object.keys(acsData_1).reduce(function (acc, fieldName) {
                            acc.push({
                                name: fieldName,
                                value: acsData_1[fieldName]
                            });
                            return acc;
                        }, []);
                        if (customPay) {
                            customPay.complete('success');
                        }
                        resolve();
                        _this.redirectWithPost(acsUrl, acsFields);
                        return;
                    }
                    setTimeout(function () {
                        resolve();
                        window.location.href = res.payment.payment_url;
                    }, 250);
                }, function () {
                    if (customPay) {
                        customPay.complete('fail');
                    }
                    reject();
                });
            }, function () {
                reject();
            });
        });
    };
    PaymentService.prototype.redirectWithPost = function (url, fields) {
        var form = document.createElement('form');
        form.method = 'POST';
        form.action = url;
        if (fields) {
            fields.map(function (field) {
                form.innerHTML += "<input type=\"hidden\" name=\"" + field.name + "\" value=\"" + field.value + "\" />";
            });
        }
        document.body.appendChild(form);
        form.submit();
    };
    PaymentService.ctorParameters = function () { return [
        { type: _transport_service__WEBPACK_IMPORTED_MODULE_4__["TransportService"] }
    ]; };
    PaymentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_transport_service__WEBPACK_IMPORTED_MODULE_4__["TransportService"]])
    ], PaymentService);
    return PaymentService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-checkout-checkout-module.js.map