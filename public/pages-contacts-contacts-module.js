(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contacts-contacts-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contacts/contacts.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contacts/contacts.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-layout>\r\n  <div class=\"section section_bg-2 section_sz-6 section_theme-2\">\r\n    <div class=\"contacts\">\r\n      <div class=\"contacts__row\">\r\n        <a href=\"tel: +79037984478\" class=\"contacts__text\">\r\n          +7 903 798 44 78\r\n        </a>\r\n      </div>\r\n      <div class=\"contacts__row\">\r\n        <a href=\"mailto: info@zapomni.gift\" class=\"contacts__link\">info@zapomni.gift</a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</app-layout>\r\n");

/***/ }),

/***/ "./src/app/pages/contacts/contacts.component.styl":
/*!********************************************************!*\
  !*** ./src/app/pages/contacts/contacts.component.styl ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":root {\n  --skin-text: #fff;\n  --skin-card-text: #fff;\n  --skin-active-color: #fff;\n  --skin-gradient-angle-1: rgba(255,255,255,0.3);\n  --skin-gradient-angle-2: transparent;\n  --skin-gradient-bottom-1: transparent;\n  --skin-gradient-bottom-2: #311004;\n  --skin-page-bg: #311004;\n  --skin-page-gradient-bottom-1: rgba(28,12,5,0.55);\n  --skin-page-gradient-bottom-2: rgba(87,26,4,0.11);\n  --skin-page-gradient-bottom-3: #311004;\n  --skin-btn-bg: rgba(255,255,255,0.1);\n  --skin-btn-text: rgba(255,255,255,0.6);\n  --skin-btn-bg-hover: rgba(255,255,255,0.2);\n  --skin-btn-text-hover: rgba(255,255,255,0.6);\n  --skin-btn-bg-active: #fff;\n  --skin-btn-text-active: #311004;\n  --skin-btn-2-bg: rgba(173,167,160,0.2);\n  --skin-btn-2-text: #857667;\n  --skin-btn-2-bg-hover: rgba(173,167,160,0.4);\n  --skin-btn-2-text-hover: #857667;\n  --skin-btn-2-bg-active: #bda776;\n  --skin-btn-2-text-active: #fff;\n  --skin-header-logo-w-1: #fff;\n  --skin-header-logo-w-2: #bda776;\n  --skin-tab-text: #afa8a0;\n  --skin-tab-text-active: #000;\n  --skin-tab-bd: #bda776;\n  --skin-fee-nominal: #a09488;\n}\n.contacts {\n  font-size: 22px;\n  font-weight: 600;\n}\n.contacts__link {\n  color: #bda776;\n}\n@media all and (min-width: 768px) {\n  .contacts {\n    font-size: 45px;\n  }\n}\n/*# sourceMappingURL=src/app/pages/contacts/contacts.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGFjdHMvc3JjL3N0eWxlcy92YXJpYWJsZXMuc3R5bCIsInNyYy9hcHAvcGFnZXMvY29udGFjdHMvY29udGFjdHMuY29tcG9uZW50LnN0eWwiLCJzcmMvYXBwL3BhZ2VzL2NvbnRhY3RzL3NyYy9zdHlsZXMvcGFnZXMvY29udGFjdHMuc3R5bCIsInNyYy9hcHAvcGFnZXMvY29udGFjdHMvc3JjL3N0eWxlcy9oZWxwZXJzLnN0eWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBYTtFQUNiLHNCQUFrQjtFQUNsQix5QkFBcUI7RUFDckIsOENBQTZDO0VBQzdDLG9DQUF5QjtFQUN6QixxQ0FBMEI7RUFDMUIsaUNBQTBCO0VBQzFCLHVCQUFnQjtFQUNoQixpREFBZ0Q7RUFDaEQsaURBQWdEO0VBQ2hELHNDQUErQjtFQUMvQixvQ0FBbUM7RUFDbkMsc0NBQXFDO0VBQ3JDLDBDQUF5QztFQUN6Qyw0Q0FBMkM7RUFDM0MsMEJBQXNCO0VBQ3RCLCtCQUF3QjtFQUN4QixzQ0FBcUM7RUFDckMsMEJBQW1CO0VBQ25CLDRDQUEyQztFQUMzQyxnQ0FBeUI7RUFDekIsK0JBQXdCO0VBQ3hCLDhCQUEwQjtFQUMxQiw0QkFBd0I7RUFDeEIsK0JBQXdCO0VBQ3hCLHdCQUFpQjtFQUNqQiw0QkFBd0I7RUFDeEIsc0JBQWU7RUFDZiwyQkFBb0I7QUNDdEI7QUMxQkE7RUFDRSxlQUFXO0VBQ1gsZ0JBQWE7QUQ0QmY7QUMzQkU7RUFDRSxjQUFPO0FENkJYO0FFckJzQztFRExwQztJQUNFLGVBQVc7RUQ2QmI7QUFDRjtBQUNBLHdFQUF3RSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbnRhY3RzL2NvbnRhY3RzLmNvbXBvbmVudC5zdHlsIiwic291cmNlc0NvbnRlbnQiOlsiOnJvb3Qge1xyXG4gIC0tc2tpbi10ZXh0OiAjZmZmO1xyXG4gIC0tc2tpbi1jYXJkLXRleHQ6ICNmZmY7XHJcbiAgLS1za2luLWFjdGl2ZS1jb2xvcjogI2ZmZjtcclxuICAtLXNraW4tZ3JhZGllbnQtYW5nbGUtMTogcmdiYSgyNTUsMjU1LDI1NSwwLjMpO1xyXG4gIC0tc2tpbi1ncmFkaWVudC1hbmdsZS0yOiB0cmFuc3BhcmVudDtcclxuICAtLXNraW4tZ3JhZGllbnQtYm90dG9tLTE6IHRyYW5zcGFyZW50O1xyXG4gIC0tc2tpbi1ncmFkaWVudC1ib3R0b20tMjogIzMxMTAwNDtcclxuICAtLXNraW4tcGFnZS1iZzogIzMxMTAwNCA7XHJcbiAgLS1za2luLXBhZ2UtZ3JhZGllbnQtYm90dG9tLTE6IHJnYmEoMjgsMTIsNSwwLjU1KTtcclxuICAtLXNraW4tcGFnZS1ncmFkaWVudC1ib3R0b20tMjogcmdiYSg4NywyNiw0LDAuMTEpO1xyXG4gIC0tc2tpbi1wYWdlLWdyYWRpZW50LWJvdHRvbS0zOiAjMzExMDA0O1xyXG4gIC0tc2tpbi1idG4tYmc6IHJnYmEoMjU1LDI1NSwyNTUsMC4xKTtcclxuICAtLXNraW4tYnRuLXRleHQ6IHJnYmEoMjU1LDI1NSwyNTUsMC42KTtcclxuICAtLXNraW4tYnRuLWJnLWhvdmVyOiByZ2JhKDI1NSwyNTUsMjU1LDAuMik7XHJcbiAgLS1za2luLWJ0bi10ZXh0LWhvdmVyOiByZ2JhKDI1NSwyNTUsMjU1LDAuNik7XHJcbiAgLS1za2luLWJ0bi1iZy1hY3RpdmU6ICNmZmY7XHJcbiAgLS1za2luLWJ0bi10ZXh0LWFjdGl2ZTogIzMxMTAwNDtcclxuICAtLXNraW4tYnRuLTItYmc6IHJnYmEoMTczLDE2NywxNjAsMC4yKTtcclxuICAtLXNraW4tYnRuLTItdGV4dDogIzg1NzY2NztcclxuICAtLXNraW4tYnRuLTItYmctaG92ZXI6IHJnYmEoMTczLDE2NywxNjAsMC40KTtcclxuICAtLXNraW4tYnRuLTItdGV4dC1ob3ZlcjogIzg1NzY2NztcclxuICAtLXNraW4tYnRuLTItYmctYWN0aXZlOiAjYmRhNzc2O1xyXG4gIC0tc2tpbi1idG4tMi10ZXh0LWFjdGl2ZTogI2ZmZjtcclxuICAtLXNraW4taGVhZGVyLWxvZ28tdy0xOiAjZmZmO1xyXG4gIC0tc2tpbi1oZWFkZXItbG9nby13LTI6ICNiZGE3NzY7XHJcbiAgLS1za2luLXRhYi10ZXh0OiAjYWZhOGEwO1xyXG4gIC0tc2tpbi10YWItdGV4dC1hY3RpdmU6ICMwMDA7XHJcbiAgLS1za2luLXRhYi1iZDogI2JkYTc3NjtcclxuICAtLXNraW4tZmVlLW5vbWluYWw6ICNhMDk0ODg7XHJcbn1cclxuIiwiOnJvb3Qge1xuICAtLXNraW4tdGV4dDogI2ZmZjtcbiAgLS1za2luLWNhcmQtdGV4dDogI2ZmZjtcbiAgLS1za2luLWFjdGl2ZS1jb2xvcjogI2ZmZjtcbiAgLS1za2luLWdyYWRpZW50LWFuZ2xlLTE6IHJnYmEoMjU1LDI1NSwyNTUsMC4zKTtcbiAgLS1za2luLWdyYWRpZW50LWFuZ2xlLTI6IHRyYW5zcGFyZW50O1xuICAtLXNraW4tZ3JhZGllbnQtYm90dG9tLTE6IHRyYW5zcGFyZW50O1xuICAtLXNraW4tZ3JhZGllbnQtYm90dG9tLTI6ICMzMTEwMDQ7XG4gIC0tc2tpbi1wYWdlLWJnOiAjMzExMDA0O1xuICAtLXNraW4tcGFnZS1ncmFkaWVudC1ib3R0b20tMTogcmdiYSgyOCwxMiw1LDAuNTUpO1xuICAtLXNraW4tcGFnZS1ncmFkaWVudC1ib3R0b20tMjogcmdiYSg4NywyNiw0LDAuMTEpO1xuICAtLXNraW4tcGFnZS1ncmFkaWVudC1ib3R0b20tMzogIzMxMTAwNDtcbiAgLS1za2luLWJ0bi1iZzogcmdiYSgyNTUsMjU1LDI1NSwwLjEpO1xuICAtLXNraW4tYnRuLXRleHQ6IHJnYmEoMjU1LDI1NSwyNTUsMC42KTtcbiAgLS1za2luLWJ0bi1iZy1ob3ZlcjogcmdiYSgyNTUsMjU1LDI1NSwwLjIpO1xuICAtLXNraW4tYnRuLXRleHQtaG92ZXI6IHJnYmEoMjU1LDI1NSwyNTUsMC42KTtcbiAgLS1za2luLWJ0bi1iZy1hY3RpdmU6ICNmZmY7XG4gIC0tc2tpbi1idG4tdGV4dC1hY3RpdmU6ICMzMTEwMDQ7XG4gIC0tc2tpbi1idG4tMi1iZzogcmdiYSgxNzMsMTY3LDE2MCwwLjIpO1xuICAtLXNraW4tYnRuLTItdGV4dDogIzg1NzY2NztcbiAgLS1za2luLWJ0bi0yLWJnLWhvdmVyOiByZ2JhKDE3MywxNjcsMTYwLDAuNCk7XG4gIC0tc2tpbi1idG4tMi10ZXh0LWhvdmVyOiAjODU3NjY3O1xuICAtLXNraW4tYnRuLTItYmctYWN0aXZlOiAjYmRhNzc2O1xuICAtLXNraW4tYnRuLTItdGV4dC1hY3RpdmU6ICNmZmY7XG4gIC0tc2tpbi1oZWFkZXItbG9nby13LTE6ICNmZmY7XG4gIC0tc2tpbi1oZWFkZXItbG9nby13LTI6ICNiZGE3NzY7XG4gIC0tc2tpbi10YWItdGV4dDogI2FmYThhMDtcbiAgLS1za2luLXRhYi10ZXh0LWFjdGl2ZTogIzAwMDtcbiAgLS1za2luLXRhYi1iZDogI2JkYTc3NjtcbiAgLS1za2luLWZlZS1ub21pbmFsOiAjYTA5NDg4O1xufVxuLmNvbnRhY3RzIHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLmNvbnRhY3RzX19saW5rIHtcbiAgY29sb3I6ICNiZGE3NzY7XG59XG5AbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA3NjhweCkge1xuICAuY29udGFjdHMge1xuICAgIGZvbnQtc2l6ZTogNDVweDtcbiAgfVxufVxuLyojIHNvdXJjZU1hcHBpbmdVUkw9c3JjL2FwcC9wYWdlcy9jb250YWN0cy9jb250YWN0cy5jb21wb25lbnQuY3NzLm1hcCAqLyIsIkBpbXBvcnQgXCJ2YXJzLnN0eWxcIlxyXG5AaW1wb3J0IFwidmFyaWFibGVzLnN0eWxcIlxyXG5AaW1wb3J0IFwiaGVscGVycy5zdHlsXCJcclxuXHJcbi5jb250YWN0c1xyXG4gIGZvbnQtc2l6ZTogMjJweDtcclxuICBmb250LXdlaWdodDogNjAwXHJcbiAgJl9fbGlua1xyXG4gICAgY29sb3I6ICNiZGE3NzY7XHJcblxyXG4rbWVkaXVtKClcclxuICAuY29udGFjdHNcclxuICAgIGZvbnQtc2l6ZTogNDVweFxyXG4iLCJtb2JpbGUoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6ICR3X20pXHJcbiAgICB7YmxvY2t9XHJcbmRlc2t0b3AoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X2QpXHJcbiAgICB7YmxvY2t9XHJcbm1pbl9pbml0X2QoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X2luaXRfZClcclxuICAgIHtibG9ja31cclxuc21hbGwoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X3NtYWxsKVxyXG4gICAge2Jsb2NrfVxyXG5zbWFsbF8yKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19zbWFsbF8yKVxyXG4gICAge2Jsb2NrfVxyXG5tZWRpdW0oKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X21lZGl1bSlcclxuICAgIHtibG9ja31cclxubGFyZ2UoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X2xhcmdlKVxyXG4gICAge2Jsb2NrfVxyXG5leHRyYV9sYXJnZSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfZXh0cmFfbGFyZ2UpXHJcbiAgICB7YmxvY2t9XHJcbm1heF93KClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19tYXhfcGFnZV93aWR0aClcclxuICAgIHtibG9ja31cclxuXHJcbmhfbWVkaXVtKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLWhlaWdodDogJGhfbWVkaXVtKVxyXG4gICAge2Jsb2NrfVxyXG5cclxucGxhY2Vob2xkZXIoKVxyXG4gICY6OnBsYWNlaG9sZGVyXHJcbiAgICB7YmxvY2t9XHJcblxyXG5hdXRvY29tcGxldGUoKVxyXG4gICY6LXdlYmtpdC1hdXRvZmlsbCxcclxuICAmOi13ZWJraXQtYXV0b2ZpbGw6aG92ZXIsXHJcbiAgJjotd2Via2l0LWF1dG9maWxsOmZvY3VzLFxyXG4gICY6LXdlYmtpdC1hdXRvZmlsbDphY3RpdmUsXHJcbiAgJjotaW50ZXJuYWwtYXV0b2ZpbGwtcHJldmlld2VkLFxyXG4gICY6LWludGVybmFsLWF1dG9maWxsLXNlbGVjdGVkXHJcbiAgICB7YmxvY2t9XHJcblxyXG5fY2FsYygkcGVyLCAkdmFsKVxyXG4gIHJldHVybiBcImNhbGMoJXMgLSAlcylcIiAlICgkcGVyICR2YWwpXHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/contacts/contacts.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/contacts/contacts.component.ts ***!
  \******************************************************/
/*! exports provided: ContactsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsComponent", function() { return ContactsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ContactsComponent = /** @class */ (function () {
    function ContactsComponent() {
    }
    ContactsComponent.prototype.ngOnInit = function () {
    };
    ContactsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contacts',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contacts.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contacts/contacts.component.html")).default,
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contacts.component.styl */ "./src/app/pages/contacts/contacts.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ContactsComponent);
    return ContactsComponent;
}());



/***/ }),

/***/ "./src/app/pages/contacts/contacts.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/contacts/contacts.module.ts ***!
  \***************************************************/
/*! exports provided: ROUTES, ContactsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsModule", function() { return ContactsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _contacts_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contacts.component */ "./src/app/pages/contacts/contacts.component.ts");






var ROUTES = [{ path: '', component: _contacts_component__WEBPACK_IMPORTED_MODULE_5__["ContactsComponent"] }];
var ContactsModule = /** @class */ (function () {
    function ContactsModule() {
    }
    ContactsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _contacts_component__WEBPACK_IMPORTED_MODULE_5__["ContactsComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES)
            ],
        })
    ], ContactsModule);
    return ContactsModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-contacts-contacts-module.js.map