(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-help-help-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--Подарите знакомому врачу или больнице подарочный сертификат на мероприятия. Оплаченную вами сумму мы увеличим на 30% и отправим куда скажете.      -->\r\n<div style=\"--skin-text: #fff;\r\n    --skin-gradient-angle-1: rgba(255,255,255,0.21);\r\n    --skin-gradient-angle-2: transparent;\r\n    --skin-gradient-bottom-1: rgba(141,44,5,0);\r\n    --skin-gradient-bottom-2: #0f0431;\r\n    --skin-page-bg: #060431;\r\n    --skin-page-gradient-bottom-1: rgba(255, 255, 255, 0.29);\r\n    --skin-page-gradient-bottom-2: transparent;\r\n    --skin-page-gradient-bottom-3: #060431;\r\n    --skin-btn-bg: rgba(255,255,255,0.1);\r\n    --skin-btn-text: rgba(255,255,255,0.6);\r\n    --skin-btn-bg-hover: rgba(255,255,255,0.2);\r\n    --skin-btn-text-hover: rgba(255,255,255,0.6);\r\n    --skin-btn-bg-active: #010fad;\r\n    --skin-btn-text-active: #ffd040;\r\n    --skin-tab-text: #afa8a0;\r\n    --skin-tab-text-active: #000;\r\n    --skin-tab-bd: #e74e27;\r\n    --skin-fee-nominal: #e74e27;\r\n    --skin-header-logo-w-1: #fff;\r\n    --skin-header-logo-w-2: #e74e27;\r\n    --skin-active-color: #e74e27;\">\r\n  <app-gift\r\n    [showCover]=\"false\"\r\n    [showName]=\"false\"\r\n    [showHeading]=\"true\"\r\n    [showBFee]=\"false\"\r\n    [showSubscribeForm]=\"true\"\r\n    [showSubscribeMessage]=\"false\"\r\n    [showMoreBtn]=\"false\"\r\n    [corpPhoneInput]=\"false\"\r\n    [giftPhoneInput]=\"false\"\r\n    [first_logos]=\"null\"\r\n    [headingText]=\"\r\n    '<h1>' +\r\n    '  Поможем миру искусства вместе <strong>в период карантина</strong>' +\r\n    '</h1>' +\r\n    '<p>' +\r\n    'В условиях пандемии в первую очередь пострадали именно те заведения, которые работают с посетителями. Ваш культурный объект является важной точкой на культурном ландшафте города, поэтому мы предлагаем вам присоединиться к <strong>Zapomni.gift</strong> именно сейчас.' +\r\n    '</p>' +\r\n    '<p>' +\r\n    'Мы помогаем:<br>'+\r\n      '<strong>•</strong> создать удобный онлайн-сервис по продаже подарочных сертификатов на вашем сайте<br>'+\r\n      '<strong>•</strong> установить готовый платежный инструмент, интегрированный с Apple Pay и Google Pay<br>'+\r\n      '<strong>•</strong> привлечь новых клиентов, размещая ваш подарочный сертификат на нашей платформе' +\r\n    '</p>' +\r\n    '<p>' +\r\n      'Именно сейчас каждый купленный сертификат станет существенным вкладом в поддержку культурных институций. Так мы сможем продлить жизнь любимому музею, парку или театру.' +\r\n    '</p>' +\r\n    '<p>' +\r\n      'Мы будем рады видеть вас среди наших партнеров!' +\r\n    '</p>' +\r\n    '<div class=\\'footer-btn\\'>' +\r\n        '<a download target=\\'_blank\\' href=\\'https:\\/\\/st1.lastick.ru\\/digest\\/common\\/gift_moscow\\/zapomni.gift.presentation.pdf\\' class=\\'mxn-btn mxn-btn_ta-c mxn-btn_sz-5 mxn-btn_theme-special\\'>' +\r\n          'Скачать презентацию' +\r\n        '</a>' +\r\n    '</div>'\r\n  \">\r\n  </app-gift>\r\n</div>\r\n\r\n<!--\"<a href=\\\"https://st1.lastick.ru/digest/common/gift_moscow/zapomni.gift.presentation.pdf\\\" download class=\\\"mxn-btn mxn-btn_ta-c mxn-btn_sz-5 mxn-btn_theme-special\\\">\" +-->\r\n<!--'Скачать презентацию' +-->\r\n<!--\"</a>\" +-->\r\n");

/***/ }),

/***/ "./src/app/pages/help/help.component.styl":
/*!************************************************!*\
  !*** ./src/app/pages/help/help.component.styl ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/pages/help/help.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaGVscC9oZWxwLmNvbXBvbmVudC5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdFQUFnRSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvaGVscC5jb21wb25lbnQuc3R5bCJ9 */");

/***/ }),

/***/ "./src/app/pages/help/help.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/help/help.component.ts ***!
  \**********************************************/
/*! exports provided: HelpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpComponent", function() { return HelpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HelpComponent = /** @class */ (function () {
    function HelpComponent() {
    }
    HelpComponent.prototype.ngOnInit = function () {
    };
    HelpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-help',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./help.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./help.component.styl */ "./src/app/pages/help/help.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HelpComponent);
    return HelpComponent;
}());



/***/ }),

/***/ "./src/app/pages/help/help.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/help/help.module.ts ***!
  \*******************************************/
/*! exports provided: ROUTES, HelpModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpModule", function() { return HelpModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/gift/gift.module */ "./src/app/components/gift/gift.module.ts");
/* harmony import */ var _help_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./help.component */ "./src/app/pages/help/help.component.ts");







var ROUTES = [{ path: '', component: _help_component__WEBPACK_IMPORTED_MODULE_6__["HelpComponent"] }];
var HelpModule = /** @class */ (function () {
    function HelpModule() {
    }
    HelpModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _help_component__WEBPACK_IMPORTED_MODULE_6__["HelpComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES),
                _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_5__["GiftModule"]
            ],
        })
    ], HelpModule);
    return HelpModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-help-help-module.js.map