(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pages-cert-index-index-page-module~pages-help-help-module~pages-index-index-module~pages-par~bdc04c47"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/card-buttons/card-buttons.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/card-buttons/card-buttons.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"card-buttons\">\r\n  <div class=\"card-buttons__head card-buttons__head_sz-{{sz}}\">\r\n    <h6 class=\"card-buttons__head-title card-buttons__head-title_sz-{{sz}}\">\r\n      {{ 'GiftCard.ButtonsTitle' | translate }}<span class=\"mxn-fee mxn-fee_skin-{{skin.skin}} mxn-fee_theme-1 mxn-fee_ml-1 mxn-fee_sz-1\" *ngIf=\"fee && fee.formatted_value\">\r\n        {{\r\n          'GiftCard.FeeTitle' | translate: {value: fee.formatted_value}\r\n        }}\r\n    </span>\r\n    </h6>\r\n  </div>\r\n  <div class=\"card-buttons__body\">\r\n    <div class=\"card-buttons__buttons\">\r\n      <div class=\"card-buttons__buttons-row card-buttons__buttons-row_sz-{{sz}}\">\r\n        <div class=\"card-buttons__buttons-item card-buttons__buttons-item_item card-buttons__buttons-item_item_sz-{{sz}}\" *ngFor=\"let value of (prices | pricesSort)\">\r\n            <span\r\n              [ngClass]=\"{'f-active': currPrice === value && !currCustomValue}\"\r\n              (click)=\"onPriceClick(value)\"\r\n              class=\"mxn-btn card-buttons__btn card-buttons__btn_theme-{{theme}} card-buttons__btn_skin-{{skin.skin}} card-buttons__btn_sz-{{sz}}\">\r\n              {{ value | money }}\r\n            </span>\r\n        </div>\r\n        <div class=\"card-buttons__buttons-item card-buttons__buttons-item_custom card-buttons__buttons-item_custom_sz-{{sz}}\" *ngIf=\"custom\">\r\n          <div\r\n            [ngClass]=\"{'f-active': showCustomInput}\"\r\n\r\n            class=\"card-buttons__i-popover\" [appClickOutside]=\"showCustomInput\" (clickOutside)=\"onPopoverOutsideClick()\">\r\n            <div class=\"card-buttons__i-popover-wrap\">\r\n              <div class=\"card-buttons__i-popover-row\">\r\n                <input type=\"number\"\r\n                       [appMaxInputValue]=\"\r\n                            prices_range && prices_range[1] ? prices_range[1] / 100 : 10000000\r\n                          \"\r\n                       [maxInputValuePattern]=\"maxValuePattern\"\r\n                       [(ngModel)]=\"currCustomValue\"\r\n                       [required]=\"showCustomInput\"\r\n                       [ngClass]=\"{'f-error':  prices_range && prices_range[0] && (currCustomValue * 100) < prices_range[0] && submitted}\"\r\n                       name=\"custom_price\"\r\n                       autocomplete=\"off\"\r\n                       class=\"mxn-i-txt mxn-i-txt_skin-default mxn-i-txt_theme-1 card-buttons__i-popover-input\" [appFocusInput]=\"showCustomInput\">\r\n                <div class=\"card-buttons__i-popover-icon card-buttons__i-popover-icon_skin-{{skin.skin}} mxn-cur-p\" (click)=\"onCustomValueSubmit()\">\r\n                  <svg-icon src=\"/assets/svg/check.svg\"></svg-icon>\r\n                </div>\r\n              </div>\r\n              <div class=\"card-buttons__i-popover-row\" *ngIf=\"prices_range && prices_range[0]\">\r\n                <span class=\"card-buttons__i-popover-message\">\r\n                  Минимум: {{prices_range[0] | money}}\r\n                </span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <span\r\n            [ngClass]=\"{'f-active': showCustomInput || currCustomValue}\"\r\n            (click)=\"onCustomValueClick()\"\r\n            class=\"mxn-btn card-buttons__btn card-buttons__btn_theme-{{theme}} card-buttons__btn_skin-{{skin.skin}} card-buttons__btn_sz-{{sz}}\">\r\n              <ng-template [ngIf]=\"!currCustomValue\">\r\n                {{\"Common.CustomNominal\" | translate}}\r\n              </ng-template>\r\n              <ng-template [ngIf]=\"currCustomValue\">\r\n                {{(currCustomValue * 100) | money}}\r\n              </ng-template>\r\n            </span>\r\n        </div>\r\n        <div\r\n          *ngIf=\"route\"\r\n          class=\"card-buttons__buttons-item card-buttons__buttons-item_full-width\">\r\n            <span\r\n              routerLink=\"{{route.link}}\" [queryParams]=\"{price: price}\"\r\n              class=\"mxn-btn mxn-btn_ta-c mxn-btn_full-width card-buttons__l-btn\">\r\n              {{ route.text }}\r\n            </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gift/gift-form/gift-form.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/gift/gift-form/gift-form.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"f-gift\">\r\n  <div class=\"f-gift__body\">\r\n    <div class=\"f-gift__heading mxn-desktop\">\r\n      <h6 class=\"f-gift__heading-title mxn-title mxn-title_sz-1 mxn-title_theme-1\">\r\n        {{ 'GiftCard.Receiver' | translate }}\r\n      </h6>\r\n    </div>\r\n    <div class=\"f-gift__form\">\r\n      <div class=\"f-gift__form-body\">\r\n        <div class=\"f-gift__form-row\" *ngIf=\"mdItem\">\r\n          <div class=\"mxn-i-txt-wrap\">\r\n            <label\r\n              class=\"mxn-i-txt-wrap-label mxn-i-txt-wrap-label_section_theme-1 mxn-i-txt-wrap-label_flex mxn-i-txt-wrap-label_flex_ali-c mxn-i-txt-wrap-label_sz-1 mxn-i-txt-wrap-label_skin-default mxn-i-txt-wrap-label_cur-t\"\r\n            >\r\n              <span\r\n                [ngClass]=\"{ 'f-active': mdItem.sender_name && mdItem.sender_name.length }\"\r\n                class=\"mxn-i-txt-wrap-placeholder mxn-i-txt-wrap-placeholder_dots mxn-i-txt-wrap-placeholder_sz-1 mxn-i-txt-wrap-placeholder_skin-default\"\r\n              >\r\n                {{ 'GiftForm.FromWho' | translate }}</span\r\n              >\r\n              <input\r\n                type=\"text\"\r\n                [(ngModel)]=\"mdItem.sender_name\"\r\n                name=\"name{{ type }}{{ index }}\"\r\n                autocomplete=\"off\"\r\n                class=\"mxn-i-txt-wrap-input mxn-i-txt mxn-i-txt_skin-default mxn-i-txt_theme-1 mxn-i-txt-wrap-input_skin-default f-gift__input\"\r\n              />\r\n            </label>\r\n          </div>\r\n        </div>\r\n        <ng-template [ngIf]=\"mdAddressee\">\r\n          <div class=\"f-gift__form-row\">\r\n            <div class=\"mxn-i-txt-wrap\">\r\n              <label\r\n                appInputError\r\n                [inpRef]=\"addresseeNameInp\"\r\n                class=\"mxn-i-txt-wrap-label mxn-i-txt-wrap-label_section_theme-1 mxn-i-txt-wrap-label_flex mxn-i-txt-wrap-label_flex_ali-c mxn-i-txt-wrap-label_sz-1 mxn-i-txt-wrap-label_skin-default mxn-i-txt-wrap-label_cur-t\"\r\n              >\r\n                <span\r\n                  [ngClass]=\"{ 'f-active': mdAddressee.name && mdAddressee.name.length }\"\r\n                  class=\"mxn-i-txt-wrap-placeholder mxn-i-txt-wrap-placeholder_dots mxn-i-txt-wrap-placeholder_sz-1 mxn-i-txt-wrap-placeholder_skin-default\"\r\n                >\r\n                  {{ 'GiftForm.Who' | translate }}</span\r\n                >\r\n                <input\r\n                  type=\"text\"\r\n                  #addresseeNameInp=\"ngModel\"\r\n                  #addresseeNameRef\r\n                  appNativeElementControl\r\n                  required\r\n                  [(ngModel)]=\"mdAddressee.name\"\r\n                  (focus)=\"onAddresseeNameFocus(addresseeNameInp.value, $event)\"\r\n                  (blur)=\"onAddresseeNameBlur($event)\"\r\n                  (ngModelChange)=\"onAddresseeNameChange($event)\"\r\n                  name=\"addresseeName{{ type }}{{ index }}\"\r\n                  autocomplete=\"off\"\r\n                  class=\"mxn-i-txt-wrap-input mxn-i-txt mxn-i-txt_skin-default mxn-i-txt_theme-1 mxn-i-txt-wrap-input_skin-default f-gift__input\"\r\n                />\r\n              </label>\r\n              <div class=\"mxn-form-input-error\" *ngIf=\"addresseeNameInp.invalid && addresseeNameInp.dirty && addresseeNameRef !== fhFocusedElement\">\r\n                <app-status-message\r\n                  [message]=\"addresseeNameInp.value ? 'Укажите получателя' : 'Укажите получателя'\"\r\n                ></app-status-message>\r\n              </div>\r\n            </div>\r\n            <div class=\"select-list\" [appClickOutside]=\"showAddresseeData\" (clickOutside)=\"showAddresseeData = false\" *ngIf=\"(_addresseeData || []).length && showAddresseeData\">\r\n              <div class=\"select-list__wrap\">\r\n                <div class=\"select-list__list\">\r\n                  <div class=\"select-list__item\" *ngFor=\"let item of _addresseeData\">\r\n                    <span class=\"select-list__label\" (click)=\"onAddresseeDataItemClick($event, item)\">\r\n                      {{item.name}}\r\n                    </span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"f-gift__form-row\">\r\n            <div class=\"mxn-i-txt-wrap\">\r\n              <label\r\n                appInputError\r\n                [inpRef]=\"addresseeEmailInp\"\r\n                class=\"mxn-i-txt-wrap-label mxn-i-txt-wrap-label_section_theme-1 mxn-i-txt-wrap-label_flex mxn-i-txt-wrap-label_flex_ali-c mxn-i-txt-wrap-label_sz-1 mxn-i-txt-wrap-label_skin-default mxn-i-txt-wrap-label_cur-t\"\r\n              >\r\n                <span\r\n                  [ngClass]=\"{ 'f-active': mdAddressee.email && mdAddressee.email.length }\"\r\n                  class=\"mxn-i-txt-wrap-placeholder mxn-i-txt-wrap-placeholder_dots mxn-i-txt-wrap-placeholder_sz-1 mxn-i-txt-wrap-placeholder_skin-default\"\r\n                >\r\n                  {{ 'GiftForm.ReceiveEmail' | translate }}</span\r\n                >\r\n                <input\r\n                  type=\"email\"\r\n                  #addresseeEmailInp=\"ngModel\"\r\n                  #addresseeEmailRef\r\n                  appEmailValidator\r\n                  appNativeElementControl\r\n                  required\r\n                  [(ngModel)]=\"mdAddressee.email\"\r\n                  (focus)=\"fhOnInputFocus($event)\"\r\n                  (blur)=\"fhOnInputBlur()\"\r\n                  name=\"addresseeEmail{{ type }}{{ index }}\"\r\n                  autocomplete=\"off\"\r\n                  class=\"mxn-i-txt-wrap-input mxn-i-txt mxn-i-txt_skin-default mxn-i-txt_theme-1 mxn-i-txt-wrap-input_skin-default f-gift__input\"\r\n                />\r\n              </label>\r\n              <div class=\"mxn-form-input-error\" *ngIf=\"addresseeEmailInp.invalid && addresseeEmailInp.dirty && addresseeEmailRef !== fhFocusedElement\">\r\n                <app-status-message\r\n                  [message]=\"addresseeEmailInp.value ? 'Проверьте формат email' : 'Введите email получателя'\"\r\n                ></app-status-message>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"f-gift__form-row\" *ngIf=\"phoneInput\">\r\n            <div class=\"mxn-i-txt-wrap\">\r\n              <label\r\n                appInputError\r\n                [inpRef]=\"addresseePhoneInp\"\r\n                class=\"mxn-i-txt-wrap-label mxn-i-txt-wrap-label_section_theme-1 mxn-i-txt-wrap-label_flex mxn-i-txt-wrap-label_flex_ali-c mxn-i-txt-wrap-label_sz-1 mxn-i-txt-wrap-label_skin-default mxn-i-txt-wrap-label_cur-t\"\r\n              >\r\n                <span\r\n                  [ngClass]=\"{ 'f-active': mdAddressee.phone && mdAddressee.phone.length }\"\r\n                  class=\"mxn-i-txt-wrap-placeholder mxn-i-txt-wrap-placeholder_dots mxn-i-txt-wrap-placeholder_sz-1 mxn-i-txt-wrap-placeholder_skin-default\"\r\n                >\r\n                  {{ 'GiftForm.ReceivePhone' | translate }}</span\r\n                >\r\n                <input\r\n                  type=\"tel\"\r\n                  #addresseePhoneInp=\"ngModel\"\r\n                  #addresseePhoneRef\r\n                  appMaskInputValue=\"+7\"\r\n                  appPhoneValidator\r\n                  appPhoneInputValue\r\n                  appNativeElementControl\r\n                  required\r\n                  [(ngModel)]=\"mdAddressee.phone\"\r\n                  (focus)=\"fhOnInputFocus($event)\"\r\n                  (blur)=\"fhOnInputBlur()\"\r\n                  name=\"phone{{ type }}{{ index }}\"\r\n                  autocomplete=\"off\"\r\n                  class=\"mxn-i-txt-wrap-input mxn-i-txt mxn-i-txt_skin-default mxn-i-txt_theme-1 mxn-i-txt-wrap-input_skin-default f-gift__input\"\r\n                />\r\n              </label>\r\n              <div class=\"mxn-form-input-error\" *ngIf=\"addresseePhoneInp.invalid && addresseePhoneInp.dirty && addresseePhoneRef !== fhFocusedElement\">\r\n                <app-status-message\r\n                  [message]=\"addresseePhoneInp.value ? 'Проверьте телефон получателя' : 'Введите телефон получателя'\"\r\n                ></app-status-message>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"f-gift__form-row\">\r\n            <div class=\"mxn-i-txt-wrap\">\r\n              <label\r\n                class=\"mxn-i-txt-wrap-label mxn-i-txt-wrap-label_section_theme-1 mxn-i-txt-wrap-label_flex mxn-i-txt-wrap-label_area_sz-1 mxn-i-txt-wrap-label_sz-2 mxn-i-txt-wrap-label_skin-default mxn-i-txt-wrap-label_cur-t\"\r\n              >\r\n                <span\r\n                  [ngClass]=\"{ 'f-active': mdAddressee.message && mdAddressee.message.length }\"\r\n                  class=\"mxn-i-txt-wrap-placeholder mxn-i-txt-wrap-placeholder_dots mxn-i-txt-wrap-placeholder_sz-1 mxn-i-txt-wrap-placeholder_skin-default\"\r\n                >\r\n                  {{ 'GiftForm.Message' | translate }}</span>\r\n                <textarea\r\n                  name=\"message{{ type }}{{ index }}\"\r\n                  [(ngModel)]=\"mdAddressee.message\"\r\n                  maxlength=\"400\"\r\n                  autocomplete=\"off\"\r\n                  class=\"mxn-i-txt-wrap-area mxn-i-txt-wrap-area_sz-1 mxn-i-txt-wrap-input mxn-i-txt mxn-i-txt_skin-default mxn-i-txt_theme-1 mxn-i-txt-wrap-input_skin-default f-gift__input\"\r\n                ></textarea>\r\n              </label>\r\n              <div class=\"mxn-form-input-error\" *ngIf=\"mdAddressee.message && mdAddressee.message.length >= 400\">\r\n                <app-status-message\r\n                  [message]=\"'Максимальная длина сообщения 400 символов'\"\r\n                  status=\"changed\"\r\n                ></app-status-message>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </ng-template>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gift/gift.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/gift/gift.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<app-layout-gift\r\n  theme=\"2\"\r\n  [bg]=\"gift && gift.background_image && showCover ? gift.background_image.path : ''\"\r\n  [skin]=\"skin || { skin: 'default' }\"\r\n  [hash_tag]=\"head_hash_tag\"\r\n  [customLogo]=\"headerLogo\"\r\n  [customLogoSz]=\"headerLogoSz\"\r\n  [showHeaderCompanyLogo]=\"showHeaderCompanyLogo\"\r\n  [fee]=\"\r\n    fee && showBFee\r\n      ? {\r\n          value: fee.formatted_value,\r\n          text:\r\n            'Поддержим вместе культуру, приобретайте подарочные сертификаты в любимые музеи и театры, а мы подарим вам ' +\r\n            '<span class=\\'strong\\'>' +\r\n            fee.formatted_value +\r\n            ' к номиналу' +\r\n            '<\\/span>' +\r\n            ' и увеличенный срок действия сертификата.'\r\n        }\r\n      : null\r\n  \"\r\n>\r\n  <div class=\"gift\">\r\n    <!--        <img [src]=\"logoPath | safe: 'resourceUrl'\" width=\"200\" alt=\"\">-->\r\n    <section class=\"section section_mb-2 section_sz-3\" *ngIf=\"showHeading && headingText\">\r\n      <header class=\"special-text special-text_skin-{{ (skin || { skin: 'default' }).skin }}\">\r\n        <article class=\"special-text__article\" [innerHTML]=\"headingText\"></article>\r\n      </header>\r\n    </section>\r\n\r\n    <ng-template [ngIf]=\"gift && showGiftCheckout\">\r\n      <section class=\"section section_mb-4 section_bg-1\" [ngClass]=\"{'section_sz-1': !showName, 'section_sz-7': showName}\">\r\n        <form class=\"gift__form\" (ngSubmit)=\"submit()\" #form=\"ngForm\">\r\n          <ng-template #counterTmp let-md=\"md\" let-index=\"index\">\r\n            <app-plus-minus\r\n              [source]=\"md\"\r\n              (change)=\"onCountChange($event, md, index)\"\r\n              [value]=\"md.items[index].count\"\r\n              [count]=\"md.items[index].count\"\r\n              [min_value]=\"1\"\r\n              [mark]=\"\r\n                md.items[index].count\r\n                  | numerative\r\n                    : [\r\n                        'Common.Cert1' | translate,\r\n                        'Common.Cert2' | translate,\r\n                        'Common.Cert3' | translate\r\n                      ]\r\n              \"\r\n            >\r\n            </app-plus-minus>\r\n            <div\r\n              class=\"mxn-form-input-error\"\r\n              *ngIf=\"\r\n              form.controls['nominal' + tab + index] && (\r\n              (formSubmitted && form.controls['nominal' + tab + index].invalid) ||\r\n              (form.controls['nominal' + tab + index].value && form.controls['nominal' + tab + index].errors && form.controls['nominal' + tab + index].errors.minValidator)\r\n              )\r\n              \"\r\n            >\r\n              <app-status-message\r\n                [message]=\"\r\n                  form.controls['nominal' + tab + index].value &&\r\n                  form.controls['nominal' + tab + index].errors &&\r\n                  form.controls['nominal' + tab + index].errors.minValidator\r\n                    ? 'Минимальный номинал ' + gift.prices_range[0] / 100 + ' руб'\r\n                    : 'Выберите номинал'\r\n                \"\r\n              ></app-status-message>\r\n            </div>\r\n          </ng-template>\r\n\r\n          <ng-template #cardTmp let-md=\"md\" let-index=\"index\">\r\n            <app-gift-card\r\n              [gift]=\"gift\"\r\n              [name]=\"gift.name | multilang\"\r\n              [logo]=\"_logo\"\r\n              [skin]=\"skin\"\r\n              [price]=\"\r\n                md.items && md.items[index] && form.controls['nominal' + tab + index] && form.controls['nominal' + tab + index].valid ? md.items[index].price : null\r\n              \"\r\n              [logoInput]=\"tab === 'corporate'\"\r\n              sz=\"2\"\r\n              pos=\"2\"\r\n              theme=\"2\"\r\n              (changeFiles)=\"logo = $event\"\r\n              (logoImageLoaded)=\"logoSize = $event.size\"\r\n              (removeClick)=\"onCardRemoveClick(index)\"\r\n              [showRelated]=\"false\"\r\n              [showTable]=\"true\"\r\n              [showRemove]=\"index > 0\"\r\n            >\r\n            </app-gift-card>\r\n          </ng-template>\r\n{{log(gift.prices)}}\r\n          <ng-template #buttonsTmp let-md=\"md\" let-index=\"index\">\r\n            <app-card-buttons\r\n              [prices]=\"gift.prices\"\r\n              [price]=\"(md.items[index] || {}).price\"\r\n              [prices_range]=\"prices_range\"\r\n              [custom]=\"true\"\r\n              [fee]=\"fee\"\r\n              sz=\"2\"\r\n              theme=\"2\"\r\n              [skin]=\"skin\"\r\n              (priceClick)=\"onPriceClick(md, $event, index)\"\r\n              (customValueShowHide)=\"showCustomValue = $event\"\r\n            ></app-card-buttons>\r\n            <input\r\n              type=\"hidden\"\r\n              name=\"nominal{{ tab }}{{ index }}\"\r\n              [(ngModel)]=\"(md.items[index] || {}).price\"\r\n              [required]=\"true\"\r\n              [appMinValidator]=\"prices_range && prices_range[0] ? prices_range[0] : 1\"\r\n            />\r\n          </ng-template>\r\n\r\n          <ng-template #desktopBody>\r\n            <app-tabs\r\n              uuid=\"gift_tabs\"\r\n              sz=\"2\"\r\n              theme=\"2\"\r\n              (showTabs)=\"onTabsShow($event)\"\r\n              [activeTab]=\"tab\"\r\n              [skin]=\"skin\"\r\n            >\r\n              <app-tabs-tab\r\n                [tabTitle]=\"giftTabTitle || ('GiftTabs.GiftTab' | translate)\"\r\n                [data]=\"{ name: 'gift' }\"\r\n                uuid=\"gift\"\r\n              >\r\n                <div *app-tabs-tab-content-tmp>\r\n                  <fieldset\r\n                    class=\"gift__fieldset\"\r\n                    *ngFor=\"let item of tabs.gift.items; let index = index\"\r\n                  >\r\n                    <div class=\"gift__body-row gift__body-row_cert\">\r\n                      <div class=\"gift__body-col gift__body-col_card\">\r\n                        <ng-container\r\n                          [ngTemplateOutlet]=\"cardTmp\"\r\n                          [ngTemplateOutletContext]=\"{ md: tabs.gift, index: index }\"\r\n                        ></ng-container>\r\n                      </div>\r\n                      <div class=\"gift__body-col gift__body-col_form\">\r\n                        <div class=\"gift__section\">\r\n                          <div class=\"gift__section-row\">\r\n                            <ng-container\r\n                              [ngTemplateOutlet]=\"buttonsTmp\"\r\n                              [ngTemplateOutletContext]=\"{ md: tabs.gift, index: index }\"\r\n                            ></ng-container>\r\n                          </div>\r\n                          <div class=\"gift__section-row\">\r\n                            <ng-container\r\n                              [ngTemplateOutlet]=\"counterTmp\"\r\n                              [ngTemplateOutletContext]=\"{ md: tabs.gift, index: index }\"\r\n                            ></ng-container>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"gift__section\">\r\n                          <app-gift-form\r\n                            [md]=\"tabs.gift\"\r\n                            [mdItem]=\"item\"\r\n                            [type]=\"'gift'\"\r\n                            *ngIf=\"tabs.gift\"\r\n                            [index]=\"index\"\r\n                            [phoneInput]=\"giftPhoneInput\"\r\n                            [formSubmitted]=\"formSubmitted\"\r\n                          ></app-gift-form>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </fieldset>\r\n                </div>\r\n\r\n                <!--                <div class=\"gift__body-row gift__body-row_more\">-->\r\n                <!--                  <div class=\"gift__body-col\">-->\r\n                <!--                    more-->\r\n                <!--                  </div>-->\r\n                <!--                </div>-->\r\n              </app-tabs-tab>\r\n              <app-tabs-tab\r\n                [tabTitle]=\"corpTabTitle || ('GiftTabs.CorpTab' | translate)\"\r\n                [data]=\"{ name: 'corporate' }\"\r\n                uuid=\"corporate\"\r\n              >\r\n                <div *app-tabs-tab-content-tmp>\r\n                  <fieldset\r\n                    class=\"gift__fieldset\"\r\n                    *ngFor=\"let item of tabs.corporate.items; let index = index\"\r\n                  >\r\n                    <div class=\"gift__body-row gift__body-row_cert\">\r\n                      <div class=\"gift__body-col gift__body-col_card\">\r\n                        <ng-container\r\n                          [ngTemplateOutlet]=\"cardTmp\"\r\n                          [ngTemplateOutletContext]=\"{ md: tabs.corporate, index: index }\"\r\n                        ></ng-container>\r\n                      </div>\r\n                      <div class=\"gift__body-col gift__body-col_form\">\r\n                        <div class=\"gift__section\">\r\n                          <div class=\"gift__section-row\">\r\n                            <ng-container\r\n                              [ngTemplateOutlet]=\"buttonsTmp\"\r\n                              [ngTemplateOutletContext]=\"{ md: tabs.corporate, index: index }\"\r\n                            ></ng-container>\r\n                          </div>\r\n                          <div class=\"gift__section-row\">\r\n                            <ng-container\r\n                              [ngTemplateOutlet]=\"counterTmp\"\r\n                              [ngTemplateOutletContext]=\"{ md: tabs.corporate, index: index }\"\r\n                            ></ng-container>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"gift__section\">\r\n                          <app-gift-form\r\n                            [md]=\"tabs.corporate\"\r\n                            [mdItem]=\"item\"\r\n                            [addresseeData]=\"addresseeData\"\r\n                            [type]=\"'corporate'\"\r\n                            *ngIf=\"tabs.corporate\"\r\n                            [index]=\"index\"\r\n                            [phoneInput]=\"corpPhoneInput\"\r\n                            [formSubmitted]=\"formSubmitted\"\r\n                          ></app-gift-form>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </fieldset>\r\n                </div>\r\n              </app-tabs-tab>\r\n            </app-tabs>\r\n          </ng-template>\r\n\r\n          <ng-template #mobileBody>\r\n            <section class=\"gift__fieldset\" *ngFor=\"let item of tabs[tab].items; let index = index\">\r\n              <div class=\"gift__section\">\r\n                <div class=\"gift__card\">\r\n                  <ng-container\r\n                    [ngTemplateOutlet]=\"cardTmp\"\r\n                    [ngTemplateOutletContext]=\"{ md: tabs[tab], index: index }\"\r\n                  ></ng-container>\r\n                </div>\r\n              </div>\r\n              <div class=\"gift__section\">\r\n                <div class=\"gift__section-row\">\r\n                  <ng-container\r\n                    [ngTemplateOutlet]=\"buttonsTmp\"\r\n                    [ngTemplateOutletContext]=\"{ md: tabs[tab], index: index }\"\r\n                  ></ng-container>\r\n                </div>\r\n                <div class=\"gift__section-row\">\r\n                  <ng-container\r\n                    [ngTemplateOutlet]=\"counterTmp\"\r\n                    [ngTemplateOutletContext]=\"{ md: tabs[tab], index: index }\"\r\n                  ></ng-container>\r\n                </div>\r\n              </div>\r\n              <div class=\"gift__section\">\r\n                <app-tabs\r\n                  *ngIf=\"index === 0\"\r\n                  uuid=\"gift_tabs\"\r\n                  sz=\"2\"\r\n                  (showTabs)=\"onTabsShow($event)\"\r\n                  [activeTab]=\"tab\"\r\n                  [skin]=\"skin\"\r\n                >\r\n                  <app-tabs-tab\r\n                    [tabTitle]=\"giftTabTitle || ('GiftTabs.GiftTab' | translate)\"\r\n                    [data]=\"{ name: 'gift' }\"\r\n                    uuid=\"gift\"\r\n                  >\r\n                    <div *app-tabs-tab-content-tmp>\r\n                      <app-gift-form\r\n                        type=\"gift\"\r\n                        [md]=\"tabs[tab]\"\r\n                        [index]=\"index\"\r\n                        [mdItem]=\"item\"\r\n                        [phoneInput]=\"giftPhoneInput\"\r\n                        [formSubmitted]=\"formSubmitted\"\r\n                      >\r\n                      </app-gift-form>\r\n                    </div>\r\n                  </app-tabs-tab>\r\n                  <app-tabs-tab\r\n                    [tabTitle]=\"corpTabTitle || ('GiftTabs.CorpTab' | translate)\"\r\n                    [data]=\"{ name: 'corporate' }\"\r\n                    uuid=\"corporate\"\r\n                  >\r\n                    <div *app-tabs-tab-content-tmp>\r\n                      <app-gift-form\r\n                        type=\"corporate\"\r\n                        [addresseeData]=\"addresseeData\"\r\n                        [phoneInput]=\"corpPhoneInput\"\r\n                        [mdItem]=\"item\"\r\n                        [md]=\"tabs[tab]\"\r\n                        [formSubmitted]=\"formSubmitted\"\r\n                      ></app-gift-form>\r\n                    </div>\r\n                  </app-tabs-tab>\r\n                </app-tabs>\r\n                <app-gift-form\r\n                  [type]=\"tab\"\r\n                  [md]=\"tabs[tab]\"\r\n                  [mdItem]=\"item\"\r\n                  [addresseeData]=\"tab === 'corporate' ? addresseeData : null\"\r\n                  [index]=\"index\"\r\n                  [phoneInput]=\"tab === 'corporate' ? corpPhoneInput : giftPhoneInput\"\r\n                  [formSubmitted]=\"formSubmitted\"\r\n                  *ngIf=\"index !== 0\"\r\n                ></app-gift-form>\r\n              </div>\r\n            </section>\r\n          </ng-template>\r\n\r\n          <input\r\n            type=\"hidden\"\r\n            name=\"card_logo\"\r\n            #cardLogo=\"ngModel\"\r\n            [(ngModel)]=\"logo\"\r\n            [appFileValidator]=\"{\r\n              required: false,\r\n              maxWeight: 50000\r\n            }\"\r\n            [imgSize]=\"logoSize\"\r\n          />\r\n          <header class=\"gift__head\" *ngIf=\"showName\">\r\n            <div class=\"gift__head-bl gift__head-bl_heading\">\r\n              <div class=\"gift__head-item gift__head-item_logo\" *ngIf=\"gift.entity_icons && gift.entity_icons.length\">\r\n                <img [src]=\"(gift.entity_icons[1] || gift.entity_icons[0]).path\" alt=\"\" class=\"gift__head-logo\">\r\n              </div>\r\n              <div class=\"gift__head-item gift__head-item_title\">\r\n                <div class=\"gift__head-row\">\r\n                  <h1 class=\"gift__title\">{{ (gift.name_variant || gift.name) | multilang }}</h1>\r\n                </div>\r\n                <div class=\"gift__head-row\" *ngIf=\"gift.summary | multilang\">\r\n                  <div class=\"gift__summary\" [innerHTML]=\"gift.summary | multilang\"></div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <!--      <div class=\"section__head-bl\" *ngIf=\"partners && partners.length\">-->\r\n            <!--        <div class=\"section__line\">-->\r\n            <!--          <div class=\"img-tags\">-->\r\n            <!--            <div class=\"img-tags__list\">-->\r\n            <!--              <div class=\"img-tags__item\" *ngFor=\"let partner of partners\">-->\r\n            <!--                <img [src]=\"partner.media && partner.media.path ? partner.media.path : ''\" alt=\"\" class=\"img-tags__img\">-->\r\n            <!--              </div>-->\r\n            <!--            </div>-->\r\n            <!--          </div>-->\r\n            <!--        </div>-->\r\n            <!--      </div>-->\r\n          </header>\r\n\r\n          <div class=\"gift__body\" *ngIf=\"isDesktop\">\r\n            <ng-container *ngTemplateOutlet=\"desktopBody\"></ng-container>\r\n          </div>\r\n          <div class=\"gift__body\" *ngIf=\"!isDesktop\">\r\n            <ng-container *ngTemplateOutlet=\"mobileBody\"></ng-container>\r\n          </div>\r\n          <div class=\"gift__more\" *ngIf=\"tab === 'gift' && showMoreBtn\">\r\n            <button\r\n              type=\"button\"\r\n              class=\"mxn-btn mxn-btn_theme-more mxn-btn_sz-8 mxn-btn_full-width mxn-btn_ta-c gift__more-btn\"\r\n              (click)=\"addMore()\"\r\n            >\r\n              {{ 'GiftForm.MoreBtn' | translate }}\r\n            </button>\r\n          </div>\r\n          <div class=\"gift__footer\">\r\n            <div class=\"mxn-form-error\" *ngIf=\"form.controls.card_logo && form.controls.card_logo.invalid\">\r\n              <app-status-message\r\n                [message]=\"form.controls.card_logo.errors.fileValidatorEmptySize ? 'У логотипа отсутствуют размеры' : 'Логотип не соответствует требованиям'\"\r\n              ></app-status-message>\r\n            </div>\r\n            <div class=\"mxn-form-error\" *ngIf=\"formSubmitted && !order.agreement\">\r\n              <app-status-message\r\n                [message]=\"'Подтвердите согласие с условиями обработки данных'\"\r\n              ></app-status-message>\r\n            </div>\r\n            <div class=\"gift__footer-row gift__footer-row_buttons\">\r\n              <div class=\"gift__footer-item\">\r\n                <label class=\"mxn-i-txt-wrap-label mxn-i-txt-wrap-label_flex\">\r\n                  <input\r\n                    type=\"checkbox\"\r\n                    name=\"agreement\"\r\n                    [(ngModel)]=\"order.agreement\"\r\n                    required\r\n                    class=\"mxn-i-check mxn-i-check_input mxn-i-hidden\"\r\n                  />\r\n                  <span\r\n                    class=\"mxn-i-check mxn-i-check_fake mxn-i-check_sz-1 mxn-i-check_skin-default mxn-i-check_mr-1\"\r\n                  >\r\n                    <svg-icon src=\"/assets/svg/check.svg\"></svg-icon>\r\n                  </span>\r\n                  <span>\r\n                    <span\r\n                      [innerHTML]=\"'Common.Agreement1' | translate\"\r\n                      class=\"mxn-i-txt-wrap-label-text mxn-i-txt-wrap-label-text_skin-default mxn-i-txt-wrap-label-text_sz-1\"\r\n                    >\r\n                    </span\r\n                    >&nbsp;<span\r\n                      class=\"mxn-cur-p mxn-link mxn-link_theme-1\"\r\n                      (click)=\"doc.openPopup('gift_offer')\"\r\n                      >{{ 'Common.PersonalDataProcessRules' | translate }}</span\r\n                    >\r\n                  </span>\r\n                </label>\r\n              </div>\r\n              <div class=\"gift__footer-item gift__footer-item_btn\">\r\n                <button\r\n                  type=\"submit\"\r\n                  [disabled]=\"showCustomValue\"\r\n                  [ngClass]=\"{ 'f-disabled': !order.agreement }\"\r\n                  class=\"mxn-btn mxn-btn_icon_sz-1 mxn-btn_icon_mr-1 mxn-btn_full-width mxn-btn_ta-c mxn-btn_sz-1 mxn-btn_theme-1 mxn-btn_skin-{{\r\n                    skin.skin\r\n                  }}\"\r\n                >\r\n                  <svg-icon src=\"/assets/svg/payment-lock.svg\" class=\"mxn-desktop\"></svg-icon>\r\n                  {{ 'GiftForm.Submit' | translate }}&nbsp;<span *ngIf=\"total_cost\">{{\r\n                    total_cost | money\r\n                  }}</span>\r\n                </button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n      </section>\r\n    </ng-template>\r\n\r\n    <section class=\"section section_mb-4 section_sz-3\" *ngIf=\"first_logos\">\r\n      <app-gifts-related [logos]=\"first_logos\" [title]=\"first_logos_title\" [slider_settings]=\"first_logos_slider_settings\"></app-gifts-related>\r\n    </section>\r\n\r\n    <section\r\n      class=\"section section_sz-7 section_bg-1 section_mb-4\"\r\n      *ngIf=\"gift && gift.tabs && showGiftTabs\"\r\n    >\r\n      <app-tabs sz=\"2\" headMb=\"2\" [skin]=\"skin\" theme=\"2\">\r\n        <app-tabs-tab *ngFor=\"let tab of gift.tabs\" [tabTitle]=\"tab.title | multilang\">\r\n          <div class=\"r-tab r-tab_html r-tab_html_rules\" [innerHTML]=\"tab.text | multilang\"></div>\r\n        </app-tabs-tab>\r\n      </app-tabs>\r\n    </section>\r\n\r\n<!--    <section-->\r\n<!--      *ngIf=\"showActivate\"-->\r\n<!--      class=\"section section_sz-7 section_bg-2 section_mb-4\">-->\r\n<!--      <app-b-activate [fetchGifts]=\"true\" [listenActivate]=\"true\"></app-b-activate>-->\r\n<!--    </section>-->\r\n    <div id=\"activate\" [ngStyle]=\"{'min-height.px': showActivate ? 400 : 0}\">\r\n      <app-b-activate *ngIf=\"showActivate\" [fetchGifts]=\"true\" [listenActivate]=\"true\" theme=\"2\" [text]=\"activate_text\"></app-b-activate>\r\n    </div>\r\n\r\n    <section class=\"section section_mb-4 section_sz-3\" *ngIf=\"second_logos\">\r\n      <app-gifts-related [logos]=\"second_logos\" [title]=\"second_logos_title\" [slider_settings]=\"second_logos_slider_settings\"></app-gifts-related>\r\n    </section>\r\n\r\n    <section class=\"section section_mb-4 section_sz-3\" *ngIf=\"related\">\r\n      <app-gifts-related [gifts]=\"related\" [title]=\"relatedTitle\"></app-gifts-related>\r\n    </section>\r\n\r\n    <section class=\"section section_sz-4 section_bg-3 section_mb-4\" *ngIf=\"showSubscribeForm\">\r\n      <app-subscribe-form\r\n        [title]=\"subscribeFormTitle\"\r\n        [showCompany]=\"showSubscribeCompany\"\r\n        [showMessage]=\"showSubscribeMessage\"\r\n        [showPhone]=\"showSubscribePhone\"\r\n        theme=\"2\"\r\n      ></app-subscribe-form>\r\n    </section>\r\n  </div>\r\n</app-layout-gift>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts-related/gifts-related.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts-related/gifts-related.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"gifts-related\" *ngIf=\"gifts || logos\">\r\n  <ng-template #controlsTmp>\r\n    <div class=\"gifts-related__controls\" *ngIf=\"showControls\">\r\n      <div class=\"gifts-related__controls-item\">\r\n        <div\r\n          [ngClass]=\"{ 'f-disabled': isFirstSlide }\"\r\n          class=\"gifts-related__controls-btn gifts-related__controls-btn_left mxn-cur-p\"\r\n          (click)=\"swiper.prevSlide()\"\r\n        ></div>\r\n      </div>\r\n      <div class=\"gifts-related__controls-item\">\r\n        <div\r\n          [ngClass]=\"{ 'f-disabled': isLastSlide }\"\r\n          class=\"gifts-related__controls-btn gifts-related__controls-btn_right mxn-cur-p\"\r\n          (click)=\"swiper.nextSlide()\"\r\n        ></div>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n  <header class=\"gifts-related__head\">\r\n    <div class=\"gifts-related__head-item\">\r\n      <h4 class=\"gifts-related__title\" *ngIf=\"title\" [innerHTML]=\"title\"></h4>\r\n    </div>\r\n    <div class=\"gifts-related__head-item\" *ngIf=\"isDesktop\">\r\n      <ng-container *ngTemplateOutlet=\"controlsTmp\"></ng-container>\r\n    </div>\r\n  </header>\r\n  <div class=\"gifts-related__body\" [ngClass]=\"{'mxn-cur-p': showControls}\">\r\n    <div\r\n      class=\"swiper-container\"\r\n      [swiper]=\"sliderSettings\"\r\n      [index]=\"0\"\r\n      (indexChange)=\"onIndexChange($event)\"\r\n    >\r\n      <div class=\"gifts-related__swiper-wrapper swiper-wrapper\" *ngIf=\"gifts\">\r\n          <div class=\"swiper-slide\" *ngFor=\"let gift of gifts\">\r\n            <app-gift-card [gift]=\"gift\" [showRelated]=\"false\" [showTable]=\"false\"></app-gift-card>\r\n          </div>\r\n      </div>\r\n      <div class=\"gifts-related__swiper-wrapper swiper-wrapper\" *ngIf=\"logos\">\r\n        <div class=\"swiper-slide\" *ngFor=\"let logo of logos\">\r\n          <div class=\"gifts-related__logo-wrap\" *ngIf=\"!logo.url\">\r\n            <img [src]=\"logo.img\" alt=\"\" class=\"gifts-related__logo{{logo.classes ? ' ' + logo.classes : ''}}\">\r\n          </div>\r\n          <a [href]=\"logo.url | safe: 'url'\" target=\"_blank\" class=\"gifts-related__logo-wrap\" *ngIf=\"logo.url\">\r\n            <img [src]=\"logo.img\" alt=\"\" class=\"gifts-related__logo{{logo.classes ? ' ' + logo.classes : ''}}\">\r\n          </a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"gifts-related__footer\" *ngIf=\"!isDesktop\">\r\n    <ng-container *ngTemplateOutlet=\"controlsTmp\"></ng-container>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./src/app/components/card-buttons/card-buttons.component.styl":
/*!*********************************************************************!*\
  !*** ./src/app/components/card-buttons/card-buttons.component.styl ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/components/card-buttons/card-buttons.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jYXJkLWJ1dHRvbnMvY2FyZC1idXR0b25zLmNvbXBvbmVudC5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHFGQUFxRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FyZC1idXR0b25zL2NhcmQtYnV0dG9ucy5jb21wb25lbnQuc3R5bCJ9 */");

/***/ }),

/***/ "./src/app/components/card-buttons/card-buttons.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/card-buttons/card-buttons.component.ts ***!
  \*******************************************************************/
/*! exports provided: CardButtonsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardButtonsComponent", function() { return CardButtonsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CardButtonsComponent = /** @class */ (function () {
    function CardButtonsComponent() {
        this.sz = '1';
        this.theme = '1';
        this.custom = false;
        this.route = null;
        this.priceClick = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.customValueShowHide = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.showCustomInput = false;
        this.maxValuePattern = /^[1-9]+\d*$/;
        this.submitted = false;
    }
    CardButtonsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currPrice = this.price;
        this.prevPrice = this.price;
        this.currCustomValue = this.customValue || 0;
        if (this.price && this.prices && !this.prices.some(function (p) { return p === _this.price; })) {
            // if (this.isValidValue(this.price)) {
            this.currCustomValue = this.price / 100;
            this.onCustomValueSubmit();
            // }
        }
    };
    CardButtonsComponent.prototype.onPriceClick = function (price) {
        if (this.showCustomInput)
            return;
        this.currPrice = this.currPrice !== price ? price : undefined;
        this.showCustomInput = false;
        this.currCustomValue = undefined;
        this.priceClick.emit(this.currPrice);
    };
    CardButtonsComponent.prototype.closeCustomValue = function () {
        this.showCustomInput = false;
        this.customValueShowHide.emit(false);
    };
    CardButtonsComponent.prototype.openCustomValue = function () {
        this.showCustomInput = true;
        this.customValueShowHide.emit(true);
    };
    CardButtonsComponent.prototype.isValidValue = function (value) {
        if (!value)
            return !!value;
        var min = this.prices_range[0];
        var max = this.prices_range[1];
        if (this.prices_range && (min || max)) {
            return (max && value <= max) || (min && value >= min);
        }
        else {
            return true;
        }
    };
    CardButtonsComponent.prototype.onPopoverOutsideClick = function () {
        var _this = this;
        this.closeCustomValue();
        setTimeout(function () {
            _this.currCustomValue = _this.prevCustomValue;
            if (!_this.isValidValue(_this.currCustomValue)) {
                _this.currPrice = _this.prevPrice;
                _this.prevPrice = _this.currPrice;
            }
            else {
                _this.currPrice = undefined;
                _this.prevPrice = _this.currPrice;
            }
        }, 1);
    };
    CardButtonsComponent.prototype.customValueToggle = function () {
        if (this.showCustomInput) {
            this.currCustomValue = this.prevCustomValue;
            if (!this.currCustomValue) {
                this.currPrice = this.prevPrice;
                this.prevPrice = this.currPrice;
            }
            this.closeCustomValue();
        }
        else {
            this.prevCustomValue = this.currCustomValue;
            this.prevPrice = this.currPrice;
            this.currPrice = undefined;
            this.openCustomValue();
        }
        // this.prevCustomValue = this.currCustomValue;
    };
    CardButtonsComponent.prototype.onCustomValueClick = function () {
        this.customValueToggle();
    };
    CardButtonsComponent.prototype.onCustomValueSubmit = function () {
        this.submitted = true;
        var value = (this.currCustomValue || 0) * 100;
        if (!this.isValidValue(value)) {
            return;
        }
        this.closeCustomValue();
        if (!value) {
            this.currPrice = this.prevPrice;
            this.prevPrice = this.currPrice;
        }
        else {
            this.currPrice = value;
            this.priceClick.emit(this.currPrice);
        }
        // if (this.currCustomValue) {
        // }
        // else {
        //   this.currPrice = this.prevPrice;
        //   this.prevPrice = this.currPrice;
        // }
        this.prevCustomValue = value;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], CardButtonsComponent.prototype, "prices", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CardButtonsComponent.prototype, "gift", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CardButtonsComponent.prototype, "sz", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CardButtonsComponent.prototype, "theme", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CardButtonsComponent.prototype, "skin", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CardButtonsComponent.prototype, "custom", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], CardButtonsComponent.prototype, "customValue", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CardButtonsComponent.prototype, "route", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CardButtonsComponent.prototype, "price", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], CardButtonsComponent.prototype, "prices_range", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CardButtonsComponent.prototype, "fee", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], CardButtonsComponent.prototype, "priceClick", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], CardButtonsComponent.prototype, "customValueShowHide", void 0);
    CardButtonsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-card-buttons',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./card-buttons.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/card-buttons/card-buttons.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./card-buttons.component.styl */ "./src/app/components/card-buttons/card-buttons.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CardButtonsComponent);
    return CardButtonsComponent;
}());



/***/ }),

/***/ "./src/app/components/gift/gift-form/gift-form.component.styl":
/*!********************************************************************!*\
  !*** ./src/app/components/gift/gift-form/gift-form.component.styl ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/components/gift/gift-form/gift-form.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9naWZ0L2dpZnQtZm9ybS9naWZ0LWZvcm0uY29tcG9uZW50LnN0eWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0ZBQW9GIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9naWZ0L2dpZnQtZm9ybS9naWZ0LWZvcm0uY29tcG9uZW50LnN0eWwifQ== */");

/***/ }),

/***/ "./src/app/components/gift/gift-form/gift-form.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/components/gift/gift-form/gift-form.component.ts ***!
  \******************************************************************/
/*! exports provided: GiftFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftFormComponent", function() { return GiftFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _form_helper_form_helper_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../form-helper/form-helper.component */ "./src/app/components/form-helper/form-helper.component.ts");




var GiftFormComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](GiftFormComponent, _super);
    function GiftFormComponent() {
        var _this = _super.call(this) || this;
        _this.type = 'gift';
        _this.index = 0;
        _this.phoneInput = true;
        _this.showAddresseeData = false;
        return _this;
    }
    GiftFormComponent.prototype.ngOnInit = function () {
    };
    GiftFormComponent.prototype.ngOnChanges = function (changes) {
        if (changes.md && changes.md.currentValue) {
            if (this.md.items) {
                var item = this.md.items[this.index];
                this.mdAddressee = (item || {}).addressee;
                // this.mdClient = (item || {}).;
            }
        }
        if (changes.addresseeData && changes.addresseeData.currentValue) {
            this._addresseeData = this.addresseeData;
        }
    };
    GiftFormComponent.prototype.searchAddresseeDataFn = function (term, item) {
        var normalizeString = function (str) {
            return (str || '').replace(/\s/, '').toLowerCase();
        };
        return normalizeString(item.name).indexOf(normalizeString(term)) > -1;
    };
    GiftFormComponent.prototype.onAddresseeNameChange = function (term) {
        var _this = this;
        this.selectedItem = null;
        clearTimeout(this.debounceTimer);
        this.debounceTimer = setTimeout(function () {
            _this.checkAndFilterData(term);
        }, 100);
    };
    GiftFormComponent.prototype.checkAndFilterData = function (term) {
        var _this = this;
        if (!this._addresseeData)
            return;
        this._addresseeData = this.addresseeData.filter(function (item) {
            return _this.searchAddresseeDataFn(term, item);
        });
        if (!term) {
            this._addresseeData = this.addresseeData;
        }
        this.showAddresseeData = !!(this._addresseeData || []).length;
    };
    GiftFormComponent.prototype.onAddresseeDataItemClick = function (e, item) {
        e.stopPropagation();
        this.mdAddressee.name = item.name;
        this.mdAddressee.email = item.email;
        this.selectedItem = item;
        this.showAddresseeData = false;
        // this.mdAddressee.name = item.name;
    };
    GiftFormComponent.prototype.onAddresseeNameFocus = function (term, e) {
        if (this.selectedItem) {
            this.checkAndFilterData('');
        }
        else {
            this.checkAndFilterData(term);
        }
        this.fhOnInputFocus(e);
    };
    GiftFormComponent.prototype.onAddresseeNameBlur = function (e) {
        var _this = this;
        setTimeout(function () {
            _this.showAddresseeData = false;
            if (e.target === _this.fhFocusedElement) {
                _this.fhOnInputBlur(e);
            }
        }, 200);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftFormComponent.prototype, "md", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftFormComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftFormComponent.prototype, "index", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftFormComponent.prototype, "phoneInput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GiftFormComponent.prototype, "addresseeData", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftFormComponent.prototype, "mdItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], GiftFormComponent.prototype, "formSubmitted", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], GiftFormComponent.prototype, "form", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('addresseeNameInp', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgModel"])
    ], GiftFormComponent.prototype, "_addresseeNameInp", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('addresseeEmailInp', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgModel"])
    ], GiftFormComponent.prototype, "_addresseeEmailInp", void 0);
    GiftFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gift-form',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gift-form.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gift/gift-form/gift-form.component.html")).default,
            viewProviders: [{ provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ControlContainer"], useExisting: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"] }],
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gift-form.component.styl */ "./src/app/components/gift/gift-form/gift-form.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GiftFormComponent);
    return GiftFormComponent;
}(_form_helper_form_helper_component__WEBPACK_IMPORTED_MODULE_3__["FormHelperComponent"]));



/***/ }),

/***/ "./src/app/components/gift/gift.component.styl":
/*!*****************************************************!*\
  !*** ./src/app/components/gift/gift.component.styl ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".gift__head {\n  margin-bottom: 27px;\n}\n.gift__head-bl {\n  margin-bottom: 28px;\n}\n.gift__head-bl_heading:after {\n  content: '';\n  width: 100%;\n  font-size: 0;\n  line-height: 0;\n  height: 0;\n  clear: both;\n}\n.gift__head-row {\n  margin-bottom: 12px;\n}\n.gift__head-row:last-child,\n.gift__head-bl:last-child {\n  margin-bottom: 0;\n}\n.gift__head-item_logo {\n  float: right;\n}\n.gift__head-logo {\n  display: inline-block;\n  max-width: 56px;\n  max-height: 56px;\n  width: auto;\n  height: auto;\n}\n.gift__title {\n  display: flex;\n  padding-right: 12px;\n  font-size: 28px;\n  font-weight: 600;\n}\n.gift__summary {\n  font-size: 18px;\n}\n.gift__form {\n  color: #000;\n}\n.gift__body-col,\n.gift__section {\n  margin-bottom: 30px;\n}\n.gift__body-col:last-child,\n.gift__section:last-child {\n  margin-bottom: 0;\n}\n.gift__section-row {\n  margin-bottom: 16px;\n}\n.gift__section-row:last-child {\n  margin-bottom: 0;\n}\n.gift__fieldset:not(:last-child) {\n  padding-bottom: 32px;\n  margin-bottom: 32px;\n  border-bottom: 1px solid #f5f2ee;\n}\n.gift__fieldset-col_card {\n  max-width: 432px;\n}\n.gift__footer {\n  margin-top: 22px;\n}\n.gift__footer-row {\n  margin-bottom: -26px;\n}\n.gift__footer-item {\n  margin-bottom: 26px;\n}\n.gift__card {\n  max-width: 432px;\n}\n.gift__more {\n  margin-top: 32px;\n}\n@media all and (min-width: 992px) {\n  .gift__head {\n    margin-bottom: 45px;\n  }\n  .gift__head-bl {\n    margin-bottom: 28px;\n  }\n  .gift__head-bl_heading {\n    display: flex;\n    justify-content: space-between;\n  }\n  .gift__head-bl_heading:after {\n    content: none;\n  }\n  .gift__head-row {\n    margin-bottom: 10px;\n  }\n  .gift__head-row:last-child,\n  .gift__head-bl:last-child {\n    margin-bottom: 0;\n  }\n  .gift__head-item_title {\n    order: 1;\n    flex: 1 0 66%;\n    padding-right: 18%;\n  }\n  .gift__head-item_logo {\n    float: none;\n    order: 2;\n    text-align: right;\n  }\n  .gift__head-logo {\n    max-width: 150px;\n    max-height: 150px;\n  }\n  .gift__title {\n    font-size: 52px;\n  }\n  .gift__text {\n    font-size: 24px;\n  }\n  .gift__summary {\n    font-size: 24px;\n  }\n  .gift__section {\n    margin-bottom: 34px;\n  }\n  .gift__body-row {\n    display: flex;\n  }\n  .gift__body-col {\n    flex: 1;\n    margin-bottom: 0;\n  }\n  .gift__body-col_card {\n    order: 2;\n    flex: 0 1 432px;\n    margin-left: 48px;\n    display: flex;\n    max-height: 670px;\n  }\n  .gift__body-col_form {\n    order: 1;\n    padding-top: 5px;\n  }\n  .gift__footer {\n    margin-top: 40px;\n  }\n  .gift__footer-row_buttons {\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n  }\n  .gift__footer-item_btn {\n    min-width: 226px;\n  }\n}\n.f-gift__head {\n  margin-bottom: 34px;\n}\n.f-gift__heading {\n  margin-bottom: 12px;\n}\n.f-gift__form-row {\n  margin-bottom: 16px;\n}\n.f-gift__form-row:last-child {\n  margin-bottom: 0;\n}\n@media all and (min-width: 992px) {\n  .f-gift__heading {\n    margin-bottom: 22px;\n  }\n  .f-gift__form-row {\n    margin-bottom: 24px;\n  }\n}\n.tab-list {\n  counter-reset: item;\n  font-size: 16px;\n  font-weight: 400;\n  line-height: 1.3;\n  color: #060606;\n}\n.tab-list a {\n  color: #bda776;\n}\n.tab-list__item {\n  display: flex;\n  position: relative;\n  margin-bottom: 24px;\n}\n.tab-list__item:last-child {\n  margin-bottom: 0;\n}\n.tab-list__item:before {\n  content: counter(item);\n  counter-increment: item;\n  display: inline-flex;\n  align-items: center;\n  justify-content: center;\n  box-sizing: border-box;\n  flex: 0 1 32px;\n  height: 32px;\n  padding: 0 6px;\n  margin-right: 16px;\n  border-radius: 10px;\n  font-size: 18px;\n  font-weight: 500;\n  line-height: 1;\n  background-color: #a09488;\n  color: #f5f2ee;\n}\n.tab-list__item-body {\n  flex: 1;\n}\n.tab-list__title {\n  font-weight: 500;\n}\n.tab-list__sub {\n  color: #a09488;\n}\n@media all and (min-width: 992px) {\n  .tab-list__item {\n    margin-bottom: 40px;\n  }\n  .tab-list__item:before {\n    flex: 0 1 40px;\n    min-height: 40px;\n    margin-right: 24px;\n    font-size: 24px;\n  }\n}\n.b-fee {\n  background: #ff3838;\n  color: #fff;\n}\n.b-fee_sz-1 {\n  padding: 14px 12px 14px 12px;\n  border-radius: 31px;\n  font-size: 13px;\n  line-height: 1.3;\n}\n.b-fee__row {\n  display: flex;\n  align-items: center;\n}\n.b-fee__item_hr_sz-1 {\n  padding: 5px 10px 5px 0;\n  margin-right: 15px;\n  border-right: 1px solid rgba(255,255,255,0.2);\n}\n.b-fee__val {\n  line-height: 1;\n}\n.b-fee__val_sz-1 {\n  font-size: 18px;\n}\n.b-fee__val_theme-1 {\n  font-weight: 600;\n}\n@media all and (min-width: 768px) {\n  .b-fee {\n    padding: 26px 60px 26px 36px;\n    font-size: 24px;\n  }\n  .b-fee__item_hr_sz-1 {\n    padding: 10px 26px 10px 0;\n    margin-right: 30px;\n  }\n  .b-fee__val_sz-1 {\n    font-size: 52px;\n  }\n}\n.special-text {\n  max-width: 912px;\n}\n.special-text__article {\n  font-size: 16px;\n  line-height: 1.2;\n  font-weight: 500;\n}\n.special-text__article h1 {\n  font-size: 28px;\n  margin-bottom: 20px;\n}\n.special-text__article strong {\n  color: var(--skin-active-color);\n}\n.special-text__article p {\n  margin: 10px 0;\n}\n.special-text__article p:last-child {\n  margin-bottom: 0;\n}\n.special-text__article img {\n  width: 100px;\n  height: auto;\n  margin: 0 8px;\n}\n.special-text__article .footer-btn {\n  margin-top: 30px;\n}\n@media all and (min-width: 992px) {\n  .special-text__article {\n    font-size: 32px;\n  }\n  .special-text__article h1 {\n    font-size: 70px;\n    margin-bottom: 50px;\n  }\n  .special-text__article p {\n    margin: 16px 0;\n  }\n}\n.gifts-related__head {\n  display: flex;\n  justify-content: space-between;\n  align-items: flex-end;\n  margin-bottom: 18px;\n}\n.gifts-related__title {\n  font-size: 28px;\n  font-weight: 600;\n  line-height: 1.3;\n}\n.gifts-related__logo {\n  width: 110px;\n  max-width: 100%;\n  height: auto;\n}\n.gifts-related__logo.sz-2 {\n  width: 220px;\n}\n.gifts-related__logo-wrap {\n  display: block;\n  text-align: center;\n}\n.gifts-related__swiper-wrapper {\n  align-items: center;\n}\n.gifts-related__controls {\n  display: inline-flex;\n}\n.gifts-related__controls-item {\n  margin-right: 26px;\n}\n.gifts-related__controls-item:last-child {\n  margin-right: 0;\n}\n.gifts-related__controls-btn {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 50px;\n  height: 50px;\n  border: 2px solid #fff;\n  border-radius: 100%;\n  transition: opacity 0.2s ease-in-out;\n}\n.gifts-related__controls-btn:after {\n  content: '';\n  width: 10px;\n  height: 10px;\n  border: 2px solid #fff;\n  border-bottom-color: transparent;\n  border-left-color: transparent;\n}\n.gifts-related__controls-btn_right:after {\n  transform: rotate(45deg);\n  margin-left: -5px;\n}\n.gifts-related__controls-btn_left:after {\n  transform: rotate(-135deg);\n  margin-right: -5px;\n}\n.gifts-related__controls-btn.f-disabled {\n  opacity: 0.2;\n  cursor: initial;\n}\n.gifts-related__footer {\n  margin-top: 20px;\n  text-align: center;\n}\n@media all and (min-width: 992px) {\n  .gifts-related__head {\n    padding-right: 16px;\n    margin-bottom: 37px;\n  }\n  .gifts-related__logo {\n    width: 120px;\n  }\n  .gifts-related__logo.sz-2 {\n    width: 240px;\n  }\n  .gifts-related__title {\n    font-size: 32px;\n    font-weight: 500;\n  }\n}\n.r-tab {\n  color: #060606;\n  font-size: 18px;\n  font-weight: 500;\n}\n.r-tab_html > p {\n  margin: 8px 0;\n}\n.r-tab_html > p:last-child {\n  margin-bottom: 0;\n}\n.r-tab_html > p:first-child {\n  margin-top: 0;\n}\n/*# sourceMappingURL=src/app/components/gift/gift.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9naWZ0L3NyYy9zdHlsZXMvYmxvY2tzL2dpZnQuc3R5bCIsInNyYy9hcHAvY29tcG9uZW50cy9naWZ0L2dpZnQuY29tcG9uZW50LnN0eWwiLCJzcmMvYXBwL2NvbXBvbmVudHMvZ2lmdC9zcmMvc3R5bGVzL2hlbHBlcnMuc3R5bCIsInNyYy9hcHAvY29tcG9uZW50cy9naWZ0L3NyYy9zdHlsZXMvYmxvY2tzL2YtZ2lmdC5zdHlsIiwic3JjL2FwcC9jb21wb25lbnRzL2dpZnQvc3JjL3N0eWxlcy9ibG9ja3MvdGFiLWxpc3Quc3R5bCIsInNyYy9hcHAvY29tcG9uZW50cy9naWZ0L3NyYy9zdHlsZXMvYmxvY2tzL2ItZmVlLnN0eWwiLCJzcmMvYXBwL2NvbXBvbmVudHMvZ2lmdC9zcmMvc3R5bGVzL2Jsb2Nrcy9zcGVjaWFsLXRleHQuc3R5bCIsInNyYy9hcHAvY29tcG9uZW50cy9naWZ0L3NyYy9zdHlsZXMvYmxvY2tzL2dpZnRzLXJlbGF0ZWQuc3R5bCIsInNyYy9hcHAvY29tcG9uZW50cy9naWZ0L3NyYy9zdHlsZXMvYmxvY2tzL3ItdGFiLnN0eWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxtQkFBZTtBQ0FuQjtBRENJO0VBQ0UsbUJBQWU7QUNDckI7QURDUTtFQUNFLFdBQVE7RUFDUixXQUFPO0VBQ1AsWUFBVztFQUNYLGNBQWE7RUFDYixTQUFRO0VBQ1IsV0FBTTtBQ0NoQjtBREFJO0VBQ0UsbUJBQWU7QUNFckI7QURBTTs7RUFDRSxnQkFBZTtBQ0d2QjtBRERNO0VBQ0UsWUFBTztBQ0dmO0FEQUk7RUFDRSxxQkFBUztFQUNULGVBQVc7RUFDWCxnQkFBWTtFQUNaLFdBQU87RUFDUCxZQUFRO0FDRWQ7QURBRTtFQUNFLGFBQVM7RUFDVCxtQkFBZTtFQUNmLGVBQVc7RUFDWCxnQkFBYTtBQ0VqQjtBREFFO0VBQ0UsZUFBVztBQ0VmO0FEQUU7RUFDRSxXQUFPO0FDRVg7QURBRTs7RUFDRSxtQkFBZTtBQ0duQjtBREZJOztFQUNFLGdCQUFlO0FDS3JCO0FESEk7RUFDRSxtQkFBZTtBQ0tyQjtBREpNO0VBQ0UsZ0JBQWU7QUNNdkI7QURKSTtFQUNFLG9CQUFnQjtFQUNoQixtQkFBZTtFQUNmLGdDQUFlO0FDTXJCO0FESE07RUFDRSxnQkFBVztBQ0tuQjtBREpFO0VBQ0UsZ0JBQVk7QUNNaEI7QURMSTtFQUNFLG9CQUFlO0FDT3JCO0FETkk7RUFDRSxtQkFBZTtBQ1FyQjtBRFBFO0VBQ0UsZ0JBQVU7QUNTZDtBRFJFO0VBQ0UsZ0JBQVk7QUNVaEI7QUMzRWlDO0VGcUU3QjtJQUNFLG1CQUFlO0VDU25CO0VEUkk7SUFDRSxtQkFBZTtFQ1VyQjtFRFRNO0lBQ0UsYUFBUztJQUNULDhCQUFnQjtFQ1d4QjtFRFZRO0lBQ0UsYUFBUTtFQ1lsQjtFRFhJO0lBQ0UsbUJBQWU7RUNhckI7RURYTTs7SUFDRSxnQkFBZTtFQ2N2QjtFRFpNO0lBQ0UsUUFBTztJQUNQLGFBQU07SUFDTixrQkFBZTtFQ2N2QjtFRGJNO0lBQ0UsV0FBTztJQUNQLFFBQU87SUFDUCxpQkFBWTtFQ2VwQjtFRGRJO0lBQ0UsZ0JBQVU7SUFDVixpQkFBVztFQ2dCakI7RURkRTtJQUNFLGVBQVc7RUNnQmY7RURmRTtJQUNFLGVBQVc7RUNpQmY7RURoQkU7SUFDRSxlQUFXO0VDa0JmO0VEakJFO0lBQ0UsbUJBQWU7RUNtQm5CO0VEakJJO0lBQ0UsYUFBUztFQ21CZjtFRGxCSTtJQUNFLE9BQU07SUFDTixnQkFBZTtFQ29CckI7RURuQk07SUFDRSxRQUFPO0lBQ1AsZUFBTTtJQUNOLGlCQUFhO0lBQ2IsYUFBUztJQUNULGlCQUFXO0VDcUJuQjtFRHBCTTtJQUNFLFFBQU87SUFDUCxnQkFBYTtFQ3NCckI7RURyQkU7SUFDRSxnQkFBWTtFQ3VCaEI7RURyQk07SUFDRSxhQUFTO0lBQ1QsbUJBQVk7SUFDWiw4QkFBZ0I7RUN1QnhCO0VEckJNO0lBQ0UsZ0JBQVU7RUN1QmxCO0FBQ0Y7QUUzSkU7RUFDRSxtQkFBZTtBRjZKbkI7QUU1SkU7RUFDRSxtQkFBZTtBRjhKbkI7QUU1Skk7RUFDRSxtQkFBZTtBRjhKckI7QUU3Sk07RUFDRSxnQkFBZTtBRitKdkI7QUNwS2lDO0VDVTdCO0lBQ0UsbUJBQWU7RUY2Sm5CO0VFM0pJO0lBQ0UsbUJBQWU7RUY2SnJCO0FBQ0Y7QUdoTEE7RUFDRSxtQkFBZTtFQUNmLGVBQVc7RUFDWCxnQkFBYTtFQUNiLGdCQUFhO0VBQ2IsY0FBTztBSGtMVDtBR2pMRTtFQUNFLGNBQU87QUhtTFg7QUdsTEU7RUFDRSxhQUFTO0VBQ1Qsa0JBQVU7RUFDVixtQkFBZTtBSG9MbkI7QUduTEk7RUFDRSxnQkFBZTtBSHFMckI7QUdwTEk7RUFDRSxzQkFBcUI7RUFDckIsdUJBQW1CO0VBQ25CLG9CQUFTO0VBQ1QsbUJBQVk7RUFDWix1QkFBaUI7RUFDakIsc0JBQVc7RUFDWCxjQUFNO0VBQ04sWUFBUTtFQUNSLGNBQVM7RUFDVCxrQkFBYztFQUNkLG1CQUFlO0VBQ2YsZUFBVztFQUNYLGdCQUFhO0VBQ2IsY0FBYTtFQUNiLHlCQUFrQjtFQUNsQixjQUFPO0FIc0xiO0FHckxJO0VBQ0UsT0FBTTtBSHVMWjtBR3RMRTtFQUNFLGdCQUFhO0FId0xqQjtBR3ZMRTtFQUNFLGNBQU87QUh5TFg7QUN6TmlDO0VFb0M3QjtJQUNFLG1CQUFlO0VId0xuQjtFR3ZMSTtJQUNFLGNBQU07SUFDTixnQkFBVztJQUNYLGtCQUFjO0lBQ2QsZUFBVztFSHlMakI7QUFDRjtBSXhPQTtFQUNFLG1CQUFZO0VBQ1osV0FBTztBSjBPVDtBSXpPRTtFQUNFLDRCQUFTO0VBQ1QsbUJBQWU7RUFDZixlQUFXO0VBQ1gsZ0JBQWE7QUoyT2pCO0FJek9FO0VBQ0UsYUFBUztFQUNULG1CQUFZO0FKMk9oQjtBSXZPTTtFQUNFLHVCQUFTO0VBQ1Qsa0JBQWM7RUFDZCw2Q0FBYztBSnlPdEI7QUl2T0U7RUFDRSxjQUFhO0FKeU9qQjtBSXhPSTtFQUNFLGVBQVc7QUowT2pCO0FJek9JO0VBQ0UsZ0JBQWE7QUoyT25CO0FDcFBzQztFR1lwQztJQUNFLDRCQUFTO0lBQ1QsZUFBVztFSjJPYjtFSXhPTTtJQUNFLHlCQUFTO0lBQ1Qsa0JBQWM7RUowT3RCO0VJeE9JO0lBQ0UsZUFBVztFSjBPakI7QUFDRjtBS2pSQTtFQUNFLGdCQUFVO0FMbVJaO0FLbFJFO0VBQ0UsZUFBVztFQUNYLGdCQUFhO0VBQ2IsZ0JBQWE7QUxvUmpCO0FLblJJO0VBQ0UsZUFBVztFQUNYLG1CQUFlO0FMcVJyQjtBS3BSSTtFQUNFLCtCQUE4QjtBTHNScEM7QUtyUkk7RUFDRSxjQUFRO0FMdVJkO0FLdFJNO0VBQ0UsZ0JBQWU7QUx3UnZCO0FLdlJJO0VBQ0UsWUFBTztFQUNQLFlBQVE7RUFDUixhQUFRO0FMeVJkO0FLeFJJO0VBQ0UsZ0JBQVk7QUwwUmxCO0FDMVNpQztFSW9CN0I7SUFDRSxlQUFXO0VMeVJmO0VLeFJJO0lBQ0UsZUFBVztJQUNYLG1CQUFlO0VMMFJyQjtFS3pSSTtJQUNFLGNBQVE7RUwyUmQ7QUFDRjtBTXpURTtFQUNFLGFBQVM7RUFDVCw4QkFBaUI7RUFDakIscUJBQVk7RUFDWixtQkFBZTtBTjJUbkI7QU0xVEU7RUFDRSxlQUFXO0VBQ1gsZ0JBQWE7RUFDYixnQkFBYTtBTjRUakI7QU0xVEU7RUFDRSxZQUFNO0VBQ04sZUFBVTtFQUNWLFlBQVE7QU40VFo7QU0zVEk7RUFDRSxZQUFPO0FONlRiO0FNNVRJO0VBQ0UsY0FBUztFQUNULGtCQUFZO0FOOFRsQjtBTTNUSTtFQUNFLG1CQUFZO0FONlRsQjtBTTNURTtFQUNFLG9CQUFTO0FONlRiO0FNNVRJO0VBQ0Usa0JBQWM7QU44VHBCO0FNN1RNO0VBQ0UsZUFBYztBTitUdEI7QU05VEk7RUFDRSxhQUFTO0VBQ1QsbUJBQVk7RUFDWix1QkFBaUI7RUFDakIsV0FBTztFQUNQLFlBQVE7RUFDUixzQkFBUTtFQUNSLG1CQUFlO0VBQ2Ysb0NBQVc7QU5nVWpCO0FNL1RNO0VBQ0UsV0FBUTtFQUNSLFdBQU87RUFDUCxZQUFRO0VBQ1Isc0JBQVE7RUFDUixnQ0FBcUI7RUFDckIsOEJBQW1CO0FOaVUzQjtBTWhVTTtFQUNFLHdCQUF1QjtFQUN2QixpQkFBYTtBTmtVckI7QU1qVU07RUFDRSwwQkFBeUI7RUFDekIsa0JBQWM7QU5tVXRCO0FNalVNO0VBQ0UsWUFBUztFQUNULGVBQVE7QU5tVWhCO0FNalVFO0VBQ0UsZ0JBQVk7RUFDWixrQkFBWTtBTm1VaEI7QUMzWGlDO0VLNEQ3QjtJQUNFLG1CQUFlO0lBQ2YsbUJBQWU7RU5rVW5CO0VNaFVFO0lBQ0UsWUFBTztFTmtVWDtFTWpVSTtJQUNFLFlBQU87RU5tVWI7RU1qVUU7SUFDRSxlQUFXO0lBQ1gsZ0JBQWE7RU5tVWpCO0FBQ0Y7QU8vWUE7RUFDRSxjQUFPO0VBQ1AsZUFBVztFQUNYLGdCQUFhO0FQaVpmO0FPL1lJO0VBQ0UsYUFBUTtBUGlaZDtBT2haTTtFQUNFLGdCQUFlO0FQa1p2QjtBT2paTTtFQUNFLGFBQVk7QVBtWnBCO0FBQ0EscUVBQXFFIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9naWZ0L2dpZnQuY29tcG9uZW50LnN0eWwiLCJzb3VyY2VzQ29udGVudCI6WyIuZ2lmdFxyXG4gICZfX2hlYWRcclxuICAgIG1hcmdpbi1ib3R0b206IDI3cHhcclxuICAgICYtYmxcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMjhweFxyXG4gICAgICAmX2hlYWRpbmdcclxuICAgICAgICAmOmFmdGVyXHJcbiAgICAgICAgICBjb250ZW50ICcnO1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCVcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMFxyXG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDBcclxuICAgICAgICAgIGhlaWdodDogMFxyXG4gICAgICAgICAgY2xlYXIgYm90aFxyXG4gICAgJi1yb3dcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMTJweFxyXG4gICAgJi1yb3csICYtYmxcclxuICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICAgJi1pdGVtXHJcbiAgICAgICZfbG9nb1xyXG4gICAgICAgIGZsb2F0OiByaWdodDtcclxuICAgICAgLy8mX3RpdGxlXHJcbiAgICAgIC8vICBwYWRkaW5nLXJpZ2h0OiAxOCVcclxuICAgICYtbG9nb1xyXG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgIG1heC13aWR0aDogNTZweDtcclxuICAgICAgbWF4LWhlaWdodDogNTZweDtcclxuICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICAgIGhlaWdodDogYXV0b1xyXG5cclxuICAmX190aXRsZVxyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDEycHhcclxuICAgIGZvbnQtc2l6ZTogMjhweFxyXG4gICAgZm9udC13ZWlnaHQ6IDYwMFxyXG5cclxuICAmX19zdW1tYXJ5XHJcbiAgICBmb250LXNpemU6IDE4cHhcclxuXHJcbiAgJl9fZm9ybVxyXG4gICAgY29sb3I6ICRjLTI7XHJcblxyXG4gICZfX2JvZHktY29sLCAmX19zZWN0aW9uXHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4XHJcbiAgICAmOmxhc3QtY2hpbGRcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICZfX3NlY3Rpb25cclxuICAgICYtcm93XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDE2cHhcclxuICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICZfX2ZpZWxkc2V0XHJcbiAgICAmOm5vdCg6bGFzdC1jaGlsZClcclxuICAgICAgcGFkZGluZy1ib3R0b206IDMycHhcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMzJweFxyXG4gICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJGMtY2FyZC0xLXRoZW1lLTEtaHJcclxuXHJcbiAgICAmLWNvbFxyXG4gICAgICAmX2NhcmRcclxuICAgICAgICBtYXgtd2lkdGg6IDQzMnB4XHJcbiAgJl9fZm9vdGVyXHJcbiAgICBtYXJnaW4tdG9wOiAyMnB4XHJcbiAgICAmLXJvd1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAtMjZweFxyXG4gICAgJi1pdGVtXHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDI2cHhcclxuICAmX19jYXJkXHJcbiAgICBtYXgtd2lkdGggNDMycHhcclxuICAmX19tb3JlXHJcbiAgICBtYXJnaW4tdG9wOiAzMnB4XHJcblxyXG4rZGVza3RvcCgpXHJcbiAgLmdpZnRcclxuICAgICZfX2hlYWRcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNDVweFxyXG4gICAgICAmLWJsXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjhweFxyXG4gICAgICAgICZfaGVhZGluZ1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudCBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgICAgJjphZnRlclxyXG4gICAgICAgICAgICBjb250ZW50IG5vbmVcclxuICAgICAgJi1yb3dcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4XHJcbiAgICAgICYtcm93LCAmLWJsXHJcbiAgICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwXHJcbiAgICAgICYtaXRlbVxyXG4gICAgICAgICZfdGl0bGVcclxuICAgICAgICAgIG9yZGVyOiAxXHJcbiAgICAgICAgICBmbGV4OiAxIDAgNjYlXHJcbiAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxOCVcclxuICAgICAgICAmX2xvZ29cclxuICAgICAgICAgIGZsb2F0OiBub25lO1xyXG4gICAgICAgICAgb3JkZXI6IDJcclxuICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgICAmLWxvZ29cclxuICAgICAgICBtYXgtd2lkdGggMTUwcHhcclxuICAgICAgICBtYXgtaGVpZ2h0IDE1MHB4XHJcblxyXG4gICAgJl9fdGl0bGVcclxuICAgICAgZm9udC1zaXplOiA1MnB4XHJcbiAgICAmX190ZXh0XHJcbiAgICAgIGZvbnQtc2l6ZTogMjRweFxyXG4gICAgJl9fc3VtbWFyeVxyXG4gICAgICBmb250LXNpemU6IDI0cHhcclxuICAgICZfX3NlY3Rpb25cclxuICAgICAgbWFyZ2luLWJvdHRvbTogMzRweFxyXG4gICAgJl9fYm9keVxyXG4gICAgICAmLXJvd1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICYtY29sXHJcbiAgICAgICAgZmxleDogMVxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuICAgICAgICAmX2NhcmRcclxuICAgICAgICAgIG9yZGVyOiAyXHJcbiAgICAgICAgICBmbGV4OiAwIDEgNDMycHhcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiA0OHB4XHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgbWF4LWhlaWdodCA2NzBweFxyXG4gICAgICAgICZfZm9ybVxyXG4gICAgICAgICAgb3JkZXI6IDFcclxuICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHhcclxuICAgICZfX2Zvb3RlclxyXG4gICAgICBtYXJnaW4tdG9wOiA0MHB4XHJcbiAgICAgICYtcm93XHJcbiAgICAgICAgJl9idXR0b25zXHJcbiAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgYWxpZ24taXRlbXMgY2VudGVyXHJcbiAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQgc3BhY2UtYmV0d2VlblxyXG4gICAgICAmLWl0ZW1cclxuICAgICAgICAmX2J0blxyXG4gICAgICAgICAgbWluLXdpZHRoIDIyNnB4XHJcbiIsIi5naWZ0X19oZWFkIHtcbiAgbWFyZ2luLWJvdHRvbTogMjdweDtcbn1cbi5naWZ0X19oZWFkLWJsIHtcbiAgbWFyZ2luLWJvdHRvbTogMjhweDtcbn1cbi5naWZ0X19oZWFkLWJsX2hlYWRpbmc6YWZ0ZXIge1xuICBjb250ZW50OiAnJztcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMDtcbiAgbGluZS1oZWlnaHQ6IDA7XG4gIGhlaWdodDogMDtcbiAgY2xlYXI6IGJvdGg7XG59XG4uZ2lmdF9faGVhZC1yb3cge1xuICBtYXJnaW4tYm90dG9tOiAxMnB4O1xufVxuLmdpZnRfX2hlYWQtcm93Omxhc3QtY2hpbGQsXG4uZ2lmdF9faGVhZC1ibDpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5naWZ0X19oZWFkLWl0ZW1fbG9nbyB7XG4gIGZsb2F0OiByaWdodDtcbn1cbi5naWZ0X19oZWFkLWxvZ28ge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1heC13aWR0aDogNTZweDtcbiAgbWF4LWhlaWdodDogNTZweDtcbiAgd2lkdGg6IGF1dG87XG4gIGhlaWdodDogYXV0bztcbn1cbi5naWZ0X190aXRsZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBhZGRpbmctcmlnaHQ6IDEycHg7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5naWZ0X19zdW1tYXJ5IHtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuLmdpZnRfX2Zvcm0ge1xuICBjb2xvcjogIzAwMDtcbn1cbi5naWZ0X19ib2R5LWNvbCxcbi5naWZ0X19zZWN0aW9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cbi5naWZ0X19ib2R5LWNvbDpsYXN0LWNoaWxkLFxuLmdpZnRfX3NlY3Rpb246bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4uZ2lmdF9fc2VjdGlvbi1yb3cge1xuICBtYXJnaW4tYm90dG9tOiAxNnB4O1xufVxuLmdpZnRfX3NlY3Rpb24tcm93Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLmdpZnRfX2ZpZWxkc2V0Om5vdCg6bGFzdC1jaGlsZCkge1xuICBwYWRkaW5nLWJvdHRvbTogMzJweDtcbiAgbWFyZ2luLWJvdHRvbTogMzJweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmNWYyZWU7XG59XG4uZ2lmdF9fZmllbGRzZXQtY29sX2NhcmQge1xuICBtYXgtd2lkdGg6IDQzMnB4O1xufVxuLmdpZnRfX2Zvb3RlciB7XG4gIG1hcmdpbi10b3A6IDIycHg7XG59XG4uZ2lmdF9fZm9vdGVyLXJvdyB7XG4gIG1hcmdpbi1ib3R0b206IC0yNnB4O1xufVxuLmdpZnRfX2Zvb3Rlci1pdGVtIHtcbiAgbWFyZ2luLWJvdHRvbTogMjZweDtcbn1cbi5naWZ0X19jYXJkIHtcbiAgbWF4LXdpZHRoOiA0MzJweDtcbn1cbi5naWZ0X19tb3JlIHtcbiAgbWFyZ2luLXRvcDogMzJweDtcbn1cbkBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6IDk5MnB4KSB7XG4gIC5naWZ0X19oZWFkIHtcbiAgICBtYXJnaW4tYm90dG9tOiA0NXB4O1xuICB9XG4gIC5naWZ0X19oZWFkLWJsIHtcbiAgICBtYXJnaW4tYm90dG9tOiAyOHB4O1xuICB9XG4gIC5naWZ0X19oZWFkLWJsX2hlYWRpbmcge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB9XG4gIC5naWZ0X19oZWFkLWJsX2hlYWRpbmc6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IG5vbmU7XG4gIH1cbiAgLmdpZnRfX2hlYWQtcm93IHtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICB9XG4gIC5naWZ0X19oZWFkLXJvdzpsYXN0LWNoaWxkLFxuICAuZ2lmdF9faGVhZC1ibDpsYXN0LWNoaWxkIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICB9XG4gIC5naWZ0X19oZWFkLWl0ZW1fdGl0bGUge1xuICAgIG9yZGVyOiAxO1xuICAgIGZsZXg6IDEgMCA2NiU7XG4gICAgcGFkZGluZy1yaWdodDogMTglO1xuICB9XG4gIC5naWZ0X19oZWFkLWl0ZW1fbG9nbyB7XG4gICAgZmxvYXQ6IG5vbmU7XG4gICAgb3JkZXI6IDI7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIH1cbiAgLmdpZnRfX2hlYWQtbG9nbyB7XG4gICAgbWF4LXdpZHRoOiAxNTBweDtcbiAgICBtYXgtaGVpZ2h0OiAxNTBweDtcbiAgfVxuICAuZ2lmdF9fdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogNTJweDtcbiAgfVxuICAuZ2lmdF9fdGV4dCB7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICB9XG4gIC5naWZ0X19zdW1tYXJ5IHtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gIH1cbiAgLmdpZnRfX3NlY3Rpb24ge1xuICAgIG1hcmdpbi1ib3R0b206IDM0cHg7XG4gIH1cbiAgLmdpZnRfX2JvZHktcm93IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICB9XG4gIC5naWZ0X19ib2R5LWNvbCB7XG4gICAgZmxleDogMTtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICB9XG4gIC5naWZ0X19ib2R5LWNvbF9jYXJkIHtcbiAgICBvcmRlcjogMjtcbiAgICBmbGV4OiAwIDEgNDMycHg7XG4gICAgbWFyZ2luLWxlZnQ6IDQ4cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXgtaGVpZ2h0OiA2NzBweDtcbiAgfVxuICAuZ2lmdF9fYm9keS1jb2xfZm9ybSB7XG4gICAgb3JkZXI6IDE7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbiAgfVxuICAuZ2lmdF9fZm9vdGVyIHtcbiAgICBtYXJnaW4tdG9wOiA0MHB4O1xuICB9XG4gIC5naWZ0X19mb290ZXItcm93X2J1dHRvbnMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIH1cbiAgLmdpZnRfX2Zvb3Rlci1pdGVtX2J0biB7XG4gICAgbWluLXdpZHRoOiAyMjZweDtcbiAgfVxufVxuLmYtZ2lmdF9faGVhZCB7XG4gIG1hcmdpbi1ib3R0b206IDM0cHg7XG59XG4uZi1naWZ0X19oZWFkaW5nIHtcbiAgbWFyZ2luLWJvdHRvbTogMTJweDtcbn1cbi5mLWdpZnRfX2Zvcm0tcm93IHtcbiAgbWFyZ2luLWJvdHRvbTogMTZweDtcbn1cbi5mLWdpZnRfX2Zvcm0tcm93Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmYtZ2lmdF9faGVhZGluZyB7XG4gICAgbWFyZ2luLWJvdHRvbTogMjJweDtcbiAgfVxuICAuZi1naWZ0X19mb3JtLXJvdyB7XG4gICAgbWFyZ2luLWJvdHRvbTogMjRweDtcbiAgfVxufVxuLnRhYi1saXN0IHtcbiAgY291bnRlci1yZXNldDogaXRlbTtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogNDAwO1xuICBsaW5lLWhlaWdodDogMS4zO1xuICBjb2xvcjogIzA2MDYwNjtcbn1cbi50YWItbGlzdCBhIHtcbiAgY29sb3I6ICNiZGE3NzY7XG59XG4udGFiLWxpc3RfX2l0ZW0ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XG59XG4udGFiLWxpc3RfX2l0ZW06bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4udGFiLWxpc3RfX2l0ZW06YmVmb3JlIHtcbiAgY29udGVudDogY291bnRlcihpdGVtKTtcbiAgY291bnRlci1pbmNyZW1lbnQ6IGl0ZW07XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgZmxleDogMCAxIDMycHg7XG4gIGhlaWdodDogMzJweDtcbiAgcGFkZGluZzogMCA2cHg7XG4gIG1hcmdpbi1yaWdodDogMTZweDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBsaW5lLWhlaWdodDogMTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EwOTQ4ODtcbiAgY29sb3I6ICNmNWYyZWU7XG59XG4udGFiLWxpc3RfX2l0ZW0tYm9keSB7XG4gIGZsZXg6IDE7XG59XG4udGFiLWxpc3RfX3RpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbi50YWItbGlzdF9fc3ViIHtcbiAgY29sb3I6ICNhMDk0ODg7XG59XG5AbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA5OTJweCkge1xuICAudGFiLWxpc3RfX2l0ZW0ge1xuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XG4gIH1cbiAgLnRhYi1saXN0X19pdGVtOmJlZm9yZSB7XG4gICAgZmxleDogMCAxIDQwcHg7XG4gICAgbWluLWhlaWdodDogNDBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDI0cHg7XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICB9XG59XG4uYi1mZWUge1xuICBiYWNrZ3JvdW5kOiAjZmYzODM4O1xuICBjb2xvcjogI2ZmZjtcbn1cbi5iLWZlZV9zei0xIHtcbiAgcGFkZGluZzogMTRweCAxMnB4IDE0cHggMTJweDtcbiAgYm9yZGVyLXJhZGl1czogMzFweDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBsaW5lLWhlaWdodDogMS4zO1xufVxuLmItZmVlX19yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmItZmVlX19pdGVtX2hyX3N6LTEge1xuICBwYWRkaW5nOiA1cHggMTBweCA1cHggMDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCByZ2JhKDI1NSwyNTUsMjU1LDAuMik7XG59XG4uYi1mZWVfX3ZhbCB7XG4gIGxpbmUtaGVpZ2h0OiAxO1xufVxuLmItZmVlX192YWxfc3otMSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cbi5iLWZlZV9fdmFsX3RoZW1lLTEge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmItZmVlIHtcbiAgICBwYWRkaW5nOiAyNnB4IDYwcHggMjZweCAzNnB4O1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgfVxuICAuYi1mZWVfX2l0ZW1faHJfc3otMSB7XG4gICAgcGFkZGluZzogMTBweCAyNnB4IDEwcHggMDtcbiAgICBtYXJnaW4tcmlnaHQ6IDMwcHg7XG4gIH1cbiAgLmItZmVlX192YWxfc3otMSB7XG4gICAgZm9udC1zaXplOiA1MnB4O1xuICB9XG59XG4uc3BlY2lhbC10ZXh0IHtcbiAgbWF4LXdpZHRoOiA5MTJweDtcbn1cbi5zcGVjaWFsLXRleHRfX2FydGljbGUge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjI7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4uc3BlY2lhbC10ZXh0X19hcnRpY2xlIGgxIHtcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuLnNwZWNpYWwtdGV4dF9fYXJ0aWNsZSBzdHJvbmcge1xuICBjb2xvcjogdmFyKC0tc2tpbi1hY3RpdmUtY29sb3IpO1xufVxuLnNwZWNpYWwtdGV4dF9fYXJ0aWNsZSBwIHtcbiAgbWFyZ2luOiAxMHB4IDA7XG59XG4uc3BlY2lhbC10ZXh0X19hcnRpY2xlIHA6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4uc3BlY2lhbC10ZXh0X19hcnRpY2xlIGltZyB7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBtYXJnaW46IDAgOHB4O1xufVxuLnNwZWNpYWwtdGV4dF9fYXJ0aWNsZSAuZm9vdGVyLWJ0biB7XG4gIG1hcmdpbi10b3A6IDMwcHg7XG59XG5AbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA5OTJweCkge1xuICAuc3BlY2lhbC10ZXh0X19hcnRpY2xlIHtcbiAgICBmb250LXNpemU6IDMycHg7XG4gIH1cbiAgLnNwZWNpYWwtdGV4dF9fYXJ0aWNsZSBoMSB7XG4gICAgZm9udC1zaXplOiA3MHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XG4gIH1cbiAgLnNwZWNpYWwtdGV4dF9fYXJ0aWNsZSBwIHtcbiAgICBtYXJnaW46IDE2cHggMDtcbiAgfVxufVxuLmdpZnRzLXJlbGF0ZWRfX2hlYWQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgbWFyZ2luLWJvdHRvbTogMThweDtcbn1cbi5naWZ0cy1yZWxhdGVkX190aXRsZSB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbn1cbi5naWZ0cy1yZWxhdGVkX19sb2dvIHtcbiAgd2lkdGg6IDExMHB4O1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbi5naWZ0cy1yZWxhdGVkX19sb2dvLnN6LTIge1xuICB3aWR0aDogMjIwcHg7XG59XG4uZ2lmdHMtcmVsYXRlZF9fbG9nby13cmFwIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5naWZ0cy1yZWxhdGVkX19zd2lwZXItd3JhcHBlciB7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uZ2lmdHMtcmVsYXRlZF9fY29udHJvbHMge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbn1cbi5naWZ0cy1yZWxhdGVkX19jb250cm9scy1pdGVtIHtcbiAgbWFyZ2luLXJpZ2h0OiAyNnB4O1xufVxuLmdpZnRzLXJlbGF0ZWRfX2NvbnRyb2xzLWl0ZW06bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1yaWdodDogMDtcbn1cbi5naWZ0cy1yZWxhdGVkX19jb250cm9scy1idG4ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogNTBweDtcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjJzIGVhc2UtaW4tb3V0O1xufVxuLmdpZnRzLXJlbGF0ZWRfX2NvbnRyb2xzLWJ0bjphZnRlciB7XG4gIGNvbnRlbnQ6ICcnO1xuICB3aWR0aDogMTBweDtcbiAgaGVpZ2h0OiAxMHB4O1xuICBib3JkZXI6IDJweCBzb2xpZCAjZmZmO1xuICBib3JkZXItYm90dG9tLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyLWxlZnQtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuLmdpZnRzLXJlbGF0ZWRfX2NvbnRyb2xzLWJ0bl9yaWdodDphZnRlciB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG59XG4uZ2lmdHMtcmVsYXRlZF9fY29udHJvbHMtYnRuX2xlZnQ6YWZ0ZXIge1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgtMTM1ZGVnKTtcbiAgbWFyZ2luLXJpZ2h0OiAtNXB4O1xufVxuLmdpZnRzLXJlbGF0ZWRfX2NvbnRyb2xzLWJ0bi5mLWRpc2FibGVkIHtcbiAgb3BhY2l0eTogMC4yO1xuICBjdXJzb3I6IGluaXRpYWw7XG59XG4uZ2lmdHMtcmVsYXRlZF9fZm9vdGVyIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmdpZnRzLXJlbGF0ZWRfX2hlYWQge1xuICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMzdweDtcbiAgfVxuICAuZ2lmdHMtcmVsYXRlZF9fbG9nbyB7XG4gICAgd2lkdGg6IDEyMHB4O1xuICB9XG4gIC5naWZ0cy1yZWxhdGVkX19sb2dvLnN6LTIge1xuICAgIHdpZHRoOiAyNDBweDtcbiAgfVxuICAuZ2lmdHMtcmVsYXRlZF9fdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMzJweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICB9XG59XG4uci10YWIge1xuICBjb2xvcjogIzA2MDYwNjtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogNTAwO1xufVxuLnItdGFiX2h0bWwgPiBwIHtcbiAgbWFyZ2luOiA4cHggMDtcbn1cbi5yLXRhYl9odG1sID4gcDpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5yLXRhYl9odG1sID4gcDpmaXJzdC1jaGlsZCB7XG4gIG1hcmdpbi10b3A6IDA7XG59XG4vKiMgc291cmNlTWFwcGluZ1VSTD1zcmMvYXBwL2NvbXBvbmVudHMvZ2lmdC9naWZ0LmNvbXBvbmVudC5jc3MubWFwICovIiwibW9iaWxlKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiAkd19tKVxyXG4gICAge2Jsb2NrfVxyXG5kZXNrdG9wKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19kKVxyXG4gICAge2Jsb2NrfVxyXG5taW5faW5pdF9kKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19pbml0X2QpXHJcbiAgICB7YmxvY2t9XHJcbnNtYWxsKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19zbWFsbClcclxuICAgIHtibG9ja31cclxuc21hbGxfMigpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfc21hbGxfMilcclxuICAgIHtibG9ja31cclxubWVkaXVtKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19tZWRpdW0pXHJcbiAgICB7YmxvY2t9XHJcbmxhcmdlKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19sYXJnZSlcclxuICAgIHtibG9ja31cclxuZXh0cmFfbGFyZ2UoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X2V4dHJhX2xhcmdlKVxyXG4gICAge2Jsb2NrfVxyXG5tYXhfdygpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfbWF4X3BhZ2Vfd2lkdGgpXHJcbiAgICB7YmxvY2t9XHJcblxyXG5oX21lZGl1bSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi1oZWlnaHQ6ICRoX21lZGl1bSlcclxuICAgIHtibG9ja31cclxuXHJcbnBsYWNlaG9sZGVyKClcclxuICAmOjpwbGFjZWhvbGRlclxyXG4gICAge2Jsb2NrfVxyXG5cclxuYXV0b2NvbXBsZXRlKClcclxuICAmOi13ZWJraXQtYXV0b2ZpbGwsXHJcbiAgJjotd2Via2l0LWF1dG9maWxsOmhvdmVyLFxyXG4gICY6LXdlYmtpdC1hdXRvZmlsbDpmb2N1cyxcclxuICAmOi13ZWJraXQtYXV0b2ZpbGw6YWN0aXZlLFxyXG4gICY6LWludGVybmFsLWF1dG9maWxsLXByZXZpZXdlZCxcclxuICAmOi1pbnRlcm5hbC1hdXRvZmlsbC1zZWxlY3RlZFxyXG4gICAge2Jsb2NrfVxyXG5cclxuX2NhbGMoJHBlciwgJHZhbClcclxuICByZXR1cm4gXCJjYWxjKCVzIC0gJXMpXCIgJSAoJHBlciAkdmFsKVxyXG4iLCIuZi1naWZ0XHJcbiAgJl9faGVhZFxyXG4gICAgbWFyZ2luLWJvdHRvbTogMzRweFxyXG4gICZfX2hlYWRpbmdcclxuICAgIG1hcmdpbi1ib3R0b206IDEycHhcclxuICAmX19mb3JtXHJcbiAgICAmLXJvd1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxNnB4XHJcbiAgICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBcclxuK2Rlc2t0b3AoKVxyXG4gIC5mLWdpZnRcclxuICAgIC8vJl9faGVhZFxyXG4gICAgLy8gIG1hcmdpbi1ib3R0b206IDM0cHhcclxuICAgICZfX2hlYWRpbmdcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMjJweFxyXG4gICAgJl9fZm9ybVxyXG4gICAgICAmLXJvd1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDI0cHg7XHJcbiIsIi50YWItbGlzdFxyXG4gIGNvdW50ZXItcmVzZXQ6IGl0ZW07XHJcbiAgZm9udC1zaXplOiAxNnB4XHJcbiAgZm9udC13ZWlnaHQ6IDQwMFxyXG4gIGxpbmUtaGVpZ2h0OiAxLjNcclxuICBjb2xvcjogJGMtdGFiLWxpc3QtMS10aGVtZS0xLXRleHQ7XHJcbiAgYVxyXG4gICAgY29sb3I6ICNiZGE3NzY7XHJcbiAgJl9faXRlbVxyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi1ib3R0b206IDI0cHhcclxuICAgICY6bGFzdC1jaGlsZFxyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwXHJcbiAgICAmOmJlZm9yZVxyXG4gICAgICBjb250ZW50OiBjb3VudGVyKGl0ZW0pO1xyXG4gICAgICBjb3VudGVyLWluY3JlbWVudDogaXRlbTtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICAgIGFsaWduLWl0ZW1zIGNlbnRlclxyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgYm94LXNpemluZyBib3JkZXItYm94XHJcbiAgICAgIGZsZXg6IDAgMSAzMnB4XHJcbiAgICAgIGhlaWdodDogMzJweFxyXG4gICAgICBwYWRkaW5nOiAwIDZweFxyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDE2cHhcclxuICAgICAgYm9yZGVyLXJhZGl1czogMTBweFxyXG4gICAgICBmb250LXNpemU6IDE4cHhcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMFxyXG4gICAgICBsaW5lLWhlaWdodDogMVxyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYy10YWItbGlzdC0xLXRoZW1lLTEtY291bnRlci1iZztcclxuICAgICAgY29sb3I6ICRjLXRhYi1saXN0LTEtdGhlbWUtMS1jb3VudGVyLXRleHQ7XHJcbiAgICAmLWJvZHlcclxuICAgICAgZmxleDogMVxyXG4gICZfX3RpdGxlXHJcbiAgICBmb250LXdlaWdodDogNTAwXHJcbiAgJl9fc3ViXHJcbiAgICBjb2xvcjogJGMtdGFiLWxpc3QtMS10aGVtZS0xLXN1YjtcclxuXHJcbitkZXNrdG9wKClcclxuICAudGFiLWxpc3RcclxuICAgICZfX2l0ZW1cclxuICAgICAgbWFyZ2luLWJvdHRvbTogNDBweFxyXG4gICAgICAmOmJlZm9yZVxyXG4gICAgICAgIGZsZXg6IDAgMSA0MHB4XHJcbiAgICAgICAgbWluLWhlaWdodCA0MHB4XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyNHB4XHJcbiAgICAgICAgZm9udC1zaXplOiAyNHB4XHJcbiIsIi5iLWZlZVxyXG4gIGJhY2tncm91bmQ6ICRjLTM7XHJcbiAgY29sb3I6ICRjLTE7XHJcbiAgJl9zei0xXHJcbiAgICBwYWRkaW5nOiAxNHB4IDEycHggMTRweCAxMnB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzFweFxyXG4gICAgZm9udC1zaXplOiAxM3B4XHJcbiAgICBsaW5lLWhlaWdodDogMS4zXHJcblxyXG4gICZfX3Jvd1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zIGNlbnRlclxyXG5cclxuICAmX19pdGVtXHJcbiAgICAmX2hyXHJcbiAgICAgICZfc3otMVxyXG4gICAgICAgIHBhZGRpbmc6IDVweCAxMHB4IDVweCAwO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTVweDtcclxuICAgICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAkYy1oci0yO1xyXG5cclxuICAmX192YWxcclxuICAgIGxpbmUtaGVpZ2h0OiAxXHJcbiAgICAmX3N6LTFcclxuICAgICAgZm9udC1zaXplOiAxOHB4XHJcbiAgICAmX3RoZW1lLTFcclxuICAgICAgZm9udC13ZWlnaHQ6IDYwMFxyXG5cclxuK21lZGl1bSgpXHJcbiAgLmItZmVlXHJcbiAgICBwYWRkaW5nOiAyNnB4IDYwcHggMjZweCAzNnB4XHJcbiAgICBmb250LXNpemU6IDI0cHhcclxuICAgICZfX2l0ZW1cclxuICAgICAgJl9oclxyXG4gICAgICAgICZfc3otMVxyXG4gICAgICAgICAgcGFkZGluZzogMTBweCAyNnB4IDEwcHggMFxyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAzMHB4XHJcbiAgICAmX192YWxcclxuICAgICAgJl9zei0xXHJcbiAgICAgICAgZm9udC1zaXplOiA1MnB4XHJcbiIsIi5zcGVjaWFsLXRleHRcclxuICBtYXgtd2lkdGggOTEycHhcclxuICAmX19hcnRpY2xlXHJcbiAgICBmb250LXNpemU6IDE2cHhcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjJcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDBcclxuICAgIGgxXHJcbiAgICAgIGZvbnQtc2l6ZTogMjhweFxyXG4gICAgICBtYXJnaW4tYm90dG9tOiAyMHB4XHJcbiAgICBzdHJvbmdcclxuICAgICAgY29sb3I6IHZhcigtLXNraW4tYWN0aXZlLWNvbG9yKTtcclxuICAgIHBcclxuICAgICAgbWFyZ2luOiAxMHB4IDBcclxuICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICAgaW1nXHJcbiAgICAgIHdpZHRoOiAxMDBweFxyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgIG1hcmdpbjogMCA4cHhcclxuICAgIC5mb290ZXItYnRuXHJcbiAgICAgIG1hcmdpbi10b3A6IDMwcHhcclxuXHJcbitkZXNrdG9wKClcclxuICAuc3BlY2lhbC10ZXh0XHJcbiAgICAmX19hcnRpY2xlXHJcbiAgICAgIGZvbnQtc2l6ZTogMzJweFxyXG4gICAgICBoMVxyXG4gICAgICAgIGZvbnQtc2l6ZTogNzBweFxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDUwcHhcclxuICAgICAgcFxyXG4gICAgICAgIG1hcmdpbjogMTZweCAwXHJcbiIsIi5naWZ0cy1yZWxhdGVkXHJcbiAgJl9faGVhZFxyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zIGZsZXgtZW5kXHJcbiAgICBtYXJnaW4tYm90dG9tOiAxOHB4XHJcbiAgJl9fdGl0bGVcclxuICAgIGZvbnQtc2l6ZTogMjhweFxyXG4gICAgZm9udC13ZWlnaHQ6IDYwMFxyXG4gICAgbGluZS1oZWlnaHQ6IDEuM1xyXG5cclxuICAmX19sb2dvXHJcbiAgICB3aWR0aCAxMTBweFxyXG4gICAgbWF4LXdpZHRoIDEwMCVcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgICYuc3otMlxyXG4gICAgICB3aWR0aDogMjIwcHhcclxuICAgICYtd3JhcFxyXG4gICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAmX19zd2lwZXJcclxuICAgICYtd3JhcHBlclxyXG4gICAgICBhbGlnbi1pdGVtcyBjZW50ZXJcclxuXHJcbiAgJl9fY29udHJvbHNcclxuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgJi1pdGVtXHJcbiAgICAgIG1hcmdpbi1yaWdodDogMjZweDtcclxuICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAwXHJcbiAgICAmLWJ0blxyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBhbGlnbi1pdGVtcyBjZW50ZXJcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgIHdpZHRoOiA1MHB4XHJcbiAgICAgIGhlaWdodDogNTBweFxyXG4gICAgICBib3JkZXI6IDJweCBzb2xpZCAjZmZmXHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCVcclxuICAgICAgdHJhbnNpdGlvbiBvcGFjaXR5ICRzRmFzdCBlYXNlLWluLW91dFxyXG4gICAgICAmOmFmdGVyXHJcbiAgICAgICAgY29udGVudCAnJ1xyXG4gICAgICAgIHdpZHRoOiAxMHB4XHJcbiAgICAgICAgaGVpZ2h0OiAxMHB4XHJcbiAgICAgICAgYm9yZGVyOiAycHggc29saWQgI2ZmZlxyXG4gICAgICAgIGJvcmRlci1ib3R0b20tY29sb3I6IHRyYW5zcGFyZW50XHJcbiAgICAgICAgYm9yZGVyLWxlZnQtY29sb3I6IHRyYW5zcGFyZW50XHJcbiAgICAgICZfcmlnaHQ6YWZ0ZXJcclxuICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZylcclxuICAgICAgICBtYXJnaW4tbGVmdDogLTVweFxyXG4gICAgICAmX2xlZnQ6YWZ0ZXJcclxuICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgtMTM1ZGVnKVxyXG4gICAgICAgIG1hcmdpbi1yaWdodDogLTVweFxyXG5cclxuICAgICAgJi5mLWRpc2FibGVkXHJcbiAgICAgICAgb3BhY2l0eTogLjI7XHJcbiAgICAgICAgY3Vyc29yOiBpbml0aWFsO1xyXG5cclxuICAmX19mb290ZXJcclxuICAgIG1hcmdpbi10b3A6IDIwcHhcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbitkZXNrdG9wKClcclxuICAuZ2lmdHMtcmVsYXRlZFxyXG4gICAgJl9faGVhZFxyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxNnB4XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDM3cHhcclxuXHJcbiAgICAmX19sb2dvXHJcbiAgICAgIHdpZHRoOiAxMjBweFxyXG4gICAgICAmLnN6LTJcclxuICAgICAgICB3aWR0aDogMjQwcHhcclxuXHJcbiAgICAmX190aXRsZVxyXG4gICAgICBmb250LXNpemU6IDMycHhcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMFxyXG4iLCIuci10YWJcclxuICBjb2xvcjogIzA2MDYwNjtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAmX2h0bWxcclxuICAgICYgPiBwXHJcbiAgICAgIG1hcmdpbjogOHB4IDBcclxuICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICAgICAmOmZpcnN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLXRvcDogMFxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/components/gift/gift.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/gift/gift.component.ts ***!
  \***************************************************/
/*! exports provided: GiftComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftComponent", function() { return GiftComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var _services_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/storage.service */ "./src/app/services/storage.service.ts");
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ramda */ "./node_modules/ramda/es/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_form_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/form.service */ "./src/app/services/form.service.ts");
/* harmony import */ var _services_metric_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/metric.service */ "./src/app/services/metric.service.ts");
/* harmony import */ var _services_fs_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/fs.service */ "./src/app/services/fs.service.ts");
/* harmony import */ var _services_doc_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/doc.service */ "./src/app/services/doc.service.ts");
/* harmony import */ var _services_logger_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../services/logger.service */ "./src/app/services/logger.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");















var GiftComponent = /** @class */ (function () {
    function GiftComponent(router, route, api, storage, formService, metric, fsService, doc, logger, meta, translate) {
        this.router = router;
        this.route = route;
        this.api = api;
        this.storage = storage;
        this.formService = formService;
        this.metric = metric;
        this.fsService = fsService;
        this.doc = doc;
        this.logger = logger;
        this.meta = meta;
        this.translate = translate;
        this.showGiftCheckout = true;
        this.showActivate = false;
        this.showGiftTabs = true;
        this.showCover = true;
        this.showName = true;
        this.showBFee = true;
        this.showSubscribeForm = false;
        this.showSubscribePhone = true;
        this.showSubscribeMessage = true;
        this.showSubscribeCompany = true;
        this.showHeaderCompanyLogo = true;
        this.setMeta = true;
        this.corpPhoneInput = true;
        this.giftPhoneInput = true;
        this.showMoreBtn = true;
        this.desktopWidth = 992;
        this.isDesktop = false;
        this.initialTab = {
            client: {},
            // addressee: {},
            // is_gift: true,
            items: [],
            payment_method: undefined,
        };
        this.tab = 'gift';
        this.tabs = {
            gift: {},
            corporate: {},
        };
        this.total_cost = 0;
        this.showCustomValue = false;
        this.formSubmitted = false;
    }
    GiftComponent.prototype.onResize = function () {
        this.isDesktop = window.innerWidth >= this.desktopWidth;
    };
    Object.defineProperty(GiftComponent.prototype, "isInvalid", {
        get: function () {
            return this._form.invalid || !this.total_cost;
        },
        enumerable: true,
        configurable: true
    });
    GiftComponent.prototype.ngOnInit = function () {
        this.isDesktop = window.innerWidth >= this.desktopWidth;
    };
    GiftComponent.prototype.log = function (data) {
        console.log(data);
    };
    GiftComponent.prototype.ngOnChanges = function (changes) {
        if (changes.gift && changes.gift.currentValue) {
            this.skin = this.skin || (this.gift || { skin_settings: { skin: 'default' } }).skin_settings;
            var time_mark = this.storage.getItem('time_mark');
            if (!time_mark) {
                this.storage.setItem('time_mark', Date.now());
            }
            var order = time_mark && Date.now() - parseInt(time_mark, 10) >= 1000 * 60 * 60 ?
                null : this.storage.getItem('gift_order');
            var lang = this.translate.currentLang;
            this.order = order
                ? JSON.parse(order)
                : { type: 'gift', data: Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(this.initialTab), agreement: false };
            this.tabs[this.order.type] = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(this.order.data);
            this.fee = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["isEmpty"])(this.gift.price_modifier) ? null : this.gift.price_modifier;
            this.prices_range = this.gift.prices_range;
            this.initialItem = {
                count: 0,
                price: 0,
                cert_config: this.gift._uuid,
                cert_view: this.gift.Views[0]._uuid,
                addressee: {},
                is_corporate: this.order.type === 'corporate',
                is_gift: this.order.type === 'gift',
            };
            this.initialTab = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.initialTab, { items: [Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(this.initialItem)] });
            this.initTabsData();
            this.initQueryPrice();
            this.calculate();
            if (this.setMeta && this.gift.seo && this.gift.seo.meta) {
                var meta = this.gift.seo.meta;
                var description = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["path"])([lang], meta.description) || Object(ramda__WEBPACK_IMPORTED_MODULE_5__["path"])([this.translate.defaultLang], meta.description);
                var title = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["path"])([lang], meta.title) || Object(ramda__WEBPACK_IMPORTED_MODULE_5__["path"])([this.translate.defaultLang], meta.title);
                var keywords = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["path"])([lang], meta.keywords) || Object(ramda__WEBPACK_IMPORTED_MODULE_5__["path"])([this.translate.defaultLang], meta.keywords);
                if (description) {
                    this.meta.updateTag({
                        property: 'og:description', content: description
                    });
                }
                if (title) {
                    this.meta.updateTag({
                        property: 'og:title', content: title
                    });
                }
                if (keywords) {
                    this.meta.updateTag({
                        name: 'keywords', content: keywords
                    });
                }
            }
            this.logger.l(['gift', this.gift]);
            this.logger.l(['tabs', this.tabs]);
            setTimeout(function () {
                var hash = location.hash || '';
                if (hash.length) {
                    var $el = document.querySelector("" + hash);
                    if ($el) {
                        window.scrollTo(0, $el.offsetTop);
                    }
                }
            }, 400);
        }
    };
    GiftComponent.prototype.initTabsData = function () {
        var _this = this;
        Object.keys(this.tabs).map(function (type) {
            if (!_this.tabs[type]) {
                _this.tabs[type] = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(_this.initialTab);
            }
            var initItem = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(_this.initialItem);
            var _items = _this.tabs[type].items;
            _this.tabs[type].client = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(_this.order.data.client);
            _this.tabs[type].items = (_items && _items.length ? _items : [{}]).map(function (item) {
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, initItem, Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(item), { cert_view: initItem.cert_view, cert_config: initItem.cert_config });
            });
        });
    };
    GiftComponent.prototype.initQueryPrice = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (query) {
            if (query && query.price) {
                _this.calcPrice(_this.tabs[_this.tab], parseInt(query.price, 10), 0);
            }
            _this.calculate();
        });
    };
    GiftComponent.prototype.calcPrice = function (md, price, index) {
        var item = md.items[index];
        if (price) {
            item.price = price;
            item.count = item.count || 1;
        }
        else {
            item.price = 0;
            item.count = 0;
        }
    };
    GiftComponent.prototype.calculate = function (nullify) {
        if (nullify === void 0) { nullify = true; }
        var order = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.order, { type: this.tab, gift: this.gift, data: tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(this.initialTab), { items: [] }) });
        var total_cost = 0;
        var data = this.tabs[this.tab];
        var _items = data.items || [];
        _items.map(function (item) {
            if (item.count > 0) {
                var _data = item;
                var price = _data.price * _data.count;
                // if (this.prices_range && (this.prices_range[0] || this.prices_range[1])) {
                //   const min = this.prices_range[0];
                //   const max = this.prices_range[1];
                //   if ((max && _data.price <= max) || (min && _data.price >= min)) {
                //     total_cost += price;
                //   } else if (_data.price && nullify) {
                //     _data.price = 0;
                //     _data.count = 0;
                //   }
                // } else {
                //   total_cost += price;
                // }
                total_cost += price;
                order.data.items.push(_data);
                order.data.client = Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(data.client);
                // order.data.addressee = clone(data.addressee);
            }
        });
        this.order = order;
        // this.order.data.is_gift = order.type === 'gift';
        this.order.total_cost = total_cost;
        this.total_cost = total_cost;
    };
    GiftComponent.prototype.onPriceClick = function (md, price, index) {
        this.logger.l(['_form', this._form]);
        // if (!price) return;
        this.calcPrice(md, price, index);
        this.calculate(false);
        this.metric.trackFb('AddToCart', { value: price });
    };
    GiftComponent.prototype.onCountChange = function (data, md, index) {
        md.items[index].count = data.value;
        this.calculate(false);
    };
    GiftComponent.prototype.onTabsShow = function (data) {
        var _this = this;
        this.tab = data.name;
        this.tabs[this.tab].items.map(function (item) {
            item.is_gift = _this.tab === 'gift';
            item.is_corporate = _this.tab === 'corporate';
        });
        this.calculate();
    };
    GiftComponent.prototype.onCardRemoveClick = function (index) {
        this.tabs[this.tab].items.splice(index, 1);
        this.calculate();
    };
    GiftComponent.prototype.addMore = function () {
        var _tab = this.tabs[this.tab];
        _tab.items.push(Object(ramda__WEBPACK_IMPORTED_MODULE_5__["clone"])(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.initialItem)));
    };
    GiftComponent.prototype.submit = function () {
        var _this = this;
        this.formService.markFormGroupTouched(this._form);
        this.formSubmitted = true;
        this.logger.l(['_form', this._form]);
        if (this.isInvalid) {
            return;
        }
        this.calculate();
        var navigate = function () {
            _this.saveToStorage();
            _this.router.navigate(["checkout"]);
            _this.metric.trackFb('InitiateCheckout', { value: _this.total_cost });
            _this.metric.trackFb('InitiateCheckout', { value: _this.total_cost });
            _this.logger.l(['logo', _this._logo]);
            _this.logger.l(['order', _this.order]);
            _this.logger.l(['form', _this._form]);
        };
        if (this.logo) {
            var path_1 = "cert/" + this.fsService.makeFilePathForUpload(this.logo);
            this.api.uploadFile({ filePath: path_1, file: this.logo }).subscribe(function (data) {
                // this._logo = {path};
                _this.order.data.items[0].logo = path_1;
                navigate();
            });
        }
        else {
            this.order.data.items.map(function (item) {
                delete item.logo;
            });
            navigate();
        }
    };
    GiftComponent.prototype.saveToStorage = function () {
        if (this.order) {
            this.storage.setItem('gift_order', JSON.stringify(this.order));
            this.storage.setItem('time_mark', Date.now());
        }
    };
    GiftComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_3__["TransportService"] },
        { type: _services_storage_service__WEBPACK_IMPORTED_MODULE_4__["StorageService"] },
        { type: _services_form_service__WEBPACK_IMPORTED_MODULE_7__["FormService"] },
        { type: _services_metric_service__WEBPACK_IMPORTED_MODULE_8__["MetricService"] },
        { type: _services_fs_service__WEBPACK_IMPORTED_MODULE_9__["FsService"] },
        { type: _services_doc_service__WEBPACK_IMPORTED_MODULE_10__["DocService"] },
        { type: _services_logger_service__WEBPACK_IMPORTED_MODULE_11__["LoggerService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["Meta"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_13__["TranslateService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "gift", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "skin", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "widgetUrl", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showGiftCheckout", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showActivate", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], GiftComponent.prototype, "showHeading", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "headingText", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showGiftTabs", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showCover", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showName", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showBFee", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showSubscribeForm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showSubscribePhone", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showSubscribeMessage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showSubscribeCompany", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showHeaderCompanyLogo", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "setMeta", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "subscribeFormTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "giftTabTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "corpTabTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GiftComponent.prototype, "related", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "relatedTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GiftComponent.prototype, "first_logos", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "first_logos_title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GiftComponent.prototype, "second_logos", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "second_logos_title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "activate_text", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "first_logos_slider_settings", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "second_logos_slider_settings", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "head_hash_tag", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "corpPhoneInput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "giftPhoneInput", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GiftComponent.prototype, "addresseeData", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftComponent.prototype, "showMoreBtn", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "headerLogo", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftComponent.prototype, "headerLogoSz", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('form', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"])
    ], GiftComponent.prototype, "_form", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], GiftComponent.prototype, "onResize", null);
    GiftComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gift',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gift.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gift/gift.component.html")).default,
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gift.component.styl */ "./src/app/components/gift/gift.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_transport_service__WEBPACK_IMPORTED_MODULE_3__["TransportService"],
            _services_storage_service__WEBPACK_IMPORTED_MODULE_4__["StorageService"],
            _services_form_service__WEBPACK_IMPORTED_MODULE_7__["FormService"],
            _services_metric_service__WEBPACK_IMPORTED_MODULE_8__["MetricService"],
            _services_fs_service__WEBPACK_IMPORTED_MODULE_9__["FsService"],
            _services_doc_service__WEBPACK_IMPORTED_MODULE_10__["DocService"],
            _services_logger_service__WEBPACK_IMPORTED_MODULE_11__["LoggerService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__["Meta"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_13__["TranslateService"]])
    ], GiftComponent);
    return GiftComponent;
}());



/***/ }),

/***/ "./src/app/components/gift/gift.module.ts":
/*!************************************************!*\
  !*** ./src/app/components/gift/gift.module.ts ***!
  \************************************************/
/*! exports provided: GiftModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftModule", function() { return GiftModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _gift_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gift.component */ "./src/app/components/gift/gift.component.ts");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _modules_tabs_tabs_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modules/tabs/tabs.module */ "./src/app/modules/tabs/tabs.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var angular_svg_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-svg-icon */ "./node_modules/angular-svg-icon/fesm5/angular-svg-icon.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _gift_form_gift_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./gift-form/gift-form.component */ "./src/app/components/gift/gift-form/gift-form.component.ts");
/* harmony import */ var _validators_validators_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../validators/validators.module */ "./src/app/validators/validators.module.ts");
/* harmony import */ var _modules_card_buttons_card_buttons_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../modules/card-buttons/card-buttons.module */ "./src/app/modules/card-buttons/card-buttons.module.ts");
/* harmony import */ var _modules_gifts_related_gifts_related_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../modules/gifts-related/gifts-related.module */ "./src/app/modules/gifts-related/gifts-related.module.ts");













var GiftModule = /** @class */ (function () {
    function GiftModule() {
    }
    GiftModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _gift_component__WEBPACK_IMPORTED_MODULE_3__["GiftComponent"],
                _gift_form_gift_form_component__WEBPACK_IMPORTED_MODULE_9__["GiftFormComponent"],
            ],
            exports: [
                _gift_component__WEBPACK_IMPORTED_MODULE_3__["GiftComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _modules_tabs_tabs_module__WEBPACK_IMPORTED_MODULE_5__["TabsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"],
                angular_svg_icon__WEBPACK_IMPORTED_MODULE_7__["AngularSvgIconModule"],
                _validators_validators_module__WEBPACK_IMPORTED_MODULE_10__["ValidatorsModule"],
                _modules_card_buttons_card_buttons_module__WEBPACK_IMPORTED_MODULE_11__["CardButtonsModule"],
                _modules_gifts_related_gifts_related_module__WEBPACK_IMPORTED_MODULE_12__["GiftsRelatedModule"],
            ],
        })
    ], GiftModule);
    return GiftModule;
}());



/***/ }),

/***/ "./src/app/components/gifts-related/gifts-related.component.styl":
/*!***********************************************************************!*\
  !*** ./src/app/components/gifts-related/gifts-related.component.styl ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/**\n * Swiper 4.5.1\n * Most modern mobile touch slider and framework with hardware accelerated transitions\n * http://www.idangero.us/swiper/\n *\n * Copyright 2014-2019 Vladimir Kharlampidi\n *\n * Released under the MIT License\n *\n * Released on: September 13, 2019\n */\n.swiper-container{margin-left:auto;margin-right:auto;position:relative;overflow:hidden;list-style:none;padding:0;z-index:1}\n.swiper-container-no-flexbox .swiper-slide{float:left}\n.swiper-container-vertical>.swiper-wrapper{flex-direction:column}\n.swiper-wrapper{position:relative;width:100%;height:100%;z-index:1;display:flex;transition-property:transform;box-sizing:content-box}\n.swiper-container-android .swiper-slide,.swiper-wrapper{transform:translate3d(0,0,0)}\n.swiper-container-multirow>.swiper-wrapper{flex-wrap:wrap}\n.swiper-container-free-mode>.swiper-wrapper{transition-timing-function:ease-out;margin:0 auto}\n.swiper-slide{flex-shrink:0;width:100%;height:100%;position:relative;transition-property:transform}\n.swiper-slide-invisible-blank{visibility:hidden}\n.swiper-container-autoheight,.swiper-container-autoheight .swiper-slide{height:auto}\n.swiper-container-autoheight .swiper-wrapper{align-items:flex-start;transition-property:transform,height}\n.swiper-container-3d{perspective:1200px}\n.swiper-container-3d .swiper-cube-shadow,.swiper-container-3d .swiper-slide,.swiper-container-3d .swiper-slide-shadow-bottom,.swiper-container-3d .swiper-slide-shadow-left,.swiper-container-3d .swiper-slide-shadow-right,.swiper-container-3d .swiper-slide-shadow-top,.swiper-container-3d .swiper-wrapper{transform-style:preserve-3d}\n.swiper-container-3d .swiper-slide-shadow-bottom,.swiper-container-3d .swiper-slide-shadow-left,.swiper-container-3d .swiper-slide-shadow-right,.swiper-container-3d .swiper-slide-shadow-top{position:absolute;left:0;top:0;width:100%;height:100%;pointer-events:none;z-index:10}\n.swiper-container-3d .swiper-slide-shadow-left{background-image:linear-gradient(to left,rgba(0,0,0,.5),rgba(0,0,0,0))}\n.swiper-container-3d .swiper-slide-shadow-right{background-image:linear-gradient(to right,rgba(0,0,0,.5),rgba(0,0,0,0))}\n.swiper-container-3d .swiper-slide-shadow-top{background-image:linear-gradient(to top,rgba(0,0,0,.5),rgba(0,0,0,0))}\n.swiper-container-3d .swiper-slide-shadow-bottom{background-image:linear-gradient(to bottom,rgba(0,0,0,.5),rgba(0,0,0,0))}\n.swiper-container-wp8-horizontal,.swiper-container-wp8-horizontal>.swiper-wrapper{touch-action:pan-y}\n.swiper-container-wp8-vertical,.swiper-container-wp8-vertical>.swiper-wrapper{touch-action:pan-x}\n.swiper-button-next,.swiper-button-prev{position:absolute;top:50%;width:27px;height:44px;margin-top:-22px;z-index:10;cursor:pointer;background-size:27px 44px;background-position:center;background-repeat:no-repeat}\n.swiper-button-next.swiper-button-disabled,.swiper-button-prev.swiper-button-disabled{opacity:.35;cursor:auto;pointer-events:none}\n.swiper-button-prev,.swiper-container-rtl .swiper-button-next{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E\");left:10px;right:auto}\n.swiper-button-next,.swiper-container-rtl .swiper-button-prev{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E\");right:10px;left:auto}\n.swiper-button-prev.swiper-button-white,.swiper-container-rtl .swiper-button-next.swiper-button-white{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E\")}\n.swiper-button-next.swiper-button-white,.swiper-container-rtl .swiper-button-prev.swiper-button-white{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E\")}\n.swiper-button-prev.swiper-button-black,.swiper-container-rtl .swiper-button-next.swiper-button-black{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E\")}\n.swiper-button-next.swiper-button-black,.swiper-container-rtl .swiper-button-prev.swiper-button-black{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E\")}\n.swiper-button-lock{display:none}\n.swiper-pagination{position:absolute;text-align:center;transition:.3s opacity;transform:translate3d(0,0,0);z-index:10}\n.swiper-pagination.swiper-pagination-hidden{opacity:0}\n.swiper-container-horizontal>.swiper-pagination-bullets,.swiper-pagination-custom,.swiper-pagination-fraction{bottom:10px;left:0;width:100%}\n.swiper-pagination-bullets-dynamic{overflow:hidden;font-size:0}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{transform:scale(.33);position:relative}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active{transform:scale(1)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-main{transform:scale(1)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev{transform:scale(.66)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev-prev{transform:scale(.33)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next{transform:scale(.66)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next-next{transform:scale(.33)}\n.swiper-pagination-bullet{width:8px;height:8px;display:inline-block;border-radius:100%;background:#000;opacity:.2}\nbutton.swiper-pagination-bullet{border:none;margin:0;padding:0;box-shadow:none;-webkit-appearance:none;-moz-appearance:none;appearance:none}\n.swiper-pagination-clickable .swiper-pagination-bullet{cursor:pointer}\n.swiper-pagination-bullet-active{opacity:1;background:#007aff}\n.swiper-container-vertical>.swiper-pagination-bullets{right:10px;top:50%;transform:translate3d(0,-50%,0)}\n.swiper-container-vertical>.swiper-pagination-bullets .swiper-pagination-bullet{margin:6px 0;display:block}\n.swiper-container-vertical>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic{top:50%;transform:translateY(-50%);width:8px}\n.swiper-container-vertical>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{display:inline-block;transition:.2s top,.2s -webkit-transform;transition:.2s transform,.2s top;transition:.2s transform,.2s top,.2s -webkit-transform}\n.swiper-container-horizontal>.swiper-pagination-bullets .swiper-pagination-bullet{margin:0 4px}\n.swiper-container-horizontal>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic{left:50%;transform:translateX(-50%);white-space:nowrap}\n.swiper-container-horizontal>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{transition:.2s left,.2s -webkit-transform;transition:.2s transform,.2s left;transition:.2s transform,.2s left,.2s -webkit-transform}\n.swiper-container-horizontal.swiper-container-rtl>.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{transition:.2s right,.2s -webkit-transform;transition:.2s transform,.2s right;transition:.2s transform,.2s right,.2s -webkit-transform}\n.swiper-pagination-progressbar{background:rgba(0,0,0,.25);position:absolute}\n.swiper-pagination-progressbar .swiper-pagination-progressbar-fill{background:#007aff;position:absolute;left:0;top:0;width:100%;height:100%;transform:scale(0);transform-origin:left top}\n.swiper-container-rtl .swiper-pagination-progressbar .swiper-pagination-progressbar-fill{transform-origin:right top}\n.swiper-container-horizontal>.swiper-pagination-progressbar,.swiper-container-vertical>.swiper-pagination-progressbar.swiper-pagination-progressbar-opposite{width:100%;height:4px;left:0;top:0}\n.swiper-container-horizontal>.swiper-pagination-progressbar.swiper-pagination-progressbar-opposite,.swiper-container-vertical>.swiper-pagination-progressbar{width:4px;height:100%;left:0;top:0}\n.swiper-pagination-white .swiper-pagination-bullet-active{background:#fff}\n.swiper-pagination-progressbar.swiper-pagination-white{background:rgba(255,255,255,.25)}\n.swiper-pagination-progressbar.swiper-pagination-white .swiper-pagination-progressbar-fill{background:#fff}\n.swiper-pagination-black .swiper-pagination-bullet-active{background:#000}\n.swiper-pagination-progressbar.swiper-pagination-black{background:rgba(0,0,0,.25)}\n.swiper-pagination-progressbar.swiper-pagination-black .swiper-pagination-progressbar-fill{background:#000}\n.swiper-pagination-lock{display:none}\n.swiper-scrollbar{border-radius:10px;position:relative;-ms-touch-action:none;background:rgba(0,0,0,.1)}\n.swiper-container-horizontal>.swiper-scrollbar{position:absolute;left:1%;bottom:3px;z-index:50;height:5px;width:98%}\n.swiper-container-vertical>.swiper-scrollbar{position:absolute;right:3px;top:1%;z-index:50;width:5px;height:98%}\n.swiper-scrollbar-drag{height:100%;width:100%;position:relative;background:rgba(0,0,0,.5);border-radius:10px;left:0;top:0}\n.swiper-scrollbar-cursor-drag{cursor:move}\n.swiper-scrollbar-lock{display:none}\n.swiper-zoom-container{width:100%;height:100%;display:flex;justify-content:center;align-items:center;text-align:center}\n.swiper-zoom-container>canvas,.swiper-zoom-container>img,.swiper-zoom-container>svg{max-width:100%;max-height:100%;-o-object-fit:contain;object-fit:contain}\n.swiper-slide-zoomed{cursor:move}\n.swiper-lazy-preloader{width:42px;height:42px;position:absolute;left:50%;top:50%;margin-left:-21px;margin-top:-21px;z-index:10;transform-origin:50%;-webkit-animation:swiper-preloader-spin 1s steps(12,end) infinite;animation:swiper-preloader-spin 1s steps(12,end) infinite}\n.swiper-lazy-preloader:after{display:block;content:'';width:100%;height:100%;background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%236c6c6c'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E\");background-position:50%;background-size:100%;background-repeat:no-repeat}\n.swiper-lazy-preloader-white:after{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%23fff'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E\")}\n@-webkit-keyframes swiper-preloader-spin{100%{transform:rotate(360deg)}}\n@keyframes swiper-preloader-spin{100%{transform:rotate(360deg)}}\n.swiper-container .swiper-notification{position:absolute;left:0;top:0;pointer-events:none;opacity:0;z-index:-1000}\n.swiper-container-fade.swiper-container-free-mode .swiper-slide{transition-timing-function:ease-out}\n.swiper-container-fade .swiper-slide{pointer-events:none;transition-property:opacity}\n.swiper-container-fade .swiper-slide .swiper-slide{pointer-events:none}\n.swiper-container-fade .swiper-slide-active,.swiper-container-fade .swiper-slide-active .swiper-slide-active{pointer-events:auto}\n.swiper-container-cube{overflow:visible}\n.swiper-container-cube .swiper-slide{pointer-events:none;-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:1;visibility:hidden;transform-origin:0 0;width:100%;height:100%}\n.swiper-container-cube .swiper-slide .swiper-slide{pointer-events:none}\n.swiper-container-cube.swiper-container-rtl .swiper-slide{transform-origin:100% 0}\n.swiper-container-cube .swiper-slide-active,.swiper-container-cube .swiper-slide-active .swiper-slide-active{pointer-events:auto}\n.swiper-container-cube .swiper-slide-active,.swiper-container-cube .swiper-slide-next,.swiper-container-cube .swiper-slide-next+.swiper-slide,.swiper-container-cube .swiper-slide-prev{pointer-events:auto;visibility:visible}\n.swiper-container-cube .swiper-slide-shadow-bottom,.swiper-container-cube .swiper-slide-shadow-left,.swiper-container-cube .swiper-slide-shadow-right,.swiper-container-cube .swiper-slide-shadow-top{z-index:0;-webkit-backface-visibility:hidden;backface-visibility:hidden}\n.swiper-container-cube .swiper-cube-shadow{position:absolute;left:0;bottom:0;width:100%;height:100%;background:#000;opacity:.6;-webkit-filter:blur(50px);filter:blur(50px);z-index:0}\n.swiper-container-flip{overflow:visible}\n.swiper-container-flip .swiper-slide{pointer-events:none;-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:1}\n.swiper-container-flip .swiper-slide .swiper-slide{pointer-events:none}\n.swiper-container-flip .swiper-slide-active,.swiper-container-flip .swiper-slide-active .swiper-slide-active{pointer-events:auto}\n.swiper-container-flip .swiper-slide-shadow-bottom,.swiper-container-flip .swiper-slide-shadow-left,.swiper-container-flip .swiper-slide-shadow-right,.swiper-container-flip .swiper-slide-shadow-top{z-index:0;-webkit-backface-visibility:hidden;backface-visibility:hidden}\n.swiper-container-coverflow .swiper-wrapper{-ms-perspective:1200px}\n/*# sourceMappingURL=src/app/components/gifts-related/gifts-related.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9zd2lwZXIvZGlzdC9jc3Mvc3dpcGVyLm1pbi5jc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvZ2lmdHMtcmVsYXRlZC9naWZ0cy1yZWxhdGVkLmNvbXBvbmVudC5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7O0VBVUU7QUFDRixrQkFBa0IsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsU0FBUztBQUFDLDJDQUEyQyxVQUFVO0FBQUMsMkNBQTRKLHFCQUFxQjtBQUFDLGdCQUFnQixpQkFBaUIsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBOEQsWUFBWSxDQUFzSCw2QkFBNkIsQ0FBZ0Ysc0JBQXNCO0FBQUMsd0RBQTZGLDRCQUE0QjtBQUFDLDJDQUFxRixjQUFjO0FBQUMsNENBQStILG1DQUFtQyxDQUFDLGFBQWE7QUFBQyxjQUF3RCxhQUFhLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBc0gsNkJBQTZFO0FBQUMsOEJBQThCLGlCQUFpQjtBQUFDLHdFQUF3RSxXQUFXO0FBQUMsNkNBQXlILHNCQUFzQixDQUEySSxvQ0FBMkY7QUFBQyxxQkFBZ0Qsa0JBQWtCO0FBQUMsK1NBQW1WLDJCQUEyQjtBQUFDLDhMQUE4TCxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsVUFBVTtBQUFDLCtDQUF3UyxzRUFBc0U7QUFBQyxnREFBdVMsdUVBQXVFO0FBQUMsOENBQTJTLHFFQUFxRTtBQUFDLGlEQUF3Uyx3RUFBd0U7QUFBQyxrRkFBeUcsa0JBQWtCO0FBQUMsOEVBQXFHLGtCQUFrQjtBQUFDLHdDQUF3QyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLHlCQUF5QixDQUFDLDBCQUEwQixDQUFDLDJCQUEyQjtBQUFDLHNGQUFzRixXQUFXLENBQUMsV0FBVyxDQUFDLG1CQUFtQjtBQUFDLDhEQUE4RCxtUkFBbVIsQ0FBQyxTQUFTLENBQUMsVUFBVTtBQUFDLDhEQUE4RCxtUkFBbVIsQ0FBQyxVQUFVLENBQUMsU0FBUztBQUFDLHNHQUFzRyxtUkFBbVI7QUFBQyxzR0FBc0csbVJBQW1SO0FBQUMsc0dBQXNHLG1SQUFtUjtBQUFDLHNHQUFzRyxtUkFBbVI7QUFBQyxvQkFBb0IsWUFBWTtBQUFDLG1CQUFtQixpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBMEQsc0JBQXNCLENBQXNDLDRCQUE0QixDQUFDLFVBQVU7QUFBQyw0Q0FBNEMsU0FBUztBQUFDLDhHQUE4RyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVU7QUFBQyxtQ0FBbUMsZUFBZSxDQUFDLFdBQVc7QUFBQyw2REFBbUgsb0JBQW9CLENBQUMsaUJBQWlCO0FBQUMsb0VBQXNILGtCQUFrQjtBQUFDLHlFQUEySCxrQkFBa0I7QUFBQyx5RUFBK0gsb0JBQW9CO0FBQUMsOEVBQW9JLG9CQUFvQjtBQUFDLHlFQUErSCxvQkFBb0I7QUFBQyw4RUFBb0ksb0JBQW9CO0FBQUMsMEJBQTBCLFNBQVMsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLFVBQVU7QUFBQyxnQ0FBZ0MsV0FBVyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQXlCLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlO0FBQUMsdURBQXVELGNBQWM7QUFBQyxpQ0FBaUMsU0FBUyxDQUFDLGtCQUFrQjtBQUFDLHNEQUFzRCxVQUFVLENBQUMsT0FBTyxDQUF5QywrQkFBK0I7QUFBQyxnRkFBZ0YsWUFBWSxDQUFDLGFBQWE7QUFBQyx3RkFBd0YsT0FBTyxDQUFtRSwwQkFBMEIsQ0FBQyxTQUFTO0FBQUMsa0hBQWtILG9CQUFvQixDQUFrRCx3Q0FBd0MsQ0FBcUMsZ0NBQWdDLENBQUMsc0RBQXNEO0FBQUMsa0ZBQWtGLFlBQVk7QUFBQywwRkFBMEYsUUFBUSxDQUFtRSwwQkFBMEIsQ0FBQyxrQkFBa0I7QUFBQyxvSEFBc0sseUNBQXlDLENBQXNDLGlDQUFpQyxDQUFDLHVEQUF1RDtBQUFDLCtHQUFrSywwQ0FBMEMsQ0FBdUMsa0NBQWtDLENBQUMsd0RBQXdEO0FBQUMsK0JBQStCLDBCQUEwQixDQUFDLGlCQUFpQjtBQUFDLG1FQUFtRSxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQW1ELGtCQUFrQixDQUFpRSx5QkFBeUI7QUFBQyx5RkFBMkosMEJBQTBCO0FBQUMsNkpBQTZKLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUs7QUFBQyw2SkFBNkosU0FBUyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSztBQUFDLDBEQUEwRCxlQUFlO0FBQUMsdURBQXVELGdDQUFnQztBQUFDLDJGQUEyRixlQUFlO0FBQUMsMERBQTBELGVBQWU7QUFBQyx1REFBdUQsMEJBQTBCO0FBQUMsMkZBQTJGLGVBQWU7QUFBQyx3QkFBd0IsWUFBWTtBQUFDLGtCQUFrQixrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyx5QkFBeUI7QUFBQywrQ0FBK0MsaUJBQWlCLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFNBQVM7QUFBQyw2Q0FBNkMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFVBQVU7QUFBQyx1QkFBdUIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyx5QkFBeUIsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsS0FBSztBQUFDLDhCQUE4QixXQUFXO0FBQUMsdUJBQXVCLFlBQVk7QUFBQyx1QkFBdUIsVUFBVSxDQUFDLFdBQVcsQ0FBOEQsWUFBWSxDQUE2RSxzQkFBc0IsQ0FBMkUsa0JBQWtCLENBQUMsaUJBQWlCO0FBQUMsb0ZBQW9GLGNBQWMsQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsa0JBQWtCO0FBQUMscUJBQXFCLFdBQVc7QUFBQyx1QkFBdUIsVUFBVSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBdUQsb0JBQW9CLENBQUMsaUVBQWlFLENBQUMseURBQXlEO0FBQUMsNkJBQTZCLGFBQWEsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyx3N0NBQXc3QyxDQUFDLHVCQUF1QixDQUFDLG9CQUFvQixDQUFDLDJCQUEyQjtBQUFDLG1DQUFtQyxxN0NBQXE3QztBQUFDLHlDQUF5QyxLQUFzQyx3QkFBd0IsQ0FBQztBQUFDLGlDQUFpQyxLQUFzQyx3QkFBd0IsQ0FBQztBQUFDLHVDQUF1QyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxhQUFhO0FBQUMsZ0VBQW1KLG1DQUFtQztBQUFDLHFDQUFxQyxtQkFBbUIsQ0FBb0UsMkJBQTJCO0FBQUMsbURBQW1ELG1CQUFtQjtBQUFDLDZHQUE2RyxtQkFBbUI7QUFBQyx1QkFBdUIsZ0JBQWdCO0FBQUMscUNBQXFDLG1CQUFtQixDQUFDLGtDQUFrQyxDQUFDLDBCQUEwQixDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBdUQsb0JBQW9CLENBQUMsVUFBVSxDQUFDLFdBQVc7QUFBQyxtREFBbUQsbUJBQW1CO0FBQUMsMERBQXNILHVCQUF1QjtBQUFDLDZHQUE2RyxtQkFBbUI7QUFBQyx3TEFBd0wsbUJBQW1CLENBQUMsa0JBQWtCO0FBQUMsc01BQXNNLFNBQVMsQ0FBQyxrQ0FBa0MsQ0FBQywwQkFBMEI7QUFBQywyQ0FBMkMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsaUJBQWlCLENBQUMsU0FBUztBQUFDLHVCQUF1QixnQkFBZ0I7QUFBQyxxQ0FBcUMsbUJBQW1CLENBQUMsa0NBQWtDLENBQUMsMEJBQTBCLENBQUMsU0FBUztBQUFDLG1EQUFtRCxtQkFBbUI7QUFBQyw2R0FBNkcsbUJBQW1CO0FBQUMsc01BQXNNLFNBQVMsQ0FBQyxrQ0FBa0MsQ0FBQywwQkFBMEI7QUFBQyw0Q0FBNEMsc0JBQXNCO0FDVjVrbUIsdUZBQXVGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9naWZ0cy1yZWxhdGVkL2dpZnRzLXJlbGF0ZWQuY29tcG9uZW50LnN0eWwiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFN3aXBlciA0LjUuMVxuICogTW9zdCBtb2Rlcm4gbW9iaWxlIHRvdWNoIHNsaWRlciBhbmQgZnJhbWV3b3JrIHdpdGggaGFyZHdhcmUgYWNjZWxlcmF0ZWQgdHJhbnNpdGlvbnNcbiAqIGh0dHA6Ly93d3cuaWRhbmdlcm8udXMvc3dpcGVyL1xuICpcbiAqIENvcHlyaWdodCAyMDE0LTIwMTkgVmxhZGltaXIgS2hhcmxhbXBpZGlcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2VcbiAqXG4gKiBSZWxlYXNlZCBvbjogU2VwdGVtYmVyIDEzLCAyMDE5XG4gKi9cbi5zd2lwZXItY29udGFpbmVye21hcmdpbi1sZWZ0OmF1dG87bWFyZ2luLXJpZ2h0OmF1dG87cG9zaXRpb246cmVsYXRpdmU7b3ZlcmZsb3c6aGlkZGVuO2xpc3Qtc3R5bGU6bm9uZTtwYWRkaW5nOjA7ei1pbmRleDoxfS5zd2lwZXItY29udGFpbmVyLW5vLWZsZXhib3ggLnN3aXBlci1zbGlkZXtmbG9hdDpsZWZ0fS5zd2lwZXItY29udGFpbmVyLXZlcnRpY2FsPi5zd2lwZXItd3JhcHBlcnstd2Via2l0LWJveC1vcmllbnQ6dmVydGljYWw7LXdlYmtpdC1ib3gtZGlyZWN0aW9uOm5vcm1hbDstd2Via2l0LWZsZXgtZGlyZWN0aW9uOmNvbHVtbjstbXMtZmxleC1kaXJlY3Rpb246Y29sdW1uO2ZsZXgtZGlyZWN0aW9uOmNvbHVtbn0uc3dpcGVyLXdyYXBwZXJ7cG9zaXRpb246cmVsYXRpdmU7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTt6LWluZGV4OjE7ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi13ZWJraXQtZmxleDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LXRyYW5zaXRpb24tcHJvcGVydHk6LXdlYmtpdC10cmFuc2Zvcm07dHJhbnNpdGlvbi1wcm9wZXJ0eTotd2Via2l0LXRyYW5zZm9ybTstby10cmFuc2l0aW9uLXByb3BlcnR5OnRyYW5zZm9ybTt0cmFuc2l0aW9uLXByb3BlcnR5OnRyYW5zZm9ybTt0cmFuc2l0aW9uLXByb3BlcnR5OnRyYW5zZm9ybSwtd2Via2l0LXRyYW5zZm9ybTstd2Via2l0LWJveC1zaXppbmc6Y29udGVudC1ib3g7Ym94LXNpemluZzpjb250ZW50LWJveH0uc3dpcGVyLWNvbnRhaW5lci1hbmRyb2lkIC5zd2lwZXItc2xpZGUsLnN3aXBlci13cmFwcGVyey13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZTNkKDAsMCwwKTt0cmFuc2Zvcm06dHJhbnNsYXRlM2QoMCwwLDApfS5zd2lwZXItY29udGFpbmVyLW11bHRpcm93Pi5zd2lwZXItd3JhcHBlcnstd2Via2l0LWZsZXgtd3JhcDp3cmFwOy1tcy1mbGV4LXdyYXA6d3JhcDtmbGV4LXdyYXA6d3JhcH0uc3dpcGVyLWNvbnRhaW5lci1mcmVlLW1vZGU+LnN3aXBlci13cmFwcGVyey13ZWJraXQtdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246ZWFzZS1vdXQ7LW8tdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246ZWFzZS1vdXQ7dHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246ZWFzZS1vdXQ7bWFyZ2luOjAgYXV0b30uc3dpcGVyLXNsaWRley13ZWJraXQtZmxleC1zaHJpbms6MDstbXMtZmxleC1uZWdhdGl2ZTowO2ZsZXgtc2hyaW5rOjA7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTtwb3NpdGlvbjpyZWxhdGl2ZTstd2Via2l0LXRyYW5zaXRpb24tcHJvcGVydHk6LXdlYmtpdC10cmFuc2Zvcm07dHJhbnNpdGlvbi1wcm9wZXJ0eTotd2Via2l0LXRyYW5zZm9ybTstby10cmFuc2l0aW9uLXByb3BlcnR5OnRyYW5zZm9ybTt0cmFuc2l0aW9uLXByb3BlcnR5OnRyYW5zZm9ybTt0cmFuc2l0aW9uLXByb3BlcnR5OnRyYW5zZm9ybSwtd2Via2l0LXRyYW5zZm9ybX0uc3dpcGVyLXNsaWRlLWludmlzaWJsZS1ibGFua3t2aXNpYmlsaXR5OmhpZGRlbn0uc3dpcGVyLWNvbnRhaW5lci1hdXRvaGVpZ2h0LC5zd2lwZXItY29udGFpbmVyLWF1dG9oZWlnaHQgLnN3aXBlci1zbGlkZXtoZWlnaHQ6YXV0b30uc3dpcGVyLWNvbnRhaW5lci1hdXRvaGVpZ2h0IC5zd2lwZXItd3JhcHBlcnstd2Via2l0LWJveC1hbGlnbjpzdGFydDstd2Via2l0LWFsaWduLWl0ZW1zOmZsZXgtc3RhcnQ7LW1zLWZsZXgtYWxpZ246c3RhcnQ7YWxpZ24taXRlbXM6ZmxleC1zdGFydDstd2Via2l0LXRyYW5zaXRpb24tcHJvcGVydHk6aGVpZ2h0LC13ZWJraXQtdHJhbnNmb3JtO3RyYW5zaXRpb24tcHJvcGVydHk6aGVpZ2h0LC13ZWJraXQtdHJhbnNmb3JtOy1vLXRyYW5zaXRpb24tcHJvcGVydHk6dHJhbnNmb3JtLGhlaWdodDt0cmFuc2l0aW9uLXByb3BlcnR5OnRyYW5zZm9ybSxoZWlnaHQ7dHJhbnNpdGlvbi1wcm9wZXJ0eTp0cmFuc2Zvcm0saGVpZ2h0LC13ZWJraXQtdHJhbnNmb3JtfS5zd2lwZXItY29udGFpbmVyLTNkey13ZWJraXQtcGVyc3BlY3RpdmU6MTIwMHB4O3BlcnNwZWN0aXZlOjEyMDBweH0uc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLWN1YmUtc2hhZG93LC5zd2lwZXItY29udGFpbmVyLTNkIC5zd2lwZXItc2xpZGUsLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctYm90dG9tLC5zd2lwZXItY29udGFpbmVyLTNkIC5zd2lwZXItc2xpZGUtc2hhZG93LWxlZnQsLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctcmlnaHQsLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctdG9wLC5zd2lwZXItY29udGFpbmVyLTNkIC5zd2lwZXItd3JhcHBlcnstd2Via2l0LXRyYW5zZm9ybS1zdHlsZTpwcmVzZXJ2ZS0zZDt0cmFuc2Zvcm0tc3R5bGU6cHJlc2VydmUtM2R9LnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctYm90dG9tLC5zd2lwZXItY29udGFpbmVyLTNkIC5zd2lwZXItc2xpZGUtc2hhZG93LWxlZnQsLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctcmlnaHQsLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctdG9we3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6MDt0b3A6MDt3aWR0aDoxMDAlO2hlaWdodDoxMDAlO3BvaW50ZXItZXZlbnRzOm5vbmU7ei1pbmRleDoxMH0uc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLXNsaWRlLXNoYWRvdy1sZWZ0e2JhY2tncm91bmQtaW1hZ2U6LXdlYmtpdC1ncmFkaWVudChsaW5lYXIscmlnaHQgdG9wLGxlZnQgdG9wLGZyb20ocmdiYSgwLDAsMCwuNSkpLHRvKHJnYmEoMCwwLDAsMCkpKTtiYWNrZ3JvdW5kLWltYWdlOi13ZWJraXQtbGluZWFyLWdyYWRpZW50KHJpZ2h0LHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpO2JhY2tncm91bmQtaW1hZ2U6LW8tbGluZWFyLWdyYWRpZW50KHJpZ2h0LHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpO2JhY2tncm91bmQtaW1hZ2U6bGluZWFyLWdyYWRpZW50KHRvIGxlZnQscmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSl9LnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctcmlnaHR7YmFja2dyb3VuZC1pbWFnZTotd2Via2l0LWdyYWRpZW50KGxpbmVhcixsZWZ0IHRvcCxyaWdodCB0b3AsZnJvbShyZ2JhKDAsMCwwLC41KSksdG8ocmdiYSgwLDAsMCwwKSkpO2JhY2tncm91bmQtaW1hZ2U6LXdlYmtpdC1saW5lYXItZ3JhZGllbnQobGVmdCxyZ2JhKDAsMCwwLC41KSxyZ2JhKDAsMCwwLDApKTtiYWNrZ3JvdW5kLWltYWdlOi1vLWxpbmVhci1ncmFkaWVudChsZWZ0LHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpO2JhY2tncm91bmQtaW1hZ2U6bGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpfS5zd2lwZXItY29udGFpbmVyLTNkIC5zd2lwZXItc2xpZGUtc2hhZG93LXRvcHtiYWNrZ3JvdW5kLWltYWdlOi13ZWJraXQtZ3JhZGllbnQobGluZWFyLGxlZnQgYm90dG9tLGxlZnQgdG9wLGZyb20ocmdiYSgwLDAsMCwuNSkpLHRvKHJnYmEoMCwwLDAsMCkpKTtiYWNrZ3JvdW5kLWltYWdlOi13ZWJraXQtbGluZWFyLWdyYWRpZW50KGJvdHRvbSxyZ2JhKDAsMCwwLC41KSxyZ2JhKDAsMCwwLDApKTtiYWNrZ3JvdW5kLWltYWdlOi1vLWxpbmVhci1ncmFkaWVudChib3R0b20scmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSk7YmFja2dyb3VuZC1pbWFnZTpsaW5lYXItZ3JhZGllbnQodG8gdG9wLHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpfS5zd2lwZXItY29udGFpbmVyLTNkIC5zd2lwZXItc2xpZGUtc2hhZG93LWJvdHRvbXtiYWNrZ3JvdW5kLWltYWdlOi13ZWJraXQtZ3JhZGllbnQobGluZWFyLGxlZnQgdG9wLGxlZnQgYm90dG9tLGZyb20ocmdiYSgwLDAsMCwuNSkpLHRvKHJnYmEoMCwwLDAsMCkpKTtiYWNrZ3JvdW5kLWltYWdlOi13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCxyZ2JhKDAsMCwwLC41KSxyZ2JhKDAsMCwwLDApKTtiYWNrZ3JvdW5kLWltYWdlOi1vLWxpbmVhci1ncmFkaWVudCh0b3AscmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSk7YmFja2dyb3VuZC1pbWFnZTpsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpfS5zd2lwZXItY29udGFpbmVyLXdwOC1ob3Jpem9udGFsLC5zd2lwZXItY29udGFpbmVyLXdwOC1ob3Jpem9udGFsPi5zd2lwZXItd3JhcHBlcnstbXMtdG91Y2gtYWN0aW9uOnBhbi15O3RvdWNoLWFjdGlvbjpwYW4teX0uc3dpcGVyLWNvbnRhaW5lci13cDgtdmVydGljYWwsLnN3aXBlci1jb250YWluZXItd3A4LXZlcnRpY2FsPi5zd2lwZXItd3JhcHBlcnstbXMtdG91Y2gtYWN0aW9uOnBhbi14O3RvdWNoLWFjdGlvbjpwYW4teH0uc3dpcGVyLWJ1dHRvbi1uZXh0LC5zd2lwZXItYnV0dG9uLXByZXZ7cG9zaXRpb246YWJzb2x1dGU7dG9wOjUwJTt3aWR0aDoyN3B4O2hlaWdodDo0NHB4O21hcmdpbi10b3A6LTIycHg7ei1pbmRleDoxMDtjdXJzb3I6cG9pbnRlcjtiYWNrZ3JvdW5kLXNpemU6MjdweCA0NHB4O2JhY2tncm91bmQtcG9zaXRpb246Y2VudGVyO2JhY2tncm91bmQtcmVwZWF0Om5vLXJlcGVhdH0uc3dpcGVyLWJ1dHRvbi1uZXh0LnN3aXBlci1idXR0b24tZGlzYWJsZWQsLnN3aXBlci1idXR0b24tcHJldi5zd2lwZXItYnV0dG9uLWRpc2FibGVke29wYWNpdHk6LjM1O2N1cnNvcjphdXRvO3BvaW50ZXItZXZlbnRzOm5vbmV9LnN3aXBlci1idXR0b24tcHJldiwuc3dpcGVyLWNvbnRhaW5lci1ydGwgLnN3aXBlci1idXR0b24tbmV4dHtiYWNrZ3JvdW5kLWltYWdlOnVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbDtjaGFyc2V0PXV0Zi04LCUzQ3N2ZyUyMHhtbG5zJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMjAwMCUyRnN2ZyclMjB2aWV3Qm94JTNEJzAlMjAwJTIwMjclMjA0NCclM0UlM0NwYXRoJTIwZCUzRCdNMCUyQzIyTDIyJTJDMGwyLjElMkMyLjFMNC4yJTJDMjJsMTkuOSUyQzE5LjlMMjIlMkM0NEwwJTJDMjJMMCUyQzIyTDAlMkMyMnonJTIwZmlsbCUzRCclMjMwMDdhZmYnJTJGJTNFJTNDJTJGc3ZnJTNFXCIpO2xlZnQ6MTBweDtyaWdodDphdXRvfS5zd2lwZXItYnV0dG9uLW5leHQsLnN3aXBlci1jb250YWluZXItcnRsIC5zd2lwZXItYnV0dG9uLXByZXZ7YmFja2dyb3VuZC1pbWFnZTp1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWw7Y2hhcnNldD11dGYtOCwlM0NzdmclMjB4bWxucyUzRCdodHRwJTNBJTJGJTJGd3d3LnczLm9yZyUyRjIwMDAlMkZzdmcnJTIwdmlld0JveCUzRCcwJTIwMCUyMDI3JTIwNDQnJTNFJTNDcGF0aCUyMGQlM0QnTTI3JTJDMjJMMjclMkMyMkw1JTJDNDRsLTIuMS0yLjFMMjIuOCUyQzIyTDIuOSUyQzIuMUw1JTJDMEwyNyUyQzIyTDI3JTJDMjJ6JyUyMGZpbGwlM0QnJTIzMDA3YWZmJyUyRiUzRSUzQyUyRnN2ZyUzRVwiKTtyaWdodDoxMHB4O2xlZnQ6YXV0b30uc3dpcGVyLWJ1dHRvbi1wcmV2LnN3aXBlci1idXR0b24td2hpdGUsLnN3aXBlci1jb250YWluZXItcnRsIC5zd2lwZXItYnV0dG9uLW5leHQuc3dpcGVyLWJ1dHRvbi13aGl0ZXtiYWNrZ3JvdW5kLWltYWdlOnVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbDtjaGFyc2V0PXV0Zi04LCUzQ3N2ZyUyMHhtbG5zJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMjAwMCUyRnN2ZyclMjB2aWV3Qm94JTNEJzAlMjAwJTIwMjclMjA0NCclM0UlM0NwYXRoJTIwZCUzRCdNMCUyQzIyTDIyJTJDMGwyLjElMkMyLjFMNC4yJTJDMjJsMTkuOSUyQzE5LjlMMjIlMkM0NEwwJTJDMjJMMCUyQzIyTDAlMkMyMnonJTIwZmlsbCUzRCclMjNmZmZmZmYnJTJGJTNFJTNDJTJGc3ZnJTNFXCIpfS5zd2lwZXItYnV0dG9uLW5leHQuc3dpcGVyLWJ1dHRvbi13aGl0ZSwuc3dpcGVyLWNvbnRhaW5lci1ydGwgLnN3aXBlci1idXR0b24tcHJldi5zd2lwZXItYnV0dG9uLXdoaXRle2JhY2tncm91bmQtaW1hZ2U6dXJsKFwiZGF0YTppbWFnZS9zdmcreG1sO2NoYXJzZXQ9dXRmLTgsJTNDc3ZnJTIweG1sbnMlM0QnaHR0cCUzQSUyRiUyRnd3dy53My5vcmclMkYyMDAwJTJGc3ZnJyUyMHZpZXdCb3glM0QnMCUyMDAlMjAyNyUyMDQ0JyUzRSUzQ3BhdGglMjBkJTNEJ00yNyUyQzIyTDI3JTJDMjJMNSUyQzQ0bC0yLjEtMi4xTDIyLjglMkMyMkwyLjklMkMyLjFMNSUyQzBMMjclMkMyMkwyNyUyQzIyeiclMjBmaWxsJTNEJyUyM2ZmZmZmZiclMkYlM0UlM0MlMkZzdmclM0VcIil9LnN3aXBlci1idXR0b24tcHJldi5zd2lwZXItYnV0dG9uLWJsYWNrLC5zd2lwZXItY29udGFpbmVyLXJ0bCAuc3dpcGVyLWJ1dHRvbi1uZXh0LnN3aXBlci1idXR0b24tYmxhY2t7YmFja2dyb3VuZC1pbWFnZTp1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWw7Y2hhcnNldD11dGYtOCwlM0NzdmclMjB4bWxucyUzRCdodHRwJTNBJTJGJTJGd3d3LnczLm9yZyUyRjIwMDAlMkZzdmcnJTIwdmlld0JveCUzRCcwJTIwMCUyMDI3JTIwNDQnJTNFJTNDcGF0aCUyMGQlM0QnTTAlMkMyMkwyMiUyQzBsMi4xJTJDMi4xTDQuMiUyQzIybDE5LjklMkMxOS45TDIyJTJDNDRMMCUyQzIyTDAlMkMyMkwwJTJDMjJ6JyUyMGZpbGwlM0QnJTIzMDAwMDAwJyUyRiUzRSUzQyUyRnN2ZyUzRVwiKX0uc3dpcGVyLWJ1dHRvbi1uZXh0LnN3aXBlci1idXR0b24tYmxhY2ssLnN3aXBlci1jb250YWluZXItcnRsIC5zd2lwZXItYnV0dG9uLXByZXYuc3dpcGVyLWJ1dHRvbi1ibGFja3tiYWNrZ3JvdW5kLWltYWdlOnVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbDtjaGFyc2V0PXV0Zi04LCUzQ3N2ZyUyMHhtbG5zJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMjAwMCUyRnN2ZyclMjB2aWV3Qm94JTNEJzAlMjAwJTIwMjclMjA0NCclM0UlM0NwYXRoJTIwZCUzRCdNMjclMkMyMkwyNyUyQzIyTDUlMkM0NGwtMi4xLTIuMUwyMi44JTJDMjJMMi45JTJDMi4xTDUlMkMwTDI3JTJDMjJMMjclMkMyMnonJTIwZmlsbCUzRCclMjMwMDAwMDAnJTJGJTNFJTNDJTJGc3ZnJTNFXCIpfS5zd2lwZXItYnV0dG9uLWxvY2t7ZGlzcGxheTpub25lfS5zd2lwZXItcGFnaW5hdGlvbntwb3NpdGlvbjphYnNvbHV0ZTt0ZXh0LWFsaWduOmNlbnRlcjstd2Via2l0LXRyYW5zaXRpb246LjNzIG9wYWNpdHk7LW8tdHJhbnNpdGlvbjouM3Mgb3BhY2l0eTt0cmFuc2l0aW9uOi4zcyBvcGFjaXR5Oy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZTNkKDAsMCwwKTt0cmFuc2Zvcm06dHJhbnNsYXRlM2QoMCwwLDApO3otaW5kZXg6MTB9LnN3aXBlci1wYWdpbmF0aW9uLnN3aXBlci1wYWdpbmF0aW9uLWhpZGRlbntvcGFjaXR5OjB9LnN3aXBlci1jb250YWluZXItaG9yaXpvbnRhbD4uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cywuc3dpcGVyLXBhZ2luYXRpb24tY3VzdG9tLC5zd2lwZXItcGFnaW5hdGlvbi1mcmFjdGlvbntib3R0b206MTBweDtsZWZ0OjA7d2lkdGg6MTAwJX0uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy1keW5hbWlje292ZXJmbG93OmhpZGRlbjtmb250LXNpemU6MH0uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy1keW5hbWljIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXR7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoLjMzKTstbXMtdHJhbnNmb3JtOnNjYWxlKC4zMyk7dHJhbnNmb3JtOnNjYWxlKC4zMyk7cG9zaXRpb246cmVsYXRpdmV9LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pYyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZXstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgxKTstbXMtdHJhbnNmb3JtOnNjYWxlKDEpO3RyYW5zZm9ybTpzY2FsZSgxKX0uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy1keW5hbWljIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZlLW1haW57LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoMSk7LW1zLXRyYW5zZm9ybTpzY2FsZSgxKTt0cmFuc2Zvcm06c2NhbGUoMSl9LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pYyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZS1wcmV2ey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKC42Nik7LW1zLXRyYW5zZm9ybTpzY2FsZSguNjYpO3RyYW5zZm9ybTpzY2FsZSguNjYpfS5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldC1hY3RpdmUtcHJldi1wcmV2ey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKC4zMyk7LW1zLXRyYW5zZm9ybTpzY2FsZSguMzMpO3RyYW5zZm9ybTpzY2FsZSguMzMpfS5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldC1hY3RpdmUtbmV4dHstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSguNjYpOy1tcy10cmFuc2Zvcm06c2NhbGUoLjY2KTt0cmFuc2Zvcm06c2NhbGUoLjY2KX0uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy1keW5hbWljIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZlLW5leHQtbmV4dHstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSguMzMpOy1tcy10cmFuc2Zvcm06c2NhbGUoLjMzKTt0cmFuc2Zvcm06c2NhbGUoLjMzKX0uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0e3dpZHRoOjhweDtoZWlnaHQ6OHB4O2Rpc3BsYXk6aW5saW5lLWJsb2NrO2JvcmRlci1yYWRpdXM6MTAwJTtiYWNrZ3JvdW5kOiMwMDA7b3BhY2l0eTouMn1idXR0b24uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0e2JvcmRlcjpub25lO21hcmdpbjowO3BhZGRpbmc6MDstd2Via2l0LWJveC1zaGFkb3c6bm9uZTtib3gtc2hhZG93Om5vbmU7LXdlYmtpdC1hcHBlYXJhbmNlOm5vbmU7LW1vei1hcHBlYXJhbmNlOm5vbmU7YXBwZWFyYW5jZTpub25lfS5zd2lwZXItcGFnaW5hdGlvbi1jbGlja2FibGUgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHtjdXJzb3I6cG9pbnRlcn0uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZXtvcGFjaXR5OjE7YmFja2dyb3VuZDojMDA3YWZmfS5zd2lwZXItY29udGFpbmVyLXZlcnRpY2FsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRze3JpZ2h0OjEwcHg7dG9wOjUwJTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGUzZCgwLC01MCUsMCk7dHJhbnNmb3JtOnRyYW5zbGF0ZTNkKDAsLTUwJSwwKX0uc3dpcGVyLWNvbnRhaW5lci12ZXJ0aWNhbD4uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0e21hcmdpbjo2cHggMDtkaXNwbGF5OmJsb2NrfS5zd2lwZXItY29udGFpbmVyLXZlcnRpY2FsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pY3t0b3A6NTAlOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSk7LW1zLXRyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpO3RyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpO3dpZHRoOjhweH0uc3dpcGVyLWNvbnRhaW5lci12ZXJ0aWNhbD4uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHtkaXNwbGF5OmlubGluZS1ibG9jazstd2Via2l0LXRyYW5zaXRpb246LjJzIHRvcCwuMnMgLXdlYmtpdC10cmFuc2Zvcm07dHJhbnNpdGlvbjouMnMgdG9wLC4ycyAtd2Via2l0LXRyYW5zZm9ybTstby10cmFuc2l0aW9uOi4ycyB0cmFuc2Zvcm0sLjJzIHRvcDt0cmFuc2l0aW9uOi4ycyB0cmFuc2Zvcm0sLjJzIHRvcDt0cmFuc2l0aW9uOi4ycyB0cmFuc2Zvcm0sLjJzIHRvcCwuMnMgLXdlYmtpdC10cmFuc2Zvcm19LnN3aXBlci1jb250YWluZXItaG9yaXpvbnRhbD4uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0e21hcmdpbjowIDRweH0uc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pY3tsZWZ0OjUwJTstd2Via2l0LXRyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpOy1tcy10cmFuc2Zvcm06dHJhbnNsYXRlWCgtNTAlKTt0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNTAlKTt3aGl0ZS1zcGFjZTpub3dyYXB9LnN3aXBlci1jb250YWluZXItaG9yaXpvbnRhbD4uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHstd2Via2l0LXRyYW5zaXRpb246LjJzIGxlZnQsLjJzIC13ZWJraXQtdHJhbnNmb3JtO3RyYW5zaXRpb246LjJzIGxlZnQsLjJzIC13ZWJraXQtdHJhbnNmb3JtOy1vLXRyYW5zaXRpb246LjJzIHRyYW5zZm9ybSwuMnMgbGVmdDt0cmFuc2l0aW9uOi4ycyB0cmFuc2Zvcm0sLjJzIGxlZnQ7dHJhbnNpdGlvbjouMnMgdHJhbnNmb3JtLC4ycyBsZWZ0LC4ycyAtd2Via2l0LXRyYW5zZm9ybX0uc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsLnN3aXBlci1jb250YWluZXItcnRsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHstd2Via2l0LXRyYW5zaXRpb246LjJzIHJpZ2h0LC4ycyAtd2Via2l0LXRyYW5zZm9ybTt0cmFuc2l0aW9uOi4ycyByaWdodCwuMnMgLXdlYmtpdC10cmFuc2Zvcm07LW8tdHJhbnNpdGlvbjouMnMgdHJhbnNmb3JtLC4ycyByaWdodDt0cmFuc2l0aW9uOi4ycyB0cmFuc2Zvcm0sLjJzIHJpZ2h0O3RyYW5zaXRpb246LjJzIHRyYW5zZm9ybSwuMnMgcmlnaHQsLjJzIC13ZWJraXQtdHJhbnNmb3JtfS5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2JhcntiYWNrZ3JvdW5kOnJnYmEoMCwwLDAsLjI1KTtwb3NpdGlvbjphYnNvbHV0ZX0uc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIgLnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFyLWZpbGx7YmFja2dyb3VuZDojMDA3YWZmO3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6MDt0b3A6MDt3aWR0aDoxMDAlO2hlaWdodDoxMDAlOy13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDApOy1tcy10cmFuc2Zvcm06c2NhbGUoMCk7dHJhbnNmb3JtOnNjYWxlKDApOy13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbjpsZWZ0IHRvcDstbXMtdHJhbnNmb3JtLW9yaWdpbjpsZWZ0IHRvcDt0cmFuc2Zvcm0tb3JpZ2luOmxlZnQgdG9wfS5zd2lwZXItY29udGFpbmVyLXJ0bCAuc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIgLnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFyLWZpbGx7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOnJpZ2h0IHRvcDstbXMtdHJhbnNmb3JtLW9yaWdpbjpyaWdodCB0b3A7dHJhbnNmb3JtLW9yaWdpbjpyaWdodCB0b3B9LnN3aXBlci1jb250YWluZXItaG9yaXpvbnRhbD4uc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIsLnN3aXBlci1jb250YWluZXItdmVydGljYWw+LnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFyLnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFyLW9wcG9zaXRle3dpZHRoOjEwMCU7aGVpZ2h0OjRweDtsZWZ0OjA7dG9wOjB9LnN3aXBlci1jb250YWluZXItaG9yaXpvbnRhbD4uc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIuc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXItb3Bwb3NpdGUsLnN3aXBlci1jb250YWluZXItdmVydGljYWw+LnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFye3dpZHRoOjRweDtoZWlnaHQ6MTAwJTtsZWZ0OjA7dG9wOjB9LnN3aXBlci1wYWdpbmF0aW9uLXdoaXRlIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZle2JhY2tncm91bmQ6I2ZmZn0uc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIuc3dpcGVyLXBhZ2luYXRpb24td2hpdGV7YmFja2dyb3VuZDpyZ2JhKDI1NSwyNTUsMjU1LC4yNSl9LnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFyLnN3aXBlci1wYWdpbmF0aW9uLXdoaXRlIC5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2Jhci1maWxse2JhY2tncm91bmQ6I2ZmZn0uc3dpcGVyLXBhZ2luYXRpb24tYmxhY2sgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldC1hY3RpdmV7YmFja2dyb3VuZDojMDAwfS5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2Jhci5zd2lwZXItcGFnaW5hdGlvbi1ibGFja3tiYWNrZ3JvdW5kOnJnYmEoMCwwLDAsLjI1KX0uc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIuc3dpcGVyLXBhZ2luYXRpb24tYmxhY2sgLnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFyLWZpbGx7YmFja2dyb3VuZDojMDAwfS5zd2lwZXItcGFnaW5hdGlvbi1sb2Nre2Rpc3BsYXk6bm9uZX0uc3dpcGVyLXNjcm9sbGJhcntib3JkZXItcmFkaXVzOjEwcHg7cG9zaXRpb246cmVsYXRpdmU7LW1zLXRvdWNoLWFjdGlvbjpub25lO2JhY2tncm91bmQ6cmdiYSgwLDAsMCwuMSl9LnN3aXBlci1jb250YWluZXItaG9yaXpvbnRhbD4uc3dpcGVyLXNjcm9sbGJhcntwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjElO2JvdHRvbTozcHg7ei1pbmRleDo1MDtoZWlnaHQ6NXB4O3dpZHRoOjk4JX0uc3dpcGVyLWNvbnRhaW5lci12ZXJ0aWNhbD4uc3dpcGVyLXNjcm9sbGJhcntwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDozcHg7dG9wOjElO3otaW5kZXg6NTA7d2lkdGg6NXB4O2hlaWdodDo5OCV9LnN3aXBlci1zY3JvbGxiYXItZHJhZ3toZWlnaHQ6MTAwJTt3aWR0aDoxMDAlO3Bvc2l0aW9uOnJlbGF0aXZlO2JhY2tncm91bmQ6cmdiYSgwLDAsMCwuNSk7Ym9yZGVyLXJhZGl1czoxMHB4O2xlZnQ6MDt0b3A6MH0uc3dpcGVyLXNjcm9sbGJhci1jdXJzb3ItZHJhZ3tjdXJzb3I6bW92ZX0uc3dpcGVyLXNjcm9sbGJhci1sb2Nre2Rpc3BsYXk6bm9uZX0uc3dpcGVyLXpvb20tY29udGFpbmVye3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7ZGlzcGxheTotd2Via2l0LWJveDtkaXNwbGF5Oi13ZWJraXQtZmxleDtkaXNwbGF5Oi1tcy1mbGV4Ym94O2Rpc3BsYXk6ZmxleDstd2Via2l0LWJveC1wYWNrOmNlbnRlcjstd2Via2l0LWp1c3RpZnktY29udGVudDpjZW50ZXI7LW1zLWZsZXgtcGFjazpjZW50ZXI7anVzdGlmeS1jb250ZW50OmNlbnRlcjstd2Via2l0LWJveC1hbGlnbjpjZW50ZXI7LXdlYmtpdC1hbGlnbi1pdGVtczpjZW50ZXI7LW1zLWZsZXgtYWxpZ246Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjt0ZXh0LWFsaWduOmNlbnRlcn0uc3dpcGVyLXpvb20tY29udGFpbmVyPmNhbnZhcywuc3dpcGVyLXpvb20tY29udGFpbmVyPmltZywuc3dpcGVyLXpvb20tY29udGFpbmVyPnN2Z3ttYXgtd2lkdGg6MTAwJTttYXgtaGVpZ2h0OjEwMCU7LW8tb2JqZWN0LWZpdDpjb250YWluO29iamVjdC1maXQ6Y29udGFpbn0uc3dpcGVyLXNsaWRlLXpvb21lZHtjdXJzb3I6bW92ZX0uc3dpcGVyLWxhenktcHJlbG9hZGVye3dpZHRoOjQycHg7aGVpZ2h0OjQycHg7cG9zaXRpb246YWJzb2x1dGU7bGVmdDo1MCU7dG9wOjUwJTttYXJnaW4tbGVmdDotMjFweDttYXJnaW4tdG9wOi0yMXB4O3otaW5kZXg6MTA7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOjUwJTstbXMtdHJhbnNmb3JtLW9yaWdpbjo1MCU7dHJhbnNmb3JtLW9yaWdpbjo1MCU7LXdlYmtpdC1hbmltYXRpb246c3dpcGVyLXByZWxvYWRlci1zcGluIDFzIHN0ZXBzKDEyLGVuZCkgaW5maW5pdGU7YW5pbWF0aW9uOnN3aXBlci1wcmVsb2FkZXItc3BpbiAxcyBzdGVwcygxMixlbmQpIGluZmluaXRlfS5zd2lwZXItbGF6eS1wcmVsb2FkZXI6YWZ0ZXJ7ZGlzcGxheTpibG9jaztjb250ZW50OicnO3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7YmFja2dyb3VuZC1pbWFnZTp1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWw7Y2hhcnNldD11dGYtOCwlM0NzdmclMjB2aWV3Qm94JTNEJzAlMjAwJTIwMTIwJTIwMTIwJyUyMHhtbG5zJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMjAwMCUyRnN2ZyclMjB4bWxucyUzQXhsaW5rJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMTk5OSUyRnhsaW5rJyUzRSUzQ2RlZnMlM0UlM0NsaW5lJTIwaWQlM0QnbCclMjB4MSUzRCc2MCclMjB4MiUzRCc2MCclMjB5MSUzRCc3JyUyMHkyJTNEJzI3JyUyMHN0cm9rZSUzRCclMjM2YzZjNmMnJTIwc3Ryb2tlLXdpZHRoJTNEJzExJyUyMHN0cm9rZS1saW5lY2FwJTNEJ3JvdW5kJyUyRiUzRSUzQyUyRmRlZnMlM0UlM0NnJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMjcnJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMjcnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgzMCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjI3JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoNjAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDkwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMjcnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgxMjAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDE1MCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjM3JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMTgwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuNDYnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgyMTAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy41NiclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDI0MCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjY2JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMjcwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuNzUnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgzMDAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy44NSclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDMzMCUyMDYwJTJDNjApJyUyRiUzRSUzQyUyRmclM0UlM0MlMkZzdmclM0VcIik7YmFja2dyb3VuZC1wb3NpdGlvbjo1MCU7YmFja2dyb3VuZC1zaXplOjEwMCU7YmFja2dyb3VuZC1yZXBlYXQ6bm8tcmVwZWF0fS5zd2lwZXItbGF6eS1wcmVsb2FkZXItd2hpdGU6YWZ0ZXJ7YmFja2dyb3VuZC1pbWFnZTp1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWw7Y2hhcnNldD11dGYtOCwlM0NzdmclMjB2aWV3Qm94JTNEJzAlMjAwJTIwMTIwJTIwMTIwJyUyMHhtbG5zJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMjAwMCUyRnN2ZyclMjB4bWxucyUzQXhsaW5rJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMTk5OSUyRnhsaW5rJyUzRSUzQ2RlZnMlM0UlM0NsaW5lJTIwaWQlM0QnbCclMjB4MSUzRCc2MCclMjB4MiUzRCc2MCclMjB5MSUzRCc3JyUyMHkyJTNEJzI3JyUyMHN0cm9rZSUzRCclMjNmZmYnJTIwc3Ryb2tlLXdpZHRoJTNEJzExJyUyMHN0cm9rZS1saW5lY2FwJTNEJ3JvdW5kJyUyRiUzRSUzQyUyRmRlZnMlM0UlM0NnJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMjcnJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMjcnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgzMCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjI3JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoNjAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDkwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMjcnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgxMjAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDE1MCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjM3JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMTgwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuNDYnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgyMTAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy41NiclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDI0MCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjY2JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMjcwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuNzUnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgzMDAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy44NSclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDMzMCUyMDYwJTJDNjApJyUyRiUzRSUzQyUyRmclM0UlM0MlMkZzdmclM0VcIil9QC13ZWJraXQta2V5ZnJhbWVzIHN3aXBlci1wcmVsb2FkZXItc3BpbnsxMDAley13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgzNjBkZWcpO3RyYW5zZm9ybTpyb3RhdGUoMzYwZGVnKX19QGtleWZyYW1lcyBzd2lwZXItcHJlbG9hZGVyLXNwaW57MTAwJXstd2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMzYwZGVnKTt0cmFuc2Zvcm06cm90YXRlKDM2MGRlZyl9fS5zd2lwZXItY29udGFpbmVyIC5zd2lwZXItbm90aWZpY2F0aW9ue3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6MDt0b3A6MDtwb2ludGVyLWV2ZW50czpub25lO29wYWNpdHk6MDt6LWluZGV4Oi0xMDAwfS5zd2lwZXItY29udGFpbmVyLWZhZGUuc3dpcGVyLWNvbnRhaW5lci1mcmVlLW1vZGUgLnN3aXBlci1zbGlkZXstd2Via2l0LXRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uOmVhc2Utb3V0Oy1vLXRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uOmVhc2Utb3V0O3RyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uOmVhc2Utb3V0fS5zd2lwZXItY29udGFpbmVyLWZhZGUgLnN3aXBlci1zbGlkZXtwb2ludGVyLWV2ZW50czpub25lOy13ZWJraXQtdHJhbnNpdGlvbi1wcm9wZXJ0eTpvcGFjaXR5Oy1vLXRyYW5zaXRpb24tcHJvcGVydHk6b3BhY2l0eTt0cmFuc2l0aW9uLXByb3BlcnR5Om9wYWNpdHl9LnN3aXBlci1jb250YWluZXItZmFkZSAuc3dpcGVyLXNsaWRlIC5zd2lwZXItc2xpZGV7cG9pbnRlci1ldmVudHM6bm9uZX0uc3dpcGVyLWNvbnRhaW5lci1mYWRlIC5zd2lwZXItc2xpZGUtYWN0aXZlLC5zd2lwZXItY29udGFpbmVyLWZhZGUgLnN3aXBlci1zbGlkZS1hY3RpdmUgLnN3aXBlci1zbGlkZS1hY3RpdmV7cG9pbnRlci1ldmVudHM6YXV0b30uc3dpcGVyLWNvbnRhaW5lci1jdWJle292ZXJmbG93OnZpc2libGV9LnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRle3BvaW50ZXItZXZlbnRzOm5vbmU7LXdlYmtpdC1iYWNrZmFjZS12aXNpYmlsaXR5OmhpZGRlbjtiYWNrZmFjZS12aXNpYmlsaXR5OmhpZGRlbjt6LWluZGV4OjE7dmlzaWJpbGl0eTpoaWRkZW47LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOjAgMDstbXMtdHJhbnNmb3JtLW9yaWdpbjowIDA7dHJhbnNmb3JtLW9yaWdpbjowIDA7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJX0uc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItc2xpZGUgLnN3aXBlci1zbGlkZXtwb2ludGVyLWV2ZW50czpub25lfS5zd2lwZXItY29udGFpbmVyLWN1YmUuc3dpcGVyLWNvbnRhaW5lci1ydGwgLnN3aXBlci1zbGlkZXstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46MTAwJSAwOy1tcy10cmFuc2Zvcm0tb3JpZ2luOjEwMCUgMDt0cmFuc2Zvcm0tb3JpZ2luOjEwMCUgMH0uc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItc2xpZGUtYWN0aXZlLC5zd2lwZXItY29udGFpbmVyLWN1YmUgLnN3aXBlci1zbGlkZS1hY3RpdmUgLnN3aXBlci1zbGlkZS1hY3RpdmV7cG9pbnRlci1ldmVudHM6YXV0b30uc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItc2xpZGUtYWN0aXZlLC5zd2lwZXItY29udGFpbmVyLWN1YmUgLnN3aXBlci1zbGlkZS1uZXh0LC5zd2lwZXItY29udGFpbmVyLWN1YmUgLnN3aXBlci1zbGlkZS1uZXh0Ky5zd2lwZXItc2xpZGUsLnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRlLXByZXZ7cG9pbnRlci1ldmVudHM6YXV0bzt2aXNpYmlsaXR5OnZpc2libGV9LnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRlLXNoYWRvdy1ib3R0b20sLnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRlLXNoYWRvdy1sZWZ0LC5zd2lwZXItY29udGFpbmVyLWN1YmUgLnN3aXBlci1zbGlkZS1zaGFkb3ctcmlnaHQsLnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRlLXNoYWRvdy10b3B7ei1pbmRleDowOy13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTpoaWRkZW47YmFja2ZhY2UtdmlzaWJpbGl0eTpoaWRkZW59LnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLWN1YmUtc2hhZG93e3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6MDtib3R0b206MDt3aWR0aDoxMDAlO2hlaWdodDoxMDAlO2JhY2tncm91bmQ6IzAwMDtvcGFjaXR5Oi42Oy13ZWJraXQtZmlsdGVyOmJsdXIoNTBweCk7ZmlsdGVyOmJsdXIoNTBweCk7ei1pbmRleDowfS5zd2lwZXItY29udGFpbmVyLWZsaXB7b3ZlcmZsb3c6dmlzaWJsZX0uc3dpcGVyLWNvbnRhaW5lci1mbGlwIC5zd2lwZXItc2xpZGV7cG9pbnRlci1ldmVudHM6bm9uZTstd2Via2l0LWJhY2tmYWNlLXZpc2liaWxpdHk6aGlkZGVuO2JhY2tmYWNlLXZpc2liaWxpdHk6aGlkZGVuO3otaW5kZXg6MX0uc3dpcGVyLWNvbnRhaW5lci1mbGlwIC5zd2lwZXItc2xpZGUgLnN3aXBlci1zbGlkZXtwb2ludGVyLWV2ZW50czpub25lfS5zd2lwZXItY29udGFpbmVyLWZsaXAgLnN3aXBlci1zbGlkZS1hY3RpdmUsLnN3aXBlci1jb250YWluZXItZmxpcCAuc3dpcGVyLXNsaWRlLWFjdGl2ZSAuc3dpcGVyLXNsaWRlLWFjdGl2ZXtwb2ludGVyLWV2ZW50czphdXRvfS5zd2lwZXItY29udGFpbmVyLWZsaXAgLnN3aXBlci1zbGlkZS1zaGFkb3ctYm90dG9tLC5zd2lwZXItY29udGFpbmVyLWZsaXAgLnN3aXBlci1zbGlkZS1zaGFkb3ctbGVmdCwuc3dpcGVyLWNvbnRhaW5lci1mbGlwIC5zd2lwZXItc2xpZGUtc2hhZG93LXJpZ2h0LC5zd2lwZXItY29udGFpbmVyLWZsaXAgLnN3aXBlci1zbGlkZS1zaGFkb3ctdG9we3otaW5kZXg6MDstd2Via2l0LWJhY2tmYWNlLXZpc2liaWxpdHk6aGlkZGVuO2JhY2tmYWNlLXZpc2liaWxpdHk6aGlkZGVufS5zd2lwZXItY29udGFpbmVyLWNvdmVyZmxvdyAuc3dpcGVyLXdyYXBwZXJ7LW1zLXBlcnNwZWN0aXZlOjEyMDBweH0iLCJAaW1wb3J0ICd+c3dpcGVyL2Rpc3QvY3NzL3N3aXBlci5taW4uY3NzJztcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPXNyYy9hcHAvY29tcG9uZW50cy9naWZ0cy1yZWxhdGVkL2dpZnRzLXJlbGF0ZWQuY29tcG9uZW50LmNzcy5tYXAgKi8iXX0= */");

/***/ }),

/***/ "./src/app/components/gifts-related/gifts-related.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/gifts-related/gifts-related.component.ts ***!
  \*********************************************************************/
/*! exports provided: GiftsRelatedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftsRelatedComponent", function() { return GiftsRelatedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-swiper-wrapper */ "./node_modules/ngx-swiper-wrapper/dist/ngx-swiper-wrapper.es5.js");
/* harmony import */ var _services_platform_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/platform.service */ "./src/app/services/platform.service.ts");




var GiftsRelatedComponent = /** @class */ (function () {
    function GiftsRelatedComponent(elRef, platform) {
        this.elRef = elRef;
        this.platform = platform;
        this.desktopWidth = 992;
        this.isDesktop = false;
        // public itemWidth = 400;
        this.itemWidth = 150;
        this.slideGap = 32;
        this.isFirstSlide = false;
        this.isLastSlide = false;
        this.showControls = true;
    }
    GiftsRelatedComponent.prototype.onResize = function () {
        this.checkAndSetSlides();
    };
    GiftsRelatedComponent.prototype.ngOnInit = function () {
        this.setSettings();
        // const s = this.slider_settings || {};
        // this.sliderSettings = {
        //   ...this.defaultSliderSettings, ...(this.sliderSettings || {}), ...s
        // };
        // this.defaultSliderSettings.slidesPerView = s.slidesPerView ||;
    };
    GiftsRelatedComponent.prototype.ngAfterViewInit = function () {
        if (this.platform.isBrowser) {
            this.checkAndSetSlides();
        }
    };
    GiftsRelatedComponent.prototype.setSettings = function () {
        var s = this.slider_settings || {};
        this.defaultSliderSettings = {
            followFinger: true,
            centeredSlides: false,
            slidesPerView: 6,
            slidesPerGroup: 6,
            spaceBetween: this.slideGap,
        };
        this.defaultSliderSettings.slidesPerView = s.slidesPerView || this.defaultSliderSettings.slidesPerView;
        this.defaultSliderSettings.slidesPerGroup = s.slidesPerGroup || this.defaultSliderSettings.slidesPerGroup;
        this.sliderSettings = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.defaultSliderSettings, (this.sliderSettings || {}), s);
    };
    GiftsRelatedComponent.prototype.checkAndSetSlides = function () {
        var _this = this;
        clearTimeout(this.timeOut);
        this.timeOut = setTimeout(function () {
            _this.setSize();
            _this.checkSlideIndex();
        }, 200);
    };
    GiftsRelatedComponent.prototype.setSize = function () {
        var length = Math.round(this.elRef.nativeElement.getBoundingClientRect().width / this.itemWidth);
        var l = length > this.defaultSliderSettings.slidesPerView ? this.defaultSliderSettings.slidesPerView : length;
        var gr = this.slider_settings.slidesPerGroup;
        this.isDesktop = window.innerWidth >= this.desktopWidth;
        this.sliderSettings.slidesPerView = l;
        this.sliderSettings.slidesPerGroup = this.slider_settings && gr && gr < l ? gr : l;
        // this.setSettings();
        this.slideGap = this.sliderSettings.slidesPerGroup > 1 ? 32 : 10;
    };
    GiftsRelatedComponent.prototype.checkSlideIndex = function () {
        if (this.swiper && !this.sliderSettings.loop) {
            this.isFirstSlide = this.swiper.getIndex() === 0;
            this.isLastSlide = this.swiper.getIndex() === (this.gifts || this.logos).length - this.sliderSettings.slidesPerGroup;
            this.showControls = !(this.isFirstSlide && this.isLastSlide);
        }
        this.sliderSettings.allowTouchMove = this.showControls;
    };
    GiftsRelatedComponent.prototype.onIndexChange = function (index) {
        this.checkSlideIndex();
    };
    GiftsRelatedComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _services_platform_service__WEBPACK_IMPORTED_MODULE_3__["PlatformService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GiftsRelatedComponent.prototype, "gifts", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GiftsRelatedComponent.prototype, "logos", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], GiftsRelatedComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftsRelatedComponent.prototype, "slider_settings", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_2__["SwiperDirective"], { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_2__["SwiperDirective"])
    ], GiftsRelatedComponent.prototype, "swiper", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], GiftsRelatedComponent.prototype, "onResize", null);
    GiftsRelatedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gifts-related',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gifts-related.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts-related/gifts-related.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gifts-related.component.styl */ "./src/app/components/gifts-related/gifts-related.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"], _services_platform_service__WEBPACK_IMPORTED_MODULE_3__["PlatformService"]])
    ], GiftsRelatedComponent);
    return GiftsRelatedComponent;
}());



/***/ }),

/***/ "./src/app/modules/card-buttons/card-buttons.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/modules/card-buttons/card-buttons.module.ts ***!
  \*************************************************************/
/*! exports provided: CardButtonsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardButtonsModule", function() { return CardButtonsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _validators_validators_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../validators/validators.module */ "./src/app/validators/validators.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_svg_icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-svg-icon */ "./node_modules/angular-svg-icon/fesm5/angular-svg-icon.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_card_buttons_card_buttons_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/card-buttons/card-buttons.component */ "./src/app/components/card-buttons/card-buttons.component.ts");










var CardButtonsModule = /** @class */ (function () {
    function CardButtonsModule() {
    }
    CardButtonsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_components_card_buttons_card_buttons_component__WEBPACK_IMPORTED_MODULE_9__["CardButtonsComponent"]],
            exports: [_components_card_buttons_card_buttons_component__WEBPACK_IMPORTED_MODULE_9__["CardButtonsComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"],
                _validators_validators_module__WEBPACK_IMPORTED_MODULE_3__["ValidatorsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                angular_svg_icon__WEBPACK_IMPORTED_MODULE_5__["AngularSvgIconModule"],
            ]
        })
    ], CardButtonsModule);
    return CardButtonsModule;
}());



/***/ }),

/***/ "./src/app/modules/gifts-related/gifts-related.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/modules/gifts-related/gifts-related.module.ts ***!
  \***************************************************************/
/*! exports provided: GiftsRelatedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftsRelatedModule", function() { return GiftsRelatedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-swiper-wrapper */ "./node_modules/ngx-swiper-wrapper/dist/ngx-swiper-wrapper.es5.js");
/* harmony import */ var _components_gifts_related_gifts_related_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/gifts-related/gifts-related.component */ "./src/app/components/gifts-related/gifts-related.component.ts");






var GiftsRelatedModule = /** @class */ (function () {
    function GiftsRelatedModule() {
    }
    GiftsRelatedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_components_gifts_related_gifts_related_component__WEBPACK_IMPORTED_MODULE_5__["GiftsRelatedComponent"]],
            exports: [_components_gifts_related_gifts_related_component__WEBPACK_IMPORTED_MODULE_5__["GiftsRelatedComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
                ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_4__["SwiperModule"]
            ]
        })
    ], GiftsRelatedModule);
    return GiftsRelatedModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~pages-cert-index-index-page-module~pages-help-help-module~pages-index-index-module~pages-par~bdc04c47.js.map