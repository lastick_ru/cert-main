(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-index-index-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts/gifts-nav/gifts-nav.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts/gifts-nav/gifts-nav.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"tags tags_nav\">\r\n  <app-scroll [itemClick$]=\"itemClick$\" [options]=\"{'scrollOnClick': true}\">\r\n    <div class=\"tags__list tags__list_nav\">\r\n      <div class=\"tags__item tags__item_nav tags__item_all\" (click)=\"onClick($event, 'all')\">\r\n        <span class=\"tags__link tags__link_nav\" [ngClass]=\"{'f-active': all}\" #allTag>\r\n          {{'Common.All' | translate}}\r\n        </span>\r\n      </div>\r\n      <div class=\"tags__item tags__item_nav\"\r\n           *ngFor=\"let tag of tags\"\r\n           (click)=\"onClick($event, tag)\"\r\n            [class.disabled]=\"disabled\"\r\n           >\r\n        <span class=\"tags__link tags__link_nav\" [ngClass]=\"{'f-active': activeTag && activeTag._uuid === tag._uuid}\">\r\n          {{tag.name | multilang}}\r\n        </span>\r\n      </div>\r\n    </div>\r\n  </app-scroll>\r\n</nav>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts/gifts.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts/gifts.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"gifts\">\r\n  <div class=\"gifts__head\" *ngIf=\"tags && tags.length\">\r\n    <div class=\"gifts__head-row\" [ngClass]=\"{ 'f-vh': showSearch }\">\r\n      <div class=\"gifts__head-item gifts__head-item_tags\">\r\n        <app-gifts-nav\r\n          [tags]=\"tags\"\r\n          [activeTag]=\"currState.tag\"\r\n          (tagClick)=\"onTagClick($event)\"\r\n          (allClick)=\"onAllClick()\"\r\n         \r\n        >\r\n        </app-gifts-nav>\r\n      </div>\r\n      <div class=\"label label-map\" (click)=\"toggleCard()\"\r\n        [class.label-map--active]=\"cardIsOpen\"\r\n        >\r\n        <svg class=\"icon\" fill=\"none\" viewBox=\"0 0 24 25\" xmlns=\"http://www.w3.org/2000/svg\">\r\n          <path\r\n            d=\"M12 24.6917C11.7415 24.6917 11.5065 24.5716 11.342 24.3796C10.4961 23.3956 3 14.6837 3 9.88365C3 4.81965 7.04177 0.69165 12 0.69165C16.9582 0.69165 21 4.81965 21 9.88365C21 14.6596 13.5039 23.3956 12.658 24.3796C12.4935 24.5716 12.2585 24.6917 12 24.6917ZM12 2.51565C8.02872 2.51565 4.7859 5.82765 4.7859 9.88365C4.7859 13.0517 9.50914 19.3636 12 22.3876C14.4909 19.3876 19.2141 13.0517 19.2141 9.88365C19.2141 5.82765 15.9713 2.51565 12 2.51565Z\"\r\n            fill=\"#A8A8A8\"\r\n            stroke=\"#A8A8A8\"\r\n            stroke-width=\"0.3\"\r\n          ></path>\r\n          <path\r\n            d=\"M11.9998 14.6118C9.50892 14.6118 7.48804 12.5478 7.48804 10.0038C7.48804 7.45975 9.50892 5.39575 11.9998 5.39575C14.4906 5.39575 16.5115 7.45975 16.5115 10.0038C16.5115 12.5478 14.4906 14.6118 11.9998 14.6118ZM11.9998 7.21975C10.4959 7.21975 9.27394 8.46775 9.27394 10.0038C9.27394 11.5398 10.4959 12.7878 11.9998 12.7878C13.5037 12.7878 14.7256 11.5398 14.7256 10.0038C14.7256 8.46775 13.5037 7.21975 11.9998 7.21975Z\"\r\n            fill=\"#A8A8A8\"\r\n          ></path></svg>\r\n          <span class=\"hidden-mobile\">Карта</span>\r\n      </div>\r\n      <div class=\"gifts__head-item gifts__head-item_icon mxn-cur-p\" (click)=\"onSearchClick()\">\r\n        <svg-icon src=\"/assets/svg/loupe.svg\"></svg-icon>\r\n      </div>\r\n    </div>\r\n    <div class=\"gifts__input-wrap\" [ngClass]=\"{ 'f-active': showSearch }\">\r\n      <div class=\"gifts__input-inner\">\r\n        <input\r\n          type=\"text\"\r\n          appNativeElementControl\r\n          [appFocusInput]=\"showSearch\"\r\n          [(ngModel)]=\"currState.search\"\r\n          (ngModelChange)=\"onSearchChange()\"\r\n          placeholder=\"Поиск по сертификатам\"\r\n          class=\"gifts__input mxn-i-txt mxn-i-txt_full-width\"\r\n        />\r\n        <div class=\"gifts__input-icon mxn-cur-p\" (click)=\"onCloseSearchClick()\">\r\n          <svg-icon src=\"/assets/svg/close.svg\"></svg-icon>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"gifts__body\">\r\n    <div class=\"gifts__empty-message\" *ngIf=\"currState.name === 'search' && !_gifts.length\">\r\n      Ничего не найдено\r\n    </div>\r\n    <div *ngIf=\"cardIsOpen\" class=\"main-map-container\">\r\n      <iframe src=\"https://snazzymaps.com/embed/254084\" width=\"100%\" height=\"100%\" style=\"border:none;\"></iframe>\r\n    </div>\r\n    <div\r\n      class=\"cards\"\r\n      *ngIf=\"_gifts && _gifts.length && !cardIsOpen\"\r\n      infinite-scroll\r\n      [infiniteScrollDistance]=\"5\"\r\n      [infiniteScrollDisabled]=\"infiniteScrollDisabled\"\r\n      (scrolled)=\"onInfiniteScroll()\"\r\n    >\r\n      <div class=\"cards__list\">\r\n        <div class=\"cards__item\" *ngFor=\"let gift of _gifts\">\r\n          <app-gift-card\r\n            [gift]=\"gift\"\r\n            [name]=\"gift.name_variant | multilang\"\r\n            url=\"{{ gift.user_friendly_url || gift._uuid }}\"\r\n            [showRelated]=\"true\"\r\n            [showTable]=\"false\"\r\n            [logo]=\"(gift.entity_icons || [])[0]\"\r\n          ></app-gift-card>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- <div class=\"cards\"  >\r\n      <div class=\"cards__list\">\r\n        <div class=\"cards__item\" >\r\n          \r\n          <app-gift-card\r\n           \r\n          ></app-gift-card> \r\n        \r\n        </div>\r\n        <div class=\"cards__item\" >\r\n          \r\n          <app-gift-card\r\n           \r\n          ></app-gift-card> \r\n        \r\n        </div>\r\n        <div class=\"cards__item\" >\r\n          \r\n          <app-gift-card\r\n           \r\n          ></app-gift-card> \r\n        \r\n        </div>\r\n        <div class=\"cards__item\" >\r\n          \r\n          <app-gift-card\r\n           \r\n          ></app-gift-card> \r\n        \r\n        </div>\r\n      </div>\r\n    </div> -->\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/index/index.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/index/index.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-layout [showLogo]=\"!isDesktop\" >\r\n  <section class=\"mxn-section mxn-section_mb-2\">\r\n    <div class=\"container container_gap-1\">\r\n      <app-company-heading [showLogo]=\"isDesktop\" [banner]=\"bannerCert\"></app-company-heading> \r\n    </div>\r\n  </section>\r\n  <section class=\"mxn-section mxn-section_mb-2\">\r\n    <app-tabs uuid=\"index_tabs\" headGap=\"1\" [activeTab]=\"activeTab\" [minh]=\"580\">\r\n      <app-tabs-tab [tabTitle]=\"'Tabs.Buy' | translate\" uuid=\"gifts\" gap=\"1\" url=\"/\">\r\n        <app-gifts (giftsFetched)=\"onGiftsFetched($event)\"></app-gifts>\r\n      </app-tabs-tab>\r\n    </app-tabs>\r\n  </section>\r\n</app-layout>\r\n");

/***/ }),

/***/ "./src/app/components/gifts/gifts-nav/gifts-nav.component.styl":
/*!*********************************************************************!*\
  !*** ./src/app/components/gifts/gifts-nav/gifts-nav.component.styl ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/components/gifts/gifts-nav/gifts-nav.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9naWZ0cy9naWZ0cy1uYXYvZ2lmdHMtbmF2LmNvbXBvbmVudC5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHFGQUFxRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZ2lmdHMvZ2lmdHMtbmF2L2dpZnRzLW5hdi5jb21wb25lbnQuc3R5bCJ9 */");

/***/ }),

/***/ "./src/app/components/gifts/gifts-nav/gifts-nav.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/gifts/gifts-nav/gifts-nav.component.ts ***!
  \*******************************************************************/
/*! exports provided: GiftsNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftsNavComponent", function() { return GiftsNavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var GiftsNavComponent = /** @class */ (function () {
    function GiftsNavComponent() {
        this.activeTag = null;
        this.disabled = false;
        this.tagClick = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.allClick = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.itemClick$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.all = true;
    }
    GiftsNavComponent.prototype.ngOnInit = function () {
        this.all = !this.activeTag;
    };
    GiftsNavComponent.prototype.ngOnChanges = function (changes) {
        this.all = !this.activeTag;
        console.log(this.disabled);
        if (this.disabled) {
            this.all = true;
            this.activeTag = null;
            this.allClick.emit();
        }
    };
    GiftsNavComponent.prototype.onClick = function (e, tag) {
        if (this.disabled) {
            return;
        }
        if (tag === 'all') {
            this.all = true;
            this.activeTag = null;
            this.allClick.emit();
            return;
        }
        else {
            this.all = false;
            this.activeTag = tag;
            this.tagClick.emit(tag);
        }
        this.itemClick$.next({ $: e.currentTarget });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GiftsNavComponent.prototype, "tags", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], GiftsNavComponent.prototype, "activeTag", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], GiftsNavComponent.prototype, "disabled", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], GiftsNavComponent.prototype, "tagClick", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], GiftsNavComponent.prototype, "allClick", void 0);
    GiftsNavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gifts-nav',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gifts-nav.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts/gifts-nav/gifts-nav.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gifts-nav.component.styl */ "./src/app/components/gifts/gifts-nav/gifts-nav.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], GiftsNavComponent);
    return GiftsNavComponent;
}());



/***/ }),

/***/ "./src/app/components/gifts/gifts.component.styl":
/*!*******************************************************!*\
  !*** ./src/app/components/gifts/gifts.component.styl ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/components/gifts/gifts.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9naWZ0cy9naWZ0cy5jb21wb25lbnQuc3R5bCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1RUFBdUUiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2dpZnRzL2dpZnRzLmNvbXBvbmVudC5zdHlsIn0= */");

/***/ }),

/***/ "./src/app/components/gifts/gifts.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/gifts/gifts.component.ts ***!
  \*****************************************************/
/*! exports provided: GiftsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftsComponent", function() { return GiftsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! date-fns */ "./node_modules/date-fns/esm/index.js");
/* harmony import */ var _services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/storage.service */ "./src/app/services/storage.service.ts");
/* harmony import */ var _services_logger_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/logger.service */ "./src/app/services/logger.service.ts");








var GiftsComponent = /** @class */ (function () {
    function GiftsComponent(api, storage, logger) {
        this.api = api;
        this.storage = storage;
        this.logger = logger;
        this.giftsFetched = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.storageKey = 'gifts';
        this.showSearch = false;
        this.pageLength = 20;
        this.pages = [];
        this.prevState = {};
        this.initialState = {
            page: 0,
            scroll: 0,
            tag: null,
            search: '',
            name: 'all',
        };
        this.currState = {
            page: 0,
            scroll: 0,
            tag: null,
            search: '',
            name: 'all',
        };
        this.stateHistory = [];
        this.infiniteScrollDisabled = true;
        this.searchDebounce = 600;
        this.hide_gifts = ['spasibodoc'];
        this.cardIsOpen = false;
    }
    GiftsComponent.prototype.onScroll = function (e) {
        var _this = this;
        clearTimeout(this.scrollTimeout);
        this.scrollTimeout = setTimeout(function () {
            _this.currState.scroll = pageYOffset;
            _this.saveToStorage();
        }, 400);
    };
    GiftsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var storageData = this.storage.getCookie(this.storageKey);
        var storage = storageData ? JSON.parse(storageData) : null;
        this.currState = storage || this.currState;
        this.showSearch = this.currState.name === 'search';
        Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["combineLatest"])([
            this.api.getGifts().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (gifts) {
                // const x = new Array(40).fill(gifts[3]);
                // this.gifts = gifts.concat(x);
                _this.gifts = gifts.filter(function (gift) { return !_this.hide_gifts.find(function (url) { return gift.user_friendly_url === url; }); });
                _this.initPages();
                _this.giftsFetched.emit(gifts);
                return gifts;
            })),
        ]).subscribe(function (_a) {
            var gifts = _a[0];
            _this.api.getTags().subscribe(function (data) {
                _this.tags = data;
            });
            if (gifts && gifts.length) {
                // this.tags = gifts.reduce((acc, gift) => {
                //   if (gift.Tags && gift.Tags.length) {
                //     acc = acc.concat(
                //       gift.Tags.filter(tag => {
                //         return !acc.some(t => t._uuid === tag._uuid);
                //       })
                //     );
                //   }
                //   return acc;
                // }, [] as ITag[]);
                _this.setState(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, _this.currState));
                setTimeout(function () {
                    if (storage && storage.scroll) {
                        window.scrollTo(0, storage.scroll);
                    }
                    setTimeout(function () {
                        _this.infiniteScrollDisabled = false;
                    }, 1);
                }, 200);
            }
        });
    };
    Object.defineProperty(GiftsComponent.prototype, "storageExpires", {
        get: function () {
            return Object(date_fns__WEBPACK_IMPORTED_MODULE_5__["addMinutes"])(new Date(), 30);
        },
        enumerable: true,
        configurable: true
    });
    GiftsComponent.prototype.getUrl = function (gift) {
        var type = gift.meta.restaurant_gift_type;
        if (type === 'one') {
            return 'certificate';
        }
        else if (type === 'few' || type === 'universal') {
            return 'certificates';
        }
    };
    GiftsComponent.prototype.setState = function (newState, history, back) {
        if (history === void 0) { history = true; }
        if (back === void 0) { back = false; }
        if (history) {
            this.prevState = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.currState);
            this.stateHistory.push(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.currState, newState));
        }
        this.currState = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.currState, newState);
        this.logger.l(['state history', this.stateHistory]);
    };
    GiftsComponent.prototype.getStateFromHistory = function (state, reverse) {
        if (reverse === void 0) { reverse = false; }
        var filtered = this.stateHistory.filter(function (item) {
            return reverse ? item.name !== state.name : item.name === state.name;
        });
        return filtered[filtered.length - 1];
    };
    GiftsComponent.prototype.initPages = function () {
        var _this = this;
        var currPage = 0;
        this.gifts.map(function (item, i) {
            var page = _this.pages[currPage] || { data: [] };
            page.data.push(item);
            _this.pages[currPage] = page;
            if ((i + 1) % _this.pageLength === 0) {
                currPage++;
            }
        });
        this._gifts = this.getPageData(0, this.currState.page);
        this.logger.l(['pages', this.pages]);
    };
    GiftsComponent.prototype.filterByTag = function (data, tag) {
        return data.filter(function (gift) {
            if (gift.Partners && gift.Partners.length > 0) {
                var flag = gift.Partners.find(function (el) { return (el.tags || []).some(function (_uuid) { return _uuid === tag._uuid; }); });
                if (flag) {
                    return gift;
                }
            }
            return (gift.tags || []).some(function (_uuid) { return _uuid === tag._uuid; });
        });
    };
    GiftsComponent.prototype.search = function (data) {
        if (!this.currState.search) {
            return data;
        }
        else {
            var normalizeStr_1 = function (str) {
                return str.replace(/\s/g, '').toLowerCase();
            };
            var search_1 = this.currState.search;
            // const fuse = new Fuse(data, {
            //   // includeScore: true,
            //   minMatchCharLength: 4,
            //   threshold: 0.5,
            //   keys: ['name.ru'],
            // });
            // const result = fuse.search(this.currState.search).map(fItem => fItem.item) as ICertificate[];
            return data.filter(function (item) {
                return normalizeStr_1((item.name_variant || { ru: '' }).ru).match(normalizeStr_1(search_1)) ||
                    normalizeStr_1((item.name || { ru: '' }).ru).match(normalizeStr_1(search_1));
            });
        }
    };
    GiftsComponent.prototype.getPageData = function (page, to) {
        var tag = this.currState.tag;
        var _page = this.currState.page;
        var data = to >= 0 ? this.getGiftsFromToPage(to) : (this.pages[page] || {}).data;
        var isSearch = this.currState.name === 'search';
        if (!data)
            return;
        var allFiltered = tag ? this.filterByTag(this.gifts, tag) : this.gifts;
        var gifts;
        if (isSearch) {
            gifts = this.search(this.pages[page].data);
        }
        else if (tag) {
            gifts = this.filterByTag(data, tag);
        }
        else {
            gifts = data;
        }
        if (gifts.length < this.pageLength && this.pages[page + 1]) {
            while (gifts.length < this.pageLength &&
                gifts.length < allFiltered.length &&
                this.pages[_page]) {
                var _data = this.pages[page + 1].data;
                gifts = gifts.concat(tag && !isSearch ? this.filterByTag(_data, tag) : isSearch ? this.search(_data) : []);
                _page++;
                this.setState({ page: _page }, false);
            }
        }
        return gifts;
    };
    GiftsComponent.prototype.getGiftsFromToPage = function (to, from) {
        if (from === void 0) { from = 0; }
        var gifts = [];
        for (var page = from; page <= to && this.pages[page]; page++) {
            gifts = gifts.concat(this.pages[page].data);
        }
        return gifts;
    };
    GiftsComponent.prototype.onTagClick = function (tag) {
        var _this = this;
        clearTimeout(this.tagTimeout);
        this.tagTimeout = setTimeout(function () {
            _this.setState({ tag: tag, page: 0, name: 'tag' });
            _this._gifts = _this.getPageData(0);
            _this.saveToStorage();
        }, 200);
    };
    GiftsComponent.prototype.onAllClick = function () {
        var _this = this;
        clearTimeout(this.tagTimeout);
        this.tagTimeout = setTimeout(function () {
            _this.setState(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, _this.getStateFromHistory({ name: 'all' }), { tag: null }));
            _this._gifts = _this.getGiftsFromToPage(_this.currState.page);
        }, 200);
        this.saveToStorage();
    };
    GiftsComponent.prototype.onSearchChange = function () {
        var _this = this;
        clearTimeout(this.searchTimeout);
        this.searchTimeout = setTimeout(function () {
            _this.setState({ page: 0 });
            _this._gifts = _this.getPageData(_this.currState.page);
            _this.saveToStorage();
        }, this.searchDebounce);
    };
    GiftsComponent.prototype.toggleCard = function () {
        this.cardIsOpen = !this.cardIsOpen;
    };
    GiftsComponent.prototype.onSearchClick = function () {
        this.showSearch = true;
        this.cardIsOpen = false;
        this.setState({ name: 'search', page: 0 });
        if (this.currState.search) {
            this.setState({ tag: null });
            this._gifts = this.getPageData(this.currState.page);
        }
        // if (!(this.prevState.search === this.currState.search))
    };
    GiftsComponent.prototype.onCloseSearchClick = function () {
        this.showSearch = false;
        this.setState(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.initialState, this.getStateFromHistory({ name: 'search' }, true), { page: 0 }));
        this._gifts = this.getPageData(this.currState.page);
    };
    GiftsComponent.prototype.onInfiniteScroll = function () {
        var _a = this.currState, page = _a.page, tag = _a.tag;
        var pageData = this.getPageData(page + 1);
        if (pageData && pageData.length) {
            this.setState({ page: page + 1 });
            this._gifts = this._gifts.concat(pageData);
        }
    };
    GiftsComponent.prototype.saveToStorage = function () {
        this.storage.setCookie(this.storageKey, JSON.stringify(this.currState), { expires: this.storageExpires });
    };
    GiftsComponent.ctorParameters = function () { return [
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"] },
        { type: _services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
        { type: _services_logger_service__WEBPACK_IMPORTED_MODULE_7__["LoggerService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], GiftsComponent.prototype, "giftsFetched", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], GiftsComponent.prototype, "onScroll", null);
    GiftsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-gifts',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gifts.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/gifts/gifts.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gifts.component.styl */ "./src/app/components/gifts/gifts.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"],
            _services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"],
            _services_logger_service__WEBPACK_IMPORTED_MODULE_7__["LoggerService"]])
    ], GiftsComponent);
    return GiftsComponent;
}());



/***/ }),

/***/ "./src/app/pages/index/index.component.styl":
/*!**************************************************!*\
  !*** ./src/app/pages/index/index.component.styl ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/**\n * Swiper 4.5.1\n * Most modern mobile touch slider and framework with hardware accelerated transitions\n * http://www.idangero.us/swiper/\n *\n * Copyright 2014-2019 Vladimir Kharlampidi\n *\n * Released under the MIT License\n *\n * Released on: September 13, 2019\n */\n.swiper-container{margin-left:auto;margin-right:auto;position:relative;overflow:hidden;list-style:none;padding:0;z-index:1}\n.swiper-container-no-flexbox .swiper-slide{float:left}\n.swiper-container-vertical>.swiper-wrapper{flex-direction:column}\n.swiper-wrapper{position:relative;width:100%;height:100%;z-index:1;display:flex;transition-property:transform;box-sizing:content-box}\n.swiper-container-android .swiper-slide,.swiper-wrapper{transform:translate3d(0,0,0)}\n.swiper-container-multirow>.swiper-wrapper{flex-wrap:wrap}\n.swiper-container-free-mode>.swiper-wrapper{transition-timing-function:ease-out;margin:0 auto}\n.swiper-slide{flex-shrink:0;width:100%;height:100%;position:relative;transition-property:transform}\n.swiper-slide-invisible-blank{visibility:hidden}\n.swiper-container-autoheight,.swiper-container-autoheight .swiper-slide{height:auto}\n.swiper-container-autoheight .swiper-wrapper{align-items:flex-start;transition-property:transform,height}\n.swiper-container-3d{perspective:1200px}\n.swiper-container-3d .swiper-cube-shadow,.swiper-container-3d .swiper-slide,.swiper-container-3d .swiper-slide-shadow-bottom,.swiper-container-3d .swiper-slide-shadow-left,.swiper-container-3d .swiper-slide-shadow-right,.swiper-container-3d .swiper-slide-shadow-top,.swiper-container-3d .swiper-wrapper{transform-style:preserve-3d}\n.swiper-container-3d .swiper-slide-shadow-bottom,.swiper-container-3d .swiper-slide-shadow-left,.swiper-container-3d .swiper-slide-shadow-right,.swiper-container-3d .swiper-slide-shadow-top{position:absolute;left:0;top:0;width:100%;height:100%;pointer-events:none;z-index:10}\n.swiper-container-3d .swiper-slide-shadow-left{background-image:linear-gradient(to left,rgba(0,0,0,.5),rgba(0,0,0,0))}\n.swiper-container-3d .swiper-slide-shadow-right{background-image:linear-gradient(to right,rgba(0,0,0,.5),rgba(0,0,0,0))}\n.swiper-container-3d .swiper-slide-shadow-top{background-image:linear-gradient(to top,rgba(0,0,0,.5),rgba(0,0,0,0))}\n.swiper-container-3d .swiper-slide-shadow-bottom{background-image:linear-gradient(to bottom,rgba(0,0,0,.5),rgba(0,0,0,0))}\n.swiper-container-wp8-horizontal,.swiper-container-wp8-horizontal>.swiper-wrapper{touch-action:pan-y}\n.swiper-container-wp8-vertical,.swiper-container-wp8-vertical>.swiper-wrapper{touch-action:pan-x}\n.swiper-button-next,.swiper-button-prev{position:absolute;top:50%;width:27px;height:44px;margin-top:-22px;z-index:10;cursor:pointer;background-size:27px 44px;background-position:center;background-repeat:no-repeat}\n.swiper-button-next.swiper-button-disabled,.swiper-button-prev.swiper-button-disabled{opacity:.35;cursor:auto;pointer-events:none}\n.swiper-button-prev,.swiper-container-rtl .swiper-button-next{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E\");left:10px;right:auto}\n.swiper-button-next,.swiper-container-rtl .swiper-button-prev{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23007aff'%2F%3E%3C%2Fsvg%3E\");right:10px;left:auto}\n.swiper-button-prev.swiper-button-white,.swiper-container-rtl .swiper-button-next.swiper-button-white{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E\")}\n.swiper-button-next.swiper-button-white,.swiper-container-rtl .swiper-button-prev.swiper-button-white{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23ffffff'%2F%3E%3C%2Fsvg%3E\")}\n.swiper-button-prev.swiper-button-black,.swiper-container-rtl .swiper-button-next.swiper-button-black{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E\")}\n.swiper-button-next.swiper-button-black,.swiper-container-rtl .swiper-button-prev.swiper-button-black{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23000000'%2F%3E%3C%2Fsvg%3E\")}\n.swiper-button-lock{display:none}\n.swiper-pagination{position:absolute;text-align:center;transition:.3s opacity;transform:translate3d(0,0,0);z-index:10}\n.swiper-pagination.swiper-pagination-hidden{opacity:0}\n.swiper-container-horizontal>.swiper-pagination-bullets,.swiper-pagination-custom,.swiper-pagination-fraction{bottom:10px;left:0;width:100%}\n.swiper-pagination-bullets-dynamic{overflow:hidden;font-size:0}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{transform:scale(.33);position:relative}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active{transform:scale(1)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-main{transform:scale(1)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev{transform:scale(.66)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev-prev{transform:scale(.33)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next{transform:scale(.66)}\n.swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next-next{transform:scale(.33)}\n.swiper-pagination-bullet{width:8px;height:8px;display:inline-block;border-radius:100%;background:#000;opacity:.2}\nbutton.swiper-pagination-bullet{border:none;margin:0;padding:0;box-shadow:none;-webkit-appearance:none;-moz-appearance:none;appearance:none}\n.swiper-pagination-clickable .swiper-pagination-bullet{cursor:pointer}\n.swiper-pagination-bullet-active{opacity:1;background:#007aff}\n.swiper-container-vertical>.swiper-pagination-bullets{right:10px;top:50%;transform:translate3d(0,-50%,0)}\n.swiper-container-vertical>.swiper-pagination-bullets .swiper-pagination-bullet{margin:6px 0;display:block}\n.swiper-container-vertical>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic{top:50%;transform:translateY(-50%);width:8px}\n.swiper-container-vertical>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{display:inline-block;transition:.2s top,.2s -webkit-transform;transition:.2s transform,.2s top;transition:.2s transform,.2s top,.2s -webkit-transform}\n.swiper-container-horizontal>.swiper-pagination-bullets .swiper-pagination-bullet{margin:0 4px}\n.swiper-container-horizontal>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic{left:50%;transform:translateX(-50%);white-space:nowrap}\n.swiper-container-horizontal>.swiper-pagination-bullets.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{transition:.2s left,.2s -webkit-transform;transition:.2s transform,.2s left;transition:.2s transform,.2s left,.2s -webkit-transform}\n.swiper-container-horizontal.swiper-container-rtl>.swiper-pagination-bullets-dynamic .swiper-pagination-bullet{transition:.2s right,.2s -webkit-transform;transition:.2s transform,.2s right;transition:.2s transform,.2s right,.2s -webkit-transform}\n.swiper-pagination-progressbar{background:rgba(0,0,0,.25);position:absolute}\n.swiper-pagination-progressbar .swiper-pagination-progressbar-fill{background:#007aff;position:absolute;left:0;top:0;width:100%;height:100%;transform:scale(0);transform-origin:left top}\n.swiper-container-rtl .swiper-pagination-progressbar .swiper-pagination-progressbar-fill{transform-origin:right top}\n.swiper-container-horizontal>.swiper-pagination-progressbar,.swiper-container-vertical>.swiper-pagination-progressbar.swiper-pagination-progressbar-opposite{width:100%;height:4px;left:0;top:0}\n.swiper-container-horizontal>.swiper-pagination-progressbar.swiper-pagination-progressbar-opposite,.swiper-container-vertical>.swiper-pagination-progressbar{width:4px;height:100%;left:0;top:0}\n.swiper-pagination-white .swiper-pagination-bullet-active{background:#fff}\n.swiper-pagination-progressbar.swiper-pagination-white{background:rgba(255,255,255,.25)}\n.swiper-pagination-progressbar.swiper-pagination-white .swiper-pagination-progressbar-fill{background:#fff}\n.swiper-pagination-black .swiper-pagination-bullet-active{background:#000}\n.swiper-pagination-progressbar.swiper-pagination-black{background:rgba(0,0,0,.25)}\n.swiper-pagination-progressbar.swiper-pagination-black .swiper-pagination-progressbar-fill{background:#000}\n.swiper-pagination-lock{display:none}\n.swiper-scrollbar{border-radius:10px;position:relative;-ms-touch-action:none;background:rgba(0,0,0,.1)}\n.swiper-container-horizontal>.swiper-scrollbar{position:absolute;left:1%;bottom:3px;z-index:50;height:5px;width:98%}\n.swiper-container-vertical>.swiper-scrollbar{position:absolute;right:3px;top:1%;z-index:50;width:5px;height:98%}\n.swiper-scrollbar-drag{height:100%;width:100%;position:relative;background:rgba(0,0,0,.5);border-radius:10px;left:0;top:0}\n.swiper-scrollbar-cursor-drag{cursor:move}\n.swiper-scrollbar-lock{display:none}\n.swiper-zoom-container{width:100%;height:100%;display:flex;justify-content:center;align-items:center;text-align:center}\n.swiper-zoom-container>canvas,.swiper-zoom-container>img,.swiper-zoom-container>svg{max-width:100%;max-height:100%;-o-object-fit:contain;object-fit:contain}\n.swiper-slide-zoomed{cursor:move}\n.swiper-lazy-preloader{width:42px;height:42px;position:absolute;left:50%;top:50%;margin-left:-21px;margin-top:-21px;z-index:10;transform-origin:50%;-webkit-animation:swiper-preloader-spin 1s steps(12,end) infinite;animation:swiper-preloader-spin 1s steps(12,end) infinite}\n.swiper-lazy-preloader:after{display:block;content:'';width:100%;height:100%;background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%236c6c6c'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E\");background-position:50%;background-size:100%;background-repeat:no-repeat}\n.swiper-lazy-preloader-white:after{background-image:url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D'0%200%20120%20120'%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%3E%3Cdefs%3E%3Cline%20id%3D'l'%20x1%3D'60'%20x2%3D'60'%20y1%3D'7'%20y2%3D'27'%20stroke%3D'%23fff'%20stroke-width%3D'11'%20stroke-linecap%3D'round'%2F%3E%3C%2Fdefs%3E%3Cg%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(30%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(60%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(90%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(120%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.27'%20transform%3D'rotate(150%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.37'%20transform%3D'rotate(180%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.46'%20transform%3D'rotate(210%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.56'%20transform%3D'rotate(240%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.66'%20transform%3D'rotate(270%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.75'%20transform%3D'rotate(300%2060%2C60)'%2F%3E%3Cuse%20xlink%3Ahref%3D'%23l'%20opacity%3D'.85'%20transform%3D'rotate(330%2060%2C60)'%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E\")}\n@-webkit-keyframes swiper-preloader-spin{100%{transform:rotate(360deg)}}\n@keyframes swiper-preloader-spin{100%{transform:rotate(360deg)}}\n.swiper-container .swiper-notification{position:absolute;left:0;top:0;pointer-events:none;opacity:0;z-index:-1000}\n.swiper-container-fade.swiper-container-free-mode .swiper-slide{transition-timing-function:ease-out}\n.swiper-container-fade .swiper-slide{pointer-events:none;transition-property:opacity}\n.swiper-container-fade .swiper-slide .swiper-slide{pointer-events:none}\n.swiper-container-fade .swiper-slide-active,.swiper-container-fade .swiper-slide-active .swiper-slide-active{pointer-events:auto}\n.swiper-container-cube{overflow:visible}\n.swiper-container-cube .swiper-slide{pointer-events:none;-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:1;visibility:hidden;transform-origin:0 0;width:100%;height:100%}\n.swiper-container-cube .swiper-slide .swiper-slide{pointer-events:none}\n.swiper-container-cube.swiper-container-rtl .swiper-slide{transform-origin:100% 0}\n.swiper-container-cube .swiper-slide-active,.swiper-container-cube .swiper-slide-active .swiper-slide-active{pointer-events:auto}\n.swiper-container-cube .swiper-slide-active,.swiper-container-cube .swiper-slide-next,.swiper-container-cube .swiper-slide-next+.swiper-slide,.swiper-container-cube .swiper-slide-prev{pointer-events:auto;visibility:visible}\n.swiper-container-cube .swiper-slide-shadow-bottom,.swiper-container-cube .swiper-slide-shadow-left,.swiper-container-cube .swiper-slide-shadow-right,.swiper-container-cube .swiper-slide-shadow-top{z-index:0;-webkit-backface-visibility:hidden;backface-visibility:hidden}\n.swiper-container-cube .swiper-cube-shadow{position:absolute;left:0;bottom:0;width:100%;height:100%;background:#000;opacity:.6;-webkit-filter:blur(50px);filter:blur(50px);z-index:0}\n.swiper-container-flip{overflow:visible}\n.swiper-container-flip .swiper-slide{pointer-events:none;-webkit-backface-visibility:hidden;backface-visibility:hidden;z-index:1}\n.swiper-container-flip .swiper-slide .swiper-slide{pointer-events:none}\n.swiper-container-flip .swiper-slide-active,.swiper-container-flip .swiper-slide-active .swiper-slide-active{pointer-events:auto}\n.swiper-container-flip .swiper-slide-shadow-bottom,.swiper-container-flip .swiper-slide-shadow-left,.swiper-container-flip .swiper-slide-shadow-right,.swiper-container-flip .swiper-slide-shadow-top{z-index:0;-webkit-backface-visibility:hidden;backface-visibility:hidden}\n.swiper-container-coverflow .swiper-wrapper{-ms-perspective:1200px}\n.b-activate {\n  position: relative;\n  margin: 0 0 30px;\n  color: #000;\n}\n.b-activate__content {\n  position: relative;\n  z-index: 2;\n  padding: 0 16px;\n  text-align: center;\n  margin-bottom: 15px;\n}\n.b-activate__behind {\n  overflow: hidden;\n}\n.b-activate__slide {\n  min-width: 98px;\n}\n.b-activate__card,\n.b-activate__card-inner {\n  border-radius: 11px;\n}\n.b-activate__card {\n  max-width: 784px;\n  margin: 0 auto;\n  overflow: hidden;\n  box-shadow: 0 2px 44px 0 rgba(0,0,0,0.06);\n  background-image: linear-gradient(to bottom, rgba(245,242,238,0.9), rgba(245,242,238,0.9)), linear-gradient(to bottom, #fff, rgba(255,255,255,0) 47%);\n  background-origin: border-box;\n  background-clip: content-box, border-box;\n}\n.b-activate__card_theme-2 {\n  -webkit-backdrop-filter: blur(20px);\n          backdrop-filter: blur(20px);\n}\n.b-activate__card-inner {\n  min-height: 304px;\n  padding: 30px 16px 50px;\n}\n.b-activate__card-inner_theme-1 {\n  -webkit-backdrop-filter: blur(20px);\n          backdrop-filter: blur(20px);\n}\n.b-activate__card-row {\n  margin-bottom: 45px;\n}\n.b-activate__card-row_mb-1 {\n  margin-bottom: 18px;\n}\n.b-activate__card-row:last-child {\n  margin-bottom: 0;\n}\n.b-activate__card-row_footer {\n  margin: 30px 0 0;\n}\n.b-activate__text {\n  font-size: 16px;\n  line-height: 1.3;\n}\n.b-activate__btn {\n  display: flex;\n  align-items: center;\n  max-width: 400px;\n  margin: 0 auto;\n  padding: 0;\n  font-size: 19px;\n  font-weight: 500;\n  line-height: 1;\n  color: #ff3838;\n}\n.b-activate__btn > svg-icon > svg {\n  height: 16px;\n  width: auto;\n  margin-left: 8px;\n}\n.b-activate__table {\n  padding: 12px;\n  margin: 0 auto;\n  border-radius: 8px;\n  font-weight: 500;\n  text-align: left;\n  background: rgba(255,255,255,0.4);\n}\n.b-activate__table-head {\n  margin-bottom: 20px;\n}\n.b-activate__table-head-row:not(:last-child) {\n  margin-bottom: 8px;\n}\n.b-activate__table-number {\n  font-size: 22px;\n}\n.b-activate__table-name {\n  font-size: 14px;\n}\n.b-activate__table-row {\n  display: flex;\n}\n.b-activate__table-td:not(:last-child) {\n  margin-right: 20px;\n}\n.b-activate__table-td-row:not(:last-child) {\n  margin-bottom: 4px;\n}\n.b-activate__table-td-title {\n  text-transform: uppercase;\n  font-size: 12px;\n  color: rgba(0,0,0,0.8);\n}\n.b-activate__table-td-value {\n  color: #bda776;\n}\n.b-activate__table-footer {\n  margin-top: 30px;\n}\n.b-activate__result-title {\n  font-size: 18px;\n  font-weight: 500;\n}\n.b-activate__result-logo {\n  display: block;\n  margin-top: 24px;\n}\n.b-activate__result-logo > svg {\n  width: 160px;\n  height: auto;\n}\n@media all and (min-width: 768px) {\n  .b-activate {\n    position: relative;\n    margin: 72px 0 100px;\n  }\n  .b-activate__behind {\n    position: absolute;\n    left: 0;\n    right: 0;\n    top: 129px;\n  }\n  .b-activate__slide {\n    min-width: 225px;\n  }\n  .b-activate__card,\n  .b-activate__card-inner {\n    border-radius: 31px;\n  }\n  .b-activate__card-inner {\n    min-height: 572px;\n    padding: 68px 90px 90px;\n  }\n  .b-activate__card-row {\n    margin-bottom: 88px;\n  }\n  .b-activate__card-row_mb-1 {\n    margin-bottom: 30px;\n  }\n  .b-activate__card-row_footer {\n    margin: 108px 0 0;\n  }\n  .b-activate__card-row_input {\n    max-width: 490px;\n    margin-left: auto;\n    margin-right: auto;\n  }\n  .b-activate__text {\n    font-size: 24px;\n  }\n  .b-activate__btn {\n    font-size: 32px;\n  }\n  .b-activate__btn > svg-icon > svg {\n    height: 25px;\n    margin-left: 10px;\n  }\n  .b-activate__table {\n    max-width: 75%;\n    padding: 24px;\n  }\n  .b-activate__table-head {\n    margin-bottom: 40px;\n  }\n  .b-activate__table-head-row:not(:last-child) {\n    margin-bottom: 8px;\n  }\n  .b-activate__table-name {\n    font-size: 16px;\n  }\n  .b-activate__table-number {\n    font-size: 30px;\n  }\n  .b-activate__table-td:not(:last-child) {\n    margin-right: 20px;\n  }\n  .b-activate__table-td-row:not(:last-child) {\n    margin-bottom: 4px;\n  }\n  .b-activate__table-td-title {\n    font-size: 12px;\n  }\n  .b-activate__table-footer {\n    margin-top: 30px;\n  }\n  .b-activate__result-title {\n    font-size: 24px;\n  }\n  .b-activate__result-logo {\n    margin-top: 24px;\n  }\n  .b-activate__result-logo > svg {\n    width: 224px;\n  }\n}\n.card-slider {\n  display: flex;\n  justify-content: center;\n}\n.card-slider__list {\n  display: inline-flex;\n  text-align: left;\n  margin: 0 -6px;\n}\n.card-slider__item {\n  margin: 0 6px;\n}\n.card-slider__card {\n  position: relative;\n  height: 304px;\n  overflow: hidden;\n  border-radius: 6px;\n  box-shadow: 0 4px 12px 0 rgba(1,13,165,0.28);\n  margin: 2px 0 17px;\n  background-repeat: no-repeat;\n  background-position: center 0;\n  background-size: cover;\n}\n.card-slider__card:before {\n  content: '';\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: linear-gradient(144deg, rgba(255,255,255,0.3) 1%, rgba(255,255,255,0) 40%), linear-gradient(to bottom, rgba(92,28,4,0) 34%, #311004 88%);\n}\n/*# sourceMappingURL=src/app/pages/index/index.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9zd2lwZXIvZGlzdC9jc3Mvc3dpcGVyLm1pbi5jc3MiLCJzcmMvYXBwL3BhZ2VzL2luZGV4L3NyYy9zdHlsZXMvYmxvY2tzL2ItYWN0aXZhdGUuc3R5bCIsInNyYy9hcHAvcGFnZXMvaW5kZXgvaW5kZXguY29tcG9uZW50LnN0eWwiLCJzcmMvYXBwL3BhZ2VzL2luZGV4L3NyYy9zdHlsZXMvaGVscGVycy5zdHlsIiwic3JjL2FwcC9wYWdlcy9pbmRleC9zcmMvc3R5bGVzL2Jsb2Nrcy9jYXJkLXNsaWRlci5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7O0VBVUU7QUFDRixrQkFBa0IsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsU0FBUztBQUFDLDJDQUEyQyxVQUFVO0FBQUMsMkNBQTRKLHFCQUFxQjtBQUFDLGdCQUFnQixpQkFBaUIsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBOEQsWUFBWSxDQUFzSCw2QkFBNkIsQ0FBZ0Ysc0JBQXNCO0FBQUMsd0RBQTZGLDRCQUE0QjtBQUFDLDJDQUFxRixjQUFjO0FBQUMsNENBQStILG1DQUFtQyxDQUFDLGFBQWE7QUFBQyxjQUF3RCxhQUFhLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBc0gsNkJBQTZFO0FBQUMsOEJBQThCLGlCQUFpQjtBQUFDLHdFQUF3RSxXQUFXO0FBQUMsNkNBQXlILHNCQUFzQixDQUEySSxvQ0FBMkY7QUFBQyxxQkFBZ0Qsa0JBQWtCO0FBQUMsK1NBQW1WLDJCQUEyQjtBQUFDLDhMQUE4TCxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsVUFBVTtBQUFDLCtDQUF3UyxzRUFBc0U7QUFBQyxnREFBdVMsdUVBQXVFO0FBQUMsOENBQTJTLHFFQUFxRTtBQUFDLGlEQUF3Uyx3RUFBd0U7QUFBQyxrRkFBeUcsa0JBQWtCO0FBQUMsOEVBQXFHLGtCQUFrQjtBQUFDLHdDQUF3QyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLHlCQUF5QixDQUFDLDBCQUEwQixDQUFDLDJCQUEyQjtBQUFDLHNGQUFzRixXQUFXLENBQUMsV0FBVyxDQUFDLG1CQUFtQjtBQUFDLDhEQUE4RCxtUkFBbVIsQ0FBQyxTQUFTLENBQUMsVUFBVTtBQUFDLDhEQUE4RCxtUkFBbVIsQ0FBQyxVQUFVLENBQUMsU0FBUztBQUFDLHNHQUFzRyxtUkFBbVI7QUFBQyxzR0FBc0csbVJBQW1SO0FBQUMsc0dBQXNHLG1SQUFtUjtBQUFDLHNHQUFzRyxtUkFBbVI7QUFBQyxvQkFBb0IsWUFBWTtBQUFDLG1CQUFtQixpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBMEQsc0JBQXNCLENBQXNDLDRCQUE0QixDQUFDLFVBQVU7QUFBQyw0Q0FBNEMsU0FBUztBQUFDLDhHQUE4RyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVU7QUFBQyxtQ0FBbUMsZUFBZSxDQUFDLFdBQVc7QUFBQyw2REFBbUgsb0JBQW9CLENBQUMsaUJBQWlCO0FBQUMsb0VBQXNILGtCQUFrQjtBQUFDLHlFQUEySCxrQkFBa0I7QUFBQyx5RUFBK0gsb0JBQW9CO0FBQUMsOEVBQW9JLG9CQUFvQjtBQUFDLHlFQUErSCxvQkFBb0I7QUFBQyw4RUFBb0ksb0JBQW9CO0FBQUMsMEJBQTBCLFNBQVMsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLFVBQVU7QUFBQyxnQ0FBZ0MsV0FBVyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQXlCLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlO0FBQUMsdURBQXVELGNBQWM7QUFBQyxpQ0FBaUMsU0FBUyxDQUFDLGtCQUFrQjtBQUFDLHNEQUFzRCxVQUFVLENBQUMsT0FBTyxDQUF5QywrQkFBK0I7QUFBQyxnRkFBZ0YsWUFBWSxDQUFDLGFBQWE7QUFBQyx3RkFBd0YsT0FBTyxDQUFtRSwwQkFBMEIsQ0FBQyxTQUFTO0FBQUMsa0hBQWtILG9CQUFvQixDQUFrRCx3Q0FBd0MsQ0FBcUMsZ0NBQWdDLENBQUMsc0RBQXNEO0FBQUMsa0ZBQWtGLFlBQVk7QUFBQywwRkFBMEYsUUFBUSxDQUFtRSwwQkFBMEIsQ0FBQyxrQkFBa0I7QUFBQyxvSEFBc0sseUNBQXlDLENBQXNDLGlDQUFpQyxDQUFDLHVEQUF1RDtBQUFDLCtHQUFrSywwQ0FBMEMsQ0FBdUMsa0NBQWtDLENBQUMsd0RBQXdEO0FBQUMsK0JBQStCLDBCQUEwQixDQUFDLGlCQUFpQjtBQUFDLG1FQUFtRSxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQW1ELGtCQUFrQixDQUFpRSx5QkFBeUI7QUFBQyx5RkFBMkosMEJBQTBCO0FBQUMsNkpBQTZKLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUs7QUFBQyw2SkFBNkosU0FBUyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSztBQUFDLDBEQUEwRCxlQUFlO0FBQUMsdURBQXVELGdDQUFnQztBQUFDLDJGQUEyRixlQUFlO0FBQUMsMERBQTBELGVBQWU7QUFBQyx1REFBdUQsMEJBQTBCO0FBQUMsMkZBQTJGLGVBQWU7QUFBQyx3QkFBd0IsWUFBWTtBQUFDLGtCQUFrQixrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyx5QkFBeUI7QUFBQywrQ0FBK0MsaUJBQWlCLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFNBQVM7QUFBQyw2Q0FBNkMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFVBQVU7QUFBQyx1QkFBdUIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyx5QkFBeUIsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsS0FBSztBQUFDLDhCQUE4QixXQUFXO0FBQUMsdUJBQXVCLFlBQVk7QUFBQyx1QkFBdUIsVUFBVSxDQUFDLFdBQVcsQ0FBOEQsWUFBWSxDQUE2RSxzQkFBc0IsQ0FBMkUsa0JBQWtCLENBQUMsaUJBQWlCO0FBQUMsb0ZBQW9GLGNBQWMsQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsa0JBQWtCO0FBQUMscUJBQXFCLFdBQVc7QUFBQyx1QkFBdUIsVUFBVSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBdUQsb0JBQW9CLENBQUMsaUVBQWlFLENBQUMseURBQXlEO0FBQUMsNkJBQTZCLGFBQWEsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyx3N0NBQXc3QyxDQUFDLHVCQUF1QixDQUFDLG9CQUFvQixDQUFDLDJCQUEyQjtBQUFDLG1DQUFtQyxxN0NBQXE3QztBQUFDLHlDQUF5QyxLQUFzQyx3QkFBd0IsQ0FBQztBQUFDLGlDQUFpQyxLQUFzQyx3QkFBd0IsQ0FBQztBQUFDLHVDQUF1QyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxhQUFhO0FBQUMsZ0VBQW1KLG1DQUFtQztBQUFDLHFDQUFxQyxtQkFBbUIsQ0FBb0UsMkJBQTJCO0FBQUMsbURBQW1ELG1CQUFtQjtBQUFDLDZHQUE2RyxtQkFBbUI7QUFBQyx1QkFBdUIsZ0JBQWdCO0FBQUMscUNBQXFDLG1CQUFtQixDQUFDLGtDQUFrQyxDQUFDLDBCQUEwQixDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBdUQsb0JBQW9CLENBQUMsVUFBVSxDQUFDLFdBQVc7QUFBQyxtREFBbUQsbUJBQW1CO0FBQUMsMERBQXNILHVCQUF1QjtBQUFDLDZHQUE2RyxtQkFBbUI7QUFBQyx3TEFBd0wsbUJBQW1CLENBQUMsa0JBQWtCO0FBQUMsc01BQXNNLFNBQVMsQ0FBQyxrQ0FBa0MsQ0FBQywwQkFBMEI7QUFBQywyQ0FBMkMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsaUJBQWlCLENBQUMsU0FBUztBQUFDLHVCQUF1QixnQkFBZ0I7QUFBQyxxQ0FBcUMsbUJBQW1CLENBQUMsa0NBQWtDLENBQUMsMEJBQTBCLENBQUMsU0FBUztBQUFDLG1EQUFtRCxtQkFBbUI7QUFBQyw2R0FBNkcsbUJBQW1CO0FBQUMsc01BQXNNLFNBQVMsQ0FBQyxrQ0FBa0MsQ0FBQywwQkFBMEI7QUFBQyw0Q0FBNEMsc0JBQXNCO0FDWDVrbUI7RUFDRSxrQkFBVTtFQUNWLGdCQUFRO0VBQ1IsV0FBTztBQ0VUO0FESUU7RUFDRSxrQkFBVTtFQUNWLFVBQVM7RUFDVCxlQUFTO0VBQ1Qsa0JBQVk7RUFDWixtQkFBZTtBQ0ZuQjtBRElFO0VBQ0UsZ0JBQVU7QUNGZDtBRElFO0VBQ0UsZUFBVTtBQ0ZkO0FESUU7O0VBQ0UsbUJBQWU7QUNEbkI7QURFRTtFQUNFLGdCQUFVO0VBQ1YsY0FBUTtFQUNSLGdCQUFVO0VBQ1YseUNBQVk7RUFLWixxSkFBZ0c7RUFDaEcsNkJBQW1CO0VBQ25CLHdDQUE0QjtBQ0poQztBRE1JO0VBQ0UsbUNBQTBCO1VBQTFCLDJCQUEwQjtBQ0poQztBRE1JO0VBQ0UsaUJBQVc7RUFDWCx1QkFBUztBQ0pmO0FES007RUFDRSxtQ0FBMEI7VUFBMUIsMkJBQTBCO0FDSGxDO0FES0k7RUFDRSxtQkFBZTtBQ0hyQjtBRElNO0VBQ0UsbUJBQWU7QUNGdkI7QURHTTtFQUNFLGdCQUFlO0FDRHZCO0FERU07RUFDRSxnQkFBUTtBQ0FoQjtBREVFO0VBQ0UsZUFBVztFQUNYLGdCQUFhO0FDQWpCO0FERUU7RUFDRSxhQUFTO0VBQ1QsbUJBQVk7RUFDWixnQkFBVTtFQUNWLGNBQVE7RUFDUixVQUFTO0VBQ1QsZUFBVztFQUNYLGdCQUFhO0VBQ2IsY0FBYTtFQUNiLGNBQU87QUNBWDtBREdNO0VBQ0UsWUFBUTtFQUNSLFdBQU87RUFDUCxnQkFBYTtBQ0RyQjtBREdFO0VBQ0UsYUFBUztFQUNULGNBQVE7RUFDUixrQkFBZTtFQUNmLGdCQUFhO0VBQ2IsZ0JBQVk7RUFDWixpQ0FBK0I7QUNEbkM7QURHSTtFQUNFLG1CQUFlO0FDRHJCO0FERU07RUFDRSxrQkFBZTtBQ0F2QjtBRENJO0VBQ0UsZUFBVztBQ0NqQjtBREFJO0VBQ0UsZUFBVztBQ0VqQjtBRERJO0VBQ0UsYUFBUztBQ0dmO0FERE07RUFDRSxrQkFBYztBQ0d0QjtBREZNO0VBQ0Usa0JBQWU7QUNJdkI7QURITTtFQUNFLHlCQUFnQjtFQUNoQixlQUFXO0VBQ1gsc0JBQW9CO0FDSzVCO0FESk07RUFDRSxjQUFPO0FDTWY7QURMSTtFQUNFLGdCQUFZO0FDT2xCO0FESkk7RUFDRSxlQUFXO0VBQ1gsZ0JBQWE7QUNNbkI7QURMSTtFQUNFLGNBQVM7RUFDVCxnQkFBWTtBQ09sQjtBRE5NO0VBQ0UsWUFBTztFQUNQLFlBQVE7QUNRaEI7QUM3R3NDO0VGd0dwQztJQUNFLGtCQUFVO0lBQ1Ysb0JBQVE7RUNRVjtFRFBFO0lBQ0Usa0JBQVU7SUFDVixPQUFNO0lBQ04sUUFBTztJQUNQLFVBQUs7RUNTVDtFRFJFO0lBQ0UsZ0JBQVU7RUNVZDtFRFRFOztJQUNFLG1CQUFlO0VDWW5CO0VEVkk7SUFDRSxpQkFBVztJQUNYLHVCQUFTO0VDWWY7RURYSTtJQUNFLG1CQUFlO0VDYXJCO0VEWk07SUFDRSxtQkFBZTtFQ2N2QjtFRGJNO0lBQ0UsaUJBQVE7RUNlaEI7RURkTTtJQUNFLGdCQUFVO0lBQ1YsaUJBQWE7SUFDYixrQkFBYztFQ2dCdEI7RURmRTtJQUNFLGVBQVc7RUNpQmY7RURmRTtJQUNFLGVBQVc7RUNpQmY7RURkTTtJQUNFLFlBQVE7SUFDUixpQkFBYTtFQ2dCckI7RURkRTtJQUNFLGNBQVc7SUFDWCxhQUFTO0VDZ0JiO0VEZEk7SUFDRSxtQkFBZTtFQ2dCckI7RURmTTtJQUNFLGtCQUFlO0VDaUJ2QjtFRGhCSTtJQUNFLGVBQVc7RUNrQmpCO0VEakJJO0lBQ0UsZUFBVztFQ21CakI7RURqQk07SUFDRSxrQkFBYztFQ21CdEI7RURsQk07SUFDRSxrQkFBZTtFQ29CdkI7RURuQk07SUFDRSxlQUFXO0VDcUJuQjtFRHBCSTtJQUNFLGdCQUFZO0VDc0JsQjtFRG5CSTtJQUNFLGVBQVc7RUNxQmpCO0VEcEJJO0lBQ0UsZ0JBQVk7RUNzQmxCO0VEckJNO0lBQ0UsWUFBTztFQ3VCZjtBQUNGO0FFak5BO0VBQ0UsYUFBUztFQUNULHVCQUFnQjtBRm1ObEI7QUVqTkU7RUFDRSxvQkFBUztFQUNULGdCQUFZO0VBQ1osY0FBUTtBRm1OWjtBRWpORTtFQUNFLGFBQVE7QUZtTlo7QUVqTkU7RUFDRSxrQkFBVTtFQUVWLGFBQVE7RUFDUixnQkFBVTtFQUNWLGtCQUFlO0VBQ2YsNENBQVc7RUFDWCxrQkFBUTtFQUNSLDRCQUFtQjtFQUNuQiw2QkFBcUI7RUFDckIsc0JBQWlCO0FGa05yQjtBRWhOSTtFQUNFLFdBQVE7RUFDUixrQkFBVTtFQUNWLE1BQUs7RUFDTCxPQUFNO0VBQ04sV0FBTztFQUNQLFlBQVE7RUFDUixvSkFBWTtBRmtObEI7QUFDQSxrRUFBa0UiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9pbmRleC9pbmRleC5jb21wb25lbnQuc3R5bCIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogU3dpcGVyIDQuNS4xXG4gKiBNb3N0IG1vZGVybiBtb2JpbGUgdG91Y2ggc2xpZGVyIGFuZCBmcmFtZXdvcmsgd2l0aCBoYXJkd2FyZSBhY2NlbGVyYXRlZCB0cmFuc2l0aW9uc1xuICogaHR0cDovL3d3dy5pZGFuZ2Vyby51cy9zd2lwZXIvXG4gKlxuICogQ29weXJpZ2h0IDIwMTQtMjAxOSBWbGFkaW1pciBLaGFybGFtcGlkaVxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgTGljZW5zZVxuICpcbiAqIFJlbGVhc2VkIG9uOiBTZXB0ZW1iZXIgMTMsIDIwMTlcbiAqL1xuLnN3aXBlci1jb250YWluZXJ7bWFyZ2luLWxlZnQ6YXV0bzttYXJnaW4tcmlnaHQ6YXV0bztwb3NpdGlvbjpyZWxhdGl2ZTtvdmVyZmxvdzpoaWRkZW47bGlzdC1zdHlsZTpub25lO3BhZGRpbmc6MDt6LWluZGV4OjF9LnN3aXBlci1jb250YWluZXItbm8tZmxleGJveCAuc3dpcGVyLXNsaWRle2Zsb2F0OmxlZnR9LnN3aXBlci1jb250YWluZXItdmVydGljYWw+LnN3aXBlci13cmFwcGVyey13ZWJraXQtYm94LW9yaWVudDp2ZXJ0aWNhbDstd2Via2l0LWJveC1kaXJlY3Rpb246bm9ybWFsOy13ZWJraXQtZmxleC1kaXJlY3Rpb246Y29sdW1uOy1tcy1mbGV4LWRpcmVjdGlvbjpjb2x1bW47ZmxleC1kaXJlY3Rpb246Y29sdW1ufS5zd2lwZXItd3JhcHBlcntwb3NpdGlvbjpyZWxhdGl2ZTt3aWR0aDoxMDAlO2hlaWdodDoxMDAlO3otaW5kZXg6MTtkaXNwbGF5Oi13ZWJraXQtYm94O2Rpc3BsYXk6LXdlYmtpdC1mbGV4O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4Oy13ZWJraXQtdHJhbnNpdGlvbi1wcm9wZXJ0eTotd2Via2l0LXRyYW5zZm9ybTt0cmFuc2l0aW9uLXByb3BlcnR5Oi13ZWJraXQtdHJhbnNmb3JtOy1vLXRyYW5zaXRpb24tcHJvcGVydHk6dHJhbnNmb3JtO3RyYW5zaXRpb24tcHJvcGVydHk6dHJhbnNmb3JtO3RyYW5zaXRpb24tcHJvcGVydHk6dHJhbnNmb3JtLC13ZWJraXQtdHJhbnNmb3JtOy13ZWJraXQtYm94LXNpemluZzpjb250ZW50LWJveDtib3gtc2l6aW5nOmNvbnRlbnQtYm94fS5zd2lwZXItY29udGFpbmVyLWFuZHJvaWQgLnN3aXBlci1zbGlkZSwuc3dpcGVyLXdyYXBwZXJ7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlM2QoMCwwLDApO3RyYW5zZm9ybTp0cmFuc2xhdGUzZCgwLDAsMCl9LnN3aXBlci1jb250YWluZXItbXVsdGlyb3c+LnN3aXBlci13cmFwcGVyey13ZWJraXQtZmxleC13cmFwOndyYXA7LW1zLWZsZXgtd3JhcDp3cmFwO2ZsZXgtd3JhcDp3cmFwfS5zd2lwZXItY29udGFpbmVyLWZyZWUtbW9kZT4uc3dpcGVyLXdyYXBwZXJ7LXdlYmtpdC10cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbjplYXNlLW91dDstby10cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbjplYXNlLW91dDt0cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbjplYXNlLW91dDttYXJnaW46MCBhdXRvfS5zd2lwZXItc2xpZGV7LXdlYmtpdC1mbGV4LXNocmluazowOy1tcy1mbGV4LW5lZ2F0aXZlOjA7ZmxleC1zaHJpbms6MDt3aWR0aDoxMDAlO2hlaWdodDoxMDAlO3Bvc2l0aW9uOnJlbGF0aXZlOy13ZWJraXQtdHJhbnNpdGlvbi1wcm9wZXJ0eTotd2Via2l0LXRyYW5zZm9ybTt0cmFuc2l0aW9uLXByb3BlcnR5Oi13ZWJraXQtdHJhbnNmb3JtOy1vLXRyYW5zaXRpb24tcHJvcGVydHk6dHJhbnNmb3JtO3RyYW5zaXRpb24tcHJvcGVydHk6dHJhbnNmb3JtO3RyYW5zaXRpb24tcHJvcGVydHk6dHJhbnNmb3JtLC13ZWJraXQtdHJhbnNmb3JtfS5zd2lwZXItc2xpZGUtaW52aXNpYmxlLWJsYW5re3Zpc2liaWxpdHk6aGlkZGVufS5zd2lwZXItY29udGFpbmVyLWF1dG9oZWlnaHQsLnN3aXBlci1jb250YWluZXItYXV0b2hlaWdodCAuc3dpcGVyLXNsaWRle2hlaWdodDphdXRvfS5zd2lwZXItY29udGFpbmVyLWF1dG9oZWlnaHQgLnN3aXBlci13cmFwcGVyey13ZWJraXQtYm94LWFsaWduOnN0YXJ0Oy13ZWJraXQtYWxpZ24taXRlbXM6ZmxleC1zdGFydDstbXMtZmxleC1hbGlnbjpzdGFydDthbGlnbi1pdGVtczpmbGV4LXN0YXJ0Oy13ZWJraXQtdHJhbnNpdGlvbi1wcm9wZXJ0eTpoZWlnaHQsLXdlYmtpdC10cmFuc2Zvcm07dHJhbnNpdGlvbi1wcm9wZXJ0eTpoZWlnaHQsLXdlYmtpdC10cmFuc2Zvcm07LW8tdHJhbnNpdGlvbi1wcm9wZXJ0eTp0cmFuc2Zvcm0saGVpZ2h0O3RyYW5zaXRpb24tcHJvcGVydHk6dHJhbnNmb3JtLGhlaWdodDt0cmFuc2l0aW9uLXByb3BlcnR5OnRyYW5zZm9ybSxoZWlnaHQsLXdlYmtpdC10cmFuc2Zvcm19LnN3aXBlci1jb250YWluZXItM2R7LXdlYmtpdC1wZXJzcGVjdGl2ZToxMjAwcHg7cGVyc3BlY3RpdmU6MTIwMHB4fS5zd2lwZXItY29udGFpbmVyLTNkIC5zd2lwZXItY3ViZS1zaGFkb3csLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZSwuc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLXNsaWRlLXNoYWRvdy1ib3R0b20sLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctbGVmdCwuc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLXNsaWRlLXNoYWRvdy1yaWdodCwuc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLXNsaWRlLXNoYWRvdy10b3AsLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci13cmFwcGVyey13ZWJraXQtdHJhbnNmb3JtLXN0eWxlOnByZXNlcnZlLTNkO3RyYW5zZm9ybS1zdHlsZTpwcmVzZXJ2ZS0zZH0uc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLXNsaWRlLXNoYWRvdy1ib3R0b20sLnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctbGVmdCwuc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLXNsaWRlLXNoYWRvdy1yaWdodCwuc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLXNsaWRlLXNoYWRvdy10b3B7cG9zaXRpb246YWJzb2x1dGU7bGVmdDowO3RvcDowO3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7cG9pbnRlci1ldmVudHM6bm9uZTt6LWluZGV4OjEwfS5zd2lwZXItY29udGFpbmVyLTNkIC5zd2lwZXItc2xpZGUtc2hhZG93LWxlZnR7YmFja2dyb3VuZC1pbWFnZTotd2Via2l0LWdyYWRpZW50KGxpbmVhcixyaWdodCB0b3AsbGVmdCB0b3AsZnJvbShyZ2JhKDAsMCwwLC41KSksdG8ocmdiYSgwLDAsMCwwKSkpO2JhY2tncm91bmQtaW1hZ2U6LXdlYmtpdC1saW5lYXItZ3JhZGllbnQocmlnaHQscmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSk7YmFja2dyb3VuZC1pbWFnZTotby1saW5lYXItZ3JhZGllbnQocmlnaHQscmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSk7YmFja2dyb3VuZC1pbWFnZTpsaW5lYXItZ3JhZGllbnQodG8gbGVmdCxyZ2JhKDAsMCwwLC41KSxyZ2JhKDAsMCwwLDApKX0uc3dpcGVyLWNvbnRhaW5lci0zZCAuc3dpcGVyLXNsaWRlLXNoYWRvdy1yaWdodHtiYWNrZ3JvdW5kLWltYWdlOi13ZWJraXQtZ3JhZGllbnQobGluZWFyLGxlZnQgdG9wLHJpZ2h0IHRvcCxmcm9tKHJnYmEoMCwwLDAsLjUpKSx0byhyZ2JhKDAsMCwwLDApKSk7YmFja2dyb3VuZC1pbWFnZTotd2Via2l0LWxpbmVhci1ncmFkaWVudChsZWZ0LHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpO2JhY2tncm91bmQtaW1hZ2U6LW8tbGluZWFyLWdyYWRpZW50KGxlZnQscmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSk7YmFja2dyb3VuZC1pbWFnZTpsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQscmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSl9LnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctdG9we2JhY2tncm91bmQtaW1hZ2U6LXdlYmtpdC1ncmFkaWVudChsaW5lYXIsbGVmdCBib3R0b20sbGVmdCB0b3AsZnJvbShyZ2JhKDAsMCwwLC41KSksdG8ocmdiYSgwLDAsMCwwKSkpO2JhY2tncm91bmQtaW1hZ2U6LXdlYmtpdC1saW5lYXItZ3JhZGllbnQoYm90dG9tLHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpO2JhY2tncm91bmQtaW1hZ2U6LW8tbGluZWFyLWdyYWRpZW50KGJvdHRvbSxyZ2JhKDAsMCwwLC41KSxyZ2JhKDAsMCwwLDApKTtiYWNrZ3JvdW5kLWltYWdlOmxpbmVhci1ncmFkaWVudCh0byB0b3AscmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSl9LnN3aXBlci1jb250YWluZXItM2QgLnN3aXBlci1zbGlkZS1zaGFkb3ctYm90dG9te2JhY2tncm91bmQtaW1hZ2U6LXdlYmtpdC1ncmFkaWVudChsaW5lYXIsbGVmdCB0b3AsbGVmdCBib3R0b20sZnJvbShyZ2JhKDAsMCwwLC41KSksdG8ocmdiYSgwLDAsMCwwKSkpO2JhY2tncm91bmQtaW1hZ2U6LXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLHJnYmEoMCwwLDAsLjUpLHJnYmEoMCwwLDAsMCkpO2JhY2tncm91bmQtaW1hZ2U6LW8tbGluZWFyLWdyYWRpZW50KHRvcCxyZ2JhKDAsMCwwLC41KSxyZ2JhKDAsMCwwLDApKTtiYWNrZ3JvdW5kLWltYWdlOmxpbmVhci1ncmFkaWVudCh0byBib3R0b20scmdiYSgwLDAsMCwuNSkscmdiYSgwLDAsMCwwKSl9LnN3aXBlci1jb250YWluZXItd3A4LWhvcml6b250YWwsLnN3aXBlci1jb250YWluZXItd3A4LWhvcml6b250YWw+LnN3aXBlci13cmFwcGVyey1tcy10b3VjaC1hY3Rpb246cGFuLXk7dG91Y2gtYWN0aW9uOnBhbi15fS5zd2lwZXItY29udGFpbmVyLXdwOC12ZXJ0aWNhbCwuc3dpcGVyLWNvbnRhaW5lci13cDgtdmVydGljYWw+LnN3aXBlci13cmFwcGVyey1tcy10b3VjaC1hY3Rpb246cGFuLXg7dG91Y2gtYWN0aW9uOnBhbi14fS5zd2lwZXItYnV0dG9uLW5leHQsLnN3aXBlci1idXR0b24tcHJldntwb3NpdGlvbjphYnNvbHV0ZTt0b3A6NTAlO3dpZHRoOjI3cHg7aGVpZ2h0OjQ0cHg7bWFyZ2luLXRvcDotMjJweDt6LWluZGV4OjEwO2N1cnNvcjpwb2ludGVyO2JhY2tncm91bmQtc2l6ZToyN3B4IDQ0cHg7YmFja2dyb3VuZC1wb3NpdGlvbjpjZW50ZXI7YmFja2dyb3VuZC1yZXBlYXQ6bm8tcmVwZWF0fS5zd2lwZXItYnV0dG9uLW5leHQuc3dpcGVyLWJ1dHRvbi1kaXNhYmxlZCwuc3dpcGVyLWJ1dHRvbi1wcmV2LnN3aXBlci1idXR0b24tZGlzYWJsZWR7b3BhY2l0eTouMzU7Y3Vyc29yOmF1dG87cG9pbnRlci1ldmVudHM6bm9uZX0uc3dpcGVyLWJ1dHRvbi1wcmV2LC5zd2lwZXItY29udGFpbmVyLXJ0bCAuc3dpcGVyLWJ1dHRvbi1uZXh0e2JhY2tncm91bmQtaW1hZ2U6dXJsKFwiZGF0YTppbWFnZS9zdmcreG1sO2NoYXJzZXQ9dXRmLTgsJTNDc3ZnJTIweG1sbnMlM0QnaHR0cCUzQSUyRiUyRnd3dy53My5vcmclMkYyMDAwJTJGc3ZnJyUyMHZpZXdCb3glM0QnMCUyMDAlMjAyNyUyMDQ0JyUzRSUzQ3BhdGglMjBkJTNEJ00wJTJDMjJMMjIlMkMwbDIuMSUyQzIuMUw0LjIlMkMyMmwxOS45JTJDMTkuOUwyMiUyQzQ0TDAlMkMyMkwwJTJDMjJMMCUyQzIyeiclMjBmaWxsJTNEJyUyMzAwN2FmZiclMkYlM0UlM0MlMkZzdmclM0VcIik7bGVmdDoxMHB4O3JpZ2h0OmF1dG99LnN3aXBlci1idXR0b24tbmV4dCwuc3dpcGVyLWNvbnRhaW5lci1ydGwgLnN3aXBlci1idXR0b24tcHJldntiYWNrZ3JvdW5kLWltYWdlOnVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbDtjaGFyc2V0PXV0Zi04LCUzQ3N2ZyUyMHhtbG5zJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMjAwMCUyRnN2ZyclMjB2aWV3Qm94JTNEJzAlMjAwJTIwMjclMjA0NCclM0UlM0NwYXRoJTIwZCUzRCdNMjclMkMyMkwyNyUyQzIyTDUlMkM0NGwtMi4xLTIuMUwyMi44JTJDMjJMMi45JTJDMi4xTDUlMkMwTDI3JTJDMjJMMjclMkMyMnonJTIwZmlsbCUzRCclMjMwMDdhZmYnJTJGJTNFJTNDJTJGc3ZnJTNFXCIpO3JpZ2h0OjEwcHg7bGVmdDphdXRvfS5zd2lwZXItYnV0dG9uLXByZXYuc3dpcGVyLWJ1dHRvbi13aGl0ZSwuc3dpcGVyLWNvbnRhaW5lci1ydGwgLnN3aXBlci1idXR0b24tbmV4dC5zd2lwZXItYnV0dG9uLXdoaXRle2JhY2tncm91bmQtaW1hZ2U6dXJsKFwiZGF0YTppbWFnZS9zdmcreG1sO2NoYXJzZXQ9dXRmLTgsJTNDc3ZnJTIweG1sbnMlM0QnaHR0cCUzQSUyRiUyRnd3dy53My5vcmclMkYyMDAwJTJGc3ZnJyUyMHZpZXdCb3glM0QnMCUyMDAlMjAyNyUyMDQ0JyUzRSUzQ3BhdGglMjBkJTNEJ00wJTJDMjJMMjIlMkMwbDIuMSUyQzIuMUw0LjIlMkMyMmwxOS45JTJDMTkuOUwyMiUyQzQ0TDAlMkMyMkwwJTJDMjJMMCUyQzIyeiclMjBmaWxsJTNEJyUyM2ZmZmZmZiclMkYlM0UlM0MlMkZzdmclM0VcIil9LnN3aXBlci1idXR0b24tbmV4dC5zd2lwZXItYnV0dG9uLXdoaXRlLC5zd2lwZXItY29udGFpbmVyLXJ0bCAuc3dpcGVyLWJ1dHRvbi1wcmV2LnN3aXBlci1idXR0b24td2hpdGV7YmFja2dyb3VuZC1pbWFnZTp1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWw7Y2hhcnNldD11dGYtOCwlM0NzdmclMjB4bWxucyUzRCdodHRwJTNBJTJGJTJGd3d3LnczLm9yZyUyRjIwMDAlMkZzdmcnJTIwdmlld0JveCUzRCcwJTIwMCUyMDI3JTIwNDQnJTNFJTNDcGF0aCUyMGQlM0QnTTI3JTJDMjJMMjclMkMyMkw1JTJDNDRsLTIuMS0yLjFMMjIuOCUyQzIyTDIuOSUyQzIuMUw1JTJDMEwyNyUyQzIyTDI3JTJDMjJ6JyUyMGZpbGwlM0QnJTIzZmZmZmZmJyUyRiUzRSUzQyUyRnN2ZyUzRVwiKX0uc3dpcGVyLWJ1dHRvbi1wcmV2LnN3aXBlci1idXR0b24tYmxhY2ssLnN3aXBlci1jb250YWluZXItcnRsIC5zd2lwZXItYnV0dG9uLW5leHQuc3dpcGVyLWJ1dHRvbi1ibGFja3tiYWNrZ3JvdW5kLWltYWdlOnVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbDtjaGFyc2V0PXV0Zi04LCUzQ3N2ZyUyMHhtbG5zJTNEJ2h0dHAlM0ElMkYlMkZ3d3cudzMub3JnJTJGMjAwMCUyRnN2ZyclMjB2aWV3Qm94JTNEJzAlMjAwJTIwMjclMjA0NCclM0UlM0NwYXRoJTIwZCUzRCdNMCUyQzIyTDIyJTJDMGwyLjElMkMyLjFMNC4yJTJDMjJsMTkuOSUyQzE5LjlMMjIlMkM0NEwwJTJDMjJMMCUyQzIyTDAlMkMyMnonJTIwZmlsbCUzRCclMjMwMDAwMDAnJTJGJTNFJTNDJTJGc3ZnJTNFXCIpfS5zd2lwZXItYnV0dG9uLW5leHQuc3dpcGVyLWJ1dHRvbi1ibGFjaywuc3dpcGVyLWNvbnRhaW5lci1ydGwgLnN3aXBlci1idXR0b24tcHJldi5zd2lwZXItYnV0dG9uLWJsYWNre2JhY2tncm91bmQtaW1hZ2U6dXJsKFwiZGF0YTppbWFnZS9zdmcreG1sO2NoYXJzZXQ9dXRmLTgsJTNDc3ZnJTIweG1sbnMlM0QnaHR0cCUzQSUyRiUyRnd3dy53My5vcmclMkYyMDAwJTJGc3ZnJyUyMHZpZXdCb3glM0QnMCUyMDAlMjAyNyUyMDQ0JyUzRSUzQ3BhdGglMjBkJTNEJ00yNyUyQzIyTDI3JTJDMjJMNSUyQzQ0bC0yLjEtMi4xTDIyLjglMkMyMkwyLjklMkMyLjFMNSUyQzBMMjclMkMyMkwyNyUyQzIyeiclMjBmaWxsJTNEJyUyMzAwMDAwMCclMkYlM0UlM0MlMkZzdmclM0VcIil9LnN3aXBlci1idXR0b24tbG9ja3tkaXNwbGF5Om5vbmV9LnN3aXBlci1wYWdpbmF0aW9ue3Bvc2l0aW9uOmFic29sdXRlO3RleHQtYWxpZ246Y2VudGVyOy13ZWJraXQtdHJhbnNpdGlvbjouM3Mgb3BhY2l0eTstby10cmFuc2l0aW9uOi4zcyBvcGFjaXR5O3RyYW5zaXRpb246LjNzIG9wYWNpdHk7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlM2QoMCwwLDApO3RyYW5zZm9ybTp0cmFuc2xhdGUzZCgwLDAsMCk7ei1pbmRleDoxMH0uc3dpcGVyLXBhZ2luYXRpb24uc3dpcGVyLXBhZ2luYXRpb24taGlkZGVue29wYWNpdHk6MH0uc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLC5zd2lwZXItcGFnaW5hdGlvbi1jdXN0b20sLnN3aXBlci1wYWdpbmF0aW9uLWZyYWN0aW9ue2JvdHRvbToxMHB4O2xlZnQ6MDt3aWR0aDoxMDAlfS5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWN7b3ZlcmZsb3c6aGlkZGVuO2ZvbnQtc2l6ZTowfS5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSguMzMpOy1tcy10cmFuc2Zvcm06c2NhbGUoLjMzKTt0cmFuc2Zvcm06c2NhbGUoLjMzKTtwb3NpdGlvbjpyZWxhdGl2ZX0uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy1keW5hbWljIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZley13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKDEpOy1tcy10cmFuc2Zvcm06c2NhbGUoMSk7dHJhbnNmb3JtOnNjYWxlKDEpfS5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldC1hY3RpdmUtbWFpbnstd2Via2l0LXRyYW5zZm9ybTpzY2FsZSgxKTstbXMtdHJhbnNmb3JtOnNjYWxlKDEpO3RyYW5zZm9ybTpzY2FsZSgxKX0uc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy1keW5hbWljIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZlLXByZXZ7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoLjY2KTstbXMtdHJhbnNmb3JtOnNjYWxlKC42Nik7dHJhbnNmb3JtOnNjYWxlKC42Nil9LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pYyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZS1wcmV2LXByZXZ7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoLjMzKTstbXMtdHJhbnNmb3JtOnNjYWxlKC4zMyk7dHJhbnNmb3JtOnNjYWxlKC4zMyl9LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pYyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZS1uZXh0ey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKC42Nik7LW1zLXRyYW5zZm9ybTpzY2FsZSguNjYpO3RyYW5zZm9ybTpzY2FsZSguNjYpfS5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLWR5bmFtaWMgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldC1hY3RpdmUtbmV4dC1uZXh0ey13ZWJraXQtdHJhbnNmb3JtOnNjYWxlKC4zMyk7LW1zLXRyYW5zZm9ybTpzY2FsZSguMzMpO3RyYW5zZm9ybTpzY2FsZSguMzMpfS5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXR7d2lkdGg6OHB4O2hlaWdodDo4cHg7ZGlzcGxheTppbmxpbmUtYmxvY2s7Ym9yZGVyLXJhZGl1czoxMDAlO2JhY2tncm91bmQ6IzAwMDtvcGFjaXR5Oi4yfWJ1dHRvbi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXR7Ym9yZGVyOm5vbmU7bWFyZ2luOjA7cGFkZGluZzowOy13ZWJraXQtYm94LXNoYWRvdzpub25lO2JveC1zaGFkb3c6bm9uZTstd2Via2l0LWFwcGVhcmFuY2U6bm9uZTstbW96LWFwcGVhcmFuY2U6bm9uZTthcHBlYXJhbmNlOm5vbmV9LnN3aXBlci1wYWdpbmF0aW9uLWNsaWNrYWJsZSAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0e2N1cnNvcjpwb2ludGVyfS5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZle29wYWNpdHk6MTtiYWNrZ3JvdW5kOiMwMDdhZmZ9LnN3aXBlci1jb250YWluZXItdmVydGljYWw+LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHN7cmlnaHQ6MTBweDt0b3A6NTAlOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZTNkKDAsLTUwJSwwKTt0cmFuc2Zvcm06dHJhbnNsYXRlM2QoMCwtNTAlLDApfS5zd2lwZXItY29udGFpbmVyLXZlcnRpY2FsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXR7bWFyZ2luOjZweCAwO2Rpc3BsYXk6YmxvY2t9LnN3aXBlci1jb250YWluZXItdmVydGljYWw+LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy1keW5hbWlje3RvcDo1MCU7LXdlYmtpdC10cmFuc2Zvcm06dHJhbnNsYXRlWSgtNTAlKTstbXMtdHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSk7dHJhbnNmb3JtOnRyYW5zbGF0ZVkoLTUwJSk7d2lkdGg6OHB4fS5zd2lwZXItY29udGFpbmVyLXZlcnRpY2FsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pYyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0e2Rpc3BsYXk6aW5saW5lLWJsb2NrOy13ZWJraXQtdHJhbnNpdGlvbjouMnMgdG9wLC4ycyAtd2Via2l0LXRyYW5zZm9ybTt0cmFuc2l0aW9uOi4ycyB0b3AsLjJzIC13ZWJraXQtdHJhbnNmb3JtOy1vLXRyYW5zaXRpb246LjJzIHRyYW5zZm9ybSwuMnMgdG9wO3RyYW5zaXRpb246LjJzIHRyYW5zZm9ybSwuMnMgdG9wO3RyYW5zaXRpb246LjJzIHRyYW5zZm9ybSwuMnMgdG9wLC4ycyAtd2Via2l0LXRyYW5zZm9ybX0uc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXR7bWFyZ2luOjAgNHB4fS5zd2lwZXItY29udGFpbmVyLWhvcml6b250YWw+LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0cy1keW5hbWlje2xlZnQ6NTAlOy13ZWJraXQtdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTUwJSk7LW1zLXRyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpO3RyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpO3doaXRlLXNwYWNlOm5vd3JhcH0uc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsPi5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXRzLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pYyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0ey13ZWJraXQtdHJhbnNpdGlvbjouMnMgbGVmdCwuMnMgLXdlYmtpdC10cmFuc2Zvcm07dHJhbnNpdGlvbjouMnMgbGVmdCwuMnMgLXdlYmtpdC10cmFuc2Zvcm07LW8tdHJhbnNpdGlvbjouMnMgdHJhbnNmb3JtLC4ycyBsZWZ0O3RyYW5zaXRpb246LjJzIHRyYW5zZm9ybSwuMnMgbGVmdDt0cmFuc2l0aW9uOi4ycyB0cmFuc2Zvcm0sLjJzIGxlZnQsLjJzIC13ZWJraXQtdHJhbnNmb3JtfS5zd2lwZXItY29udGFpbmVyLWhvcml6b250YWwuc3dpcGVyLWNvbnRhaW5lci1ydGw+LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMtZHluYW1pYyAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0ey13ZWJraXQtdHJhbnNpdGlvbjouMnMgcmlnaHQsLjJzIC13ZWJraXQtdHJhbnNmb3JtO3RyYW5zaXRpb246LjJzIHJpZ2h0LC4ycyAtd2Via2l0LXRyYW5zZm9ybTstby10cmFuc2l0aW9uOi4ycyB0cmFuc2Zvcm0sLjJzIHJpZ2h0O3RyYW5zaXRpb246LjJzIHRyYW5zZm9ybSwuMnMgcmlnaHQ7dHJhbnNpdGlvbjouMnMgdHJhbnNmb3JtLC4ycyByaWdodCwuMnMgLXdlYmtpdC10cmFuc2Zvcm19LnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFye2JhY2tncm91bmQ6cmdiYSgwLDAsMCwuMjUpO3Bvc2l0aW9uOmFic29sdXRlfS5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2JhciAuc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXItZmlsbHtiYWNrZ3JvdW5kOiMwMDdhZmY7cG9zaXRpb246YWJzb2x1dGU7bGVmdDowO3RvcDowO3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7LXdlYmtpdC10cmFuc2Zvcm06c2NhbGUoMCk7LW1zLXRyYW5zZm9ybTpzY2FsZSgwKTt0cmFuc2Zvcm06c2NhbGUoMCk7LXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOmxlZnQgdG9wOy1tcy10cmFuc2Zvcm0tb3JpZ2luOmxlZnQgdG9wO3RyYW5zZm9ybS1vcmlnaW46bGVmdCB0b3B9LnN3aXBlci1jb250YWluZXItcnRsIC5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2JhciAuc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXItZmlsbHstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46cmlnaHQgdG9wOy1tcy10cmFuc2Zvcm0tb3JpZ2luOnJpZ2h0IHRvcDt0cmFuc2Zvcm0tb3JpZ2luOnJpZ2h0IHRvcH0uc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsPi5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2Jhciwuc3dpcGVyLWNvbnRhaW5lci12ZXJ0aWNhbD4uc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIuc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXItb3Bwb3NpdGV7d2lkdGg6MTAwJTtoZWlnaHQ6NHB4O2xlZnQ6MDt0b3A6MH0uc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsPi5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2Jhci5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2Jhci1vcHBvc2l0ZSwuc3dpcGVyLWNvbnRhaW5lci12ZXJ0aWNhbD4uc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXJ7d2lkdGg6NHB4O2hlaWdodDoxMDAlO2xlZnQ6MDt0b3A6MH0uc3dpcGVyLXBhZ2luYXRpb24td2hpdGUgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldC1hY3RpdmV7YmFja2dyb3VuZDojZmZmfS5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2Jhci5zd2lwZXItcGFnaW5hdGlvbi13aGl0ZXtiYWNrZ3JvdW5kOnJnYmEoMjU1LDI1NSwyNTUsLjI1KX0uc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXIuc3dpcGVyLXBhZ2luYXRpb24td2hpdGUgLnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFyLWZpbGx7YmFja2dyb3VuZDojZmZmfS5zd2lwZXItcGFnaW5hdGlvbi1ibGFjayAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0LWFjdGl2ZXtiYWNrZ3JvdW5kOiMwMDB9LnN3aXBlci1wYWdpbmF0aW9uLXByb2dyZXNzYmFyLnN3aXBlci1wYWdpbmF0aW9uLWJsYWNre2JhY2tncm91bmQ6cmdiYSgwLDAsMCwuMjUpfS5zd2lwZXItcGFnaW5hdGlvbi1wcm9ncmVzc2Jhci5zd2lwZXItcGFnaW5hdGlvbi1ibGFjayAuc3dpcGVyLXBhZ2luYXRpb24tcHJvZ3Jlc3NiYXItZmlsbHtiYWNrZ3JvdW5kOiMwMDB9LnN3aXBlci1wYWdpbmF0aW9uLWxvY2t7ZGlzcGxheTpub25lfS5zd2lwZXItc2Nyb2xsYmFye2JvcmRlci1yYWRpdXM6MTBweDtwb3NpdGlvbjpyZWxhdGl2ZTstbXMtdG91Y2gtYWN0aW9uOm5vbmU7YmFja2dyb3VuZDpyZ2JhKDAsMCwwLC4xKX0uc3dpcGVyLWNvbnRhaW5lci1ob3Jpem9udGFsPi5zd2lwZXItc2Nyb2xsYmFye3Bvc2l0aW9uOmFic29sdXRlO2xlZnQ6MSU7Ym90dG9tOjNweDt6LWluZGV4OjUwO2hlaWdodDo1cHg7d2lkdGg6OTglfS5zd2lwZXItY29udGFpbmVyLXZlcnRpY2FsPi5zd2lwZXItc2Nyb2xsYmFye3Bvc2l0aW9uOmFic29sdXRlO3JpZ2h0OjNweDt0b3A6MSU7ei1pbmRleDo1MDt3aWR0aDo1cHg7aGVpZ2h0Ojk4JX0uc3dpcGVyLXNjcm9sbGJhci1kcmFne2hlaWdodDoxMDAlO3dpZHRoOjEwMCU7cG9zaXRpb246cmVsYXRpdmU7YmFja2dyb3VuZDpyZ2JhKDAsMCwwLC41KTtib3JkZXItcmFkaXVzOjEwcHg7bGVmdDowO3RvcDowfS5zd2lwZXItc2Nyb2xsYmFyLWN1cnNvci1kcmFne2N1cnNvcjptb3ZlfS5zd2lwZXItc2Nyb2xsYmFyLWxvY2t7ZGlzcGxheTpub25lfS5zd2lwZXItem9vbS1jb250YWluZXJ7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTtkaXNwbGF5Oi13ZWJraXQtYm94O2Rpc3BsYXk6LXdlYmtpdC1mbGV4O2Rpc3BsYXk6LW1zLWZsZXhib3g7ZGlzcGxheTpmbGV4Oy13ZWJraXQtYm94LXBhY2s6Y2VudGVyOy13ZWJraXQtanVzdGlmeS1jb250ZW50OmNlbnRlcjstbXMtZmxleC1wYWNrOmNlbnRlcjtqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyOy13ZWJraXQtYm94LWFsaWduOmNlbnRlcjstd2Via2l0LWFsaWduLWl0ZW1zOmNlbnRlcjstbXMtZmxleC1hbGlnbjpjZW50ZXI7YWxpZ24taXRlbXM6Y2VudGVyO3RleHQtYWxpZ246Y2VudGVyfS5zd2lwZXItem9vbS1jb250YWluZXI+Y2FudmFzLC5zd2lwZXItem9vbS1jb250YWluZXI+aW1nLC5zd2lwZXItem9vbS1jb250YWluZXI+c3Zne21heC13aWR0aDoxMDAlO21heC1oZWlnaHQ6MTAwJTstby1vYmplY3QtZml0OmNvbnRhaW47b2JqZWN0LWZpdDpjb250YWlufS5zd2lwZXItc2xpZGUtem9vbWVke2N1cnNvcjptb3ZlfS5zd2lwZXItbGF6eS1wcmVsb2FkZXJ7d2lkdGg6NDJweDtoZWlnaHQ6NDJweDtwb3NpdGlvbjphYnNvbHV0ZTtsZWZ0OjUwJTt0b3A6NTAlO21hcmdpbi1sZWZ0Oi0yMXB4O21hcmdpbi10b3A6LTIxcHg7ei1pbmRleDoxMDstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46NTAlOy1tcy10cmFuc2Zvcm0tb3JpZ2luOjUwJTt0cmFuc2Zvcm0tb3JpZ2luOjUwJTstd2Via2l0LWFuaW1hdGlvbjpzd2lwZXItcHJlbG9hZGVyLXNwaW4gMXMgc3RlcHMoMTIsZW5kKSBpbmZpbml0ZTthbmltYXRpb246c3dpcGVyLXByZWxvYWRlci1zcGluIDFzIHN0ZXBzKDEyLGVuZCkgaW5maW5pdGV9LnN3aXBlci1sYXp5LXByZWxvYWRlcjphZnRlcntkaXNwbGF5OmJsb2NrO2NvbnRlbnQ6Jyc7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJTtiYWNrZ3JvdW5kLWltYWdlOnVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbDtjaGFyc2V0PXV0Zi04LCUzQ3N2ZyUyMHZpZXdCb3glM0QnMCUyMDAlMjAxMjAlMjAxMjAnJTIweG1sbnMlM0QnaHR0cCUzQSUyRiUyRnd3dy53My5vcmclMkYyMDAwJTJGc3ZnJyUyMHhtbG5zJTNBeGxpbmslM0QnaHR0cCUzQSUyRiUyRnd3dy53My5vcmclMkYxOTk5JTJGeGxpbmsnJTNFJTNDZGVmcyUzRSUzQ2xpbmUlMjBpZCUzRCdsJyUyMHgxJTNEJzYwJyUyMHgyJTNEJzYwJyUyMHkxJTNEJzcnJTIweTIlM0QnMjcnJTIwc3Ryb2tlJTNEJyUyMzZjNmM2YyclMjBzdHJva2Utd2lkdGglM0QnMTEnJTIwc3Ryb2tlLWxpbmVjYXAlM0Qncm91bmQnJTJGJTNFJTNDJTJGZGVmcyUzRSUzQ2clM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDMwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMjcnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSg2MCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjI3JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoOTAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDEyMCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjI3JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMTUwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMzcnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgxODAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy40NiclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDIxMCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjU2JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMjQwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuNjYnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgyNzAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy43NSclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDMwMCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjg1JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMzMwJTIwNjAlMkM2MCknJTJGJTNFJTNDJTJGZyUzRSUzQyUyRnN2ZyUzRVwiKTtiYWNrZ3JvdW5kLXBvc2l0aW9uOjUwJTtiYWNrZ3JvdW5kLXNpemU6MTAwJTtiYWNrZ3JvdW5kLXJlcGVhdDpuby1yZXBlYXR9LnN3aXBlci1sYXp5LXByZWxvYWRlci13aGl0ZTphZnRlcntiYWNrZ3JvdW5kLWltYWdlOnVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbDtjaGFyc2V0PXV0Zi04LCUzQ3N2ZyUyMHZpZXdCb3glM0QnMCUyMDAlMjAxMjAlMjAxMjAnJTIweG1sbnMlM0QnaHR0cCUzQSUyRiUyRnd3dy53My5vcmclMkYyMDAwJTJGc3ZnJyUyMHhtbG5zJTNBeGxpbmslM0QnaHR0cCUzQSUyRiUyRnd3dy53My5vcmclMkYxOTk5JTJGeGxpbmsnJTNFJTNDZGVmcyUzRSUzQ2xpbmUlMjBpZCUzRCdsJyUyMHgxJTNEJzYwJyUyMHgyJTNEJzYwJyUyMHkxJTNEJzcnJTIweTIlM0QnMjcnJTIwc3Ryb2tlJTNEJyUyM2ZmZiclMjBzdHJva2Utd2lkdGglM0QnMTEnJTIwc3Ryb2tlLWxpbmVjYXAlM0Qncm91bmQnJTJGJTNFJTNDJTJGZGVmcyUzRSUzQ2clM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDMwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMjcnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSg2MCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjI3JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoOTAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy4yNyclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDEyMCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjI3JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMTUwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuMzcnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgxODAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy40NiclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDIxMCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjU2JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMjQwJTIwNjAlMkM2MCknJTJGJTNFJTNDdXNlJTIweGxpbmslM0FocmVmJTNEJyUyM2wnJTIwb3BhY2l0eSUzRCcuNjYnJTIwdHJhbnNmb3JtJTNEJ3JvdGF0ZSgyNzAlMjA2MCUyQzYwKSclMkYlM0UlM0N1c2UlMjB4bGluayUzQWhyZWYlM0QnJTIzbCclMjBvcGFjaXR5JTNEJy43NSclMjB0cmFuc2Zvcm0lM0Qncm90YXRlKDMwMCUyMDYwJTJDNjApJyUyRiUzRSUzQ3VzZSUyMHhsaW5rJTNBaHJlZiUzRCclMjNsJyUyMG9wYWNpdHklM0QnLjg1JyUyMHRyYW5zZm9ybSUzRCdyb3RhdGUoMzMwJTIwNjAlMkM2MCknJTJGJTNFJTNDJTJGZyUzRSUzQyUyRnN2ZyUzRVwiKX1ALXdlYmtpdC1rZXlmcmFtZXMgc3dpcGVyLXByZWxvYWRlci1zcGluezEwMCV7LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDM2MGRlZyk7dHJhbnNmb3JtOnJvdGF0ZSgzNjBkZWcpfX1Aa2V5ZnJhbWVzIHN3aXBlci1wcmVsb2FkZXItc3BpbnsxMDAley13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgzNjBkZWcpO3RyYW5zZm9ybTpyb3RhdGUoMzYwZGVnKX19LnN3aXBlci1jb250YWluZXIgLnN3aXBlci1ub3RpZmljYXRpb257cG9zaXRpb246YWJzb2x1dGU7bGVmdDowO3RvcDowO3BvaW50ZXItZXZlbnRzOm5vbmU7b3BhY2l0eTowO3otaW5kZXg6LTEwMDB9LnN3aXBlci1jb250YWluZXItZmFkZS5zd2lwZXItY29udGFpbmVyLWZyZWUtbW9kZSAuc3dpcGVyLXNsaWRley13ZWJraXQtdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246ZWFzZS1vdXQ7LW8tdHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246ZWFzZS1vdXQ7dHJhbnNpdGlvbi10aW1pbmctZnVuY3Rpb246ZWFzZS1vdXR9LnN3aXBlci1jb250YWluZXItZmFkZSAuc3dpcGVyLXNsaWRle3BvaW50ZXItZXZlbnRzOm5vbmU7LXdlYmtpdC10cmFuc2l0aW9uLXByb3BlcnR5Om9wYWNpdHk7LW8tdHJhbnNpdGlvbi1wcm9wZXJ0eTpvcGFjaXR5O3RyYW5zaXRpb24tcHJvcGVydHk6b3BhY2l0eX0uc3dpcGVyLWNvbnRhaW5lci1mYWRlIC5zd2lwZXItc2xpZGUgLnN3aXBlci1zbGlkZXtwb2ludGVyLWV2ZW50czpub25lfS5zd2lwZXItY29udGFpbmVyLWZhZGUgLnN3aXBlci1zbGlkZS1hY3RpdmUsLnN3aXBlci1jb250YWluZXItZmFkZSAuc3dpcGVyLXNsaWRlLWFjdGl2ZSAuc3dpcGVyLXNsaWRlLWFjdGl2ZXtwb2ludGVyLWV2ZW50czphdXRvfS5zd2lwZXItY29udGFpbmVyLWN1YmV7b3ZlcmZsb3c6dmlzaWJsZX0uc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItc2xpZGV7cG9pbnRlci1ldmVudHM6bm9uZTstd2Via2l0LWJhY2tmYWNlLXZpc2liaWxpdHk6aGlkZGVuO2JhY2tmYWNlLXZpc2liaWxpdHk6aGlkZGVuO3otaW5kZXg6MTt2aXNpYmlsaXR5OmhpZGRlbjstd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46MCAwOy1tcy10cmFuc2Zvcm0tb3JpZ2luOjAgMDt0cmFuc2Zvcm0tb3JpZ2luOjAgMDt3aWR0aDoxMDAlO2hlaWdodDoxMDAlfS5zd2lwZXItY29udGFpbmVyLWN1YmUgLnN3aXBlci1zbGlkZSAuc3dpcGVyLXNsaWRle3BvaW50ZXItZXZlbnRzOm5vbmV9LnN3aXBlci1jb250YWluZXItY3ViZS5zd2lwZXItY29udGFpbmVyLXJ0bCAuc3dpcGVyLXNsaWRley13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbjoxMDAlIDA7LW1zLXRyYW5zZm9ybS1vcmlnaW46MTAwJSAwO3RyYW5zZm9ybS1vcmlnaW46MTAwJSAwfS5zd2lwZXItY29udGFpbmVyLWN1YmUgLnN3aXBlci1zbGlkZS1hY3RpdmUsLnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRlLWFjdGl2ZSAuc3dpcGVyLXNsaWRlLWFjdGl2ZXtwb2ludGVyLWV2ZW50czphdXRvfS5zd2lwZXItY29udGFpbmVyLWN1YmUgLnN3aXBlci1zbGlkZS1hY3RpdmUsLnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRlLW5leHQsLnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRlLW5leHQrLnN3aXBlci1zbGlkZSwuc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItc2xpZGUtcHJldntwb2ludGVyLWV2ZW50czphdXRvO3Zpc2liaWxpdHk6dmlzaWJsZX0uc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItc2xpZGUtc2hhZG93LWJvdHRvbSwuc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItc2xpZGUtc2hhZG93LWxlZnQsLnN3aXBlci1jb250YWluZXItY3ViZSAuc3dpcGVyLXNsaWRlLXNoYWRvdy1yaWdodCwuc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItc2xpZGUtc2hhZG93LXRvcHt6LWluZGV4OjA7LXdlYmtpdC1iYWNrZmFjZS12aXNpYmlsaXR5OmhpZGRlbjtiYWNrZmFjZS12aXNpYmlsaXR5OmhpZGRlbn0uc3dpcGVyLWNvbnRhaW5lci1jdWJlIC5zd2lwZXItY3ViZS1zaGFkb3d7cG9zaXRpb246YWJzb2x1dGU7bGVmdDowO2JvdHRvbTowO3dpZHRoOjEwMCU7aGVpZ2h0OjEwMCU7YmFja2dyb3VuZDojMDAwO29wYWNpdHk6LjY7LXdlYmtpdC1maWx0ZXI6Ymx1cig1MHB4KTtmaWx0ZXI6Ymx1cig1MHB4KTt6LWluZGV4OjB9LnN3aXBlci1jb250YWluZXItZmxpcHtvdmVyZmxvdzp2aXNpYmxlfS5zd2lwZXItY29udGFpbmVyLWZsaXAgLnN3aXBlci1zbGlkZXtwb2ludGVyLWV2ZW50czpub25lOy13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTpoaWRkZW47YmFja2ZhY2UtdmlzaWJpbGl0eTpoaWRkZW47ei1pbmRleDoxfS5zd2lwZXItY29udGFpbmVyLWZsaXAgLnN3aXBlci1zbGlkZSAuc3dpcGVyLXNsaWRle3BvaW50ZXItZXZlbnRzOm5vbmV9LnN3aXBlci1jb250YWluZXItZmxpcCAuc3dpcGVyLXNsaWRlLWFjdGl2ZSwuc3dpcGVyLWNvbnRhaW5lci1mbGlwIC5zd2lwZXItc2xpZGUtYWN0aXZlIC5zd2lwZXItc2xpZGUtYWN0aXZle3BvaW50ZXItZXZlbnRzOmF1dG99LnN3aXBlci1jb250YWluZXItZmxpcCAuc3dpcGVyLXNsaWRlLXNoYWRvdy1ib3R0b20sLnN3aXBlci1jb250YWluZXItZmxpcCAuc3dpcGVyLXNsaWRlLXNoYWRvdy1sZWZ0LC5zd2lwZXItY29udGFpbmVyLWZsaXAgLnN3aXBlci1zbGlkZS1zaGFkb3ctcmlnaHQsLnN3aXBlci1jb250YWluZXItZmxpcCAuc3dpcGVyLXNsaWRlLXNoYWRvdy10b3B7ei1pbmRleDowOy13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTpoaWRkZW47YmFja2ZhY2UtdmlzaWJpbGl0eTpoaWRkZW59LnN3aXBlci1jb250YWluZXItY292ZXJmbG93IC5zd2lwZXItd3JhcHBlcnstbXMtcGVyc3BlY3RpdmU6MTIwMHB4fSIsIi5iLWFjdGl2YXRlXHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbjogMCAwIDMwcHhcclxuICBjb2xvcjogIzAwMDtcclxuICAvL1xyXG4gIC8vJl9fd3JhcFxyXG4gIC8vICBkaXNwbGF5OiBmbGV4O1xyXG4gIC8vICBmbGV4LWRpcmVjdGlvbiBjb2x1bW5cclxuXHJcbiAgJl9fY29udGVudFxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgei1pbmRleDogMlxyXG4gICAgcGFkZGluZzogMCAxNnB4XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4XHJcblxyXG4gICZfX2JlaGluZFxyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcbiAgJl9fc2xpZGVcclxuICAgIG1pbi13aWR0aCA5OHB4XHJcblxyXG4gICZfX2NhcmQsICZfX2NhcmQtaW5uZXJcclxuICAgIGJvcmRlci1yYWRpdXM6IDExcHg7XHJcbiAgJl9fY2FyZFxyXG4gICAgbWF4LXdpZHRoIDc4NHB4XHJcbiAgICBtYXJnaW46IDAgYXV0b1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJveC1zaGFkb3c6IDAgMnB4IDQ0cHggMCByZ2JhKDAsIDAsIDAsIDAuMDYpO1xyXG4gICAgLy9ib3JkZXItc3R5bGU6IHNvbGlkO1xyXG4gICAgLy9ib3JkZXItd2lkdGg6IDAuNXB4O1xyXG4gICAgLy9ib3JkZXItaW1hZ2Utc291cmNlOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCAjZmZmZmZmLCByZ2JhKDI1NSwgMjU1LCAyNTUsIDApIDQ3JSk7XHJcbiAgICAvL2JvcmRlci1pbWFnZS1zbGljZTogMTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sIHJnYmEoMjQ1LCAyNDIsIDIzOCwgMC45KSwgcmdiYSgyNDUsIDI0MiwgMjM4LCAwLjkpKSwgbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgI2ZmZmZmZiwgcmdiYSgyNTUsIDI1NSwgMjU1LCAwKSA0NyUpO1xyXG4gICAgYmFja2dyb3VuZC1vcmlnaW46IGJvcmRlci1ib3g7XHJcbiAgICBiYWNrZ3JvdW5kLWNsaXA6IGNvbnRlbnQtYm94LCBib3JkZXItYm94O1xyXG5cclxuICAgICZfdGhlbWUtMlxyXG4gICAgICBiYWNrZHJvcC1maWx0ZXI6IGJsdXIoMjBweCk7XHJcblxyXG4gICAgJi1pbm5lclxyXG4gICAgICBtaW4taGVpZ2h0IDMwNHB4XHJcbiAgICAgIHBhZGRpbmc6IDMwcHggMTZweCA1MHB4XHJcbiAgICAgICZfdGhlbWUtMVxyXG4gICAgICAgIGJhY2tkcm9wLWZpbHRlcjogYmx1cigyMHB4KTtcclxuXHJcbiAgICAmLXJvd1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA0NXB4XHJcbiAgICAgICZfbWItMVxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE4cHhcclxuICAgICAgJjpsYXN0LWNoaWxkXHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMFxyXG4gICAgICAmX2Zvb3RlclxyXG4gICAgICAgIG1hcmdpbjogMzBweCAwIDBcclxuXHJcbiAgJl9fdGV4dFxyXG4gICAgZm9udC1zaXplOiAxNnB4XHJcbiAgICBsaW5lLWhlaWdodDogMS4zXHJcblxyXG4gICZfX2J0blxyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zIGNlbnRlclxyXG4gICAgbWF4LXdpZHRoIDQwMHB4XHJcbiAgICBtYXJnaW46IDAgYXV0b1xyXG4gICAgcGFkZGluZzogMFxyXG4gICAgZm9udC1zaXplOiAxOXB4XHJcbiAgICBmb250LXdlaWdodDogNTAwXHJcbiAgICBsaW5lLWhlaWdodDogMVxyXG4gICAgY29sb3I6ICRjLTM7XHJcblxyXG4gICAgJiA+IHN2Zy1pY29uXHJcbiAgICAgICYgPiBzdmdcclxuICAgICAgICBoZWlnaHQ6IDE2cHhcclxuICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4XHJcblxyXG4gICZfX3RhYmxlXHJcbiAgICBwYWRkaW5nOiAxMnB4O1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwXHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwuNCk7XHJcblxyXG4gICAgJi1oZWFkXHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDIwcHhcclxuICAgICAgJi1yb3c6bm90KDpsYXN0LWNoaWxkKVxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDhweFxyXG4gICAgJi1udW1iZXJcclxuICAgICAgZm9udC1zaXplOiAyMnB4XHJcbiAgICAmLW5hbWVcclxuICAgICAgZm9udC1zaXplOiAxNHB4XHJcbiAgICAmLXJvd1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgJi10ZFxyXG4gICAgICAmOm5vdCg6bGFzdC1jaGlsZClcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDIwcHhcclxuICAgICAgJi1yb3c6bm90KDpsYXN0LWNoaWxkKVxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDRweFxyXG4gICAgICAmLXRpdGxlXHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgY29sb3I6IHJnYmEoMCwwLDAsLjgpO1xyXG4gICAgICAmLXZhbHVlXHJcbiAgICAgICAgY29sb3I6ICNiZGE3NzY7XHJcbiAgICAmLWZvb3RlclxyXG4gICAgICBtYXJnaW4tdG9wOiAzMHB4XHJcblxyXG4gICZfX3Jlc3VsdFxyXG4gICAgJi10aXRsZVxyXG4gICAgICBmb250LXNpemU6IDE4cHhcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMFxyXG4gICAgJi1sb2dvXHJcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICBtYXJnaW4tdG9wOiAyNHB4XHJcbiAgICAgICYgPiBzdmdcclxuICAgICAgICB3aWR0aDogMTYwcHhcclxuICAgICAgICBoZWlnaHQ6IGF1dG87XHJcblxyXG4rbWVkaXVtKClcclxuICAuYi1hY3RpdmF0ZVxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luOiA3MnB4IDAgMTAwcHhcclxuICAgICZfX2JlaGluZFxyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGxlZnQ6IDBcclxuICAgICAgcmlnaHQ6IDBcclxuICAgICAgdG9wOiAxMjlweFxyXG4gICAgJl9fc2xpZGVcclxuICAgICAgbWluLXdpZHRoIDIyNXB4XHJcbiAgICAmX19jYXJkLCAmX19jYXJkLWlubmVyXHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMxcHg7XHJcbiAgICAmX19jYXJkXHJcbiAgICAgICYtaW5uZXJcclxuICAgICAgICBtaW4taGVpZ2h0IDU3MnB4XHJcbiAgICAgICAgcGFkZGluZzogNjhweCA5MHB4IDkwcHhcclxuICAgICAgJi1yb3dcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA4OHB4XHJcbiAgICAgICAgJl9tYi0xXHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAzMHB4XHJcbiAgICAgICAgJl9mb290ZXJcclxuICAgICAgICAgIG1hcmdpbjogMTA4cHggMCAwXHJcbiAgICAgICAgJl9pbnB1dFxyXG4gICAgICAgICAgbWF4LXdpZHRoIDQ5MHB4XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogYXV0b1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgJl9fdGV4dFxyXG4gICAgICBmb250LXNpemU6IDI0cHhcclxuXHJcbiAgICAmX19idG5cclxuICAgICAgZm9udC1zaXplOiAzMnB4XHJcblxyXG4gICAgICAmID4gc3ZnLWljb25cclxuICAgICAgICAmID4gc3ZnXHJcbiAgICAgICAgICBoZWlnaHQ6IDI1cHhcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4XHJcblxyXG4gICAgJl9fdGFibGVcclxuICAgICAgbWF4LXdpZHRoOiA3NSU7XHJcbiAgICAgIHBhZGRpbmc6IDI0cHg7XHJcblxyXG4gICAgICAmLWhlYWRcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA0MHB4XHJcbiAgICAgICAgJi1yb3c6bm90KDpsYXN0LWNoaWxkKVxyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4XHJcbiAgICAgICYtbmFtZVxyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweFxyXG4gICAgICAmLW51bWJlclxyXG4gICAgICAgIGZvbnQtc2l6ZTogMzBweFxyXG4gICAgICAmLXRkXHJcbiAgICAgICAgJjpub3QoOmxhc3QtY2hpbGQpXHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDIwcHhcclxuICAgICAgICAmLXJvdzpub3QoOmxhc3QtY2hpbGQpXHJcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiA0cHhcclxuICAgICAgICAmLXRpdGxlXHJcbiAgICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICYtZm9vdGVyXHJcbiAgICAgICAgbWFyZ2luLXRvcDogMzBweFxyXG5cclxuICAgICZfX3Jlc3VsdFxyXG4gICAgICAmLXRpdGxlXHJcbiAgICAgICAgZm9udC1zaXplOiAyNHB4XHJcbiAgICAgICYtbG9nb1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDI0cHhcclxuICAgICAgICAmID4gc3ZnXHJcbiAgICAgICAgICB3aWR0aDogMjI0cHhcclxuIiwiQGltcG9ydCAnfnN3aXBlci9kaXN0L2Nzcy9zd2lwZXIubWluLmNzcyc7XG4uYi1hY3RpdmF0ZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luOiAwIDAgMzBweDtcbiAgY29sb3I6ICMwMDA7XG59XG4uYi1hY3RpdmF0ZV9fY29udGVudCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgei1pbmRleDogMjtcbiAgcGFkZGluZzogMCAxNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG4uYi1hY3RpdmF0ZV9fYmVoaW5kIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5iLWFjdGl2YXRlX19zbGlkZSB7XG4gIG1pbi13aWR0aDogOThweDtcbn1cbi5iLWFjdGl2YXRlX19jYXJkLFxuLmItYWN0aXZhdGVfX2NhcmQtaW5uZXIge1xuICBib3JkZXItcmFkaXVzOiAxMXB4O1xufVxuLmItYWN0aXZhdGVfX2NhcmQge1xuICBtYXgtd2lkdGg6IDc4NHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm94LXNoYWRvdzogMCAycHggNDRweCAwIHJnYmEoMCwwLDAsMC4wNik7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sIHJnYmEoMjQ1LDI0MiwyMzgsMC45KSwgcmdiYSgyNDUsMjQyLDIzOCwwLjkpKSwgbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgI2ZmZiwgcmdiYSgyNTUsMjU1LDI1NSwwKSA0NyUpO1xuICBiYWNrZ3JvdW5kLW9yaWdpbjogYm9yZGVyLWJveDtcbiAgYmFja2dyb3VuZC1jbGlwOiBjb250ZW50LWJveCwgYm9yZGVyLWJveDtcbn1cbi5iLWFjdGl2YXRlX19jYXJkX3RoZW1lLTIge1xuICBiYWNrZHJvcC1maWx0ZXI6IGJsdXIoMjBweCk7XG59XG4uYi1hY3RpdmF0ZV9fY2FyZC1pbm5lciB7XG4gIG1pbi1oZWlnaHQ6IDMwNHB4O1xuICBwYWRkaW5nOiAzMHB4IDE2cHggNTBweDtcbn1cbi5iLWFjdGl2YXRlX19jYXJkLWlubmVyX3RoZW1lLTEge1xuICBiYWNrZHJvcC1maWx0ZXI6IGJsdXIoMjBweCk7XG59XG4uYi1hY3RpdmF0ZV9fY2FyZC1yb3cge1xuICBtYXJnaW4tYm90dG9tOiA0NXB4O1xufVxuLmItYWN0aXZhdGVfX2NhcmQtcm93X21iLTEge1xuICBtYXJnaW4tYm90dG9tOiAxOHB4O1xufVxuLmItYWN0aXZhdGVfX2NhcmQtcm93Omxhc3QtY2hpbGQge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLmItYWN0aXZhdGVfX2NhcmQtcm93X2Zvb3RlciB7XG4gIG1hcmdpbjogMzBweCAwIDA7XG59XG4uYi1hY3RpdmF0ZV9fdGV4dCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbn1cbi5iLWFjdGl2YXRlX19idG4ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXgtd2lkdGg6IDQwMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZzogMDtcbiAgZm9udC1zaXplOiAxOXB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBsaW5lLWhlaWdodDogMTtcbiAgY29sb3I6ICNmZjM4Mzg7XG59XG4uYi1hY3RpdmF0ZV9fYnRuID4gc3ZnLWljb24gPiBzdmcge1xuICBoZWlnaHQ6IDE2cHg7XG4gIHdpZHRoOiBhdXRvO1xuICBtYXJnaW4tbGVmdDogOHB4O1xufVxuLmItYWN0aXZhdGVfX3RhYmxlIHtcbiAgcGFkZGluZzogMTJweDtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwwLjQpO1xufVxuLmItYWN0aXZhdGVfX3RhYmxlLWhlYWQge1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuLmItYWN0aXZhdGVfX3RhYmxlLWhlYWQtcm93Om5vdCg6bGFzdC1jaGlsZCkge1xuICBtYXJnaW4tYm90dG9tOiA4cHg7XG59XG4uYi1hY3RpdmF0ZV9fdGFibGUtbnVtYmVyIHtcbiAgZm9udC1zaXplOiAyMnB4O1xufVxuLmItYWN0aXZhdGVfX3RhYmxlLW5hbWUge1xuICBmb250LXNpemU6IDE0cHg7XG59XG4uYi1hY3RpdmF0ZV9fdGFibGUtcm93IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5iLWFjdGl2YXRlX190YWJsZS10ZDpub3QoOmxhc3QtY2hpbGQpIHtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xufVxuLmItYWN0aXZhdGVfX3RhYmxlLXRkLXJvdzpub3QoOmxhc3QtY2hpbGQpIHtcbiAgbWFyZ2luLWJvdHRvbTogNHB4O1xufVxuLmItYWN0aXZhdGVfX3RhYmxlLXRkLXRpdGxlIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogcmdiYSgwLDAsMCwwLjgpO1xufVxuLmItYWN0aXZhdGVfX3RhYmxlLXRkLXZhbHVlIHtcbiAgY29sb3I6ICNiZGE3NzY7XG59XG4uYi1hY3RpdmF0ZV9fdGFibGUtZm9vdGVyIHtcbiAgbWFyZ2luLXRvcDogMzBweDtcbn1cbi5iLWFjdGl2YXRlX19yZXN1bHQtdGl0bGUge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4uYi1hY3RpdmF0ZV9fcmVzdWx0LWxvZ28ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLXRvcDogMjRweDtcbn1cbi5iLWFjdGl2YXRlX19yZXN1bHQtbG9nbyA+IHN2ZyB7XG4gIHdpZHRoOiAxNjBweDtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmItYWN0aXZhdGUge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW46IDcycHggMCAxMDBweDtcbiAgfVxuICAuYi1hY3RpdmF0ZV9fYmVoaW5kIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICB0b3A6IDEyOXB4O1xuICB9XG4gIC5iLWFjdGl2YXRlX19zbGlkZSB7XG4gICAgbWluLXdpZHRoOiAyMjVweDtcbiAgfVxuICAuYi1hY3RpdmF0ZV9fY2FyZCxcbiAgLmItYWN0aXZhdGVfX2NhcmQtaW5uZXIge1xuICAgIGJvcmRlci1yYWRpdXM6IDMxcHg7XG4gIH1cbiAgLmItYWN0aXZhdGVfX2NhcmQtaW5uZXIge1xuICAgIG1pbi1oZWlnaHQ6IDU3MnB4O1xuICAgIHBhZGRpbmc6IDY4cHggOTBweCA5MHB4O1xuICB9XG4gIC5iLWFjdGl2YXRlX19jYXJkLXJvdyB7XG4gICAgbWFyZ2luLWJvdHRvbTogODhweDtcbiAgfVxuICAuYi1hY3RpdmF0ZV9fY2FyZC1yb3dfbWItMSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgfVxuICAuYi1hY3RpdmF0ZV9fY2FyZC1yb3dfZm9vdGVyIHtcbiAgICBtYXJnaW46IDEwOHB4IDAgMDtcbiAgfVxuICAuYi1hY3RpdmF0ZV9fY2FyZC1yb3dfaW5wdXQge1xuICAgIG1heC13aWR0aDogNDkwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB9XG4gIC5iLWFjdGl2YXRlX190ZXh0IHtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gIH1cbiAgLmItYWN0aXZhdGVfX2J0biB7XG4gICAgZm9udC1zaXplOiAzMnB4O1xuICB9XG4gIC5iLWFjdGl2YXRlX19idG4gPiBzdmctaWNvbiA+IHN2ZyB7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICB9XG4gIC5iLWFjdGl2YXRlX190YWJsZSB7XG4gICAgbWF4LXdpZHRoOiA3NSU7XG4gICAgcGFkZGluZzogMjRweDtcbiAgfVxuICAuYi1hY3RpdmF0ZV9fdGFibGUtaGVhZCB7XG4gICAgbWFyZ2luLWJvdHRvbTogNDBweDtcbiAgfVxuICAuYi1hY3RpdmF0ZV9fdGFibGUtaGVhZC1yb3c6bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgbWFyZ2luLWJvdHRvbTogOHB4O1xuICB9XG4gIC5iLWFjdGl2YXRlX190YWJsZS1uYW1lIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cbiAgLmItYWN0aXZhdGVfX3RhYmxlLW51bWJlciB7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICB9XG4gIC5iLWFjdGl2YXRlX190YWJsZS10ZDpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gIH1cbiAgLmItYWN0aXZhdGVfX3RhYmxlLXRkLXJvdzpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gIH1cbiAgLmItYWN0aXZhdGVfX3RhYmxlLXRkLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDEycHg7XG4gIH1cbiAgLmItYWN0aXZhdGVfX3RhYmxlLWZvb3RlciB7XG4gICAgbWFyZ2luLXRvcDogMzBweDtcbiAgfVxuICAuYi1hY3RpdmF0ZV9fcmVzdWx0LXRpdGxlIHtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gIH1cbiAgLmItYWN0aXZhdGVfX3Jlc3VsdC1sb2dvIHtcbiAgICBtYXJnaW4tdG9wOiAyNHB4O1xuICB9XG4gIC5iLWFjdGl2YXRlX19yZXN1bHQtbG9nbyA+IHN2ZyB7XG4gICAgd2lkdGg6IDIyNHB4O1xuICB9XG59XG4uY2FyZC1zbGlkZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbi5jYXJkLXNsaWRlcl9fbGlzdCB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBtYXJnaW46IDAgLTZweDtcbn1cbi5jYXJkLXNsaWRlcl9faXRlbSB7XG4gIG1hcmdpbjogMCA2cHg7XG59XG4uY2FyZC1zbGlkZXJfX2NhcmQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGhlaWdodDogMzA0cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgYm94LXNoYWRvdzogMCA0cHggMTJweCAwIHJnYmEoMSwxMywxNjUsMC4yOCk7XG4gIG1hcmdpbjogMnB4IDAgMTdweDtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIDA7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4uY2FyZC1zbGlkZXJfX2NhcmQ6YmVmb3JlIHtcbiAgY29udGVudDogJyc7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTQ0ZGVnLCByZ2JhKDI1NSwyNTUsMjU1LDAuMykgMSUsIHJnYmEoMjU1LDI1NSwyNTUsMCkgNDAlKSwgbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgcmdiYSg5MiwyOCw0LDApIDM0JSwgIzMxMTAwNCA4OCUpO1xufVxuLyojIHNvdXJjZU1hcHBpbmdVUkw9c3JjL2FwcC9wYWdlcy9pbmRleC9pbmRleC5jb21wb25lbnQuY3NzLm1hcCAqLyIsIm1vYmlsZSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogJHdfbSlcclxuICAgIHtibG9ja31cclxuZGVza3RvcCgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfZClcclxuICAgIHtibG9ja31cclxubWluX2luaXRfZCgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfaW5pdF9kKVxyXG4gICAge2Jsb2NrfVxyXG5zbWFsbCgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfc21hbGwpXHJcbiAgICB7YmxvY2t9XHJcbnNtYWxsXzIoKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X3NtYWxsXzIpXHJcbiAgICB7YmxvY2t9XHJcbm1lZGl1bSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfbWVkaXVtKVxyXG4gICAge2Jsb2NrfVxyXG5sYXJnZSgpXHJcbiAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogJHdfbGFyZ2UpXHJcbiAgICB7YmxvY2t9XHJcbmV4dHJhX2xhcmdlKClcclxuICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAkd19leHRyYV9sYXJnZSlcclxuICAgIHtibG9ja31cclxubWF4X3coKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6ICR3X21heF9wYWdlX3dpZHRoKVxyXG4gICAge2Jsb2NrfVxyXG5cclxuaF9tZWRpdW0oKVxyXG4gIEBtZWRpYSBhbGwgYW5kIChtaW4taGVpZ2h0OiAkaF9tZWRpdW0pXHJcbiAgICB7YmxvY2t9XHJcblxyXG5wbGFjZWhvbGRlcigpXHJcbiAgJjo6cGxhY2Vob2xkZXJcclxuICAgIHtibG9ja31cclxuXHJcbmF1dG9jb21wbGV0ZSgpXHJcbiAgJjotd2Via2l0LWF1dG9maWxsLFxyXG4gICY6LXdlYmtpdC1hdXRvZmlsbDpob3ZlcixcclxuICAmOi13ZWJraXQtYXV0b2ZpbGw6Zm9jdXMsXHJcbiAgJjotd2Via2l0LWF1dG9maWxsOmFjdGl2ZSxcclxuICAmOi1pbnRlcm5hbC1hdXRvZmlsbC1wcmV2aWV3ZWQsXHJcbiAgJjotaW50ZXJuYWwtYXV0b2ZpbGwtc2VsZWN0ZWRcclxuICAgIHtibG9ja31cclxuXHJcbl9jYWxjKCRwZXIsICR2YWwpXHJcbiAgcmV0dXJuIFwiY2FsYyglcyAtICVzKVwiICUgKCRwZXIgJHZhbClcclxuIiwiLmNhcmQtc2xpZGVyXHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQgY2VudGVyXHJcblxyXG4gICZfX2xpc3RcclxuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIG1hcmdpbjogMCAtNnB4XHJcblxyXG4gICZfX2l0ZW1cclxuICAgIG1hcmdpbjogMCA2cHhcclxuXHJcbiAgJl9fY2FyZFxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLy93aWR0aDogMjI1cHhcclxuICAgIGhlaWdodDogMzA0cHhcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBib3JkZXItcmFkaXVzOiA2cHhcclxuICAgIGJveC1zaGFkb3cgJGMtY2FyZC0yLXRoZW1lLTEtYnRuLTEtc2hhZG93XHJcbiAgICBtYXJnaW46IDJweCAwIDE3cHg7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIDA7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG5cclxuICAgICY6YmVmb3JlXHJcbiAgICAgIGNvbnRlbnQgJydcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICB0b3A6IDBcclxuICAgICAgbGVmdDogMFxyXG4gICAgICB3aWR0aDogMTAwJVxyXG4gICAgICBoZWlnaHQ6IDEwMCVcclxuICAgICAgYmFja2dyb3VuZDogJGMtY2FyZC0xLXRoZW1lLTEtZ3JhZGllbnQ7XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/index/index.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/index/index.component.ts ***!
  \************************************************/
/*! exports provided: IndexComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IndexComponent", function() { return IndexComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ramda */ "./node_modules/ramda/es/index.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var _services_global_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/global.service */ "./src/app/services/global.service.ts");






var IndexComponent = /** @class */ (function () {
    function IndexComponent(route, api, global) {
        this.route = route;
        this.api = api;
        this.global = global;
        this.activatePath = '/activate';
        this.activeTab = 'gifts';
        this.activateMd = '';
        this.isDesktop = true;
        this.bannerCert = null;
        this.routeSettings = {
            user_friendly_url: 'spasibodoc',
            widget_url: 'http://placekitten.com',
            giftTabTitle: 'Подарить врачу',
            corpTabTitle: 'Подарить больнице',
            first_logos_title: 'Где можно использовать сертификат',
            second_logos_title: 'Партнёры акции',
            action: true,
            actionText: "\n      <h1>\n      <strong>\u0414\u043E\u043A\u0442\u043E\u0440, \u0442\u0435\u043F\u0435\u0440\u044C \u0432\u044B \u043A \u043D\u0430\u043C.</strong>\n    </h1>\n    <p>\n      \u0416\u0435\u0440\u0442\u0432 \u043A\u043E\u0440\u043E\u043D\u0430\u0432\u0438\u0440\u0443\u0441\u0430 \u043C\u043E\u0433\u043B\u043E \u0431\u044B\u0442\u044C \u0431\u043E\u043B\u044C\u0448\u0435, \u0435\u0441\u043B\u0438 \u0431\u044B \u043D\u0435 \u0432\u0430\u0448\u0430 \u0440\u0430\u0431\u043E\u0442\u0430 - \u0440\u0430\u0431\u043E\u0442\u0430 \u0432\u0440\u0430\u0447\u0435\u0439, \u043C\u0435\u0434\u0441\u0435\u0441\u0442\u0451\u0440, \u0441\u0430\u043D\u0438\u0442\u0430\u0440\u043E\u043A, \u043D\u044F\u043D\u0435\u0447\u0435\u043A. <strong>\u0421\u043F\u0430\u0441\u0438\u0431\u043E</strong> \u0432\u0430\u043C \u0437\u0430 \u0442\u043E, \u0447\u0442\u043E \u0432\u044B \u043D\u0430\u0441 \u0441\u043F\u0430\u0441\u0430\u043B\u0438!\n    </p>\n    <p>\n      <strong>\u041C\u0443\u0437\u0435\u0438, \u0442\u0435\u0430\u0442\u0440\u044B, \u0433\u0430\u043B\u0435\u0440\u0435\u0438 \u0438 \u0434\u0440\u0443\u0433\u0438\u0435 \u043A\u0443\u043B\u044C\u0442\u0443\u0440\u043D\u044B\u0435 \u043F\u043B\u043E\u0449\u0430\u0434\u043A\u0438</strong> \u0415\u043A\u0430\u0442\u0435\u0440\u0438\u043D\u0431\u0443\u0440\u0433\u0430 \u0445\u043E\u0442\u044F\u0442 \u0434\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u0432 \u0432\u0430\u0448\u0443 \u0436\u0438\u0437\u043D\u044C \u043D\u0435\u043C\u043D\u043E\u0433\u043E \u0440\u0430\u0434\u043E\u0441\u0442\u0438 \u0438 \u043F\u043E\u043C\u043E\u0447\u044C \u0432\u0435\u0440\u043D\u0443\u0442\u044C\u0441\u044F \u043A \u00AB\u043C\u0438\u0440\u043D\u043E\u0439 \u0436\u0438\u0437\u043D\u0438\u00BB.\n    </p>\n    <p>\n      \u0412 \u0432\u0430\u0448\u0438\u0445 \u0440\u0443\u043A\u0430\u0445 <strong>\u043F\u043E\u0434\u0430\u0440\u043E\u0447\u043D\u044B\u0439 \u0441\u0435\u0440\u0442\u0438\u0444\u0438\u043A\u0430\u0442</strong>, \u0432\u044B\u043F\u0443\u0449\u0435\u043D\u043D\u044B\u0439 \u0441\u043F\u0435\u0446\u0438\u0430\u043B\u044C\u043D\u043E \u0434\u043B\u044F \u0432\u0430\u0441, \u043E\u043D \u0434\u0430\u0435\u0442 \u0432\u0430\u043C \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u043E\u0441\u0442\u044C <strong>\u0431\u0435\u0441\u043F\u043B\u0430\u0442\u043D\u043E</strong> \u0441\u0445\u043E\u0434\u0438\u0442\u044C \u0432 \u0442\u0435\u0430\u0442\u0440, \u043C\u0443\u0437\u0435\u0439, \u043D\u0430 \u043A\u043E\u043D\u0446\u0435\u0440\u0442 \u0438\u043B\u0438 \u0432\u044B\u0441\u0442\u0430\u0432\u043A\u0443. \u0411\u0443\u0434\u0435\u043C \u0440\u0430\u0434\u044B \u0432\u0438\u0434\u0435\u0442\u044C \u0432\u0430\u0441 \u0431\u0435\u0437 \u0431\u0435\u043B\u044B\u0445 \u0445\u0430\u043B\u0430\u0442\u043E\u0432.\n    </p>\n    ",
            second_logos: [
                // '/assets/img/logo/disney.svg',
                // '/assets/img/logo/smf.svg',
                { img: '/assets/img/logo/e1.svg', classes: 'sz-2' },
                { img: '/assets/img/logo/znak.svg', classes: '' },
                { img: '/assets/img/logo/tass.svg', classes: '' },
                { img: '/assets/img/logo/echom.svg', classes: 'sz-2' },
            ],
            first_logos: [
                { img: '/assets/img/support_logo/1.svg' },
                { img: '/assets/img/support_logo/2.svg' },
                { img: '/assets/img/support_logo/3.svg' },
                { img: '/assets/img/support_logo/4.svg' },
                { img: '/assets/img/support_logo/5.svg' },
                { img: '/assets/img/support_logo/6.svg' },
                { img: '/assets/img/support_logo/7.svg' },
                { img: '/assets/img/support_logo/8.svg' },
            ],
            addresseeData: [
                {
                    name: 'Больнице РАН в Троицке',
                    url: 'https://hospital-ran-troitsk.ru/',
                    email: 'admin@hospital.troitsk.ru',
                    person_name: 'Корицкий Андрей Владимирович',
                },
                {
                    name: 'Городской клинической больнице №15 им. О.М.Филатова',
                    url: 'http://gkb15.moscow/',
                    email: 'gkb15@zdrav.mos.ru',
                    person_name: 'Владимир Николаевич Мудрак',
                },
                {
                    name: 'Городской клиничеcкой больнице №67 им. Ворохобова',
                    email: 'gkb67@zdrav.mos.ru',
                },
                {
                    name: 'Государственному научному центру колопроктологии им. Рыжих',
                    email: 'info@gnck.ru',
                },
                {
                    name: 'Детской клинической больнице им. З.А. Башляевой',
                    email: 'dgkb-bashlyaevoy@zdrav.mos.ru',
                },
                {
                    name: 'Инфекционной клинической больнице №1',
                    email: 'ikb1@zdrav.mos.ru',
                },
                {
                    name: 'Инфекционной клинической больнице №2',
                    email: 'ikb2@zdrav.mos.ru',
                },
                {
                    name: 'Клинической больнице им. Семашко РЖД',
                    email: 'ckb2semashko@mail.ru',
                },
                {
                    name: 'Клинической больнице №85 ФМБА',
                    email: 'info@kb85.ru',
                },
                {
                    name: 'Лечебно-реабилитационному центру Минздрава',
                    email: 'info@med-rf.ru',
                },
                {
                    name: 'Медицинскому центру в Коммунарке',
                    email: 'gkb40@zdrav.mos.ru',
                },
                {
                    name: 'Научному центру здоровья детей',
                    email: 'info@nczd.ru',
                },
                {
                    name: 'Национальному медико-хирургическому центру им. Пирогова',
                    email: 'info@pirogov-center.ru',
                },
                {
                    name: 'НИИ медицины труда им. Измерова',
                    email: 'info@irioh.ru',
                },
                {
                    name: 'НИИ скорой помощи им. Склифосовского',
                    email: 'sklif@zdrav.mos.ru',
                },
                {
                    name: 'НМИЦ акушерства, гинекологии и перинатологии им. Кулакова',
                    email: 'secretariat@oparina4.ru',
                },
                {
                    name: 'НМИЦ кардиологии Минздрава',
                    email: 'info@cardioweb.ru',
                },
                {
                    name: 'Российскому геронтологическому научно-клиническому центру ',
                    email: 'anticorruption@rgnkc.ru',
                },
                {
                    name: 'УКБ №1 Сеченовского Университета',
                    email: 'rektorat@sechenov.ru',
                },
                {
                    name: 'Федеральному бюро медико-социальной экспертизы Минтруда',
                    email: 'fbmse@fbmse.ru',
                },
                {
                    name: 'Федеральному исследовательскому центру питания биотехнологии и безопасности пищи',
                    email: 'mailbox@ion.ru',
                },
                {
                    name: 'Федеральному медицинскому биофизическому центру им. Бурназяна',
                    email: 'fmbc-fmba@bk.ru',
                },
                {
                    name: 'Федеральному научно-клиническому центру ФМБА России',
                    email: 'info@fnkc-fmba.ru',
                },
                {
                    name: 'Центральной клинической больнице РАН',
                    email: 'ckb@ckbran.ru',
                },
                {
                    name: 'Центральной клинической больнице РЖД-Медицина',
                    email: 'ckb2semashko@mail.ru',
                },
                {
                    name: 'Центру высоких медицинских технологий ФМБА',
                    email: 'do@kb119.ru',
                },
                {
                    name: 'Челюстно-лицевому госпиталю для ветеранов войн',
                    email: 'chlg@zdrav.mos.ru',
                },
            ],
            subscribeFormTitle: "\u0415\u0441\u043B\u0438 \u0432\u044B \u043C\u0443\u0437\u0435\u0439, \u0442\u0435\u0430\u0442\u0440, <br>\u043E\u0431\u044A\u0435\u043A\u0442 \u043A\u0443\u043B\u044C\u0442\u0443\u0440\u044B<br> <strong>\u041F\u0440\u0438\u0441\u043E\u0435\u0434\u0438\u043D\u044F\u0439\u0442\u0435\u0441\u044C \u043A \u043F\u0440\u043E\u0435\u043A\u0442\u0443</strong>"
        };
    }
    IndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.global.settings) {
            this.init();
        }
        else {
            this.global.settings$.subscribe(function (s) {
                _this.init();
            });
        }
    };
    IndexComponent.prototype.init = function () {
        var _this = this;
        this.certSliderSettings = this.global.settings.widget_settings.cert_page_sliders;
        var setting = Object(ramda__WEBPACK_IMPORTED_MODULE_3__["path"])(['routeSettings', 'index'], this.global.settings.widget_settings);
        if (setting) {
            this.routeSettings = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.routeSettings, setting);
            this.api.getGift(this.routeSettings.user_friendly_url).subscribe(function (gift) { return _this.gift = gift; });
            this.state = 'gift';
        }
        else {
            this.state = 'index';
        }
        if (location.pathname === this.activatePath) {
            this.activeTab = 'gifts_activate';
            this.route.queryParams.subscribe(function (q) {
                if (q.number) {
                    _this.activateMd = q.number;
                }
            });
        }
    };
    IndexComponent.prototype.onGiftsFetched = function (gifts) {
        this.gifts = gifts;
        this.bannerCert = this.gifts.find(function (el) { return el.meta.restaurant_gift_type === 'universal'; });
    };
    IndexComponent.prototype.ngAfterViewInit = function () {
        this.isDesktop = window.innerWidth > 991;
    };
    IndexComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_4__["TransportService"] },
        { type: _services_global_service__WEBPACK_IMPORTED_MODULE_5__["GlobalService"] }
    ]; };
    IndexComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-index',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./index.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/index/index.component.html")).default,
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./index.component.styl */ "./src/app/pages/index/index.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_transport_service__WEBPACK_IMPORTED_MODULE_4__["TransportService"],
            _services_global_service__WEBPACK_IMPORTED_MODULE_5__["GlobalService"]])
    ], IndexComponent);
    return IndexComponent;
}());



/***/ }),

/***/ "./src/app/pages/index/index.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/index/index.module.ts ***!
  \*********************************************/
/*! exports provided: ROUTES, IndexModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IndexModule", function() { return IndexModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _index_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./index.component */ "./src/app/pages/index/index.component.ts");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _modules_tabs_tabs_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../modules/tabs/tabs.module */ "./src/app/modules/tabs/tabs.module.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_svg_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angular-svg-icon */ "./node_modules/angular-svg-icon/fesm5/angular-svg-icon.js");
/* harmony import */ var ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-swiper-wrapper */ "./node_modules/ngx-swiper-wrapper/dist/ngx-swiper-wrapper.es5.js");
/* harmony import */ var _components_gifts_gifts_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../components/gifts/gifts.component */ "./src/app/components/gifts/gifts.component.ts");
/* harmony import */ var _components_gifts_gifts_nav_gifts_nav_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../components/gifts/gifts-nav/gifts-nav.component */ "./src/app/components/gifts/gifts-nav/gifts-nav.component.ts");
/* harmony import */ var _modules_scroll_scroll_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../modules/scroll/scroll.module */ "./src/app/modules/scroll/scroll.module.ts");
/* harmony import */ var _validators_validators_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../validators/validators.module */ "./src/app/validators/validators.module.ts");
/* harmony import */ var ngx_infinite_scroll__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-infinite-scroll */ "./node_modules/ngx-infinite-scroll/modules/ngx-infinite-scroll.es5.js");
/* harmony import */ var _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../components/gift/gift.module */ "./src/app/components/gift/gift.module.ts");

















var ROUTES = [{ path: '', component: _index_component__WEBPACK_IMPORTED_MODULE_4__["IndexComponent"] }];
var declarations = [
    _index_component__WEBPACK_IMPORTED_MODULE_4__["IndexComponent"],
    _components_gifts_gifts_component__WEBPACK_IMPORTED_MODULE_11__["GiftsComponent"],
    _components_gifts_gifts_nav_gifts_nav_component__WEBPACK_IMPORTED_MODULE_12__["GiftsNavComponent"],
];
var IndexModule = /** @class */ (function () {
    function IndexModule() {
    }
    IndexModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: declarations,
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES),
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _modules_tabs_tabs_module__WEBPACK_IMPORTED_MODULE_6__["TabsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
                angular_svg_icon__WEBPACK_IMPORTED_MODULE_9__["AngularSvgIconModule"],
                _validators_validators_module__WEBPACK_IMPORTED_MODULE_14__["ValidatorsModule"],
                _modules_scroll_scroll_module__WEBPACK_IMPORTED_MODULE_13__["ScrollModule"],
                ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_10__["SwiperModule"],
                ngx_infinite_scroll__WEBPACK_IMPORTED_MODULE_15__["InfiniteScrollModule"],
                _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_16__["GiftModule"]
            ]
        })
    ], IndexModule);
    return IndexModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-index-index-module.js.map