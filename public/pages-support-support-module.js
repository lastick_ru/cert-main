(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-support-support-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/support/support.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/support/support.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--Подарите знакомому врачу или больнице подарочный сертификат на мероприятия. Оплаченную вами сумму мы увеличим на 30% и отправим куда скажете.      -->\r\n<!--  [head_hash_tag]=\"gift && gift.name_variant ? (gift.name_variant | multilang) : ''\"\r\n-->\r\n<app-gift\r\n  [gift]=\"gift\"\r\n  [showCover]=\"false\"\r\n  [showName]=\"false\"\r\n  [showHeading]=\"true\"\r\n  [showBFee]=\"false\"\r\n  [showSubscribeForm]=\"true\"\r\n  [showSubscribeMessage]=\"false\"\r\n  [showMoreBtn]=\"false\"\r\n  [corpPhoneInput]=\"false\"\r\n  [giftPhoneInput]=\"false\"\r\n  [addresseeData]=\"addresseeData\"\r\n  giftTabTitle=\"Подарить врачу\"\r\n  corpTabTitle=\"Подарить больнице\"\r\n  [first_logos]=\"null\"\r\n  first_logos_title=\"Больницы которые борются с COVID-19\"\r\n  [second_logos]=\"logos\"\r\n  second_logos_title=\"Где можно использовать сертификат\"\r\n  [headingText]=\"\r\n    gift && gift.price_modifier && gift.price_modifier.formatted_value\r\n      ? '<h1>' +\r\n        '  <strong>Поддержим врачей</strong> вместе. Подарите сертификат и <strong>скажите им спасибо.</strong>' +\r\n        '</h1>' +\r\n        '<p>' +\r\n        'Zapomni.Gift позволяет подарить сертификат врачу или всей больнице. Выбранную вами сумму подарка, мы <strong>увеличим на ' +\r\n        gift.price_modifier.value +\r\n        '% за свой счет.</strong>' +\r\n        '</p>' +\r\n        '<p>' +\r\n          'Как только все закончится, врачи смогут использовать полученный от вас сертификат и посетить лучшие культурные мероприятия Москвы <strong>бесплатно!</strong>' +\r\n        '</p>'\r\n      : null\r\n  \"\r\n>\r\n</app-gift>\r\n");

/***/ }),

/***/ "./src/app/pages/support/support.component.styl":
/*!******************************************************!*\
  !*** ./src/app/pages/support/support.component.styl ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/pages/support/support.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc3VwcG9ydC9zdXBwb3J0LmNvbXBvbmVudC5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHNFQUFzRSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3N1cHBvcnQvc3VwcG9ydC5jb21wb25lbnQuc3R5bCJ9 */");

/***/ }),

/***/ "./src/app/pages/support/support.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/support/support.component.ts ***!
  \****************************************************/
/*! exports provided: SupportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportComponent", function() { return SupportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var SupportComponent = /** @class */ (function () {
    function SupportComponent(api, route) {
        this.api = api;
        this.route = route;
        this.user_friendly_url = 'spasibodoc';
        this.logos = [
            // '/assets/img/logo/disney.svg',
            // '/assets/img/logo/smf.svg',
            { img: '/assets/img/logo/cons.png' },
            { img: '/assets/img/logo/planetarium.svg' },
            { img: '/assets/img/logo/zapomni.svg' },
            { img: '/assets/img/logo/park.png' },
            { img: '/assets/img/logo/zal.png' },
            { img: '/assets/img/logo/cdk.png' },
            { img: '/assets/img/logo/moskino.svg' },
            { img: '/assets/img/logo/teatrarmii_logo.png' },
            { img: '/assets/img/logo/shkola.png' },
        ];
        this.first_logos = [
            { img: '/assets/img/support_logo/1.svg' },
            { img: '/assets/img/support_logo/2.svg' },
            { img: '/assets/img/support_logo/3.svg' },
            { img: '/assets/img/support_logo/4.svg' },
            { img: '/assets/img/support_logo/5.svg' },
            { img: '/assets/img/support_logo/6.svg' },
            { img: '/assets/img/support_logo/7.svg' },
            { img: '/assets/img/support_logo/8.svg' },
        ];
        this.addresseeData = [
            {
                name: 'Больнице РАН в Троицке',
                url: 'https://hospital-ran-troitsk.ru/',
                email: 'admin@hospital.troitsk.ru',
                person_name: 'Корицкий Андрей Владимирович',
            },
            {
                name: 'Городской клинической больнице №15 им. О.М.Филатова',
                url: 'http://gkb15.moscow/',
                email: 'gkb15@zdrav.mos.ru',
                person_name: 'Владимир Николаевич Мудрак'
            },
            {
                name: 'Городской клиничеcкой больнице №67 им. Ворохобова',
                email: 'gkb67@zdrav.mos.ru',
            },
            {
                name: 'Государственному научному центру колопроктологии им. Рыжих',
                email: 'info@gnck.ru',
            },
            {
                name: 'Детской клинической больнице им. З.А. Башляевой',
                email: 'dgkb-bashlyaevoy@zdrav.mos.ru',
            },
            {
                name: 'Инфекционной клинической больнице №1',
                email: 'ikb1@zdrav.mos.ru',
            },
            {
                name: 'Инфекционной клинической больнице №2',
                email: 'ikb2@zdrav.mos.ru',
            },
            {
                name: 'Клинической больнице им. Семашко РЖД',
                email: 'ckb2semashko@mail.ru',
            },
            {
                name: 'Клинической больнице №85 ФМБА',
                email: 'info@kb85.ru',
            },
            {
                name: 'Лечебно-реабилитационному центру Минздрава',
                email: 'info@med-rf.ru',
            },
            {
                name: 'Медицинскому центру в Коммунарке',
                email: 'gkb40@zdrav.mos.ru',
            },
            {
                name: 'Научному центру здоровья детей',
                email: 'info@nczd.ru',
            },
            {
                name: 'Национальному медико-хирургическому центру им. Пирогова',
                email: 'info@pirogov-center.ru',
            },
            {
                name: 'НИИ медицины труда им. Измерова',
                email: 'info@irioh.ru',
            },
            {
                name: 'НИИ скорой помощи им. Склифосовского',
                email: 'sklif@zdrav.mos.ru',
            },
            {
                name: 'НМИЦ акушерства, гинекологии и перинатологии им. Кулакова',
                email: 'secretariat@oparina4.ru',
            },
            {
                name: 'НМИЦ кардиологии Минздрава',
                email: 'info@cardioweb.ru',
            },
            {
                name: 'Российскому геронтологическому научно-клиническому центру ',
                email: 'anticorruption@rgnkc.ru',
            },
            {
                name: 'УКБ №1 Сеченовского Университета',
                email: 'rektorat@sechenov.ru',
            },
            {
                name: 'Федеральному бюро медико-социальной экспертизы Минтруда',
                email: 'fbmse@fbmse.ru',
            },
            {
                name: 'Федеральному исследовательскому центру питания биотехнологии и безопасности пищи',
                email: 'mailbox@ion.ru',
            },
            {
                name: 'Федеральному медицинскому биофизическому центру им. Бурназяна',
                email: 'fmbc-fmba@bk.ru',
            },
            {
                name: 'Федеральному научно-клиническому центру ФМБА России',
                email: 'info@fnkc-fmba.ru',
            },
            {
                name: 'Центральной клинической больнице РАН',
                email: 'ckb@ckbran.ru',
            },
            {
                name: 'Центральной клинической больнице РЖД-Медицина',
                email: 'ckb2semashko@mail.ru',
            },
            {
                name: 'Центру высоких медицинских технологий ФМБА',
                email: 'do@kb119.ru',
            },
            {
                name: 'Челюстно-лицевому госпиталю для ветеранов войн',
                email: 'chlg@zdrav.mos.ru',
            },
        ];
    }
    SupportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getGift(this.user_friendly_url).subscribe(function (gift) {
            _this.gift = gift;
        });
        // this.api.getGifts().subscribe(gifts => {
        //   this.gift = gifts.find(gift => gift.user_friendly_url === this.user_friendly_url);
        // });
    };
    SupportComponent.ctorParameters = function () { return [
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    SupportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-support',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./support.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/support/support.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./support.component.styl */ "./src/app/pages/support/support.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], SupportComponent);
    return SupportComponent;
}());



/***/ }),

/***/ "./src/app/pages/support/support.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/support/support.module.ts ***!
  \*************************************************/
/*! exports provided: ROUTES, SupportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupportModule", function() { return SupportModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/gift/gift.module */ "./src/app/components/gift/gift.module.ts");
/* harmony import */ var _support_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./support.component */ "./src/app/pages/support/support.component.ts");







var ROUTES = [{ path: '', component: _support_component__WEBPACK_IMPORTED_MODULE_6__["SupportComponent"] }];
var SupportModule = /** @class */ (function () {
    function SupportModule() {
    }
    SupportModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _support_component__WEBPACK_IMPORTED_MODULE_6__["SupportComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES),
                _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_5__["GiftModule"]
            ],
        })
    ], SupportModule);
    return SupportModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-support-support-module.js.map