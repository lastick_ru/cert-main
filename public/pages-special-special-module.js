(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-special-special-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/special/special.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/special/special.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-gift [gift]=\"gift\" [related]=\"related\" [showCover]=\"false\"\r\n          [showName]=\"false\" [showHeading]=\"true\" [showBFee]=\"false\" [showSubscribeForm]=\"true\"\r\n          [second_logos]=\"logos\"\r\n          second_logos_title=\"Учреждения культуры, в которые<br>вы сможете приобрести билеты по этому сертификату\"\r\n          [headingText]=\"gift && gift.price_modifier && gift.price_modifier.formatted_value ?\r\n            '<h1>'+\r\n            '  Поддержим друг друга<br><strong>в период карантина</strong>'+\r\n            '</h1>'+\r\n            '<p>'+\r\n            '  Подарите единый сертификат zapomni.gift своим близким и получите <strong>' + gift.price_modifier.formatted_value + '</strong> к оплачиваемому номиналу и срок активации <strong>до 31.12.2020</strong>'+\r\n            '</p>'+\r\n            '<p>'+\r\n            '  Скоро все закончится и его можно будет обменять на билеты в лучших учреждениях культуры Москвы. Приобретая сертификат сейчас, например, на 5000 рублей, вы получите сертификат на 6000 рублей. Использовать приобретенный подарочный сертификат станет возможным после возобновления работы учреждений культуры.'+\r\n            '</p>' : null\r\n\">\r\n</app-gift>\r\n");

/***/ }),

/***/ "./src/app/pages/special/special.component.styl":
/*!******************************************************!*\
  !*** ./src/app/pages/special/special.component.styl ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/*# sourceMappingURL=src/app/pages/special/special.component.css.map */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc3BlY2lhbC9zcGVjaWFsLmNvbXBvbmVudC5zdHlsIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLHNFQUFzRSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NwZWNpYWwvc3BlY2lhbC5jb21wb25lbnQuc3R5bCJ9 */");

/***/ }),

/***/ "./src/app/pages/special/special.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/special/special.component.ts ***!
  \****************************************************/
/*! exports provided: SpecialComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecialComponent", function() { return SpecialComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_transport_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/transport.service */ "./src/app/services/transport.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var SpecialComponent = /** @class */ (function () {
    function SpecialComponent(api, route) {
        this.api = api;
        this.route = route;
        this.user_friendly_url = 'corona';
        this.logos = [
            // '/assets/img/logo/disney.svg',
            // '/assets/img/logo/smf.svg',
            { img: '/assets/img/logo/cons.png' },
            { img: '/assets/img/logo/planetarium.svg' },
            { img: '/assets/img/logo/zapomni.svg' },
            { img: '/assets/img/logo/park.png' },
            { img: '/assets/img/logo/zal.png' },
            { img: '/assets/img/logo/cdk.png' },
            { img: '/assets/img/logo/moskino.svg' },
            { img: '/assets/img/logo/teatrarmii_logo.png' },
            { img: '/assets/img/logo/shkola.png' },
        ];
    }
    SpecialComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getGifts().subscribe(function (gifts) {
            _this.gift = gifts.find(function (gift) { return gift.user_friendly_url === _this.user_friendly_url; });
            // this.related = gifts.filter(gift => gift._uuid !== this.gift._uuid);
        });
    };
    SpecialComponent.ctorParameters = function () { return [
        { type: _services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
    ]; };
    SpecialComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-special',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./special.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/special/special.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./special.component.styl */ "./src/app/pages/special/special.component.styl")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_transport_service__WEBPACK_IMPORTED_MODULE_2__["TransportService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], SpecialComponent);
    return SpecialComponent;
}());



/***/ }),

/***/ "./src/app/pages/special/special.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/special/special.module.ts ***!
  \*************************************************/
/*! exports provided: ROUTES, SpecialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROUTES", function() { return ROUTES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecialModule", function() { return SpecialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../modules/shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/gift/gift.module */ "./src/app/components/gift/gift.module.ts");
/* harmony import */ var _special_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./special.component */ "./src/app/pages/special/special.component.ts");







var ROUTES = [{ path: '', component: _special_component__WEBPACK_IMPORTED_MODULE_6__["SpecialComponent"] }];
var SpecialModule = /** @class */ (function () {
    function SpecialModule() {
    }
    SpecialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _special_component__WEBPACK_IMPORTED_MODULE_6__["SpecialComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(ROUTES),
                _components_gift_gift_module__WEBPACK_IMPORTED_MODULE_5__["GiftModule"]
            ],
        })
    ], SpecialModule);
    return SpecialModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-special-special-module.js.map